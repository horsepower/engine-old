#   ----------------------------------------------------------------------------
#	COPYRIGHT (C) 2018-2019 David Knapp
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#   ----------------------------------------------------------------------------

### --- SDL2 MODULE --- ###

option(SDL2_SHARED OFF "Engine will depend on SDL2.dll, .so, what have you.")

### WINDOWS ###

#[[
I apologize for the prefrontal-cortex-melting clusterfuck below, 
but SDL2 > 2.0.5 on Windows is broken and the only way to get it 
working without fixing the engine is to overengineer a solution 
involving a preincluded blob of SDL2 libraries.
#]]

# I'll fix it when I'm dead :3

if(WIN32)
	if(MINGW)
		find_package(SDL2 REQUIRED)
		set(SDL2 "-lmingw32 -lSDL2main -lSDL2 -mwindows")
	endif()
endif()

### LINUX ###

# Yep. All Linux needs.
if(LINUX)
	find_package(SDL2 REQUIRED)
	set(SDL2 "-lSDL2main -lSDL2")
endif()

### FREEBSD ###

# Same with FreeBSD.
if(BSD)
endif()
