#!/bin/bash
printf "\n\e[1;34m---Horsepower Build Wizard---\e[0m\n\n"

build_dir="./projects/$build_config/build"

# We can accept an arg if the user wants.
if [ $1 = "--build" ]; then
	build_config=$2
	cmake_cmd="cmake . --build -B$build_dir -DBUILD_CONFIG=$build_config"
else
	build_config=$1
	cmake_cmd="cmake . -B./projects/$build_dir/build -DBUILD_CONFIG=$build_config"	

# Run the build system.
begin_build() {
	printf " ------\n\nRunning CMake...\n\n"
	printf "Using \"${build_config}\" build config.\n\n"
	if [ $(cmake_cmd) ]; then
		printf "Build completed successfully."
	else
		printf "Build failed. Please check the output for more information."
	fi
	xdg-open ./projects/$build_config
	
	# We're done. We can end the loop now.
	finished=true
}

# Check if the build config was provided.
check_build_config()
{
	# Some checks, in case we got our file.
	if [ $build_config ]; then
		if [ ! -f ./projects/${build_config}/config.cmake ]; then
			# Name provided but no build config there.
			failed=true
		else
			# Got it.
			begin_build
			return
		fi
	fi
	get_build_config
}

# We want our build config.
get_build_config() {
	if [ $failed ]; then
		printf "\e[1;33mCould not find a config.cmake under the supplied directory '${build_config}'.\e[0m\n\n"
		failed=false
	# Don't want to keep printing this every time the user fails. Looks ugly.
	else
		printf "Please specify a folder containing a valid config.cmake, relative\nto the 'projects' subdirectory.\n\n"
	fi
	
	read -p "Enter directory name ['default']: " build_config
	build_config=${build_config:-default}
	printf "\n"
}

# Loop until we get a valid file.
while [ ! $finished ]; do
	check_build_config
done
