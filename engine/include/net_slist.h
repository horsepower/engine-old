#define SERVERLIST_VIEWLISTSIZE		SERVERLIST_TOTALSIZE

typedef enum serverlist_maskop_e
{
	// SLMO_CONTAINS is the default for strings
	// SLMO_GREATEREQUAL is the default for numbers (also used when OP == CONTAINS or NOTCONTAINS
	SLMO_CONTAINS,
	SLMO_NOTCONTAIN,

	SLMO_LESSEQUAL,
	SLMO_LESS,
	SLMO_EQUAL,
	SLMO_GREATER,
	SLMO_GREATEREQUAL,
	SLMO_NOTEQUAL,
	SLMO_STARTSWITH,
	SLMO_NOTSTARTSWITH
} serverlist_maskop_t;

/// struct with all fields that you can search for or sort by
typedef struct serverlist_info_s
{
	/// address for connecting
	char cname[128];
	/// ping time for sorting servers
	int ping;
	/// name of the game
	char game[32];
	/// name of the mod
	char mod[32];
	/// name of the map
	char map[32];
	/// name of the session
	char name[128];
	/// qc-defined short status string
	char qcstatus[128];
	/// frags/ping/name list (if they fit in the packet)
	char players[1400];
	/// max client number
	int maxplayers;
	/// number of currently connected players (including bots)
	int numplayers;
	/// number of currently connected players that are bots
	int numbots;
	/// number of currently connected players that are not bots
	int numhumans;
	/// number of free slots
	int freeslots;
	/// protocol version
	int protocol;
	/// game data version
	/// (an integer that is used for filtering incompatible servers,
	///  not filterable by QC)
	int gameversion;

	// categorized sorting
	int category;
	/// favorite server flag
	qboolean isfavorite;
} serverlist_info_t;

typedef enum
{
	SLIF_CNAME,
	SLIF_PING,
	SLIF_GAME,
	SLIF_MOD,
	SLIF_MAP,
	SLIF_NAME,
	SLIF_MAXPLAYERS,
	SLIF_NUMPLAYERS,
	SLIF_PROTOCOL,
	SLIF_NUMBOTS,
	SLIF_NUMHUMANS,
	SLIF_FREESLOTS,
	SLIF_QCSTATUS,
	SLIF_PLAYERS,
	SLIF_CATEGORY,
	SLIF_ISFAVORITE,
	SLIF_COUNT
} serverlist_infofield_t;

typedef enum
{
	SLSF_DESCENDING = 1,
	SLSF_FAVORITES = 2,
	SLSF_CATEGORIES = 4
} serverlist_sortflags_t;

typedef enum
{
	SQS_NONE = 0,
	SQS_QUERYING,
	SQS_QUERIED,
	SQS_TIMEDOUT,
	SQS_REFRESHING
} serverlist_query_state;

typedef struct serverlist_entry_s
{
	/// used to determine whether this entry should be included into the final view
	serverlist_query_state query;
	/// used to count the number of times the host has tried to query this server already
	unsigned querycounter;
	/// used to calculate ping when update comes in
	double querytime;
	/// query protocol to use on this server, may be &protocol_quakeworld or &protocol_darkplaces7
	int protocol;

	serverlist_info_t info;

	// legacy stuff
	char line1[128];
	char line2[128];
} serverlist_entry_t;

typedef struct serverlist_mask_s
{
	qboolean			active;
	serverlist_maskop_t  tests[SLIF_COUNT];
	serverlist_info_t info;
} serverlist_mask_t;

#define ServerList_GetCacheEntry(x) (&serverlist_cache[(x)])
#define ServerList_GetViewEntry(x) (ServerList_GetCacheEntry(serverlist_viewlist[(x)]))

extern serverlist_mask_t serverlist_andmasks[SERVERLIST_ANDMASKCOUNT];
extern serverlist_mask_t serverlist_ormasks[SERVERLIST_ORMASKCOUNT];

extern serverlist_infofield_t serverlist_sortbyfield;
extern int serverlist_sortflags; // not using the enum, as it is a bitmask

#if SERVERLIST_TOTALSIZE > 65536
#error too many servers, change type of index array
#endif
extern int serverlist_viewcount;
extern unsigned short serverlist_viewlist[SERVERLIST_VIEWLISTSIZE];

extern int serverlist_cachecount;
extern serverlist_entry_t *serverlist_cache;
extern const serverlist_entry_t *serverlist_callbackentry;

extern qboolean serverlist_consoleoutput;

void ServerList_GetPlayerStatistics(int *numplayerspointer, int *maxplayerspointer);
extern double masterquerytime;
extern int masterquerycount;
extern int masterreplycount;
extern int serverquerycount;
extern int serverreplycount;

void NetConn_QueryMasters(qboolean querydp, qboolean queryqw);
void NetConn_QueryQueueFrame(void);
void Net_Slist_f(void);
void Net_SlistQW_f(void);
void Net_Refresh_f(void);

/// ServerList interface (public)
/// manually refresh the view set, do this after having changed the mask or any other flag
void ServerList_RebuildViewList(void);
void ServerList_ResetMasks(void);
void ServerList_QueryList(qboolean resetcache, qboolean querydp, qboolean queryqw, qboolean consoleoutput);

/// called whenever net_slist_favorites changes
void NetConn_UpdateFavorites(void);
void Net_Slist_Init(void);
