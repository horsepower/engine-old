#ifndef CL_PARSE_H
#define CL_PARSE_H

void CL_ParseStartSoundPacket(int largesoundindex);
void CL_NetworkTimeReceived(double newtime);
qboolean CL_ExaminePrintString(const char *text);
void CL_ParseServerInfo (void);
void CL_ParseClientdata (void);
void CL_ParseStatic (int large);
void CL_ParseBaseline (entity_t *ent, int large);
void CL_ParseTempEntity(void);
void CL_SignonReply (void);
void CL_ParseStaticSound (int large);
void QW_CL_UpdateUserInfo(void);
void QW_CL_ParseDownload(void);
void QW_CL_ParseNails(void);
void QW_CL_ParseModelList(void);
void QW_CL_ParseSoundList(void);
void CL_ParseDownload(void);
void QW_CL_SetInfo(void);
void QW_CL_ServerInfo(void);
void CL_ParseEffect (void);
void CL_ParseEffect2 (void);
void CL_ParseTrailParticles (void);
void CL_ParseTrailParticles (void);
void CL_ParsePointParticles(void);
void CL_ParsePointParticles1 (void);


#endif /* CL_PARSE_H */
