/*
Copyright (C) 2019 David Knapp (Cloudwalk)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/*
 * DESCRIPTION:
 * A loose implementation of the observer pattern. You create
 * an event_t, assign a function to it, and trigger it elsewhere
 * in the code as needed. The caller doesn't care what function
 * it is.
 *
 * You can set the event to be called immediately, or to be deferred
 * until next frame, depending on the use case. You can also pass an arg
 * using a void pointer if the triggered function needs to work with
 * data from the calling function.
 *
 * This allows some great code independence and is mainly used to split
 * up the client and server but soon it will be supported by QC with
 * a builtin... maybe.
 */

#ifndef EVENT_H
#define EVENT_H

#define MAXQUEUE 256

typedef void *(*eventfunc_t)(void);

typedef struct event_s
{
	qboolean	defer;					// Defer until next frame. Copies to master queue.
	qboolean	temp;					// If it should be Z_Free'd after execution.
	int 		top;					// The "head" of the stack.
	void		*arg;					// Any arg that should be used by func.
	void		*ret;					// Any return value.
	eventfunc_t	func[MAXQUEUE];			// The actual function stack.
} event_t;

// Has to be declared as NULL every time. This macro makes it less tedious.
#define event_tm(name, defer, temp) event_t name = {defer, temp, 0, NULL, NULL}

extern event_t evqueue; // Master event queue.

void *Event_Trigger(event_t *event, void *dataptr);
void *Event_GetArg(event_t *event);
void Event_Push(event_t *event, void *func);
void Event_Pop(event_t *event);

#endif // EVENT_H
