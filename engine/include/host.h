#ifndef HOST_H
#define HOST_H

extern cvar_t host_speeds; // FIXME: temporary
extern cvar_t host_framerate;
extern cvar_t host_timescale;
extern cvar_t developer;
extern cvar_t developer_extra;
extern cvar_t developer_insane;
extern cvar_t developer_loadfile;
extern cvar_t developer_loading;
extern cvar_t sessionid;

extern event_t on_Host_Error;

/* State bitmask */
#define H_UNINITIALIZED			(1<<0)
#define H_DEDICATED				(1<<1)
#define H_SHUTTINGDOWN			(1<<2)
#define H_PAUSED				(1<<3)
#define H_CS_CONNECTED			(1<<4)
#define H_CS_DEMO				(1<<5)
#define H_SS_LOADING			(1<<6)
#define H_SS_ACTIVE				(1<<7)
#define H_SS_LOCALGAME			(1<<8)

typedef struct host_state_s
{
	int framecount;				// how many frames have occurred (checked by Host_Error and Host_SaveConfig_f)
	double realtime; 			// the accumulated mainloop time since application started (with filtering), without any slowmo or clamping
	double dirtytime; 			// the main loop wall time for this frame
	jmp_buf abortframe;
	unsigned int state;
	double wait;
	double cl_timer, sv_timer;
	char crashlog;			// Stores crash log to be written to the console and a file. NULLed after.

}
host_state_t;

extern host_state_t host;

void Host_Init(void);
void Host_Shutdown(void);
void Host_Error(const char *error, ...) DP_FUNC_PRINTF(1) DP_FUNC_NORETURN;

void Host_LockSession(void);
void Host_UnlockSession(void);
void Host_AbortCurrentFrame(void);
void Host_NoOperation_f(void);

#endif
