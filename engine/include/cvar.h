/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// cvar.h

/*

cvar_t variables are used to hold scalar or string variables that can be changed or displayed at the console or prog code as well as accessed directly
in C code.

it is sufficient to initialize a cvar_t with just the first two fields, or
you can add a ,true flag for variables that you want saved to the configuration
file when the game is quit:

cvar_t	r_draworder = {"r_draworder","1"};
cvar_t	scr_screensize = {"screensize","1",true};

Cvars must be registered before use, or they will have a 0 value instead of the float interpretation of the string.  Generally, all cvar_t declarations should be registered in the apropriate init function before any console commands are executed:
Cvar_RegisterVariable (&host_framerate);


C code usually just references a cvar in place:
if ( r_draworder.value )

It could optionally ask for the value to be looked up for a string name:
if (Cvar_VariableValue ("r_draworder"))

Interpreted prog code can access cvars with the cvar(name) or
cvar_set (name, value) internal functions:
teamplay = cvar("teamplay");
cvar_set ("registered", "1");

The user can access cvars from the console in two ways:
r_draworder			prints the current value
r_draworder 0		sets the current value to 0
Cvars are restricted from having the same names as commands to keep this
interface from being ambiguous.
*/

#ifndef CVAR_H
#define CVAR_H

/* cvar flags */

// Really confusing bitmask stuff. Careful!

#define CVAR_SAVE 1					// Saves the cvar's value in configuration.
#define CVAR_NOTIFY 2				// Notify the whole server that it changed.
#define CVAR_READONLY 4				// Can't change from the console.
#define CVAR_SERVERINFO 8			// Sets a serverinfo key.
#define CVAR_USERINFO 16			// Should update a userinfo key.
#define CVAR_PRIVATE 32				// Do not $expand or sendcvar this cvar under any circumstances (e.g. rcon_password).
#define CVAR_NQUSERINFOHACK 64		// This means that this cvar should update a userinfo key but the name does not correspond directly to the userinfo key to update, and may require additional conversion ("_cl_color" for example should update "topcolor" and "bottomcolor").
#define CVAR_NORESETTODEFAULTS 128	// For engine-owned cvars that must not be reset on gametype switch (e.g. scr_screenshot_name, which otherwise isn't set to the mod name properly).
#define CVAR_CHEAT 256				// Cheats, or cvars that can be abused by players to gain an unfair advantage (e.g. r_novis).
#define CVAR_REPLICATED 512			// The server will force a value on the client.
#define CVAR_MAXFLAGSVAL 1023		// Every cvar flag combined.

// Internal use only
#define CVAR_DEFAULTSET (1<<30)
#define CVAR_ALLOCATED (1<<31)


/* The cvar_t struct */
typedef struct cvar_s
{

	/* Basic values */
	int flags;					// Cvar flags. This is a bitmask. See flags above for details.
	const char *name;			// The name of the cvar for use by the console and cvar index.
	const char *string;			// The value of the cvar in string form.
	const char *description;	// The detailed cvar description.
	int integer;				// The value of the cvar in integer form if applicable.
	float value;				// The value of the cvar in float form. This should be used in most cases.
	float vector[3];
	const char *defstring;

	/* Values at init (for Cvar_RestoreInitState) */
	qboolean initstate; // Indicates this existed at init
	int initflags;
	const char *initstring;
	const char *initdescription;
	int initinteger;
	float initvalue;
	float initvector[3];
	const char *initdefstring;

	int globaldefindex[3];
	int globaldefindex_stringno[3];

	/* Cvar lookup */
	struct cvar_s *next;
	struct cvar_s *nextonhashchain;

	/* Cvar aliasing */
	const char ((*convert)());		// cvar value conversion... probably needs *mirror
	struct cvar_s *mirror; 			// A different cvar it points to.

} cvar_t;

/*
   Registers a cvar that already has the name,
   string, and optionally the archive elements set.
*/
void Cvar_RegisterVariable (cvar_t *variable);

/* Equivalent to "<name> <variable>" typed at the console */
void Cvar_Set (const char *var_name, const char *value);

/* Expands value to a string and calls Cvar_Set */
void Cvar_SetValue (const char *var_name, float value);

void Cvar_SetQuick (cvar_t *var, const char *value);
void Cvar_SetValueQuick (cvar_t *var, float value);

/* Returns def if not defined */
float Cvar_VariableValueOr (const char *var_name, float def);

/* Returns 0 if not defined or non-numeric */
float Cvar_VariableValue (const char *var_name);

/* Returns def if not defined */
const char *Cvar_VariableStringOr (const char *var_name, const char *def);

/* Returns an empty string if not defined */
const char *Cvar_VariableString (const char *var_name);

/* Returns an empty string if not defined */
const char *Cvar_VariableDefString (const char *var_name);

/* Returns an empty string if not defined */
const char *Cvar_VariableDescription (const char *var_name);

/*
   Attempts to match a partial variable name for
   command line completion returns NULL if nothing fits
*/
const char *Cvar_CompleteVariable (const char *partial);

void Cvar_CompleteCvarPrint (const char *partial);

/*
   Called by Cmd_ExecuteString when Cmd_Argv(0) doesn't match a known
   command.  Returns true if the command was a variable reference that
   was handled. (print or change)
*/
qboolean Cvar_Command (void);

void Cvar_SaveInitState(void);
void Cvar_RestoreInitState(void);

void Cvar_UnlockDefaults (void);
void Cvar_LockDefaults_f (void);
void Cvar_ResetToDefaults_All_f (void);
void Cvar_ResetToDefaults_NoSaveOnly_f (void);
void Cvar_ResetToDefaults_SaveOnly_f (void);

/*
   Writes lines containing "set variable value"
   for all variables with the archive flag set to true.
*/
void Cvar_WriteVariables (qfile_t *f);

cvar_t *Cvar_FindVar (const char *var_name, qboolean mirror);
cvar_t *Cvar_FindVarAfter (const char *prev_var_name, int neededflags);

int Cvar_CompleteCountPossible (const char *partial);
const char **Cvar_CompleteBuildList (const char *partial);
// Added by EvilTypeGuy - functions for tab completion system
// Thanks to Fett erich@heintz.com
// Thanks to taniwha

/// Prints a list of Cvars including a count of them to the user console
/// Referenced in cmd.c in Cmd_Init hence it's inclusion here.
/// Added by EvilTypeGuy eviltypeguy@qeradiant.com
/// Thanks to Matthias "Maddes" Buecher, http://www.inside3d.com/qip/
void Cvar_List_f (void);

void Cvar_Set_f (void);
void Cvar_SetA_f (void);
void Cvar_Del_f (void);
// commands to create new cvars (or set existing ones)
// seta creates an archived cvar (saved to config)

/// allocates a cvar by name and returns its address,
/// or merely sets its value if it already exists.
cvar_t *Cvar_Get (const char *name, const char *value, int flags, const char *newdescription);

extern const char *cvar_dummy_description; // ALWAYS the same pointer
extern cvar_t *cvar_vars; // used to list all cvars

void Cvar_UpdateAllAutoCvars(void); // updates ALL autocvars of the active prog to the cvar values (savegame loading)

#ifdef FILLALLCVARSWITHRUBBISH
void Cvar_FillAll_f();
#endif /* FILLALLCVARSWITHRUBBISH */

#endif

