#   ----------------------------------------------------------------------------
#	COPYRIGHT (C) 2018 David Knapp
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#   ----------------------------------------------------------------------------

### --- HORSEPOWER ENGINE CMAKE FILE --- ###

### DEFAULT OPTIONS ###
set(ENGINE_EXE_NAME "horsepower" CACHE STRING "What do you want to name the engine executable? If unsure, leave this default. Default = horsepower")
if(WIN32)
	set(ENGINE_WINRC "" CACHE STRING "Location of Windows resource file. If unsure, leave this blank. Default = <blank>")
endif()
option(ENGINE_GAME "Acquire or use an existing game project and build the engine with it. If you don't have one or are unsure, turn this off. Default = OFF" OFF)

option(ENGINE_CONFIG_LEGACY "Compile the engine with legacy Quake/Quakeworld features. Required for Quake and any mods that use Quake's hardcoded clientside. If unsure or you're gonna play Quake or mods built for Quake, turn this on. Default = ON" ON)
option(ENGINE_CONFIG_MENU "Compile the engine with legacy menu support. Required for Quake and any mods that use the legacy menu. If unsure or you're gonna play Quake or mods built for Quake, turn this on. Default = ON" ON)
option(ENGINE_NO_BUILD_TIMESTAMPS "Disable build timestamps in the version string. Default = OFF" OFF)
set(ENGINE_VERSION "" CACHE STRING "Set engine version string. If unsure, leave this blank. Default = <blank>")

### VARIABLES ###

set(ENGINE_SRC_DIR "${ENGINE_DIR}/src")

### CHECKS ###

if(ENGINE_EXE_NAME STREQUAL "") # Cannot be empty
    message(FATAL_ERROR "You must give the executable a name.")
endif()

if(ENGINE_EXE_NAME MATCHES "[* *]") # Cannot contain spaces.
    message(FATAL_ERROR "The executable name must not contain spaces.")
endif()

### INCLUDE ###

include("${ENGINE_DIR}/build/engine.cmake")
if(ENGINE_GAME)
	include("${ENGINE_DIR}/build/game.cmake")
endif()

### BUILD - CLIENT ###

# Dependencies
include("${INCLUDE_DIR}/module/sdl2.cmake")
#include("${INCLUDE_DIR}/module/png.cmake")
#include("${INCLUDE_DIR}/module/jpeg.cmake")

# Targets
add_executable(${ENGINE_EXE_NAME}
			   "${OBJ_CL}"
			   "${OBJ_MENU}"
			   "${OBJ_SND_COMMON}"
			   "${OBJ_CD_COMMON}"
			   "${OBJ_VIDEO_CAPTURE}"
			   "${OBJ_COMMON}"
)
set_target_properties(${ENGINE_EXE_NAME} PROPERTIES LINKER_LANGUAGE C
								         COMPILE_FLAGS "${ENGINE_CL_FLAGS} ${ENGINE_PLATFLAGS}")
target_link_libraries(${ENGINE_EXE_NAME} ${SDL2} ${ENGINE_PLATLIBS})

target_include_directories(${ENGINE_EXE_NAME} PRIVATE "${ENGINE_DIR}/include")

### BUILD - SERVER ###

# Targets
add_executable(${ENGINE_EXE_NAME}-sv
               "${OBJ_SV}"
			   "${OBJ_COMMON}"
)
set_target_properties(${ENGINE_EXE_NAME}-sv PROPERTIES LINKER_LANGUAGE C
											COMPILE_FLAGS "${ENGINE_PLATFLAGS} ${ENGINE_SV_FLAGS}")
target_link_libraries(${ENGINE_EXE_NAME}-sv ${ENGINE_PLATLIBS})

target_include_directories(${ENGINE_EXE_NAME}-sv PRIVATE "${ENGINE_DIR}/include")
