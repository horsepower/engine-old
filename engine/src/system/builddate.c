#define STRINGIFY2(arg) #arg
#define STRINGIFY(arg) STRINGIFY2(arg)

extern const char *buildstring;
const char *buildstring =
#ifdef VSTRING
"Version " STRINGIFY(VSTRING) " "
#endif
#ifndef NO_BUILD_TIMESTAMPS
"- Build " __DATE__ " " __TIME__ " "
#endif
#ifdef SVNREVISION
"- " STRINGIFY(SVNREVISION) " "
#endif
#ifdef BUILDTYPE
"- " STRINGIFY(BUILDTYPE)
#endif
;
