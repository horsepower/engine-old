#include "quakedef.h"

static cvar_t sv_masters [] =
{
	{CVAR_SAVE, "sv_master1", "", "user-chosen master server 1"},
	{CVAR_SAVE, "sv_master2", "", "user-chosen master server 2"},
	{CVAR_SAVE, "sv_master3", "", "user-chosen master server 3"},
	{CVAR_SAVE, "sv_master4", "", "user-chosen master server 4"},
	{0, "sv_masterextra1", "ghdigital.com", "ghdigital.com - default master server 1 (admin: LordHavoc)"}, // admin: LordHavoc
	{0, "sv_masterextra2", "dpmaster.deathmask.net", "dpmaster.deathmask.net - default master server 2 (admin: Willis)"}, // admin: Willis
	{0, "sv_masterextra3", "dpmaster.tchr.no", "dpmaster.tchr.no - default master server 3 (admin: tChr)"}, // admin: tChr
	{0, NULL, NULL, NULL}
};

static cvar_t sv_qwmasters [] =
{
	{CVAR_SAVE, "sv_qwmaster1", "", "user-chosen qwmaster server 1"},
	{CVAR_SAVE, "sv_qwmaster2", "", "user-chosen qwmaster server 2"},
	{CVAR_SAVE, "sv_qwmaster3", "", "user-chosen qwmaster server 3"},
	{CVAR_SAVE, "sv_qwmaster4", "", "user-chosen qwmaster server 4"},
	{0, "sv_qwmasterextra1", "master.quakeservers.net:27000", "Global master server. (admin: unknown)"},
	{0, "sv_qwmasterextra2", "asgaard.morphos-team.net:27000", "Global master server. (admin: unknown)"},
	{0, "sv_qwmasterextra3", "qwmaster.ocrana.de:27000", "German master server. (admin: unknown)"},
	{0, "sv_qwmasterextra4", "qwmaster.fodquake.net:27000", "Global master server. (admin: unknown)"},
	{0, NULL, NULL, NULL}
};

static cvar_t net_slist_queriespersecond = {0, "net_slist_queriespersecond", "20", "how many server information requests to send per second"};
static cvar_t net_slist_queriesperframe = {0, "net_slist_queriesperframe", "4", "maximum number of server information requests to send each rendered frame (guards against low framerates causing problems)"};
static cvar_t net_slist_timeout = {0, "net_slist_timeout", "4", "how long to listen for a server information response before giving up"};
static cvar_t net_slist_pause = {0, "net_slist_pause", "0", "when set to 1, the server list won't update until it is set back to 0"};
static cvar_t net_slist_maxtries = {0, "net_slist_maxtries", "3", "how many times to ask the same server for information (more times gives better ping reports but takes longer)"};
static cvar_t net_slist_favorites = {CVAR_SAVE | CVAR_NQUSERINFOHACK, "net_slist_favorites", "", "contains a list of IP addresses and ports to always query explicitly"};

// ServerList interface

/// this is only false if there are still servers left to query
static qboolean serverlist_querysleep = true;
static qboolean serverlist_paused = false;
/// this is pushed a second or two ahead of realtime whenever a master server
/// reply is received, to avoid issuing queries while master replies are still
/// flooding in (which would make a mess of the ping times)
static double serverlist_querywaittime = 0;

serverlist_mask_t serverlist_andmasks[SERVERLIST_ANDMASKCOUNT];
serverlist_mask_t serverlist_ormasks[SERVERLIST_ORMASKCOUNT];

serverlist_infofield_t serverlist_sortbyfield;
int serverlist_sortflags;

int serverlist_viewcount = 0;
unsigned short serverlist_viewlist[SERVERLIST_VIEWLISTSIZE];

int serverlist_maxcachecount = 0;
int serverlist_cachecount = 0;
serverlist_entry_t *serverlist_cache = NULL;

qboolean serverlist_consoleoutput;

static int nFavorites = 0;
static lhnetaddress_t favorites[MAX_FAVORITESERVERS];
static int nFavorites_idfp = 0;
static char favorites_idfp[MAX_FAVORITESERVERS][FP64_SIZE+1];

void NetConn_UpdateFavorites(void)
{
	const char *p;
	nFavorites = 0;
	nFavorites_idfp = 0;
	p = net_slist_favorites.string;
	while((size_t) nFavorites < sizeof(favorites) / sizeof(*favorites) && COM_ParseToken_Console(&p))
	{
		if(com_token[0] != '[' && strlen(com_token) == FP64_SIZE && !strchr(com_token, '.'))
		// currently 44 bytes, longest possible IPv6 address: 39 bytes, so this works
		// (if v6 address contains port, it must start with '[')
		{
			strlcpy(favorites_idfp[nFavorites_idfp], com_token, sizeof(favorites_idfp[nFavorites_idfp]));
			++nFavorites_idfp;
		}
		else
		{
			if(LHNETADDRESS_FromString(&favorites[nFavorites], com_token, 26000))
				++nFavorites;
		}
	}
}

/// helper function to insert a value into the viewset
/// spare entries will be removed
static void _ServerList_ViewList_Helper_InsertBefore( int index, serverlist_entry_t *entry )
{
    int i;
	if( serverlist_viewcount < SERVERLIST_VIEWLISTSIZE ) {
		i = serverlist_viewcount++;
	} else {
		i = SERVERLIST_VIEWLISTSIZE - 1;
	}

	for( ; i > index ; i-- )
		serverlist_viewlist[ i ] = serverlist_viewlist[ i - 1 ];

	serverlist_viewlist[index] = (int)(entry - serverlist_cache);
}

/// we suppose serverlist_viewcount to be valid, ie > 0
static void _ServerList_ViewList_Helper_Remove( int index )
{
	serverlist_viewcount--;
	for( ; index < serverlist_viewcount ; index++ )
		serverlist_viewlist[index] = serverlist_viewlist[index + 1];
}

/// \returns true if A should be inserted before B
static qboolean _ServerList_Entry_Compare( serverlist_entry_t *A, serverlist_entry_t *B )
{
	int result = 0; // > 0 if for numbers A > B and for text if A < B

	if( serverlist_sortflags & SLSF_CATEGORIES )
	{
		result = A->info.category - B->info.category;
		if (result != 0)
			return result < 0;
	}

	if( serverlist_sortflags & SLSF_FAVORITES )
	{
		if(A->info.isfavorite != B->info.isfavorite)
			return A->info.isfavorite;
	}

	switch( serverlist_sortbyfield ) {
		case SLIF_PING:
			result = A->info.ping - B->info.ping;
			break;
		case SLIF_MAXPLAYERS:
			result = A->info.maxplayers - B->info.maxplayers;
			break;
		case SLIF_NUMPLAYERS:
			result = A->info.numplayers - B->info.numplayers;
			break;
		case SLIF_NUMBOTS:
			result = A->info.numbots - B->info.numbots;
			break;
		case SLIF_NUMHUMANS:
			result = A->info.numhumans - B->info.numhumans;
			break;
		case SLIF_FREESLOTS:
			result = A->info.freeslots - B->info.freeslots;
			break;
		case SLIF_PROTOCOL:
			result = A->info.protocol - B->info.protocol;
			break;
		case SLIF_CNAME:
			result = strcmp( B->info.cname, A->info.cname );
			break;
		case SLIF_GAME:
			result = strcasecmp( B->info.game, A->info.game );
			break;
		case SLIF_MAP:
			result = strcasecmp( B->info.map, A->info.map );
			break;
		case SLIF_MOD:
			result = strcasecmp( B->info.mod, A->info.mod );
			break;
		case SLIF_NAME:
			result = strcasecmp( B->info.name, A->info.name );
			break;
		case SLIF_QCSTATUS:
			result = strcasecmp( B->info.qcstatus, A->info.qcstatus ); // not really THAT useful, though
			break;
		case SLIF_CATEGORY:
			result = A->info.category - B->info.category;
			break;
		case SLIF_ISFAVORITE:
			result = !!B->info.isfavorite - !!A->info.isfavorite;
			break;
		default:
			Con_DPrint( "_ServerList_Entry_Compare: Bad serverlist_sortbyfield!\n" );
			break;
	}

	if (result != 0)
	{
		if( serverlist_sortflags & SLSF_DESCENDING )
			return result > 0;
		else
			return result < 0;
	}

	// if the chosen sort key is identical, sort by index
	// (makes this a stable sort, so that later replies from servers won't
	//  shuffle the servers around when they have the same ping)
	return A < B;
}

static qboolean _ServerList_CompareInt( int A, serverlist_maskop_t op, int B )
{
	// This should actually be done with some intermediate and end-of-function return
	switch( op ) {
		case SLMO_LESS:
			return A < B;
		case SLMO_LESSEQUAL:
			return A <= B;
		case SLMO_EQUAL:
			return A == B;
		case SLMO_GREATER:
			return A > B;
		case SLMO_NOTEQUAL:
			return A != B;
		case SLMO_GREATEREQUAL:
		case SLMO_CONTAINS:
		case SLMO_NOTCONTAIN:
		case SLMO_STARTSWITH:
		case SLMO_NOTSTARTSWITH:
			return A >= B;
		default:
			Con_DPrint( "_ServerList_CompareInt: Bad op!\n" );
			return false;
	}
}

static qboolean _ServerList_CompareStr( const char *A, serverlist_maskop_t op, const char *B )
{
	int i;
	char bufferA[ 1400 ], bufferB[ 1400 ]; // should be more than enough
	COM_StringDecolorize(A, 0, bufferA, sizeof(bufferA), false);
	for (i = 0;i < (int)sizeof(bufferA)-1 && bufferA[i];i++)
		bufferA[i] = (bufferA[i] >= 'A' && bufferA[i] <= 'Z') ? (bufferA[i] + 'a' - 'A') : bufferA[i];
	bufferA[i] = 0;
	for (i = 0;i < (int)sizeof(bufferB)-1 && B[i];i++)
		bufferB[i] = (B[i] >= 'A' && B[i] <= 'Z') ? (B[i] + 'a' - 'A') : B[i];
	bufferB[i] = 0;

	// Same here, also using an intermediate & final return would be more appropriate
	// A info B mask
	switch( op ) {
		case SLMO_CONTAINS:
			return *bufferB && !!strstr( bufferA, bufferB ); // we want a real bool
		case SLMO_NOTCONTAIN:
			return !*bufferB || !strstr( bufferA, bufferB );
		case SLMO_STARTSWITH:
			//Con_Printf("startsWith: %s %s\n", bufferA, bufferB);
			return *bufferB && !memcmp(bufferA, bufferB, strlen(bufferB));
		case SLMO_NOTSTARTSWITH:
			return !*bufferB || memcmp(bufferA, bufferB, strlen(bufferB));
		case SLMO_LESS:
			return strcmp( bufferA, bufferB ) < 0;
		case SLMO_LESSEQUAL:
			return strcmp( bufferA, bufferB ) <= 0;
		case SLMO_EQUAL:
			return strcmp( bufferA, bufferB ) == 0;
		case SLMO_GREATER:
			return strcmp( bufferA, bufferB ) > 0;
		case SLMO_NOTEQUAL:
			return strcmp( bufferA, bufferB ) != 0;
		case SLMO_GREATEREQUAL:
			return strcmp( bufferA, bufferB ) >= 0;
		default:
			Con_DPrint( "_ServerList_CompareStr: Bad op!\n" );
			return false;
	}
}

static qboolean _ServerList_Entry_Mask( serverlist_mask_t *mask, serverlist_info_t *info )
{
	if( !_ServerList_CompareInt( info->ping, mask->tests[SLIF_PING], mask->info.ping ) )
		return false;
	if( !_ServerList_CompareInt( info->maxplayers, mask->tests[SLIF_MAXPLAYERS], mask->info.maxplayers ) )
		return false;
	if( !_ServerList_CompareInt( info->numplayers, mask->tests[SLIF_NUMPLAYERS], mask->info.numplayers ) )
		return false;
	if( !_ServerList_CompareInt( info->numbots, mask->tests[SLIF_NUMBOTS], mask->info.numbots ) )
		return false;
	if( !_ServerList_CompareInt( info->numhumans, mask->tests[SLIF_NUMHUMANS], mask->info.numhumans ) )
		return false;
	if( !_ServerList_CompareInt( info->freeslots, mask->tests[SLIF_FREESLOTS], mask->info.freeslots ) )
		return false;
	if( !_ServerList_CompareInt( info->protocol, mask->tests[SLIF_PROTOCOL], mask->info.protocol ))
		return false;
	if( *mask->info.cname
		&& !_ServerList_CompareStr( info->cname, mask->tests[SLIF_CNAME], mask->info.cname ) )
		return false;
	if( *mask->info.game
		&& !_ServerList_CompareStr( info->game, mask->tests[SLIF_GAME], mask->info.game ) )
		return false;
	if( *mask->info.mod
		&& !_ServerList_CompareStr( info->mod, mask->tests[SLIF_MOD], mask->info.mod ) )
		return false;
	if( *mask->info.map
		&& !_ServerList_CompareStr( info->map, mask->tests[SLIF_MAP], mask->info.map ) )
		return false;
	if( *mask->info.name
		&& !_ServerList_CompareStr( info->name, mask->tests[SLIF_NAME], mask->info.name ) )
		return false;
	if( *mask->info.qcstatus
		&& !_ServerList_CompareStr( info->qcstatus, mask->tests[SLIF_QCSTATUS], mask->info.qcstatus ) )
		return false;
	if( *mask->info.players
		&& !_ServerList_CompareStr( info->players, mask->tests[SLIF_PLAYERS], mask->info.players ) )
		return false;
	if( !_ServerList_CompareInt( info->category, mask->tests[SLIF_CATEGORY], mask->info.category ) )
		return false;
	if( !_ServerList_CompareInt( info->isfavorite, mask->tests[SLIF_ISFAVORITE], mask->info.isfavorite ))
		return false;
	return true;
}

static void ServerList_ViewList_Insert( serverlist_entry_t *entry )
{
	int start, end, mid, i;
	lhnetaddress_t addr;

	// reject incompatible servers
	if(
		entry->info.gameversion != gameversion.integer
		&&
		!(
			   gameversion_min.integer >= 0 // min/max range set by user/mod?
			&& gameversion_max.integer >= 0
			&& gameversion_min.integer <= entry->info.gameversion // version of server in min/max range?
			&& gameversion_max.integer >= entry->info.gameversion
		 )
	)
		return;

	// refresh the "favorite" status
	entry->info.isfavorite = false;
	if(LHNETADDRESS_FromString(&addr, entry->info.cname, 26000))
	{
		char idfp[FP64_SIZE+1];
		for(i = 0; i < nFavorites; ++i)
		{
			if(LHNETADDRESS_Compare(&addr, &favorites[i]) == 0)
			{
				entry->info.isfavorite = true;
				break;
			}
		}
		if(Crypto_RetrieveHostKey(&addr, 0, NULL, 0, idfp, sizeof(idfp), NULL, NULL))
		{
			for(i = 0; i < nFavorites_idfp; ++i)
			{
				if(!strcmp(idfp, favorites_idfp[i]))
				{
					entry->info.isfavorite = true;
					break;
				}
			}
		}
	}

	// refresh the "category"
	entry->info.category = MR_GetServerListEntryCategory(entry);

	// FIXME: change this to be more readable (...)
	// now check whether it passes through the masks
	for( start = 0 ; start < SERVERLIST_ANDMASKCOUNT && serverlist_andmasks[start].active; start++ )
		if( !_ServerList_Entry_Mask( &serverlist_andmasks[start], &entry->info ) )
			return;

	for( start = 0 ; start < SERVERLIST_ORMASKCOUNT && serverlist_ormasks[start].active ; start++ )
		if( _ServerList_Entry_Mask( &serverlist_ormasks[start], &entry->info ) )
			break;
	if( start == SERVERLIST_ORMASKCOUNT || (start > 0 && !serverlist_ormasks[start].active) )
		return;

	if( !serverlist_viewcount ) {
		_ServerList_ViewList_Helper_InsertBefore( 0, entry );
		return;
	}
	// ok, insert it, we just need to find out where exactly:

	// two special cases
	// check whether to insert it as new first item
	if( _ServerList_Entry_Compare( entry, ServerList_GetViewEntry(0) ) ) {
		_ServerList_ViewList_Helper_InsertBefore( 0, entry );
		return;
	} // check whether to insert it as new last item
	else if( !_ServerList_Entry_Compare( entry, ServerList_GetViewEntry(serverlist_viewcount - 1) ) ) {
		_ServerList_ViewList_Helper_InsertBefore( serverlist_viewcount, entry );
		return;
	}
	start = 0;
	end = serverlist_viewcount - 1;
	while( end > start + 1 )
	{
		mid = (start + end) / 2;
		// test the item that lies in the middle between start and end
		if( _ServerList_Entry_Compare( entry, ServerList_GetViewEntry(mid) ) )
			// the item has to be in the upper half
			end = mid;
		else
			// the item has to be in the lower half
			start = mid;
	}
	_ServerList_ViewList_Helper_InsertBefore( start + 1, entry );
}

static void ServerList_ViewList_Remove( serverlist_entry_t *entry )
{
	int i;
	for( i = 0; i < serverlist_viewcount; i++ )
	{
		if (ServerList_GetViewEntry(i) == entry)
		{
			_ServerList_ViewList_Helper_Remove(i);
			break;
		}
	}
}

void ServerList_RebuildViewList(void)
{
	int i;

	serverlist_viewcount = 0;
	for( i = 0 ; i < serverlist_cachecount ; i++ ) {
		serverlist_entry_t *entry = &serverlist_cache[i];
		// also display entries that are currently being refreshed [11/8/2007 Black]
		if( entry->query == SQS_QUERIED || entry->query == SQS_REFRESHING )
			ServerList_ViewList_Insert( entry );
	}
}

void ServerList_ResetMasks(void)
{
	int i;

	memset( &serverlist_andmasks, 0, sizeof( serverlist_andmasks ) );
	memset( &serverlist_ormasks, 0, sizeof( serverlist_ormasks ) );
	// numbots needs to be compared to -1 to always succeed
	for(i = 0; i < SERVERLIST_ANDMASKCOUNT; ++i)
		serverlist_andmasks[i].info.numbots = -1;
	for(i = 0; i < SERVERLIST_ORMASKCOUNT; ++i)
		serverlist_ormasks[i].info.numbots = -1;
}

void ServerList_GetPlayerStatistics(int *numplayerspointer, int *maxplayerspointer)
{
	int i;
	int numplayers = 0, maxplayers = 0;
	for (i = 0;i < serverlist_cachecount;i++)
	{
		if (serverlist_cache[i].query == SQS_QUERIED)
		{
			numplayers += serverlist_cache[i].info.numhumans;
			maxplayers += serverlist_cache[i].info.maxplayers;
		}
	}
	*numplayerspointer = numplayers;
	*maxplayerspointer = maxplayers;
}

#if 0
static void _ServerList_Test(void)
{
	int i;
	if (serverlist_maxcachecount <= 1024)
	{
		serverlist_maxcachecount = 1024;
		serverlist_cache = (serverlist_entry_t *)Mem_Realloc(netconn_mempool, (void *)serverlist_cache, sizeof(serverlist_entry_t) * serverlist_maxcachecount);
	}
	for( i = 0 ; i < 1024 ; i++ ) {
		memset( &serverlist_cache[serverlist_cachecount], 0, sizeof( serverlist_entry_t ) );
		serverlist_cache[serverlist_cachecount].info.ping = 1000 + 1024 - i;
		dpsnprintf( serverlist_cache[serverlist_cachecount].info.name, sizeof(serverlist_cache[serverlist_cachecount].info.name), "Black's ServerList Test %i", i );
		serverlist_cache[serverlist_cachecount].finished = true;
		dpsnprintf( serverlist_cache[serverlist_cachecount].line1, sizeof(serverlist_cache[serverlist_cachecount].info.line1), "%i %s", serverlist_cache[serverlist_cachecount].info.ping, serverlist_cache[serverlist_cachecount].info.name );
		ServerList_ViewList_Insert( &serverlist_cache[serverlist_cachecount] );
		serverlist_cachecount++;
	}
}
#endif

void ServerList_QueryList(qboolean resetcache, qboolean querydp, qboolean queryqw, qboolean consoleoutput)
{
	masterquerytime = realtime;
	masterquerycount = 0;
	masterreplycount = 0;
	if( resetcache ) {
		serverquerycount = 0;
		serverreplycount = 0;
		serverlist_cachecount = 0;
		serverlist_viewcount = 0;
		serverlist_maxcachecount = 0;
		serverlist_cache = (serverlist_entry_t *)Mem_Realloc(netconn_mempool, (void *)serverlist_cache, sizeof(serverlist_entry_t) * serverlist_maxcachecount);
	} else {
		// refresh all entries
		int n;
		for( n = 0 ; n < serverlist_cachecount ; n++ ) {
			serverlist_entry_t *entry = &serverlist_cache[ n ];
			entry->query = SQS_REFRESHING;
			entry->querycounter = 0;
		}
	}
	serverlist_consoleoutput = consoleoutput;

	//_ServerList_Test();

	NetConn_QueryMasters(querydp, queryqw);
}

static int NetConn_ClientParsePacket_ServerList_ProcessReply(const char *addressstring)
{
	int n;
	int pingtime;
	serverlist_entry_t *entry = NULL;

	// search the cache for this server and update it
	for (n = 0;n < serverlist_cachecount;n++) {
		entry = &serverlist_cache[ n ];
		if (!strcmp(addressstring, entry->info.cname))
			break;
	}

	if (n == serverlist_cachecount)
	{
		// LAN search doesnt require an answer from the master server so we wont
		// know the ping nor will it be initialized already...

		// find a slot
		if (serverlist_cachecount == SERVERLIST_TOTALSIZE)
			return -1;

		if (serverlist_maxcachecount <= serverlist_cachecount)
		{
			serverlist_maxcachecount += 64;
			serverlist_cache = (serverlist_entry_t *)Mem_Realloc(netconn_mempool, (void *)serverlist_cache, sizeof(serverlist_entry_t) * serverlist_maxcachecount);
		}
		entry = &serverlist_cache[n];

		memset(entry, 0, sizeof(*entry));
		// store the data the engine cares about (address and ping)
		strlcpy(entry->info.cname, addressstring, sizeof(entry->info.cname));
		entry->info.ping = 100000;
		entry->querytime = realtime;
		// if not in the slist menu we should print the server to console
		if (serverlist_consoleoutput)
			Con_Printf("querying %s\n", addressstring);
		++serverlist_cachecount;
	}
	// if this is the first reply from this server, count it as having replied
	pingtime = (int)((realtime - entry->querytime) * 1000.0 + 0.5);
	pingtime = bound(0, pingtime, 9999);
	if (entry->query == SQS_REFRESHING) {
		entry->info.ping = pingtime;
		entry->query = SQS_QUERIED;
	} else {
		// convert to unsigned to catch the -1
		// I still dont like this but its better than the old 10000 magic ping number - as in easier to type and read :( [11/8/2007 Black]
		entry->info.ping = min((unsigned) entry->info.ping, (unsigned) pingtime);
		serverreplycount++;
	}

	// other server info is updated by the caller
	return n;
}

static void NetConn_ClientParsePacket_ServerList_UpdateCache(int n)
{
	serverlist_entry_t *entry = &serverlist_cache[n];
	serverlist_info_t *info = &entry->info;
	// update description strings for engine menu and console output
	dpsnprintf(entry->line1, sizeof(serverlist_cache[n].line1), "^%c%5d^7 ^%c%3u^7/%3u %-65.65s", info->ping >= 300 ? '1' : (info->ping >= 200 ? '3' : '7'), (int)info->ping, ((info->numhumans > 0 && info->numhumans < info->maxplayers) ? (info->numhumans >= 4 ? '7' : '3') : '1'), info->numplayers, info->maxplayers, info->name);
	dpsnprintf(entry->line2, sizeof(serverlist_cache[n].line2), "^4%-21.21s %-19.19s ^%c%-17.17s^4 %-20.20s", info->cname, info->game,
			(
			 info->gameversion != gameversion.integer
			 &&
			 !(
				    gameversion_min.integer >= 0 // min/max range set by user/mod?
				 && gameversion_max.integer >= 0
				 && gameversion_min.integer <= info->gameversion // version of server in min/max range?
				 && gameversion_max.integer >= info->gameversion
			  )
			) ? '1' : '4',
			info->mod, info->map);
	if (entry->query == SQS_QUERIED)
	{
		if(!serverlist_paused)
			ServerList_ViewList_Remove(entry);
	}
	// if not in the slist menu we should print the server to console (if wanted)
	else if( serverlist_consoleoutput )
		Con_Printf("%s\n%s\n", serverlist_cache[n].line1, serverlist_cache[n].line2);
	// and finally, update the view set
	if(!serverlist_paused)
		ServerList_ViewList_Insert( entry );
	//	update the entry's state
	serverlist_cache[n].query = SQS_QUERIED;
}

// returns true, if it's sensible to continue the processing
static qboolean NetConn_ClientParsePacket_ServerList_PrepareQuery( int protocol, const char *ipstring, qboolean isfavorite ) {
	int n;
	serverlist_entry_t *entry;

	//	ignore the rest of the message if the serverlist is full
	if( serverlist_cachecount == SERVERLIST_TOTALSIZE )
		return false;
	//	also ignore	it	if	we	have already queried	it	(other master server	response)
	for( n =	0 ; n	< serverlist_cachecount	; n++	)
		if( !strcmp( ipstring, serverlist_cache[ n ].info.cname ) )
			break;

	if( n < serverlist_cachecount ) {
		// the entry has already been queried once or
		return true;
	}

	if (serverlist_maxcachecount <= n)
	{
		serverlist_maxcachecount += 64;
		serverlist_cache = (serverlist_entry_t *)Mem_Realloc(netconn_mempool, (void *)serverlist_cache, sizeof(serverlist_entry_t) * serverlist_maxcachecount);
	}

	entry = &serverlist_cache[n];

	memset(entry, 0, sizeof(*entry));
	entry->protocol =	protocol;
	//	store	the data	the engine cares about (address and	ping)
	strlcpy (entry->info.cname, ipstring, sizeof(entry->info.cname));

	entry->info.isfavorite = isfavorite;

	// no, then reset the ping right away
	entry->info.ping = -1;
	// we also want to increase the serverlist_cachecount then
	serverlist_cachecount++;
	serverquerycount++;

	entry->query =	SQS_QUERYING;

	return true;
}

static void NetConn_ClientParsePacket_ServerList_ParseDPList(lhnetaddress_t *senderaddress, const unsigned char *data, int length, qboolean isextended)
{
	masterreplycount++;
	if (serverlist_consoleoutput)
		Con_Printf("received DarkPlaces %sserver list...\n", isextended ? "extended " : "");
	while (length >= 7)
	{
		char ipstring [128];

		// IPv4 address
		if (data[0] == '\\')
		{
			unsigned short port = data[5] * 256 + data[6];

			if (port != 0 && (data[1] != 0xFF || data[2] != 0xFF || data[3] != 0xFF || data[4] != 0xFF))
				dpsnprintf (ipstring, sizeof (ipstring), "%u.%u.%u.%u:%hu", data[1], data[2], data[3], data[4], port);

			// move on to next address in packet
			data += 7;
			length -= 7;
		}
		// IPv6 address
		else if (data[0] == '/' && isextended && length >= 19)
		{
			unsigned short port = data[17] * 256 + data[18];

			if (port != 0)
			{
#ifdef WHY_JUST_WHY
				const char *ifname;
				char ifnamebuf[16];

				/// \TODO: make some basic checks of the IP address (broadcast, ...)

				ifname = LHNETADDRESS_GetInterfaceName(senderaddress, ifnamebuf, sizeof(ifnamebuf));
				if (ifname != NULL)
				{
					dpsnprintf (ipstring, sizeof (ipstring), "[%x:%x:%x:%x:%x:%x:%x:%x%%%s]:%hu",
								(data[1] << 8) | data[2], (data[3] << 8) | data[4], (data[5] << 8) | data[6], (data[7] << 8) | data[8],
								(data[9] << 8) | data[10], (data[11] << 8) | data[12], (data[13] << 8) | data[14], (data[15] << 8) | data[16],
								ifname, port);
				}
				else
#endif
				{
					dpsnprintf (ipstring, sizeof (ipstring), "[%x:%x:%x:%x:%x:%x:%x:%x]:%hu",
								(data[1] << 8) | data[2], (data[3] << 8) | data[4], (data[5] << 8) | data[6], (data[7] << 8) | data[8],
								(data[9] << 8) | data[10], (data[11] << 8) | data[12], (data[13] << 8) | data[14], (data[15] << 8) | data[16],
								port);
				}
			}

			// move on to next address in packet
			data += 19;
			length -= 19;
		}
		else
		{
			Con_Print("Error while parsing the server list\n");
			break;
		}

		if (serverlist_consoleoutput && developer_networking.integer)
			Con_Printf("Requesting info from DarkPlaces server %s\n", ipstring);

		if( !NetConn_ClientParsePacket_ServerList_PrepareQuery( &protocol_darkplaces7, ipstring, false ) ) {
			break;
		}

	}

	// begin or resume serverlist queries
	serverlist_querysleep = false;
	serverlist_querywaittime = realtime + 3;
}

void NetConn_QueryQueueFrame(void)
{
	int index;
	int queries;
	int maxqueries;
	double timeouttime;
	static double querycounter = 0;

	if(!net_slist_pause.integer && serverlist_paused)
		ServerList_RebuildViewList();
	serverlist_paused = net_slist_pause.integer != 0;

	if (serverlist_querysleep)
		return;

	// apply a cool down time after master server replies,
	// to avoid messing up the ping times on the servers
	if (serverlist_querywaittime > realtime)
		return;

	// each time querycounter reaches 1.0 issue a query
	querycounter += cl.realframetime * net_slist_queriespersecond.value;
	maxqueries = (int)querycounter;
	maxqueries = bound(0, maxqueries, net_slist_queriesperframe.integer);
	querycounter -= maxqueries;

	if( maxqueries == 0 ) {
		return;
	}

	//	scan serverlist and issue queries as needed
	serverlist_querysleep = true;

	timeouttime	= realtime - net_slist_timeout.value;
	for( index = 0, queries	= 0 ;	index	< serverlist_cachecount	&&	queries < maxqueries	; index++ )
	{
		serverlist_entry_t *entry = &serverlist_cache[ index ];
		if( entry->query != SQS_QUERYING && entry->query != SQS_REFRESHING )
		{
			continue;
		}

		serverlist_querysleep	= false;
		if( entry->querycounter	!=	0 && entry->querytime >	timeouttime	)
		{
			continue;
		}

		if( entry->querycounter	!=	(unsigned) net_slist_maxtries.integer )
		{
			lhnetaddress_t	address;
			int socket;

			LHNETADDRESS_FromString(&address, entry->info.cname, 0);
			if	(entry->protocol == &protocol_quakeworld)
			{
				for (socket	= 0; socket	< cl_numsockets ;	socket++)
					NetConn_WriteString(cl_sockets[socket], "\377\377\377\377status\n", &address);
			}
			else
			{
				for (socket	= 0; socket	< cl_numsockets ;	socket++)
					NetConn_WriteString(cl_sockets[socket], "\377\377\377\377getstatus", &address);
			}

			//	update the entry fields
			entry->querytime = realtime;
			entry->querycounter++;

			// if not in the slist menu we should print the server to console
			if (serverlist_consoleoutput)
				Con_Printf("querying %25s (%i. try)\n", entry->info.cname, entry->querycounter);

			queries++;
		}
		else
		{
			// have we tried to refresh this server?
			if( entry->query == SQS_REFRESHING ) {
				// yes, so update the reply count (since its not responding anymore)
				serverreplycount--;
				if(!serverlist_paused)
					ServerList_ViewList_Remove(entry);
			}
			entry->query = SQS_TIMEDOUT;
		}
	}
}

void NetConn_QueryMasters(qboolean querydp, qboolean queryqw)
{
	int i, j;
	int masternum;
	lhnetaddress_t masteraddress;
	lhnetaddress_t broadcastaddress;
	char request[256];

	if (serverlist_cachecount >= SERVERLIST_TOTALSIZE)
		return;

	// 26000 is the default quake server port, servers on other ports will not
	// be found
	// note this is IPv4-only, I doubt there are IPv6-only LANs out there
	LHNETADDRESS_FromString(&broadcastaddress, "255.255.255.255", 26000);

	if (querydp)
	{
		for (i = 0;i < cl_numsockets;i++)
		{
			if (cl_sockets[i])
			{
				const char *cmdname, *extraoptions;
				int af = LHNETADDRESS_GetAddressType(LHNET_AddressFromSocket(cl_sockets[i]));

				if(LHNETADDRESS_GetAddressType(&broadcastaddress) == af)
				{
					// search LAN for Quake servers
					SZ_Clear(&cl_message);
					// save space for the header, filled in later
					MSG_WriteLong(&cl_message, 0);
					MSG_WriteByte(&cl_message, CCREQ_SERVER_INFO);
					MSG_WriteString(&cl_message, "QUAKE");
					MSG_WriteByte(&cl_message, NET_PROTOCOL_VERSION);
					StoreBigLong(cl_message.data, NETFLAG_CTL | (cl_message.cursize & NETFLAG_LENGTH_MASK));
					NetConn_Write(cl_sockets[i], cl_message.data, cl_message.cursize, &broadcastaddress);
					SZ_Clear(&cl_message);

					// search LAN for DarkPlaces servers
					NetConn_WriteString(cl_sockets[i], "\377\377\377\377getstatus", &broadcastaddress);
				}

				// build the getservers message to send to the dpmaster master servers
				if (LHNETADDRESS_GetAddressType(LHNET_AddressFromSocket(cl_sockets[i])) == LHNETADDRESSTYPE_INET6)
				{
					cmdname = "getserversExt";
					extraoptions = " ipv4 ipv6";  // ask for IPv4 and IPv6 servers
				}
				else
				{
					cmdname = "getservers";
					extraoptions = "";
				}
				memcpy(request, "\377\377\377\377", 4);
				dpsnprintf(request+4, sizeof(request)-4, "%s %s %u empty full%s", cmdname, gamenetworkfiltername, NET_PROTOCOL_VERSION, extraoptions);

				// search internet
				for (masternum = 0;sv_masters[masternum].name;masternum++)
				{
					if (sv_masters[masternum].string && sv_masters[masternum].string[0] && LHNETADDRESS_FromString(&masteraddress, sv_masters[masternum].string, DPMASTER_PORT) && LHNETADDRESS_GetAddressType(&masteraddress) == af)
					{
						masterquerycount++;
						NetConn_WriteString(cl_sockets[i], request, &masteraddress);
					}
				}

				// search favorite servers
				for(j = 0; j < nFavorites; ++j)
				{
					if(LHNETADDRESS_GetAddressType(&favorites[j]) == af)
					{
						if(LHNETADDRESS_ToString(&favorites[j], request, sizeof(request), true))
							NetConn_ClientParsePacket_ServerList_PrepareQuery( &protocol_darkplaces7, request, true );
					}
				}
			}
		}
	}

	// only query QuakeWorld servers when the user wants to
	if (queryqw)
	{
		for (i = 0;i < cl_numsockets;i++)
		{
			if (cl_sockets[i])
			{
				int af = LHNETADDRESS_GetAddressType(LHNET_AddressFromSocket(cl_sockets[i]));

				if(LHNETADDRESS_GetAddressType(&broadcastaddress) == af)
				{
					// search LAN for QuakeWorld servers
					NetConn_WriteString(cl_sockets[i], "\377\377\377\377status\n", &broadcastaddress);

					// build the getservers message to send to the qwmaster master servers
					// note this has no -1 prefix, and the trailing nul byte is sent
					dpsnprintf(request, sizeof(request), "c\n");
				}

				// search internet
				for (masternum = 0;sv_qwmasters[masternum].name;masternum++)
				{
					if (sv_qwmasters[masternum].string && LHNETADDRESS_FromString(&masteraddress, sv_qwmasters[masternum].string, QWMASTER_PORT) && LHNETADDRESS_GetAddressType(&masteraddress) == LHNETADDRESS_GetAddressType(LHNET_AddressFromSocket(cl_sockets[i])))
					{
						#ifdef QUAKELEGACY
						if (m_state != m_slist)
						{
							char lookupstring[128];
							LHNETADDRESS_ToString(&masteraddress, lookupstring, sizeof(lookupstring), true);
							Con_Printf("Querying master %s (resolved from %s)\n", lookupstring, sv_qwmasters[masternum].string);
						}
						#endif
						masterquerycount++;
						NetConn_Write(cl_sockets[i], request, (int)strlen(request) + 1, &masteraddress);
					}
				}

				// search favorite servers
				for(j = 0; j < nFavorites; ++j)
				{
					if(LHNETADDRESS_GetAddressType(&favorites[j]) == af)
					{
						if(LHNETADDRESS_ToString(&favorites[j], request, sizeof(request), true))
						{
							NetConn_WriteString(cl_sockets[i], "\377\377\377\377status\n", &favorites[j]);
							NetConn_ClientParsePacket_ServerList_PrepareQuery( &protocol_quakeworld, request, true );
						}
					}
				}
			}
		}
	}
	if (!masterquerycount)
	{
		Con_Print("Unable to query master servers, no suitable network sockets active.\n");
		M_Update_Return_Reason("No network");
	}
}

void Net_Refresh_f(void)
{
	if (m_state != m_slist) {
		Con_Print("Sending new requests to master servers\n");
		ServerList_QueryList(false, true, false, true);
		Con_Print("Listening for replies...\n");
	}
	else
		ServerList_QueryList(false, true, false, false);
}

void Net_Slist_f(void)
{
	ServerList_ResetMasks();
	serverlist_sortbyfield = SLIF_PING;
	serverlist_sortflags = 0;

    if (m_state != m_slist) {
		Con_Print("Sending requests to master servers\n");
		ServerList_QueryList(true, true, false, true);
		Con_Print("Listening for replies...\n");
	} else
		ServerList_QueryList(true, true, false, false);
}

void Net_SlistQW_f(void)
{
	ServerList_ResetMasks();
	serverlist_sortbyfield = SLIF_PING;
	serverlist_sortflags = 0;
    if (m_state != m_slist) {
		Con_Print("Sending requests to master servers\n");
		ServerList_QueryList(true, false, true, true);
		serverlist_consoleoutput = true;
		Con_Print("Listening for replies...\n");
	} else
		ServerList_QueryList(true, false, true, false);
}

void Net_Slist_Init()
{
	Cmd_AddCommand("net_slist", Net_Slist_f, "query dp master servers and print all server information");
	Cmd_AddCommand("net_slistqw", Net_SlistQW_f, "query qw master servers and print all server information");
	Cmd_AddCommand("net_refresh", Net_Refresh_f, "query dp master servers and refresh all server information");
	Cvar_RegisterVariable(&net_slist_queriespersecond);
	Cvar_RegisterVariable(&net_slist_queriesperframe);
	Cvar_RegisterVariable(&net_slist_timeout);
	Cvar_RegisterVariable(&net_slist_maxtries);
	Cvar_RegisterVariable(&net_slist_favorites);
	Cvar_RegisterVariable(&net_slist_pause);
	for (i = 0;sv_masters[i].name;i++)
		Cvar_RegisterVariable(&sv_masters[i]);
}
