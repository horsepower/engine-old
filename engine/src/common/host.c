/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

// host.c

/*
 * DESCRIPTION:
 * The host is the foundation of the engine. It is initialized within
 * main() and is responsible for the initialization of the engine and its
 * subsystems, as well as the spawning and killing of both the client and server.
 *
 * If a client is available, it will be initialized. A server can always be started
 * from the client. A client cannot be started from a server. Memory is cleared/released
 * when a server or client begins, and not when they end.
 *
 * Being the "init" of Quake, the host initializes the console, memory management, the
 * filesystem, the cvar system, console command system, and also handles session
 * management to allow multiple instances of the engine on the same computer, accessing
 * the same game directory.
 *
 * To make note of some ambiguity, the "client" can refer to a player instance, but
 * can also refer to the binary that makes up the usable game for human players.
 * When in the main menu for instance, the "client" in the former definition
 * is not running, but the "client" in the latter definition is.
 */


#include "quakedef.h"

#include <time.h>
#include "libcurl.h"
#include "progsvm.h"
#include "thread.h"
#include "utf8lib.h"

// LordHavoc: set when quit is executed

// current client
client_t		*host_client;

// host state tracking
host_state_t	host;


// pretend frames take this amount of time (in seconds), 0 = realtime
cvar_t host_framerate 						= {0, "host_framerate","0", "locks frame timing to this value in seconds, 0.05 is 20fps for example, note that this can easily run too fast, use cl_maxfps if you want to limit your framerate instead, or sys_ticrate to limit server speed"};

cvar_t cl_maxphysicsframesperserverframe 	= {0, "cl_maxphysicsframesperserverframe","10", "maximum number of physics frames per server frame"};
cvar_t cl_minfps 							= {CVAR_SAVE, "cl_minfps", "40", "minimum fps target - while the rendering performance is below this, it will drift toward lower quality"};
cvar_t cl_minfps_fade 						= {CVAR_SAVE, "cl_minfps_fade", "1", "how fast the quality adapts to varying framerate"};
cvar_t cl_minfps_qualitymax 				= {CVAR_SAVE, "cl_minfps_qualitymax", "1", "highest allowed drawdistance multiplier"};
cvar_t cl_minfps_qualitymin 				= {CVAR_SAVE, "cl_minfps_qualitymin", "0.25", "lowest allowed drawdistance multiplier"};
cvar_t cl_minfps_qualitymultiply 			= {CVAR_SAVE, "cl_minfps_qualitymultiply", "0.2", "multiplier for quality changes in quality change per second render time (1 assumes linearity of quality and render time)"};
cvar_t cl_minfps_qualityhysteresis 			= {CVAR_SAVE, "cl_minfps_qualityhysteresis", "0.05", "reduce all quality increments by this to reduce flickering"};
cvar_t cl_minfps_qualitystepmax 			= {CVAR_SAVE, "cl_minfps_qualitystepmax", "0.1", "maximum quality change in a single frame"};
cvar_t cl_minfps_force 						= {0, "cl_minfps_force", "0", "also apply quality reductions in timedemo/capturevideo"};
cvar_t cl_maxfps 							= {CVAR_SAVE, "cl_maxfps", "0", "maximum fps cap, 0 = unlimited, if game is running faster than this it will wait before running another frame (useful to make cpu time available to other programs)"};
cvar_t cl_maxfps_alwayssleep 				= {0, "cl_maxfps_alwayssleep","1", "gives up some processing time to other applications each frame, value in milliseconds, disabled if cl_maxfps is 0"};
cvar_t cl_maxidlefps 						= {CVAR_SAVE, "cl_maxidlefps", "20", "maximum fps cap when the game is not the active window (makes cpu time available to other programs"};


// shows time used by certain subsystems
cvar_t host_speeds 							= {0, "host_speeds","0", "reports how much time is used in server/graphics/sound"};
cvar_t host_maxwait 						= {0, "host_maxwait","1000", "maximum sleep time requested from the operating system in millisecond. Larger sleeps will be done using multiple host_maxwait length sleeps. Lowering this value will increase CPU load, but may help working around problems with accuracy of sleep times."};

// Developer modes
cvar_t developer 							= {CVAR_SAVE, "developer","0", "shows debugging messages and information (recommended for all developers and level designers); the value -1 also suppresses buffering and logging these messages"};
cvar_t developer_extra 						= {0, "developer_extra", "0", "prints additional debugging messages, often very verbose!"};
cvar_t developer_insane 					= {0, "developer_insane", "0", "prints huge streams of information about internal workings, entire contents of files being read/written, etc.  Not recommended!"};
cvar_t developer_loadfile 					= {0, "developer_loadfile","0", "prints name and size of every file loaded via the FS_LoadFile function (which is almost everything)"};
cvar_t developer_loading 					= {0, "developer_loading","0", "prints information about files as they are loaded or unloaded successfully"};
cvar_t developer_entityparsing 				= {0, "developer_entityparsing", "0", "prints detailed network entities information each time a packet is received"};
cvar_t developer_networkentities 			= {0, "developer_networkentities", "0", "prints received entities, value is 0-10 (higher for more info, 10 being the most verbose)"};


// Console timestamps
cvar_t timestamps 							= {CVAR_SAVE, "timestamps", "0", "prints timestamps on console messages"};
cvar_t timeformat 							= {CVAR_SAVE, "timeformat", "[%Y-%m-%d %H:%M:%S] ", "time format to use on timestamped console messages"};

// Sessions
cvar_t sessionid 							= {CVAR_READONLY, "sessionid", "", "ID of the current session (use the -sessionid parameter to set it); this is always either empty or begins with a dot (.)"};
cvar_t locksession 							= {0, "locksession", "0", "Lock the session? 0 = no, 1 = yes and abort on failure, 2 = yes and continue on failure"};

cvar_t csqc_progname = {0, "csqc_progname","csprogs.dat","name of csprogs.dat file to load"};
cvar_t csqc_progcrc = {CVAR_READONLY, "csqc_progcrc","-1","CRC of csprogs.dat file to load (-1 is none), only used during level changes and then reset to -1"};
cvar_t csqc_progsize = {CVAR_READONLY, "csqc_progsize","-1","file size of csprogs.dat file to load (-1 is none), only used during level changes and then reset to -1"};
cvar_t csqc_usedemoprogs = {0, "csqc_usedemoprogs","1","use csprogs stored in demos"};

event_tm (on_Host_Error,false,false);

/*
================
Host_AbortCurrentFrame

aborts the current host frame and goes on with the next one
================
*/
void Host_AbortCurrentFrame(void) DP_FUNC_NORETURN;
void Host_AbortCurrentFrame(void)
{
	// in case we were previously nice, make us mean again
	Sys_MakeProcessMean();

	longjmp (host.abortframe, 1);
}

void Host_NoOperation_f(void)
{
}

/*
================
Host_Error

This shuts down both the client and server
================
*/
void Host_Error (const char *error, ...)
{
	static char 		hosterrorstring1[MAX_INPUTLINE]; // THREAD UNSAFE
	static char 		hosterrorstring2[MAX_INPUTLINE]; // THREAD UNSAFE
	static qboolean 	hosterror = false;
	va_list 			argptr;

	// turn off rcon redirect if it was active when the crash occurred
	// to prevent loops when it is a networking problem
	Con_Rcon_Redirect_Abort();

	va_start (argptr,error);
	dpvsnprintf (hosterrorstring1,sizeof(hosterrorstring1),error,argptr);
	va_end (argptr);

	Con_Printf("Host_Error: %s\n", hosterrorstring1);

	// LordHavoc: if crashing very early, do
	// Sys_Error instead
	if (host.framecount < 3)
	{
		Sys_Error ("Host_Error: %s", hosterrorstring1);
	}

	// If called recursively, quit
	if (hosterror)
	{
		Sys_Error ("Host_Error: recursively entered (original error was: %s. New error is: %s)", hosterrorstring2, hosterrorstring1);
	}

	hosterror = true;

	strlcpy(hosterrorstring2, hosterrorstring1, sizeof(hosterrorstring2));

	Event_Trigger(&on_Host_Error,NULL);

	//PR_Crash();

	// print out where the crash happened, if it was caused by QC (and do a cleanup)
	PRVM_Crash(SVVM_prog);
	PRVM_Crash(CLVM_prog);
#ifdef CONFIG_MENU
	PRVM_Crash(MVM_prog);
#endif

	cl.csqc_loaded = false;
	Cvar_SetValueQuick(&csqc_progcrc, -1);
	Cvar_SetValueQuick(&csqc_progsize, -1);

	SV_LockThreadMutex();
	SV_Shutdown ();
	SV_UnlockThreadMutex();

	if (host.state & H_DEDICATED)
	{
		Sys_Error ("Host_Error: %s",hosterrorstring2);	// dedicated servers exit
	}

	CL_Disconnect (NULL);

	cls.demonum = -1;

	hosterror = false;

	Host_AbortCurrentFrame();
}

static void Host_ServerOptions (void)
{
	int i;

	// general default
	svs.maxclients = 8;

// COMMANDLINEOPTION: Server: -dedicated starts a dedicated server (with a command console), default playerlimit is 8	

	// if no client is in the executable or -dedicated is specified on
	// commandline, start a dedicated server
	i = Sys_CheckParm ("-dedicated");
	if (i || !sys.client)
	{
		host.state |= H_DEDICATED;
		
		// default sv_public on for dedicated servers (often hosted by serious administrators), off for listen servers (often hosted by clueless users)
		Cvar_SetValue("sv_public", 1);
	}

	svs.maxclients = svs.maxclients_next = bound(1, svs.maxclients, MAX_SCOREBOARD);

	svs.clients = (client_t *)Mem_Alloc(sv_mempool, sizeof(client_t) * svs.maxclients);

	if (svs.maxclients > 1 && !deathmatch.integer && !coop.integer)
	{
		Cvar_SetValueQuick(&deathmatch, 1);
	}
}

/*
==================
Host_Quit_f
==================
*/

void Host_Quit_f (void)
{
	if(host.state & H_SHUTTINGDOWN)
		Con_Printf("shutting down already!\n");
	else
		Sys_Quit (0);
}

static void Host_Version_f (void)
{
	Con_Printf("Version: %s %s\n", gamename, buildstring);
}

/*
=======================
Host_InitLocal
======================
*/
void Host_SaveConfig_f(void);
void Host_LoadConfig_f(void);

static void Host_InitLocal (void)
{
	Cmd_AddCommand (0, "quit", Host_Quit_f, "quit the game");
	Cmd_AddCommand (0, "exit", Host_Quit_f, "quit the game");
	Cmd_AddCommand (0, "version", Host_Version_f, "print engine version");

	Cmd_AddCommand (0, "saveconfig", Host_SaveConfig_f, "save settings to config.cfg (or a specified filename) immediately (also automatic when quitting)");
	Cmd_AddCommand (0, "loadconfig", Host_LoadConfig_f, "reset everything and reload configs");


	Cvar_RegisterVariable (&host_framerate);
	Cvar_RegisterVariable (&host_speeds);
	Cvar_RegisterVariable (&host_maxwait);

	Cvar_RegisterVariable (&cl_maxphysicsframesperserverframe);
	Cvar_RegisterVariable (&cl_minfps);
	Cvar_RegisterVariable (&cl_minfps_fade);
	Cvar_RegisterVariable (&cl_minfps_qualitymax);
	Cvar_RegisterVariable (&cl_minfps_qualitymin);
	Cvar_RegisterVariable (&cl_minfps_qualitystepmax);
	Cvar_RegisterVariable (&cl_minfps_qualityhysteresis);
	Cvar_RegisterVariable (&cl_minfps_qualitymultiply);
	Cvar_RegisterVariable (&cl_minfps_force);
	Cvar_RegisterVariable (&cl_maxfps);
	Cvar_RegisterVariable (&cl_maxfps_alwayssleep);
	Cvar_RegisterVariable (&cl_maxidlefps);

	Cvar_RegisterVariable (&developer);
	Cvar_RegisterVariable (&developer_extra);
	Cvar_RegisterVariable (&developer_insane);
	Cvar_RegisterVariable (&developer_loadfile);
	Cvar_RegisterVariable (&developer_loading);
	Cvar_RegisterVariable (&developer_entityparsing);

	Cvar_RegisterVariable (&timestamps);
	Cvar_RegisterVariable (&timeformat);
}


/*
===============
Host_SaveConfig_f

Writes key bindings and archived cvars to config.cfg
===============
*/
static void Host_SaveConfig_to(const char *file)
{
	qfile_t *f;

// dedicated servers initialize the host but don't parse and set the
// config.cfg cvars
	// LordHavoc: don't save a config if it crashed in startup
	if (host.framecount >= 3 && !(host.state & H_DEDICATED) && !Sys_CheckParm("-benchmark") && !Sys_CheckParm("-capturedemo"))
	{
		f = FS_OpenRealFile(file, "wb", false);
		if (!f)
		{
			Con_Printf("Couldn't write %s.\n", file);
			return;
		}

		Key_WriteBindings (f);
		Cvar_WriteVariables (f);

		FS_Close (f);
	}
}

void Host_SaveConfig(void)
{
	Host_SaveConfig_to(CONFIGFILENAME);
}

void Host_SaveConfig_f(void)
{
	const char *file = CONFIGFILENAME;

	if(Cmd_Argc() >= 2)
	{
		file = Cmd_Argv(1);
		Con_Printf("Saving to %s\n", file);
	}

	Host_SaveConfig_to(file);
}

static void Host_AddConfigText(void)
{
	// set up the default startmap_sp and startmap_dm aliases (mods can
	// override these) and then execute the quake.rc startup script
	if (gamemode == GAME_NEHAHRA)
	{
		Cbuf_InsertText("alias startmap_sp \"map nehstart\"\nalias startmap_dm \"map nehstart\"\nexec " STARTCONFIGFILENAME "\n");
	}
	else if (gamemode == GAME_TRANSFUSION)
	{
		Cbuf_InsertText("alias startmap_sp \"map e1m1\"\n""alias startmap_dm \"map bb1\"\nexec " STARTCONFIGFILENAME "\n");
	}
	else if (gamemode == GAME_TEU)
	{
		Cbuf_InsertText("alias startmap_sp \"map start\"\nalias startmap_dm \"map start\"\nexec teu.rc\n");
	}
	else
	{
		Cbuf_InsertText("alias startmap_sp \"map start\"\nalias startmap_dm \"map start\"\nexec " STARTCONFIGFILENAME "\n");
	}
}

/*
===============
Host_LoadConfig_f

Resets key bindings and cvars to defaults and then reloads scripts
===============
*/
void Host_LoadConfig_f(void)
{
	// reset all cvars, commands and aliases to init values
	Cmd_RestoreInitState();
#ifdef CONFIG_MENU
	// prepend a menu restart command to execute after the config
	Cbuf_InsertText("\nmenu_restart\n");
#endif
	// reset cvars to their defaults, and then exec startup scripts again
	Host_AddConfigText();
}

//============================================================================

/*
===================
Host_GetConsoleCommands

Add them exactly as if they had been typed at the console
===================
*/
static void Host_GetConsoleCommands (void)
{
	char *cmd;

	while (1)
	{
		cmd = Sys_ConsoleInput ();
		if (!cmd)
		{
			break;
		}
		Cbuf_AddText (cmd);
	}
}

/*
==================
Host_TimeReport

Returns a time report string, for example for
==================
*/
const char *Host_TimingReport(char *buf, size_t buflen)
{
	return va(buf, buflen, "%.1f%% CPU, %.2f%% lost, offset avg %.1fms, max %.1fms, sdev %.1fms", svs.perf_cpuload * 100, svs.perf_lost * 100, svs.perf_offset_avg * 1000, svs.perf_offset_max * 1000, svs.perf_offset_sdev * 1000);
}

/*
==================
Host_Main

Runs all active servers
==================
*/

void SV_Frame(void);
void CL_Frame(void);

static void Host_Main(void)
{
	double 		deltacleantime, olddirtytime, dirtytime;
	int 		i;
	char 		vabuf[1024];
	qboolean 	playing;

	host.realtime = 0;
	host.dirtytime = Sys_DirtyTime();

	for (;;)
	{
		if (setjmp(host.abortframe))
		{
#ifndef SERVER
			SCR_ClearLoadingScreen(false);
#endif
			continue;			// something bad happened, or the server disconnected
		}

		olddirtytime = host.dirtytime;
		dirtytime = Sys_DirtyTime();
		deltacleantime = dirtytime - olddirtytime;

		if (deltacleantime < 0)
		{
			// warn if it's significant
			if (deltacleantime < -0.01)
			{
				Con_Printf("Host_Mingled: time stepped backwards (went from %f to %f, difference %f)\n", olddirtytime, dirtytime, deltacleantime);
			}

			deltacleantime = 0;
		}
		else if (deltacleantime >= 1800)
		{
			Con_Printf("Host_Mingled: time stepped forward (went from %f to %f, difference %f)\n", olddirtytime, dirtytime, deltacleantime);
			deltacleantime = 0;
		}

		host.realtime += deltacleantime;
		host.dirtytime = dirtytime;

		host.cl_timer += deltacleantime;
		host.sv_timer += deltacleantime;

		if (!svs.threaded)
		{
			svs.perf_acc_realtime += deltacleantime;

			// Look for clients who have spawned
			playing = false;

			for (i = 0, host_client = svs.clients;i < svs.maxclients;i++, host_client++)
			{
				if(host_client->begun)
				{
					if(host_client->netconnection)
					{
						playing = true;
					}
				}
			}

			if(sv.time < 10)
			{
				// don't accumulate time for the first 10 seconds of a match
				// so things can settle
				svs.perf_acc_realtime = svs.perf_acc_sleeptime = svs.perf_acc_lost = svs.perf_acc_offset = svs.perf_acc_offset_squared = svs.perf_acc_offset_max = svs.perf_acc_offset_samples = 0;
			}
			else if(svs.perf_acc_realtime > 5)
			{
				svs.perf_cpuload = 1 - svs.perf_acc_sleeptime / svs.perf_acc_realtime;
				svs.perf_lost = svs.perf_acc_lost / svs.perf_acc_realtime;

				if(svs.perf_acc_offset_samples > 0)
				{
					svs.perf_offset_max = svs.perf_acc_offset_max;
					svs.perf_offset_avg = svs.perf_acc_offset / svs.perf_acc_offset_samples;
					svs.perf_offset_sdev = sqrt(svs.perf_acc_offset_squared / svs.perf_acc_offset_samples - svs.perf_offset_avg * svs.perf_offset_avg);
				}

				if(svs.perf_lost > 0 && developer_extra.integer)
				{
					if(playing) // only complain if anyone is looking
					{
						Con_DPrintf("Server can't keep up: %s\n", Host_TimingReport(vabuf, sizeof(vabuf)));
					}
				}

				svs.perf_acc_realtime = svs.perf_acc_sleeptime = svs.perf_acc_lost = svs.perf_acc_offset = svs.perf_acc_offset_squared = svs.perf_acc_offset_max = svs.perf_acc_offset_samples = 0;
			}
		}

		// keep the random time dependent, but not when playing demos/benchmarking
		if(!*sv_random_seed.string && !(host.state & H_CS_DEMO))
		{
			rand();
		}

		Event_Trigger(&evqueue,NULL);

		Log_DestBuffer_Flush();

		Curl_Run();

		// check for commands typed to the host
		Host_GetConsoleCommands();

		// when a server is running we only execute console commands on server frames
		// (this mainly allows frikbot .way config files to work properly by staying in sync with the server qc)
		// otherwise we execute them on client frames
		if ((host.state & H_SS_ACTIVE) ? host.sv_timer > 0 : host.cl_timer > 0)
		{
			// process console commands
//			R_TimeReport("preconsole");
#ifndef SERVER
			CL_VM_PreventInformationLeaks();
#endif
			Cbuf_Frame();
//			R_TimeReport("console");
		}

		//Con_Printf("%6.0f %6.0f\n", cl_timer * 1000000.0, sv_timer * 1000000.0);

		// if the accumulators haven't become positive yet, wait a while
		if (host.state & H_DEDICATED)
		{
			host.wait = host.sv_timer * -1000000.0;
		}
		else if (!(host.state & H_SS_ACTIVE) || svs.threaded)
		{
			host.wait = host.cl_timer * -1000000.0;
		}
		else
		{
			host.wait = max(host.cl_timer, host.sv_timer) * -1000000.0;
		}

		if (!cls.timedemo && host.wait >= 1)
		{
			double time0, delta;

			if(host_maxwait.value <= 0)
			{
				host.wait = min(host.wait, 1000000.0);
			}
			else
			{
				host.wait = min(host.wait, host_maxwait.value * 1000.0);
			}

			if(host.wait < 1)
			{
				host.wait = 1; // because we cast to int
			}

			time0 = Sys_DirtyTime();

			if (sv_checkforpacketsduringsleep.integer && !sys_usenoclockbutbenchmark.integer && !svs.threaded)
			{
				NetConn_SleepMicroseconds((int)host.wait);

				/*if (!host.dedicated)
				{
					NetConn_ClientFrame(); // helps server browser get good ping values
					// TODO can we do the same for ServerFrame? Probably not.
				}*/
			}
			else
			{
				Sys_Sleep((int)host.wait);
			}

			delta = Sys_DirtyTime() - time0;

			if (delta < 0 || delta >= 1800) delta = 0;
			{
				if (!svs.threaded)
				{
					svs.perf_acc_sleeptime += delta;
				}

			}
//			R_TimeReport("sleep");
			continue;
		}

		// limit the frametime steps to no more than 100ms each
		if (host.cl_timer > 0.1)
		{
			host.cl_timer = 0.1;
		}

		if (host.sv_timer > 0.1)
		{
			if (!svs.threaded)
			{
				svs.perf_acc_lost += (host.sv_timer - 0.1);
			}
			host.sv_timer = 0.1;
		}

		R_TimeReport("---");

	//-------------------
	//
	// server operations
	//
	//-------------------

		SV_Frame();

	//-------------------
	//
	// client operations
	//
	//-------------------
#ifndef SERVER
		if(!(host.state & H_DEDICATED))
			CL_Frame();
#endif
#if MEMPARANOIA
		Mem_CheckSentinelsGlobal();
#else
		if (developer_memorydebug.integer)
		{
			Mem_CheckSentinelsGlobal();
		}
#endif

		// if there is some time remaining from this frame, reset the timers
		if (host.cl_timer >= 0)
		{
			host.cl_timer = 0;
		}

		if (host.sv_timer >= 0)
		{
			if (!svs.threaded)
			{
				svs.perf_acc_lost += host.sv_timer;
			}

			host.sv_timer = 0;
		}

		// Frame complete
		host.framecount++;
	}
}

//============================================================================

char 				engineversion[128];

qboolean 			sys_nostdout = false;

//extern qboolean 	host_stuffcmdsrun;

static qfile_t		*locksession_fh = NULL;
static qboolean 	locksession_run = false;

static void Host_InitSession(void)
{
	int i;
	Cvar_RegisterVariable(&sessionid);
	Cvar_RegisterVariable(&locksession);

	// load the session ID into the read-only cvar
	if ((i = Sys_CheckParm("-sessionid")) && (i + 1 < sys.argc))
	{
		char vabuf[1024];

		if(sys.argv[i+1][0] == '.')
		{
			Cvar_SetQuick(&sessionid, sys.argv[i+1]);
		}
		else
		{
			Cvar_SetQuick(&sessionid, va(vabuf, sizeof(vabuf), ".%s", sys.argv[i+1]));
		}
	}
}

void Host_LockSession(void)
{
	if(locksession_run)
	{
		return;
	}

	locksession_run = true;

	if(locksession.integer != 0 && !Sys_CheckParm("-readonly"))
	{
		char vabuf[1024];
		char *p = va(vabuf, sizeof(vabuf), "%slock%s", *fs_userdir ? fs_userdir : fs_basedir, sessionid.string);
		FS_CreatePath(p);
		locksession_fh = FS_SysOpen(p, "wl", false);

		// TODO maybe write the pid into the lockfile, while we are at it? may help server management tools
		if(!locksession_fh)
		{
			if(locksession.integer == 2)
			{
				Con_Printf("WARNING: session lock %s could not be acquired. Please run with -sessionid and an unique session name. Continuing anyway.\n", p);
			}
			else
			{
				Sys_Error("session lock %s could not be acquired. Please run with -sessionid and an unique session name.\n", p);
			}
		}
	}
}

void Host_UnlockSession(void)
{
	if(!locksession_run)
	{
		return;
	}

	locksession_run = false;

	if(locksession_fh)
	{
		FS_Close(locksession_fh);
		// NOTE: we can NOT unlink the lock here, as doing so would
		// create a race condition if another process created it
		// between our close and our unlink
		locksession_fh = NULL;
	}
}

static void Host_Init_Parms (void)
{
	if (Sys_CheckParm("-profilegameonly"))
	{
		Sys_AllowProfiling(false);
	}

	// LordHavoc: quake never seeded the random number generator before... heh
	if (Sys_CheckParm("-benchmark"))
	{
		srand(0); // predictable random sequence for -benchmark
	}
	else
	{
		srand((unsigned int)time(NULL));
	}

	// FIXME: this is evil, but possibly temporary
	// LordHavoc: doesn't seem very temporary...
	// LordHavoc: made this a saved cvar
// COMMANDLINEOPTION: Console: -developer enables warnings and other notices (RECOMMENDED for mod developers)
	if (Sys_CheckParm("-developer"))
	{
		developer.value = developer.integer = 1;
		developer.string = "1";
	}

	if (Sys_CheckParm("-developer2") || Sys_CheckParm("-developer3"))
	{
		developer.value = developer.integer = 1;
		developer.string = "1";
		developer_extra.value = developer_extra.integer = 1;
		developer_extra.string = "1";
		developer_insane.value = developer_insane.integer = 1;
		developer_insane.string = "1";
		developer_memory.value = developer_memory.integer = 1;
		developer_memory.string = "1";
		developer_memorydebug.value = developer_memorydebug.integer = 1;
		developer_memorydebug.string = "1";
	}

	if (Sys_CheckParm("-developer3"))
	{
		gl_paranoid.integer = 1;gl_paranoid.string = "1";
		gl_printcheckerror.integer = 1;gl_printcheckerror.string = "1";
	}

// COMMANDLINEOPTION: Console: -nostdout disables text output to the terminal the game was launched from
	if (Sys_CheckParm("-nostdout"))
	{
		sys_nostdout = 1;
	}
}

/*
====================
Host_Init
====================
*/
void Host_Init (void)
{
	Host_Init_Parms(); 		// Check command-line parameters before continuing

	Cmd_Init(); 			// Init command execution system and basic commands

	Con_Init(); 			// Init console system

	COM_Init_Commands(); 	// Init common commands

	Sys_Init_Commands(); 	// Init system commands

	u8_Init(); 				// Init UTF-8 subsystem. Disabled by default for compatibility

	Mathlib_Init(); 		// Init ixtable

	FS_Init(); 				// Init filesystem (including fs_basedir, fs_gamedir, -game, scr_screenshot_name)

	Sys_InitProcessNice(); 	// Init process nice level

	Host_InitSession(); 	// Init cvars for session locking

	Crypto_Init(); 			// Init crypto subsystem. Must be after FS_Init

	NetConn_Init();			// Init NetConn subsystem

	Curl_Init();			// Init curl subsystem.

	PRVM_Init();			// Init progs VM (QCVM)

	Mod_Init();				// Init model rendering

	World_Init();			// Init world

	Host_InitLocal();		// Init local host commands

	Thread_Init();			// Init threading

	SV_Init();				// Init serverside
	Host_ServerOptions();
#ifndef SERVER
	CL_Init();				// Init clientside (if available)
#endif
	Cmd_Legacy_InitCommands();

	Host_AddConfigText();

	// Execute default.cfg or autoexec.cfg if no client was available to do it
	// TODO: Make this better... Can't put this before client execution because it messes up the loading plaque resolution.
	if(host.state & H_DEDICATED)
	{
		Cbuf_AddText("exec default.cfg\nexec " CONFIGFILENAME "\nexec autoexec.cfg\nstuffcmds\n");
	}

	Cmd_SaveInitState(); 	// Save current state of aliases, commands and cvars for later restore if FS_GameDir_f is called
							// NOTE: menu commands are freed by Cmd_RestoreInitState

	if (setjmp(host.abortframe))
	{
		Sys_Error("Init failed. Try launching with -condebug");
	}

	Cbuf_Execute(); // Final execution of console buffer

	Con_DPrint("========Initialized=========\n");

	Host_Main();
}


/*
===============
Host_Shutdown

FIXME: this is a callback from Sys_Quit and Sys_Error.  It would be better
to run quit through here before the final handoff to the sys code.
===============
*/
void Host_Shutdown(void)
{
	static qboolean isdown = false;

	if (isdown)
	{
		Con_Print("recursive shutdown\n");
		return;
	}

	if (setjmp(host.abortframe))
	{
		Con_Print("aborted the quitting frame?!?\n");
		return;
	}

	isdown = true;

	// be quiet while shutting down

	// end the server thread
	if (svs.threaded)
	{
		SV_StopThread();
	}

	// shut down local server if active
	SV_LockThreadMutex();
	SV_Shutdown ();
	SV_UnlockThreadMutex();

	// AK shutdown PRVM
	// AK hmm, no PRVM_Shutdown(); yet
#ifndef SERVER
	if(!(host.state & H_DEDICATED))
	{
		CL_Shutdown();
	}
#endif
	Host_SaveConfig();


	Curl_Shutdown ();
	NetConn_Shutdown ();
	//PR_Shutdown ();

	Thread_Shutdown();
	Cmd_Shutdown();
	Sys_Shutdown();
	Log_Close();
	Crypto_Shutdown();

	Host_UnlockSession();

	Con_Shutdown();
	Memory_Shutdown();
}

