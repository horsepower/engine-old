#include "quakedef.h"

protocol_netmsg_t protocol_darkplaces7_svc;
protocol_netmsg_t protocol_darkplaces7_clc;
protocol_netmsg_t protocol_netquake_svc;
protocol_netmsg_t protocol_netquake_clc;

// LordHavoc reserves protocol ranges 96, 97, 3500-3599

protocol_t protocol_netquake =
{
	15,
	"QUAKE",
	0,
	640,
	256,
	&protocol_netquake_svc,
	&protocol_netquake_clc,
	.svcstr =
	{
			"svc_bad",
			"svc_nop",
			"svc_disconnect",
			"svc_updatestat",
			"svc_version",
			"svc_setview",
			"svc_sound",
			"svc_time",
			"svc_print",
			"svc_stufftext",
			"svc_setangle",
			"svc_serverinfo",
			"svc_lightstyle",
			"svc_updatename",
			"svc_updatefrags",
			"svc_clientdata",
			"svc_stopsound",
			"svc_updatecolors",
			"svc_particle",
			"svc_damage",
			"svc_spawnstatic",
			"OBSOLETE svc_spawnbinary",
			"svc_spawnbaseline",
			"svc_temp_entity",
			"svc_setpause",
			"svc_signonnum",
			"svc_centerprint",
			"svc_killedmonster",
			"svc_foundsecret",
			"svc_spawnstaticsound",
			"svc_intermission",
			"svc_finale",
			"svc_cdtrack",
			"svc_sellscreen",
			"svc_cutscene",
			"svc_showlmp",
			"svc_hidelmp",
			"svc_skybox",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"svc_downloaddata",
			"svc_updatestatubyte",
			"svc_effect",
			"svc_effect2",
			"svc_sound2",
			"svc_spawnbaseline2",
			"svc_spawnstatic2",
			"svc_entities",
			"svc_csqcentities",
			"svc_spawnstaticsound2",
			"svc_trailparticles",
			"svc_pointparticles",
			"svc_pointparticles1",
	}
};

protocol_t protocol_quakedp =
{
	15,
	"QUAKEDP",
	0,
	2048,
	256
};

// TODO: Implement.
protocol_t protocol_nehahramovie =
{
	250,
	"NEHAHRAMOVIE",
	.maxedicts = 2048,
	.maxmodels = 256
};
protocol_t protocol_nehahrabjp =
{
	10000,
	"NEHAHRABJP",
	.maxedicts = 4096,
	.maxmodels = 256
};
protocol_t protocol_nehahrabjp2 =
{
	10001,
	"NEHAHRABJP2",
	.maxedicts = 4096,
	.maxmodels = 256
};
protocol_t protocol_nehahrabjp3 =
{
	10002,
	"NEHAHRABJP3",
	.maxedicts = 4096,
	.maxmodels = 256
};
protocol_t protocol_darkplaces1 =
{
	96,
	"DP1",
	0,
	MAX_EDICTS,
	MAX_MODELS
};
protocol_t protocol_darkplaces2 =
{
	97,
	"DP2",
	0,
	MAX_EDICTS,
	MAX_MODELS
};
protocol_t protocol_darkplaces3 =
{
	3500,
	"DP3",
	0,
	MAX_EDICTS,
	MAX_MODELS,
	.svcmsg = &protocol_darkplaces7_svc,
	.clcmsg = &protocol_darkplaces7_clc
};
protocol_t protocol_darkplaces4 =
{
	3501,
	"DP4",
	0,
	MAX_EDICTS,
	MAX_MODELS
};
protocol_t protocol_darkplaces5 =
{
	3502,
	"DP5",
	0,
	MAX_EDICTS,
	MAX_MODELS
};
protocol_t protocol_darkplaces6 =
{
	3503,
	"DP6",
	0,
	MAX_EDICTS,
	MAX_MODELS
};

protocol_t protocol_darkplaces7 =
{
	.num = 3504,
	.name = "DP7",
	.flags = 0,
	.maxedicts = MAX_EDICTS,
	.maxmodels = MAX_MODELS,
	.svcmsg = &protocol_darkplaces7_svc,
	.clcmsg = &protocol_darkplaces7_clc,
	.svcstr =
	{
			"svc_bad",
			"svc_nop",
			"svc_disconnect",
			"svc_updatestat",
			"svc_version",
			"svc_setview",
			"svc_sound",
			"svc_time",
			"svc_print",
			"svc_stufftext",
			"svc_setangle",
			"svc_serverinfo",
			"svc_lightstyle",
			"svc_updatename",
			"svc_updatefrags",
			"svc_clientdata",
			"svc_stopsound",
			"svc_updatecolors",
			"svc_particle",
			"svc_damage",
			"svc_spawnstatic",
			"OBSOLETE svc_spawnbinary",
			"svc_spawnbaseline",
			"svc_temp_entity",
			"svc_setpause",
			"svc_signonnum",
			"svc_centerprint",
			"svc_killedmonster",
			"svc_foundsecret",
			"svc_spawnstaticsound",
			"svc_intermission",
			"svc_finale",
			"svc_cdtrack",
			"svc_sellscreen",
			"svc_cutscene",
			"svc_showlmp",
			"svc_hidelmp",
			"svc_skybox",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"svc_downloaddata",
			"svc_updatestatubyte",
			"svc_effect",
			"svc_effect2",
			"svc_sound2",
			"svc_spawnbaseline2",
			"svc_spawnstatic2",
			"svc_entities",
			"svc_csqcentities",
			"svc_spawnstaticsound2",
			"svc_trailparticles",
			"svc_pointparticles",
			"svc_pointparticles1",
	}
};

protocol_t protocol_horsepower1 =
{
	3600,
	"HP1",
	0,
	MAX_EDICTS,
	MAX_MODELS
};
protocol_t protocol_quakeworld =
{
	28,
	"QUAKEWORLD",
	0,
	512, // i think?
	.svcstr =
	{
			"svc_bad",
			"svc_nop",
			"svc_disconnect",
			"svc_updatestat",
			"",
			"svc_setview",
			"svc_sound",
			"",
			"svc_print",
			"svc_stufftext",
			"svc_setangle",
			"svc_serverdata",
			"svc_lightstyle",
			"",
			"svc_updatefrags",
			"",
			"svc_stopsound",
			"",
			"",
			"svc_damage",
			"svc_spawnstatic",
			"",
			"svc_spawnbaseline",
			"svc_temp_entity",
			"svc_setpause",
			"",
			"svc_centerprint",
			"svc_killedmonster",
			"svc_foundsecret",
			"svc_spawnstaticsound",
			"svc_intermission",
			"svc_finale",
			"svc_cdtrack",
			"svc_sellscreen",
			"svc_smallkick",
			"svc_bigkick",
			"svc_updateping",
			"svc_updateentertime",
			"svc_updatestatlong",
			"svc_muzzleflash",
			"svc_updateuserinfo",
			"svc_download",
			"svc_playerinfo",
			"svc_nails",
			"svc_chokecount",
			"svc_modellist",
			"svc_soundlist",
			"svc_packetentities",
			"svc_deltapacketentities",
			"svc_maxspeed",
			"svc_entgravity",
			"svc_setinfo",
			"svc_serverinfo",
			"svc_updatepl",
	}
};
