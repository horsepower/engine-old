#include "quakedef.h"

entityframe5_database_t *EntityFrame5_AllocDatabase(mempool_t *pool)
{
	int i;
	entityframe5_database_t *d;
	d = (entityframe5_database_t *)Mem_Alloc(pool, sizeof(*d));
	d->latestframenum = 0;
	for (i = 0;i < d->maxedicts;i++)
		d->states[i] = defaultstate;
	return d;
}

void EntityFrame5_FreeDatabase(entityframe5_database_t *d)
{
	// all the [maxedicts] memory is allocated at once, so there's only one
	// thing to free
	if (d->maxedicts)
		Mem_Free(d->deltabits);
	Mem_Free(d);
}
