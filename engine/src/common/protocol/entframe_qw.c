#include "quakedef.h"

entityframeqw_database_t *EntityFrameQW_AllocDatabase(mempool_t *pool)
{
	entityframeqw_database_t *d;
	d = (entityframeqw_database_t *)Mem_Alloc(pool, sizeof(*d));
	return d;
}

void EntityFrameQW_FreeDatabase(entityframeqw_database_t *d)
{
	Mem_Free(d);
}
