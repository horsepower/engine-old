/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// console.c

#if !defined(WIN32) || defined(__MINGW32__)
# include <unistd.h>
#endif
#include <time.h>

#include "quakedef.h"
#include "thread.h"

// for u8_encodech
#include "ft2.h"

// lines up from bottom to display
int con_backscroll;

conbuffer_t con;

void *con_mutex = NULL;

cvar_t sys_specialcharactertranslation = {0, "sys_specialcharactertranslation", "1", "terminal console conchars to ASCII translation (set to 0 if your conchars.tga is for an 8bit character set or if you want raw output)"};
#ifdef WIN32
cvar_t sys_colortranslation = {0, "sys_colortranslation", "0", "terminal console color translation (supported values: 0 = strip color codes, 1 = translate to ANSI codes, 2 = no translation)"};
#else
cvar_t sys_colortranslation = {0, "sys_colortranslation", "1", "terminal console color translation (supported values: 0 = strip color codes, 1 = translate to ANSI codes, 2 = no translation)"};
#endif


cvar_t con_nickcompletion = {CVAR_SAVE, "con_nickcompletion", "1", "tab-complete nicks in console and message input"};
cvar_t con_nickcompletion_flags = {CVAR_SAVE, "con_nickcompletion_flags", "11", "Bitfield: "
				   "0: add nothing after completion. "
				   "1: add the last color after completion. "
				   "2: add a quote when starting a quote instead of the color. "
				   "4: will replace 1, will force color, even after a quote. "
				   "8: ignore non-alphanumerics. "
				   "16: ignore spaces. "};
#define NICKS_ADD_COLOR 1
#define NICKS_ADD_QUOTE 2
#define NICKS_FORCE_COLOR 4
#define NICKS_ALPHANUMERICS_ONLY 8
#define NICKS_NO_SPACES 16

cvar_t con_completion_playdemo = {CVAR_SAVE, "con_completion_playdemo", "*.dem", "completion pattern for the playdemo command"};
cvar_t con_completion_timedemo = {CVAR_SAVE, "con_completion_timedemo", "*.dem", "completion pattern for the timedemo command"};
cvar_t con_completion_exec = {CVAR_SAVE, "con_completion_exec", "*.cfg", "completion pattern for the exec command"};

cvar_t condump_stripcolors = {CVAR_SAVE, "condump_stripcolors", "0", "strip color codes from console dumps"};

qboolean con_initialized;

// used for server replies to rcon command
lhnetsocket_t *rcon_redirect_sock = NULL;
lhnetaddress_t *rcon_redirect_dest = NULL;
int rcon_redirect_bufferpos = 0;
char rcon_redirect_buffer[1400];
qboolean rcon_redirect_proquakeprotocol = false;

// generic functions for console buffers

void ConBuffer_Init(conbuffer_t *buf, int textsize, int maxlines, mempool_t *mempool)
{
	buf->active = true;
	buf->textsize = textsize;
	buf->text = (char *) Mem_Alloc(mempool, textsize);
	buf->maxlines = maxlines;
	buf->lines = (con_lineinfo_t *) Mem_Alloc(mempool, maxlines * sizeof(*buf->lines));
	buf->lines_first = 0;
	buf->lines_count = 0;
}

/*! The translation table between the graphical font and plain ASCII  --KB */
static char qfont_table[256] = {
	'\0', '#',  '#',  '#',  '#',  '.',  '#',  '#',
	'#',  9,    10,   '#',  ' ',  13,   '.',  '.',
	'[',  ']',  '0',  '1',  '2',  '3',  '4',  '5',
	'6',  '7',  '8',  '9',  '.',  '<',  '=',  '>',
	' ',  '!',  '"',  '#',  '$',  '%',  '&',  '\'',
	'(',  ')',  '*',  '+',  ',',  '-',  '.',  '/',
	'0',  '1',  '2',  '3',  '4',  '5',  '6',  '7',
	'8',  '9',  ':',  ';',  '<',  '=',  '>',  '?',
	'@',  'A',  'B',  'C',  'D',  'E',  'F',  'G',
	'H',  'I',  'J',  'K',  'L',  'M',  'N',  'O',
	'P',  'Q',  'R',  'S',  'T',  'U',  'V',  'W',
	'X',  'Y',  'Z',  '[',  '\\', ']',  '^',  '_',
	'`',  'a',  'b',  'c',  'd',  'e',  'f',  'g',
	'h',  'i',  'j',  'k',  'l',  'm',  'n',  'o',
	'p',  'q',  'r',  's',  't',  'u',  'v',  'w',
	'x',  'y',  'z',  '{',  '|',  '}',  '~',  '<',

	'<',  '=',  '>',  '#',  '#',  '.',  '#',  '#',
	'#',  '#',  ' ',  '#',  ' ',  '>',  '.',  '.',
	'[',  ']',  '0',  '1',  '2',  '3',  '4',  '5',
	'6',  '7',  '8',  '9',  '.',  '<',  '=',  '>',
	' ',  '!',  '"',  '#',  '$',  '%',  '&',  '\'',
	'(',  ')',  '*',  '+',  ',',  '-',  '.',  '/',
	'0',  '1',  '2',  '3',  '4',  '5',  '6',  '7',
	'8',  '9',  ':',  ';',  '<',  '=',  '>',  '?',
	'@',  'A',  'B',  'C',  'D',  'E',  'F',  'G',
	'H',  'I',  'J',  'K',  'L',  'M',  'N',  'O',
	'P',  'Q',  'R',  'S',  'T',  'U',  'V',  'W',
	'X',  'Y',  'Z',  '[',  '\\', ']',  '^',  '_',
	'`',  'a',  'b',  'c',  'd',  'e',  'f',  'g',
	'h',  'i',  'j',  'k',  'l',  'm',  'n',  'o',
	'p',  'q',  'r',  's',  't',  'u',  'v',  'w',
	'x',  'y',  'z',  '{',  '|',  '}',  '~',  '<'
};

/*
	SanitizeString strips color tags from the string in
	and writes the result on string out
*/
void ConBuffer_SanitizeString(char *in, char *out)
{
	while(*in)
	{
		if(*in == STRING_COLOR_TAG)
		{
			++in;
			if(!*in)
			{
				out[0] = STRING_COLOR_TAG;
				out[1] = 0;
				return;
			}
			else if (*in >= '0' && *in <= '9') // ^[0-9] found
			{
				++in;
				if(!*in)
				{
					*out = 0;
					return;
				} else if (*in == STRING_COLOR_TAG) // ^[0-9]^ found, don't print ^[0-9]
					continue;
			}
			else if (*in == STRING_COLOR_RGB_TAG_CHAR) // ^x found
			{
				if ( isxdigit(in[1]) && isxdigit(in[2]) && isxdigit(in[3]) )
				{
					in+=4;
					if (!*in)
					{
						*out = 0;
						return;
					} else if (*in == STRING_COLOR_TAG) // ^xrgb^ found, don't print ^xrgb
						continue;
				}
				else in--;
			}
			else if (*in != STRING_COLOR_TAG)
				--in;
		}
		*out = qfont_table[*(unsigned char*)in];
		++in;
		++out;
	}
	*out = 0;
}

/*
================
ConBuffer_Clear
================
*/
void ConBuffer_Clear (conbuffer_t *buf)
{
	buf->lines_count = 0;
}

/*
================
ConBuffer_Shutdown
================
*/
void ConBuffer_Shutdown(conbuffer_t *buf)
{
	buf->active = false;
	if (buf->text)
		Mem_Free(buf->text);
	if (buf->lines)
		Mem_Free(buf->lines);
	buf->text = NULL;
	buf->lines = NULL;
}

/*
================
ConBuffer_FixTimes

Notifies the console code about the current time
(and shifts back times of other entries when the time
went backwards)
================
*/
void ConBuffer_FixTimes(conbuffer_t *buf)
{
#ifdef SERVER
	int i;
	if(buf->lines_count >= 1)
	{
		double diff = cl.time - CONBUFFER_LINES_LAST(buf).addtime;
		if(diff < 0)
		{
			for(i = 0; i < buf->lines_count; ++i)
				CONBUFFER_LINES(buf, i).addtime += diff;
		}
	}
#endif
}

/*
================
ConBuffer_DeleteLine

Deletes the first line from the console history.
================
*/
void ConBuffer_DeleteLine(conbuffer_t *buf)
{
	if(buf->lines_count == 0)
		return;
	--buf->lines_count;
	buf->lines_first = (buf->lines_first + 1) % buf->maxlines;
}

/*
================
ConBuffer_DeleteLastLine

Deletes the last line from the console history.
================
*/
void ConBuffer_DeleteLastLine(conbuffer_t *buf)
{
	if(buf->lines_count == 0)
		return;
	--buf->lines_count;
}

/*
================
ConBuffer_BytesLeft

Checks if there is space for a line of the given length, and if yes, returns a
pointer to the start of such a space, and NULL otherwise.
================
*/
static char *ConBuffer_BytesLeft(conbuffer_t *buf, int len)
{
	if(len > buf->textsize)
		return NULL;
	if(buf->lines_count == 0)
		return buf->text;
	else
	{
		char *firstline_start = buf->lines[buf->lines_first].start;
		char *lastline_onepastend = CONBUFFER_LINES_LAST(buf).start + CONBUFFER_LINES_LAST(buf).len;
		// the buffer is cyclic, so we first have two cases...
		if(firstline_start < lastline_onepastend) // buffer is contiguous
		{
			// put at end?
			if(len <= buf->text + buf->textsize - lastline_onepastend)
				return lastline_onepastend;
			// put at beginning?
			else if(len <= firstline_start - buf->text)
				return buf->text;
			else
				return NULL;
		}
		else // buffer has a contiguous hole
		{
			if(len <= firstline_start - lastline_onepastend)
				return lastline_onepastend;
			else
				return NULL;
		}
	}
}

/*
================
ConBuffer_AddLine

Appends a given string as a new line to the console.
================
*/
void ConBuffer_AddLine(conbuffer_t *buf, const char *line, int len, int mask)
{
#ifndef SERVER
	char *putpos;
	con_lineinfo_t *p;

	// developer_memory 1 during shutdown prints while conbuffer_t is being freed
	if (!buf->active)
		return;

	ConBuffer_FixTimes(buf);

	if(len >= buf->textsize)
	{
		// line too large?
		// only display end of line.
		line += len - buf->textsize + 1;
		len = buf->textsize - 1;
	}
	while(!(putpos = ConBuffer_BytesLeft(buf, len + 1)) || buf->lines_count >= buf->maxlines)
		ConBuffer_DeleteLine(buf);
	memcpy(putpos, line, len);
	putpos[len] = 0;
	++buf->lines_count;

	//fprintf(stderr, "Now have %d lines (%d -> %d).\n", buf->lines_count, buf->lines_first, CON_LINES_LAST);

	p = &CONBUFFER_LINES_LAST(buf);
	p->start = putpos;
	p->len = len;
	p->addtime = cl.time;
	p->mask = mask;
	p->height = -1; // calculate when needed
#endif
}

int ConBuffer_FindPrevLine(conbuffer_t *buf, int mask_must, int mask_mustnot, int start)
{
	int i;
	if(start == -1)
		start = buf->lines_count;
	for(i = start - 1; i >= 0; --i)
	{
		con_lineinfo_t *l = &CONBUFFER_LINES(buf, i);

		if((l->mask & mask_must) != mask_must)
			continue;
		if(l->mask & mask_mustnot)
			continue;

		return i;
	}

	return -1;
}

const char *ConBuffer_GetLine(conbuffer_t *buf, int i)
{
	static char copybuf[MAX_INPUTLINE]; // client only
	con_lineinfo_t *l = &CONBUFFER_LINES(buf, i);
	size_t sz = l->len+1 > sizeof(copybuf) ? sizeof(copybuf) : l->len+1;
	strlcpy(copybuf, l->start, sz);
	return copybuf;
}

/*
==============================================================================

LOGGING

==============================================================================
*/

/*
==============================================================================

CONSOLE

==============================================================================
*/

/*
================
Con_ToggleConsole_f
================
*/
void Con_ToggleConsole_f (void)
{
#ifndef SERVER
	if (Sys_CheckParm ("-noconsole"))
		if (!(key_consoleactive & KEY_CONSOLEACTIVE_USER))
			return; // only allow the key bind to turn off console

	// toggle the 'user wants console' bit
	key_consoleactive ^= KEY_CONSOLEACTIVE_USER;
	Con_ClearNotify();
#endif
}

/*
================
Con_ClearNotify
================
*/
void Con_ClearNotify (void)
{
	int i;
	for(i = 0; i < CON_LINES_COUNT; ++i)
		if(!(CON_LINES(i).mask & CON_MASK_CHAT))
			CON_LINES(i).mask |= CON_MASK_HIDENOTIFY;
}

//[515]: the simplest command ever
//LordHavoc: not so simple after I made it print usage...
static void Con_Maps_f (void)
{
	if (Cmd_Argc() > 2)
	{
		Con_Printf("usage: maps [mapnameprefix]\n");
		return;
	}
	else if (Cmd_Argc() == 2)
		GetMapList(Cmd_Argv(1), NULL, 0);
	else
		GetMapList("", NULL, 0);
}

static void Con_ConDump_f (void)
{
	int i;
	qfile_t *file;
	if (Cmd_Argc() != 2)
	{
		Con_Printf("usage: condump <filename>\n");
		return;
	}
	file = FS_OpenRealFile(Cmd_Argv(1), "w", false);
	if (!file)
	{
		Con_Printf("condump: unable to write file \"%s\"\n", Cmd_Argv(1));
		return;
	}
	if (con_mutex) Thread_LockMutex(con_mutex);
	for(i = 0; i < CON_LINES_COUNT; ++i)
	{
		if (condump_stripcolors.integer)
		{
			// sanitize msg
			size_t len = CON_LINES(i).len;
			char* sanitizedmsg = (char*)Mem_Alloc(tempmempool, len + 1);
			memcpy (sanitizedmsg, CON_LINES(i).start, len);
			ConBuffer_SanitizeString(sanitizedmsg, sanitizedmsg); // SanitizeString's in pointer is always ahead of the out pointer, so this should work.
			FS_Write(file, sanitizedmsg, strlen(sanitizedmsg));
			Mem_Free(sanitizedmsg);
		}
		else 
		{
			FS_Write(file, CON_LINES(i).start, CON_LINES(i).len);
		}
		FS_Write(file, "\n", 1);
	}
	if (con_mutex) Thread_UnlockMutex(con_mutex);
	FS_Close(file);
}

void Con_Clear_f (void)
{
	if (con_mutex) Thread_LockMutex(con_mutex);
	ConBuffer_Clear(&con);
	if (con_mutex) Thread_UnlockMutex(con_mutex);
}

void Con_Init_Commands (void)
{
	Cvar_RegisterVariable (&sys_colortranslation);
	Cvar_RegisterVariable (&sys_specialcharactertranslation);

	Con_Log_Init();

	// --blub
	Cvar_RegisterVariable (&con_nickcompletion);
	Cvar_RegisterVariable (&con_nickcompletion_flags);

	Cvar_RegisterVariable (&con_completion_playdemo); // *.dem
	Cvar_RegisterVariable (&con_completion_timedemo); // *.dem
	Cvar_RegisterVariable (&con_completion_exec); // *.cfg

	Cvar_RegisterVariable (&condump_stripcolors);

	// register our commands
	Cmd_AddCommand (0, "clear", Con_Clear_f, "clear console history");
	Cmd_AddCommand (0, "maps", Con_Maps_f, "list information about available maps");
	Cmd_AddCommand (0, "condump", Con_ConDump_f, "output console history to a file (see also log_file)");
}

/*
================
Con_Init
================
*/
void Con_Init (void)
{
#ifndef SERVER
	con_linewidth = 80;
#endif
	ConBuffer_Init(&con, CON_TEXTSIZE, CON_MAXLINES, zonemempool);
	if (Thread_HasThreads())
		con_mutex = Thread_CreateMutex();

	Con_Log_Init();

	Con_Init_Commands();

	con_initialized = true;

	const char *os = DP_OS_NAME;
	dpsnprintf (engineversion, sizeof (engineversion), "Horsepower %s %s", os, buildstring);
	Con_Printf("%s\n\n", engineversion);
	Con_Printf("Copyright (C) 1996-1998 Id Software, Inc\nCopyright (C) 2002-2019 LordHavoc, divVerent, and contributors\nCopyright (C) 2018-2019 Cpt. David\n\n");
	Con_Printf("Horsepower is free software, distributed under the\nGNU General Public License version 2, found in LICENSE.txt\n\n");
	Con_Printf("Horsepower comes with ABSOLUTELY NO WARRANTY,\nto the extent permitted by applicable law.\n\n");

	Con_Printf("<===============================================>\n\n");
	Con_Printf("Console initialized\n");
}

void Con_Shutdown (void)
{
	if (con_mutex) Thread_LockMutex(con_mutex);
	ConBuffer_Shutdown(&con);
	if (con_mutex) Thread_UnlockMutex(con_mutex);
	if (con_mutex) Thread_DestroyMutex(con_mutex);con_mutex = NULL;
}

/*
================
Con_PrintToHistory

// TODO: This is a clientside function.

Handles cursor positioning, line wrapping, etc
All console printing must go through this in order to be displayed
If no console is visible, the notify window will pop up.
================
*/
static void Con_PrintToHistory(const char *txt, int mask)
{
	// process:
	//   \n goes to next line
	//   \r deletes current line and makes a new one

	static int cr_pending = 0;
	static char buf[CON_TEXTSIZE]; // con_mutex
	static int bufpos = 0;

	if(!con.text) // FIXME uses a non-abstracted property of con
		return;

	for(; *txt; ++txt)
	{
		if(cr_pending)
		{
			ConBuffer_DeleteLastLine(&con);
			cr_pending = 0;
		}
		switch(*txt)
		{
			case 0:
				break;
			case '\r':
				ConBuffer_AddLine(&con, buf, bufpos, mask);
				bufpos = 0;
				cr_pending = 1;
				break;
			case '\n':
				ConBuffer_AddLine(&con, buf, bufpos, mask);
				bufpos = 0;
				break;
			default:
				buf[bufpos++] = *txt;
				if(bufpos >= con.textsize - 1) // FIXME uses a non-abstracted property of con
				{
					ConBuffer_AddLine(&con, buf, bufpos, mask);
					bufpos = 0;
				}
				break;
		}
	}
}

void Con_Rcon_Redirect_Init(lhnetsocket_t *sock, lhnetaddress_t *dest, qboolean proquakeprotocol)
{
	rcon_redirect_sock = sock;
	rcon_redirect_dest = dest;
	rcon_redirect_proquakeprotocol = proquakeprotocol;
	if (rcon_redirect_proquakeprotocol)
	{
		// reserve space for the packet header
		rcon_redirect_buffer[0] = 0;
		rcon_redirect_buffer[1] = 0;
		rcon_redirect_buffer[2] = 0;
		rcon_redirect_buffer[3] = 0;
		// this is a reply to a CCREQ_RCON
		rcon_redirect_buffer[4] = (unsigned char)CCREP_RCON;
	}
	else
		memcpy(rcon_redirect_buffer, "\377\377\377\377n", 5); // QW rcon print
	rcon_redirect_bufferpos = 5;
}

static void Con_Rcon_Redirect_Flush(void)
{
	if(rcon_redirect_sock)
	{
		rcon_redirect_buffer[rcon_redirect_bufferpos] = 0;
		if (rcon_redirect_proquakeprotocol)
		{
			// update the length in the packet header
			StoreBigLong((unsigned char *)rcon_redirect_buffer, NETFLAG_CTL | (rcon_redirect_bufferpos & NETFLAG_LENGTH_MASK));
		}
		NetConn_Write(rcon_redirect_sock, rcon_redirect_buffer, rcon_redirect_bufferpos, rcon_redirect_dest);
	}
	memcpy(rcon_redirect_buffer, "\377\377\377\377n", 5); // QW rcon print
	rcon_redirect_bufferpos = 5;
	rcon_redirect_proquakeprotocol = false;
}

void Con_Rcon_Redirect_End(void)
{
	Con_Rcon_Redirect_Flush();
	rcon_redirect_dest = NULL;
	rcon_redirect_sock = NULL;
}

void Con_Rcon_Redirect_Abort(void)
{
	rcon_redirect_dest = NULL;
	rcon_redirect_sock = NULL;
}

/*
================
Con_Rcon_AddChar
================
*/
/// Adds a character to the rcon buffer.
static void Con_Rcon_AddChar(int c)
{
	if(log_dest_buffer_appending)
		return;
	++log_dest_buffer_appending;

	// if this print is in response to an rcon command, add the character
	// to the rcon redirect buffer

	if (rcon_redirect_dest)
	{
		rcon_redirect_buffer[rcon_redirect_bufferpos++] = c;
		if(rcon_redirect_bufferpos >= (int)sizeof(rcon_redirect_buffer) - 1)
			Con_Rcon_Redirect_Flush();
	}
	else if(*log_dest_udp.string) // don't duplicate rcon command responses here, these are sent another way
	{
		if(log_dest_buffer_pos == 0)
			Log_DestBuffer_Init();
		log_dest_buffer[log_dest_buffer_pos++] = c;
		if(log_dest_buffer_pos >= sizeof(log_dest_buffer) - 1) // minus one, to allow for terminating zero
			Log_DestBuffer_Flush_NoLock();
	}
	else
		log_dest_buffer_pos = 0;

	--log_dest_buffer_appending;
}

/**
 * Convert an RGB color to its nearest quake color.
 * I'll cheat on this a bit by translating the colors to HSV first,
 * S and V decide if it's black or white, otherwise, H will decide the
 * actual color.
 * @param _r Red (0-255)
 * @param _g Green (0-255)
 * @param _b Blue (0-255)
 * @return A quake color character.
 */
static char Sys_Con_NearestColor(const unsigned char _r, const unsigned char _g, const unsigned char _b)
{
	float r = ((float)_r)/255.0;
	float g = ((float)_g)/255.0;
	float b = ((float)_b)/255.0;
	float min = min(r, min(g, b));
	float max = max(r, max(g, b));

	int h; ///< Hue angle [0,360]
	float s; ///< Saturation [0,1]
	float v = max; ///< In HSV v == max [0,1]

	if(max == min)
		s = 0;
	else
		s = 1.0 - (min/max);

	// Saturation threshold. We now say 0.2 is the minimum value for a color!
	if(s < 0.2)
	{
		// If the value is less than half, return a black color code.
		// Otherwise return a white one.
		if(v < 0.5)
			return '0';
		return '7';
	}

	// Let's get the hue angle to define some colors:
	if(max == min)
		h = 0;
	else if(max == r)
		h = (int)(60.0 * (g-b)/(max-min))%360;
	else if(max == g)
		h = (int)(60.0 * (b-r)/(max-min) + 120);
	else // if(max == b) redundant check
		h = (int)(60.0 * (r-g)/(max-min) + 240);

	if(h < 36) // *red* to orange
		return '1';
	else if(h < 80) // orange over *yellow* to evilish-bright-green
		return '3';
	else if(h < 150) // evilish-bright-green over *green* to ugly bright blue
		return '2';
	else if(h < 200) // ugly bright blue over *bright blue* to darkish blue
		return '5';
	else if(h < 270) // darkish blue over *dark blue* to cool purple
		return '4';
	else if(h < 330) // cool purple over *purple* to ugly swiny red
		return '6';
	else // ugly red to red closes the circly
		return '1';
}

/*
================
Con_MaskPrint
================
*/
extern cvar_t timestamps;
extern cvar_t timeformat;
extern qboolean sys_nostdout;
void Con_MaskPrint(int additionalmask, const char *msg)
{
	static int mask = 0;
	static int index = 0;
	static char line[MAX_INPUTLINE];

	if (con_mutex)
		Thread_LockMutex(con_mutex);

	for (;*msg;msg++)
	{
		Con_Rcon_AddChar(*msg);
		// if this is the beginning of a new line, print timestamp
		if (index == 0)
		{
			const char *timestamp = timestamps.integer ? Sys_TimeString(timeformat.string) : "";
			// reset the color
			// FIXME: 1. perhaps we should use a terminal system 2. use a constant instead of 7!
			line[index++] = STRING_COLOR_TAG;
			// assert( STRING_COLOR_DEFAULT < 10 )
			line[index++] = STRING_COLOR_DEFAULT + '0';
			// special color codes for chat messages must always come first
			// for Con_PrintToHistory to work properly
			if (*msg == 1 || *msg == 2 || *msg == 3)
			{
				// play talk wav
				/*if (*msg == 1)
				{
					if (con_chatsound.value)
					{
						if (msg[1] == '(' || msg[1] == '\r')
						{
							if (cl.foundtalk2wav)
							{
								S_LocalSound ("sound/misc/talk2.wav");
							}
						}
						else
						{
							S_LocalSound ("sound/misc/talk.wav");
						}
					}
				} TODO: no chat sounds until done*/
				
				// Send to chatbox for say/tell (1) and messages (3)
				// 3 is just so that a message can be sent to the chatbox without a sound.
				if (*msg == 1 || *msg == 3)
					mask = CON_MASK_CHAT;
				
				line[index++] = STRING_COLOR_TAG;
				line[index++] = '3';
				msg++;
				Con_Rcon_AddChar(*msg);
			}
			// store timestamp
			for (;*timestamp;index++, timestamp++)
				if (index < (int)sizeof(line) - 2)
					line[index] = *timestamp;
			// add the mask
			mask |= additionalmask;
		}
		// append the character
		line[index++] = *msg;
		// if this is a newline character, we have a complete line to print
		if (*msg == '\n' || index >= (int)sizeof(line) / 2)
		{
			// terminate the line
			line[index] = 0;
			// send to log file
			Log_ConPrint(line);
			// send to scrollable buffer
			if (con_initialized && !(host.state & H_DEDICATED))
			{
				Con_PrintToHistory(line, mask);
			}
			// send to terminal or dedicated server window
			if (!sys_nostdout)
			if (developer.integer || !(mask & CON_MASK_DEVELOPER))
			{
				if(sys_specialcharactertranslation.integer)
				{
					char *p;
					const char *q;
					p = line;
					while(*p)
					{
						int ch = u8_getchar(p, &q);
						if(ch >= 0xE000 && ch <= 0xE0FF && ((unsigned char) qfont_table[ch - 0xE000]) >= 0x20)
						{
							*p = qfont_table[ch - 0xE000];
							if(q > p+1)
								memmove(p+1, q, strlen(q)+1);
							p = p + 1;
						}
						else
							p = p + (q - p);
					}
				}

				if(sys_colortranslation.integer == 1) // ANSI
				{
					static char printline[MAX_INPUTLINE * 4 + 3];
						// 2 can become 7 bytes, rounding that up to 8, and 3 bytes are added at the end
						// a newline can transform into four bytes, but then prevents the three extra bytes from appearing
					int lastcolor = 0;
					const char *in;
					char *out;
					int color;
					for(in = line, out = printline; *in; ++in)
					{
						switch(*in)
						{
							case STRING_COLOR_TAG:
								if( in[1] == STRING_COLOR_RGB_TAG_CHAR && isxdigit(in[2]) && isxdigit(in[3]) && isxdigit(in[4]) )
								{
									char r = tolower(in[2]);
									char g = tolower(in[3]);
									char b = tolower(in[4]);
									// it's a hex digit already, so the else part needs no check --blub
									if(isdigit(r)) r -= '0';
									else r -= 87;
									if(isdigit(g)) g -= '0';
									else g -= 87;
									if(isdigit(b)) b -= '0';
									else b -= 87;
									
									color = Sys_Con_NearestColor(r * 17, g * 17, b * 17);
									in += 3; // 3 only, the switch down there does the fourth
								}
								else
									color = in[1];
								
								switch(color)
								{
									case STRING_COLOR_TAG:
										++in;
										*out++ = STRING_COLOR_TAG;
										break;
									case '0':
									case '7':
										// normal color
										++in;
										if(lastcolor == 0) break; else lastcolor = 0;
										*out++ = 0x1B; *out++ = '['; *out++ = 'm';
										break;
									case '1':
										// light red
										++in;
										if(lastcolor == 1) break; else lastcolor = 1;
										*out++ = 0x1B; *out++ = '['; *out++ = '1'; *out++ = ';'; *out++ = '3'; *out++ = '1'; *out++ = 'm';
										break;
									case '2':
										// light green
										++in;
										if(lastcolor == 2) break; else lastcolor = 2;
										*out++ = 0x1B; *out++ = '['; *out++ = '1'; *out++ = ';'; *out++ = '3'; *out++ = '2'; *out++ = 'm';
										break;
									case '3':
										// yellow
										++in;
										if(lastcolor == 3) break; else lastcolor = 3;
										*out++ = 0x1B; *out++ = '['; *out++ = '1'; *out++ = ';'; *out++ = '3'; *out++ = '3'; *out++ = 'm';
										break;
									case '4':
										// light blue
										++in;
										if(lastcolor == 4) break; else lastcolor = 4;
										*out++ = 0x1B; *out++ = '['; *out++ = '1'; *out++ = ';'; *out++ = '3'; *out++ = '4'; *out++ = 'm';
										break;
									case '5':
										// light cyan
										++in;
										if(lastcolor == 5) break; else lastcolor = 5;
										*out++ = 0x1B; *out++ = '['; *out++ = '1'; *out++ = ';'; *out++ = '3'; *out++ = '6'; *out++ = 'm';
										break;
									case '6':
										// light magenta
										++in;
										if(lastcolor == 6) break; else lastcolor = 6;
										*out++ = 0x1B; *out++ = '['; *out++ = '1'; *out++ = ';'; *out++ = '3'; *out++ = '5'; *out++ = 'm';
										break;
									// 7 handled above
									case '8':
									case '9':
										// bold normal color
										++in;
										if(lastcolor == 8) break; else lastcolor = 8;
										*out++ = 0x1B; *out++ = '['; *out++ = '0'; *out++ = ';'; *out++ = '1'; *out++ = 'm';
										break;
									default:
										*out++ = STRING_COLOR_TAG;
										break;
								}
								break;
							case '\n':
								if(lastcolor != 0)
								{
									*out++ = 0x1B; *out++ = '['; *out++ = 'm';
									lastcolor = 0;
								}
								*out++ = *in;
								break;
							default:
								*out++ = *in;
								break;
						}
					}
					if(lastcolor != 0)
					{
						*out++ = 0x1B;
						*out++ = '[';
						*out++ = 'm';
					}
					*out++ = 0;
					Sys_PrintToTerminal(printline);
				}
				else if(sys_colortranslation.integer == 2) // Quake
				{
					Sys_PrintToTerminal(line);
				}
				else // strip
				{
					static char printline[MAX_INPUTLINE]; // it can only get shorter here
					const char *in;
					char *out;
					for(in = line, out = printline; *in; ++in)
					{
						switch(*in)
						{
							case STRING_COLOR_TAG:
								switch(in[1])
								{
									case STRING_COLOR_RGB_TAG_CHAR:
										if ( isxdigit(in[2]) && isxdigit(in[3]) && isxdigit(in[4]) )
										{
											in+=4;
											break;
										}
										*out++ = STRING_COLOR_TAG;
										*out++ = STRING_COLOR_RGB_TAG_CHAR;
										++in;
										break;
									case STRING_COLOR_TAG:
										++in;
										*out++ = STRING_COLOR_TAG;
										break;
									case '0':
									case '1':
									case '2':
									case '3':
									case '4':
									case '5':
									case '6':
									case '7':
									case '8':
									case '9':
										++in;
										break;
									default:
										*out++ = STRING_COLOR_TAG;
										break;
								}
								break;
							default:
								*out++ = *in;
								break;
						}
					}
					*out++ = 0;
					Sys_PrintToTerminal(printline);
				}
			}
			// empty the line buffer
			index = 0;
			mask = 0;
		}
	}

	if (con_mutex)
		Thread_UnlockMutex(con_mutex);
}

/*
================
Con_Print
================
*/
void Con_Print(const char *msg)
{
	Con_MaskPrint(CON_MASK_PRINT, msg);
}

/*
================
Con_Printf
================
*/
void Con_Printf(const char *fmt, ...)
{
	va_list argptr;
	char msg[MAX_INPUTLINE];

	va_start(argptr,fmt);
	dpvsnprintf(msg,sizeof(msg),fmt,argptr);
	va_end(argptr);

	Con_MaskPrint(CON_MASK_PRINT, msg);
}

/*
================
Con_DPrint
================
*/
void Con_DPrint(const char *msg)
{
	if(developer.integer < 0) // at 0, we still add to the buffer but hide
		return;

	Con_MaskPrint(CON_MASK_DEVELOPER, msg);
}

/*
================
Con_DPrintf
================
*/
void Con_DPrintf(const char *fmt, ...)
{
	va_list argptr;
	char msg[MAX_INPUTLINE];

	if(developer.integer < 0) // at 0, we still add to the buffer but hide
		return;

	va_start(argptr,fmt);
	dpvsnprintf(msg,sizeof(msg),fmt,argptr);
	va_end(argptr);

	Con_MaskPrint(CON_MASK_DEVELOPER, msg);
}


/*
==============================================================================

DRAWING

==============================================================================
*/

/*
GetMapList

Made by [515]
Prints not only map filename, but also
its format (q1/q2/q3/hl) and even its message
*/
//[515]: here is an ugly hack.. two gotos... oh my... *but it works*
//LordHavoc: rewrote bsp type detection, rewrote message extraction to do proper worldspawn parsing
//LordHavoc: added .ent file loading, and redesigned error handling to still try the .ent file even if the map format is not recognized, this also eliminated one goto
//LordHavoc: FIXME: man this GetMapList is STILL ugly code even after my cleanups...
qboolean GetMapList (const char *s, char *completedname, int completednamebufferlength)
{
	fssearch_t	*t;
	char		message[1024];
	int			i, k, max, p, o, min;
	unsigned char *len;
	qfile_t		*f;
	unsigned char buf[1024];

	dpsnprintf(message, sizeof(message), "maps/%s*.bsp", s);
	t = FS_Search(message, 1, true);
	if(!t)
		return false;
	if (t->numfilenames > 1)
		Con_Printf("^1 %i maps found :\n", t->numfilenames);
	len = (unsigned char *)Z_Malloc(t->numfilenames);
	min = 666;
	for(max=i=0;i<t->numfilenames;i++)
	{
		k = (int)strlen(t->filenames[i]);
		k -= 9;
		if(max < k)
			max = k;
		else
		if(min > k)
			min = k;
		len[i] = k;
	}
	o = (int)strlen(s);
	for(i=0;i<t->numfilenames;i++)
	{
		int lumpofs = 0, lumplen = 0;
		char *entities = NULL;
		const char *data = NULL;
		char keyname[64];
		char entfilename[MAX_QPATH];
		char desc[64];
		desc[0] = 0;
		strlcpy(message, "^1ERROR: open failed^7", sizeof(message));
		p = 0;
		f = FS_OpenVirtualFile(t->filenames[i], true);
		if(f)
		{
			strlcpy(message, "^1ERROR: not a known map format^7", sizeof(message));
			memset(buf, 0, 1024);
			FS_Read(f, buf, 1024);
			if (!memcmp(buf, "IBSP", 4))
			{
				p = LittleLong(((int *)buf)[1]);
				if (p == Q3BSPVERSION)
				{
					q3dheader_t *header = (q3dheader_t *)buf;
					lumpofs = LittleLong(header->lumps[Q3LUMP_ENTITIES].fileofs);
					lumplen = LittleLong(header->lumps[Q3LUMP_ENTITIES].filelen);
					dpsnprintf(desc, sizeof(desc), "Q3BSP%i", p);
				}
				else if (p == Q2BSPVERSION)
				{
					q2dheader_t *header = (q2dheader_t *)buf;
					lumpofs = LittleLong(header->lumps[Q2LUMP_ENTITIES].fileofs);
					lumplen = LittleLong(header->lumps[Q2LUMP_ENTITIES].filelen);
					dpsnprintf(desc, sizeof(desc), "Q2BSP%i", p);
				}
				else
					dpsnprintf(desc, sizeof(desc), "IBSP%i", p);
			}
			else if (BuffLittleLong(buf) == BSPVERSION)
			{
				lumpofs = BuffLittleLong(buf + 4 + 8 * LUMP_ENTITIES);
				lumplen = BuffLittleLong(buf + 4 + 8 * LUMP_ENTITIES + 4);
				dpsnprintf(desc, sizeof(desc), "BSP29");
			}
			else if (BuffLittleLong(buf) == 30)
			{
				lumpofs = BuffLittleLong(buf + 4 + 8 * LUMP_ENTITIES);
				lumplen = BuffLittleLong(buf + 4 + 8 * LUMP_ENTITIES + 4);
				dpsnprintf(desc, sizeof(desc), "BSPHL");
			}
			else if (!memcmp(buf, "BSP2", 4))
			{
				lumpofs = BuffLittleLong(buf + 4 + 8 * LUMP_ENTITIES);
				lumplen = BuffLittleLong(buf + 4 + 8 * LUMP_ENTITIES + 4);
				dpsnprintf(desc, sizeof(desc), "BSP2");
			}
			else if (!memcmp(buf, "2PSB", 4))
			{
				lumpofs = BuffLittleLong(buf + 4 + 8 * LUMP_ENTITIES);
				lumplen = BuffLittleLong(buf + 4 + 8 * LUMP_ENTITIES + 4);
				dpsnprintf(desc, sizeof(desc), "BSP2RMQe");
			}
			else
			{
				dpsnprintf(desc, sizeof(desc), "unknown%i", BuffLittleLong(buf));
			}
			strlcpy(entfilename, t->filenames[i], sizeof(entfilename));
			memcpy(entfilename + strlen(entfilename) - 4, ".ent", 5);
			entities = (char *)FS_LoadFile(entfilename, tempmempool, true, NULL);
			if (!entities && lumplen >= 10)
			{
				FS_Seek(f, lumpofs, SEEK_SET);
				entities = (char *)Z_Malloc(lumplen + 1);
				FS_Read(f, entities, lumplen);
			}
			if (entities)
			{
				// if there are entities to parse, a missing message key just
				// means there is no title, so clear the message string now
				message[0] = 0;
				data = entities;
				for (;;)
				{
					int l;
					if (!COM_ParseToken_Simple(&data, false, false, true))
						break;
					if (com_token[0] == '{')
						continue;
					if (com_token[0] == '}')
						break;
					// skip leading whitespace
					for (k = 0;com_token[k] && ISWHITESPACE(com_token[k]);k++);
					for (l = 0;l < (int)sizeof(keyname) - 1 && com_token[k+l] && !ISWHITESPACE(com_token[k+l]);l++)
						keyname[l] = com_token[k+l];
					keyname[l] = 0;
					if (!COM_ParseToken_Simple(&data, false, false, true))
						break;
					if (developer_extra.integer)
						Con_DPrintf("key: %s %s\n", keyname, com_token);
					if (!strcmp(keyname, "message"))
					{
						// get the message contents
						strlcpy(message, com_token, sizeof(message));
						break;
					}
				}
			}
		}
		if (entities)
			Z_Free(entities);
		if(f)
			FS_Close(f);
		*(t->filenames[i]+len[i]+5) = 0;
		Con_Printf("%16s (%-8s) %s\n", t->filenames[i]+5, desc, message);
	}
	Con_Print("\n");
	for(p=o;p<min;p++)
	{
		k = *(t->filenames[0]+5+p);
		if(k == 0)
			goto endcomplete;
		for(i=1;i<t->numfilenames;i++)
			if(*(t->filenames[i]+5+p) != k)
				goto endcomplete;
	}
endcomplete:
	if(p > o && completedname && completednamebufferlength > 0)
	{
		memset(completedname, 0, completednamebufferlength);
		memcpy(completedname, (t->filenames[0]+5), min(p, completednamebufferlength - 1));
	}
	Z_Free(len);
	FS_FreeSearch(t);
	return p > o;
}

/*
	Con_DisplayList

	New function for tab-completion system
	Added by EvilTypeGuy
	MEGA Thanks to Taniwha

*/
void Con_DisplayList(const char **list)
{
	int i = 0, pos = 0, len = 0, maxlen = 0, width = (con_linewidth - 4);
	const char **walk = list;

	while (*walk) {
		len = (int)strlen(*walk);
		if (len > maxlen)
			maxlen = len;
		walk++;
	}
	maxlen += 1;

	while (*list) {
		len = (int)strlen(*list);
		if (pos + maxlen >= width) {
			Con_Print("\n");
			pos = 0;
		}

		Con_Print(*list);
		for (i = 0; i < (maxlen - len); i++)
			Con_Print(" ");

		pos += maxlen;
		list++;
	}

	if (pos)
		Con_Print("\n\n");
}


// Now it becomes TRICKY :D --blub
static char Nicks_list[MAX_SCOREBOARD][MAX_SCOREBOARDNAME];	// contains the nicks with colors and all that
static char Nicks_sanlist[MAX_SCOREBOARD][MAX_SCOREBOARDNAME];	// sanitized list for completion when there are other possible matches.
// means: when somebody uses a cvar's name as his name, we won't ever get his colors in there...
static int Nicks_offset[MAX_SCOREBOARD]; // when nicks use a space, we need this to move the completion list string starts to avoid invalid memcpys
static int Nicks_matchpos;

// co against <<:BLASTER:>> is true!?
static int Nicks_strncasecmp_nospaces(char *a, char *b, unsigned int a_len)
{
	while(a_len)
	{
		if(tolower(*a) == tolower(*b))
		{
			if(*a == 0)
				return 0;
			--a_len;
			++a;
			++b;
			continue;
		}
		if(!*a)
			return -1;
		if(!*b)
			return 1;
		if(*a == ' ')
			return (*a < *b) ? -1 : 1;
		if(*b == ' ')
			++b;
		else
			return (*a < *b) ? -1 : 1;
	}
	return 0;
}
static int Nicks_strncasecmp(char *a, char *b, unsigned int a_len)
{
#ifndef SERVER
	char space_char;
	if(!(con_nickcompletion_flags.integer & NICKS_ALPHANUMERICS_ONLY))
	{
		if(con_nickcompletion_flags.integer & NICKS_NO_SPACES)
			return Nicks_strncasecmp_nospaces(a, b, a_len);
		return strncasecmp(a, b, a_len);
	}

	space_char = (con_nickcompletion_flags.integer & NICKS_NO_SPACES) ? 'a' : ' ';

	// ignore non alphanumerics of B
	// if A contains a non-alphanumeric, B must contain it as well though!
	while(a_len)
	{
		qboolean alnum_a, alnum_b;

		if(tolower(*a) == tolower(*b))
		{
			if(*a == 0) // end of both strings, they're equal
				return 0;
			--a_len;
			++a;
			++b;
			continue;
		}
		// not equal, end of one string?
		if(!*a)
			return -1;
		if(!*b)
			return 1;
		// ignore non alphanumerics
		alnum_a = ( (*a >= 'a' && *a <= 'z') || (*a >= 'A' && *a <= 'Z') || (*a >= '0' && *a <= '9') || *a == space_char);
		alnum_b = ( (*b >= 'a' && *b <= 'z') || (*b >= 'A' && *b <= 'Z') || (*b >= '0' && *b <= '9') || *b == space_char);
		if(!alnum_a) // b must contain this
			return (*a < *b) ? -1 : 1;
		if(!alnum_b)
			++b;
		// otherwise, both are alnum, they're just not equal, return the appropriate number
		else
			return (*a < *b) ? -1 : 1;
	}
	return 0;
#endif
}


/* Nicks_CompleteCountPossible

   Count the number of possible nicks to complete
 */
static int Nicks_CompleteCountPossible(char *line, int pos, char *s, qboolean isCon)
{
#ifndef SERVER
	char name[128];
	int i, p;
	int match;
	int spos;
	int count = 0;

	if(!con_nickcompletion.integer)
		return 0;

	// changed that to 1
	if(!line[0])// || !line[1]) // we want at least... 2 written characters
		return 0;

	for(i = 0; i < cl.maxclients; ++i)
	{
		p = i;
		if(!cl.scores[p].name[0])
			continue;

		ConBuffer_SanitizeString(cl.scores[p].name, name);
		//Con_Printf(" ^2Sanitized: ^7%s -> %s", cl.scores[p].name, name);

		if(!name[0])
			continue;

		match = -1;
		spos = pos - 1; // no need for a minimum of characters :)

		while(spos >= 0)
		{
			if(spos > 0 && line[spos-1] != ' ' && line[spos-1] != ';' && line[spos-1] != '\"' && line[spos-1] != '\'')
			{
				if(!(isCon && line[spos-1] == ']' && spos == 1) && // console start
				   !(spos > 1 && line[spos-1] >= '0' && line[spos-1] <= '9' && line[spos-2] == STRING_COLOR_TAG)) // color start
				{
					--spos;
					continue;
				}
			}
			if(isCon && spos == 0)
				break;
			if(Nicks_strncasecmp(line+spos, name, pos-spos) == 0)
				match = spos;
			--spos;
		}
		if(match < 0)
			continue;
		//Con_Printf("Possible match: %s|%s\n", cl.scores[p].name, name);
		strlcpy(Nicks_list[count], cl.scores[p].name, sizeof(Nicks_list[count]));

		// the sanitized list
		strlcpy(Nicks_sanlist[count], name, sizeof(Nicks_sanlist[count]));
		if(!count)
		{
			Nicks_matchpos = match;
		}

		Nicks_offset[count] = s - (&line[match]);
		//Con_Printf("offset for %s: %i\n", name, Nicks_offset[count]);

		++count;
	}
	return count;
#endif
}

static void Cmd_CompleteNicksPrint(int count)
{
	int i;
	for(i = 0; i < count; ++i)
		Con_Printf("%s\n", Nicks_list[i]);
}

static void Nicks_CutMatchesNormal(int count)
{
	// cut match 0 down to the longest possible completion
	int i;
	unsigned int c, l;
	c = (unsigned int)strlen(Nicks_sanlist[0]) - 1;
	for(i = 1; i < count; ++i)
	{
		l = (unsigned int)strlen(Nicks_sanlist[i]) - 1;
		if(l < c)
			c = l;

		for(l = 0; l <= c; ++l)
			if(tolower(Nicks_sanlist[0][l]) != tolower(Nicks_sanlist[i][l]))
			{
				c = l-1;
				break;
			}
	}
	Nicks_sanlist[0][c+1] = 0;
	//Con_Printf("List0: %s\n", Nicks_sanlist[0]);
}

static unsigned int Nicks_strcleanlen(const char *s)
{
	unsigned int l = 0;
	while(*s)
	{
		if( (*s >= 'a' && *s <= 'z') ||
		    (*s >= 'A' && *s <= 'Z') ||
		    (*s >= '0' && *s <= '9') ||
		    *s == ' ')
			++l;
		++s;
	}
	return l;
}

static void Nicks_CutMatchesAlphaNumeric(int count)
{
	// cut match 0 down to the longest possible completion
	int i;
	unsigned int c, l;
	char tempstr[sizeof(Nicks_sanlist[0])];
	char *a, *b;
	char space_char = (con_nickcompletion_flags.integer & NICKS_NO_SPACES) ? 'a' : ' '; // yes this is correct, we want NO spaces when no spaces

	c = (unsigned int)strlen(Nicks_sanlist[0]);
	for(i = 0, l = 0; i < (int)c; ++i)
	{
		if( (Nicks_sanlist[0][i] >= 'a' && Nicks_sanlist[0][i] <= 'z') ||
		    (Nicks_sanlist[0][i] >= 'A' && Nicks_sanlist[0][i] <= 'Z') ||
		    (Nicks_sanlist[0][i] >= '0' && Nicks_sanlist[0][i] <= '9') || Nicks_sanlist[0][i] == space_char) // this is what's COPIED
		{
			tempstr[l++] = Nicks_sanlist[0][i];
		}
	}
	tempstr[l] = 0;

	for(i = 1; i < count; ++i)
	{
		a = tempstr;
		b = Nicks_sanlist[i];
		while(1)
		{
			if(!*a)
				break;
			if(!*b)
			{
				*a = 0;
				break;
			}
			if(tolower(*a) == tolower(*b))
			{
				++a;
				++b;
				continue;
			}
			if( (*b >= 'a' && *b <= 'z') || (*b >= 'A' && *b <= 'Z') || (*b >= '0' && *b <= '9') || *b == space_char)
			{
				// b is alnum, so cut
				*a = 0;
				break;
			}
			++b;
		}
	}
	// Just so you know, if cutmatchesnormal doesn't kill the first entry, then even the non-alnums fit
	Nicks_CutMatchesNormal(count);
	//if(!Nicks_sanlist[0][0])
	if(Nicks_strcleanlen(Nicks_sanlist[0]) < strlen(tempstr))
	{
		// if the clean sanitized one is longer than the current one, use it, it has crap chars which definitely are in there
		strlcpy(Nicks_sanlist[0], tempstr, sizeof(Nicks_sanlist[0]));
	}
}

static void Nicks_CutMatchesNoSpaces(int count)
{
	// cut match 0 down to the longest possible completion
	int i;
	unsigned int c, l;
	char tempstr[sizeof(Nicks_sanlist[0])];
	char *a, *b;

	c = (unsigned int)strlen(Nicks_sanlist[0]);
	for(i = 0, l = 0; i < (int)c; ++i)
	{
		if(Nicks_sanlist[0][i] != ' ') // here it's what's NOT copied
		{
			tempstr[l++] = Nicks_sanlist[0][i];
		}
	}
	tempstr[l] = 0;

	for(i = 1; i < count; ++i)
	{
		a = tempstr;
		b = Nicks_sanlist[i];
		while(1)
		{
			if(!*a)
				break;
			if(!*b)
			{
				*a = 0;
				break;
			}
			if(tolower(*a) == tolower(*b))
			{
				++a;
				++b;
				continue;
			}
			if(*b != ' ')
			{
				*a = 0;
				break;
			}
			++b;
		}
	}
	// Just so you know, if cutmatchesnormal doesn't kill the first entry, then even the non-alnums fit
	Nicks_CutMatchesNormal(count);
	//if(!Nicks_sanlist[0][0])
	//Con_Printf("TS: %s\n", tempstr);
	if(Nicks_strcleanlen(Nicks_sanlist[0]) < strlen(tempstr))
	{
		// if the clean sanitized one is longer than the current one, use it, it has crap chars which definitely are in there
		strlcpy(Nicks_sanlist[0], tempstr, sizeof(Nicks_sanlist[0]));
	}
}

static void Nicks_CutMatches(int count)
{
	if(con_nickcompletion_flags.integer & NICKS_ALPHANUMERICS_ONLY)
		Nicks_CutMatchesAlphaNumeric(count);
	else if(con_nickcompletion_flags.integer & NICKS_NO_SPACES)
		Nicks_CutMatchesNoSpaces(count);
	else
		Nicks_CutMatchesNormal(count);
}

static const char **Nicks_CompleteBuildList(int count)
{
	const char **buf;
	int bpos = 0;
	// the list is freed by Con_CompleteCommandLine, so create a char**
	buf = (const char **)Mem_Alloc(tempmempool, count * sizeof(const char *) + sizeof (const char *));

	for(; bpos < count; ++bpos)
		buf[bpos] = Nicks_sanlist[bpos] + Nicks_offset[bpos];

	Nicks_CutMatches(count);

	buf[bpos] = NULL;
	return buf;
}

/*
	Nicks_AddLastColor
	Restores the previous used color, after the autocompleted name.
*/
static int Nicks_AddLastColor(char *buffer, int pos)
{
	qboolean quote_added = false;
	int match;
	int color = STRING_COLOR_DEFAULT + '0';
	char r = 0, g = 0, b = 0;

	if(con_nickcompletion_flags.integer & NICKS_ADD_QUOTE && buffer[Nicks_matchpos-1] == '\"')
	{
		// we'll have to add a quote :)
		buffer[pos++] = '\"';
		quote_added = true;
	}

	if((!quote_added && con_nickcompletion_flags.integer & NICKS_ADD_COLOR) || con_nickcompletion_flags.integer & NICKS_FORCE_COLOR)
	{
		// add color when no quote was added, or when flags &4?
		// find last color
		for(match = Nicks_matchpos-1; match >= 0; --match)
		{
			if(buffer[match] == STRING_COLOR_TAG)
			{
				if( isdigit(buffer[match+1]) )
				{
					color = buffer[match+1];
					break;
				}
				else if(buffer[match+1] == STRING_COLOR_RGB_TAG_CHAR)
				{
					if ( isxdigit(buffer[match+2]) && isxdigit(buffer[match+3]) && isxdigit(buffer[match+4]) )
					{
						r = buffer[match+2];
						g = buffer[match+3];
						b = buffer[match+4];
						color = -1;
						break;
					}
				}
			}
		}
		if(!quote_added)
		{
			if( pos >= 2 && buffer[pos-2] == STRING_COLOR_TAG && isdigit(buffer[pos-1]) ) // when thes use &4
				pos -= 2;
			else if( pos >= 5 && buffer[pos-5] == STRING_COLOR_TAG && buffer[pos-4] == STRING_COLOR_RGB_TAG_CHAR
					 && isxdigit(buffer[pos-3]) && isxdigit(buffer[pos-2]) && isxdigit(buffer[pos-1]) )
				pos -= 5;
		}
		buffer[pos++] = STRING_COLOR_TAG;
		if (color == -1)
		{
			buffer[pos++] = STRING_COLOR_RGB_TAG_CHAR;
			buffer[pos++] = r;
			buffer[pos++] = g;
			buffer[pos++] = b;
		}
		else
			buffer[pos++] = color;
	}
	return pos;
}

int Nicks_CompleteChatLine(char *buffer, size_t size, unsigned int pos)
{
	int n;
	/*if(!con_nickcompletion.integer)
	  return; is tested in Nicks_CompletionCountPossible */
	n = Nicks_CompleteCountPossible(buffer, pos, &buffer[pos], false);
	if(n == 1)
	{
		size_t len;
		char *msg;

		msg = Nicks_list[0];
		len = min(size - Nicks_matchpos - 3, strlen(msg));
		memcpy(&buffer[Nicks_matchpos], msg, len);
		if( len < (size - 7) ) // space for color (^[0-9] or ^xrgb) and space and \0
			len = (int)Nicks_AddLastColor(buffer, Nicks_matchpos+(int)len);
		buffer[len++] = ' ';
		buffer[len] = 0;
		return (int)len;
	} else if(n > 1)
	{
		int len;
		char *msg;
		Con_Printf("\n%i possible nicks:\n", n);
		Cmd_CompleteNicksPrint(n);

		Nicks_CutMatches(n);

		msg = Nicks_sanlist[0];
		len = (int)min(size - Nicks_matchpos, strlen(msg));
		memcpy(&buffer[Nicks_matchpos], msg, len);
		buffer[Nicks_matchpos + len] = 0;
		//pos += len;
		return Nicks_matchpos + len;
	}
	return pos;
}


/*
	Con_CompleteCommandLine

	New function for tab-completion system
	Added by EvilTypeGuy
	Thanks to Fett erich@heintz.com
	Thanks to taniwha
	Enhanced to tab-complete map names by [515]

*/
void Con_CompleteCommandLine (void)
{
#ifndef SERVER
	const char *cmd = "";
	char *s;
	const char **list[4] = {0, 0, 0, 0};
	char s2[512];
	char command[512];
	int c, v, a, i, cmd_len, pos, k;
	int n; // nicks --blub
	const char *space, *patterns;
	char vabuf[1024];

	//find what we want to complete
	pos = key_linepos;
	while(--pos)
	{
		k = key_line[pos];
		if(k == '\"' || k == ';' || k == ' ' || k == '\'')
			break;
	}
	pos++;

	s = key_line + pos;
	strlcpy(s2, key_line + key_linepos, sizeof(s2));	//save chars after cursor
	key_line[key_linepos] = 0;					//hide them

	space = strchr(key_line + 1, ' ');
	if(space && pos == (space - key_line) + 1)
	{
		strlcpy(command, key_line + 1, min(sizeof(command), (unsigned int)(space - key_line)));

		patterns = Cvar_VariableString(va(vabuf, sizeof(vabuf), "con_completion_%s", command)); // TODO maybe use a better place for this?
		if(patterns && !*patterns)
			patterns = NULL; // get rid of the empty string

		if(!strcmp(command, "map") || !strcmp(command, "changelevel") || (patterns && !strcmp(patterns, "map")))
		{
			//maps search
			char t[MAX_QPATH];
			if (GetMapList(s, t, sizeof(t)))
			{
				// first move the cursor
				key_linepos += (int)strlen(t) - (int)strlen(s);

				// and now do the actual work
				*s = 0;
				strlcat(key_line, t, MAX_INPUTLINE);
				strlcat(key_line, s2, MAX_INPUTLINE); //add back chars after cursor

				// and fix the cursor
				if(key_linepos > (int) strlen(key_line))
					key_linepos = (int) strlen(key_line);
			}
			return;
		}
		else
		{
			if(patterns)
			{
				char t[MAX_QPATH];
				stringlist_t resultbuf, dirbuf;

				// Usage:
				//   // store completion patterns (space separated) for command foo in con_completion_foo
				//   set con_completion_foo "foodata/*.foodefault *.foo"
				//   foo <TAB>
				//
				// Note: patterns with slash are always treated as absolute
				// patterns; patterns without slash search in the innermost
				// directory the user specified. There is no way to "complete into"
				// a directory as of now, as directories seem to be unknown to the
				// FS subsystem.
				//
				// Examples:
				//   set con_completion_playermodel "models/player/*.zym models/player/*.md3 models/player/*.psk models/player/*.dpm"
				//   set con_completion_playdemo "*.dem"
				//   set con_completion_play "*.wav *.ogg"
				//
				// TODO somehow add support for directories; these shall complete
				// to their name + an appended slash.

				stringlistinit(&resultbuf);
				stringlistinit(&dirbuf);
				while(COM_ParseToken_Simple(&patterns, false, false, true))
				{
					fssearch_t *search;
					if(strchr(com_token, '/'))
					{
						search = FS_Search(com_token, true, true);
					}
					else
					{
						const char *slash = strrchr(s, '/');
						if(slash)
						{
							strlcpy(t, s, min(sizeof(t), (unsigned int)(slash - s + 2))); // + 2, because I want to include the slash
							strlcat(t, com_token, sizeof(t));
							search = FS_Search(t, true, true);
						}
						else
							search = FS_Search(com_token, true, true);
					}
					if(search)
					{
						for(i = 0; i < search->numfilenames; ++i)
							if(!strncmp(search->filenames[i], s, strlen(s)))
								if(FS_FileType(search->filenames[i]) == FS_FILETYPE_FILE)
									stringlistappend(&resultbuf, search->filenames[i]);
						FS_FreeSearch(search);
					}
				}

				// In any case, add directory names
				{
					fssearch_t *search;
					const char *slash = strrchr(s, '/');
					if(slash)
					{
						strlcpy(t, s, min(sizeof(t), (unsigned int)(slash - s + 2))); // + 2, because I want to include the slash
						strlcat(t, "*", sizeof(t));
						search = FS_Search(t, true, true);
					}
					else
						search = FS_Search("*", true, true);
					if(search)
					{
						for(i = 0; i < search->numfilenames; ++i)
							if(!strncmp(search->filenames[i], s, strlen(s)))
								if(FS_FileType(search->filenames[i]) == FS_FILETYPE_DIRECTORY)
									stringlistappend(&dirbuf, search->filenames[i]);
						FS_FreeSearch(search);
					}
				}

				if(resultbuf.numstrings > 0 || dirbuf.numstrings > 0)
				{
					const char *p, *q;
					unsigned int matchchars;
					if(resultbuf.numstrings == 0 && dirbuf.numstrings == 1)
					{
						dpsnprintf(t, sizeof(t), "%s/", dirbuf.strings[0]);
					}
					else
					if(resultbuf.numstrings == 1 && dirbuf.numstrings == 0)
					{
						dpsnprintf(t, sizeof(t), "%s ", resultbuf.strings[0]);
					}
					else
					{
						stringlistsort(&resultbuf, true); // dirbuf is already sorted
						Con_Printf("\n%i possible filenames\n", resultbuf.numstrings + dirbuf.numstrings);
						for(i = 0; i < dirbuf.numstrings; ++i)
						{
							Con_Printf("^4%s^7/\n", dirbuf.strings[i]);
						}
						for(i = 0; i < resultbuf.numstrings; ++i)
						{
							Con_Printf("%s\n", resultbuf.strings[i]);
						}
						matchchars = sizeof(t) - 1;
						if(resultbuf.numstrings > 0)
						{
							p = resultbuf.strings[0];
							q = resultbuf.strings[resultbuf.numstrings - 1];
							for(; *p && *p == *q; ++p, ++q);
							matchchars = (unsigned int)(p - resultbuf.strings[0]);
						}
						if(dirbuf.numstrings > 0)
						{
							p = dirbuf.strings[0];
							q = dirbuf.strings[dirbuf.numstrings - 1];
							for(; *p && *p == *q; ++p, ++q);
							matchchars = min(matchchars, (unsigned int)(p - dirbuf.strings[0]));
						}
						// now p points to the first non-equal character, or to the end
						// of resultbuf.strings[0]. We want to append the characters
						// from resultbuf.strings[0] to (not including) p as these are
						// the unique prefix
						strlcpy(t, (resultbuf.numstrings > 0 ? resultbuf : dirbuf).strings[0], min(matchchars + 1, sizeof(t)));
					}

					// first move the cursor
					key_linepos += (int)strlen(t) - (int)strlen(s);

					// and now do the actual work
					*s = 0;
					strlcat(key_line, t, MAX_INPUTLINE);
					strlcat(key_line, s2, MAX_INPUTLINE); //add back chars after cursor

					// and fix the cursor
					if(key_linepos > (int) strlen(key_line))
						key_linepos = (int) strlen(key_line);
				}
				stringlistfreecontents(&resultbuf);
				stringlistfreecontents(&dirbuf);

				return; // bail out, when we complete for a command that wants a file name
			}
		}
	}

	// Count number of possible matches and print them
	c = Cmd_CompleteCountPossible(s);
	if (c)
	{
		Con_Printf("\n%i possible command%s\n", c, (c > 1) ? "s: " : ":");
		Cmd_CompleteCommandPrint(s);
	}
	v = Cvar_CompleteCountPossible(s);
	if (v)
	{
		Con_Printf("\n%i possible variable%s\n", v, (v > 1) ? "s: " : ":");
		Cvar_CompleteCvarPrint(s);
	}
	a = Cmd_CompleteAliasCountPossible(s);
	if (a)
	{
		Con_Printf("\n%i possible alias%s\n", a, (a > 1) ? "es: " : ":");
		Cmd_CompleteAliasPrint(s);
	}
	n = Nicks_CompleteCountPossible(key_line, key_linepos, s, true);
	if (n)
	{
		Con_Printf("\n%i possible nick%s\n", n, (n > 1) ? "s: " : ":");
		Cmd_CompleteNicksPrint(n);
	}

	if (!(c + v + a + n))	// No possible matches
	{
		if(s2[0])
			strlcpy(&key_line[key_linepos], s2, sizeof(key_line) - key_linepos);
		return;
	}

	if (c)
		cmd = *(list[0] = Cmd_CompleteBuildList(s));
	if (v)
		cmd = *(list[1] = Cvar_CompleteBuildList(s));
	if (a)
		cmd = *(list[2] = Cmd_CompleteAliasBuildList(s));
	if (n)
		cmd = *(list[3] = Nicks_CompleteBuildList(n));

	for (cmd_len = (int)strlen(s);;cmd_len++)
	{
		const char **l;
		for (i = 0; i < 3; i++)
			if (list[i])
				for (l = list[i];*l;l++)
					if ((*l)[cmd_len] != cmd[cmd_len])
						goto done;
		// all possible matches share this character, so we continue...
		if (!cmd[cmd_len])
		{
			// if all matches ended at the same position, stop
			// (this means there is only one match)
			break;
		}
	}
done:

	// prevent a buffer overrun by limiting cmd_len according to remaining space
	cmd_len = min(cmd_len, (int)sizeof(key_line) - 1 - pos);
	if (cmd)
	{
		key_linepos = pos;
		memcpy(&key_line[key_linepos], cmd, cmd_len);
		key_linepos += cmd_len;
		// if there is only one match, add a space after it
		if (c + v + a + n == 1 && key_linepos < (int)sizeof(key_line) - 1)
		{
			if(n)
			{ // was a nick, might have an offset, and needs colors ;) --blub
				key_linepos = pos - Nicks_offset[0];
				cmd_len = (int)strlen(Nicks_list[0]);
				cmd_len = min(cmd_len, (int)sizeof(key_line) - 3 - pos);

				memcpy(&key_line[key_linepos] , Nicks_list[0], cmd_len);
				key_linepos += cmd_len;
				if(key_linepos < (int)(sizeof(key_line)-4)) // space for ^, X and space and \0
					key_linepos = Nicks_AddLastColor(key_line, key_linepos);
			}
			key_line[key_linepos++] = ' ';
		}
	}

	// use strlcat to avoid a buffer overrun
	key_line[key_linepos] = 0;
	strlcat(key_line, s2, sizeof(key_line));

	// free the command, cvar, and alias lists
	for (i = 0; i < 4; i++)
		if (list[i])
			Mem_Free((void *)list[i]);
#endif
}

