/*
Copyright (C) 1996-1997 Id Software, Inc.
Copyright (C) 2019 Cpt. David

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

// cmd_legacy.c -- legacy cvars and commands for Darkplaces compatibility

#include "quakedef.h"

extern cvar_t cl_cmdrate; //FIXME: laziness
extern cvar_t net_graph; //FIXME: laziness
extern cvar_t net_fakeloss_send; //FIXME: laziness
extern cvar_t net_fakeloss_receive; //FIXME: laziness

// Legacy cvars for Darkplaces compatibility.

/*
 * To map a cvar to a differently named cvar of identical functionality,
 * initialize your cvar like you normally would, but append .mirror, along
 * with a reference to the cvar you'd like to map to. It will be intercepted
 * in Cvar_Set, which will route to the appropriate cvar.
 */

cvar_t slowmo = {0,"slowmo","0","",.mirror = &host_timescale};
cvar_t cl_netlocalping = {0,"cl_netlocalping","0","",.mirror = &net_fakelag};
cvar_t cl_netpacketloss_send = {0,"cl_netpacketloss_send","0","",.mirror = &net_fakeloss_send};
cvar_t cl_netpacketloss_receive = {0,"cl_netpacketloss_receive","0","",.mirror = &net_fakeloss_receive};
cvar_t sv_protocolname = {0,"sv_protocolname","0","",.mirror = &sv_protocol};
cvar_t cl_netfps = {0,"cl_netfps","0","",.mirror = &cl_cmdrate};
cvar_t shownetgraph = {0,"shownetgraph","0","",.mirror = &net_graph};
cvar_t sv_fixedframeratesingleplayer = {0,"sv_fixedframeratesingleplayer","0","",.mirror = &host_limitlocal};


void Cmd_Legacy_InitCommands(void)
{
	Cvar_RegisterVariable(&slowmo);
	Cvar_RegisterVariable(&cl_netlocalping);
	Cvar_RegisterVariable(&cl_netpacketloss_send);
	Cvar_RegisterVariable(&cl_netpacketloss_receive);
	Cvar_RegisterVariable(&sv_protocolname);
	Cvar_RegisterVariable(&cl_netfps);
	Cvar_RegisterVariable(&shownetgraph);
	Cvar_RegisterVariable(&sv_fixedframeratesingleplayer);

}
