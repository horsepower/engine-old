#include "quakedef.h"

protocol_t *protocols[128] =
{
		&protocol_netquake,
		&protocol_nehahramovie,
		&protocol_nehahrabjp,
		&protocol_nehahrabjp2,
		&protocol_nehahrabjp3,
		&protocol_quakeworld,
		&protocol_darkplaces1,
		&protocol_darkplaces2,
		&protocol_darkplaces3,
		&protocol_darkplaces4,
		&protocol_darkplaces5,
		&protocol_darkplaces6,
		&protocol_darkplaces7
};

void Protocol_GetByName(const char *name, protocol_t **ptr)
{
	unsigned int i = 0;

	while (protocols[i] != NULL) {
		if (!strcmp((char *)protocols[i]->name,name)) {
			*ptr = protocols[i];
			break;
		} else i++;
	}
}

void Protocol_GetByNumber(int num, protocol_t **ptr)
{
	unsigned int i = 0;

	while (protocols[i] != NULL) {
		if (protocols[i]->num == num) {
			*ptr = protocols[i];
			break;
		} else i++;
	}
}

void Protocol_Names(char *buffer, size_t buffersize)
{
	int i;
	if (buffersize < 1)
		return;
	buffer[0] = 0;
	for (i = 0;protocols[i];i++)
	{
		if (i > 1)
			strlcat(buffer, " ", buffersize);
		strlcat(buffer, protocols[i]->name, buffersize);
	}
}
