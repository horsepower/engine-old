/*
Copyright (C) 2019 David Knapp (Cloudwalk)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "quakedef.h"

event_tm (evqueue, false, true);	// The master event queue.

static void Event_Exec(event_t *event)
{
	/* LIFO execution order */
	for(int i = 0; i < event->top; i++)
	{
		event->ret = event->func[i]();
		if(event->ret)
			break;
		if (event->temp)
		{
			Event_Pop(event);
		}
	}
}

void *Event_Trigger(event_t *event, void *dataptr)
{
	if (event->func)
	{
		// Call them immediately.
		if (!event->defer)
		{
			// If the event passes data around.
			if(dataptr != NULL)
			{
				event->arg = dataptr;
			}
			Event_Exec(event);
		}
		// Or wait until next frame.
		else for (int i = 0; i < event->top; i++)
		{
			Event_Push(&evqueue, event->func[i]);
		}
	}
	// Don't want memory leaks now, do we?
	event->arg = NULL;
	return event->ret;
}

void *Event_GetArg(event_t *event)
{
	return event->arg;
}

void Event_Push(event_t *event, void *func)
{
	event->func[event->top] = (eventfunc_t)func;
	Con_DPrintf("Event_Push: %p pushed to queue",(char *)func);

	event->top++;
}

void Event_Pop(event_t *event)
{
	if(!event->func)
		return;

	event->top--;
	event->func[event->top] = NULL;
}
