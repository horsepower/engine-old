/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// sv_main.c -- server main program

#include "quakedef.h"
#include "sv_demo.h"
#include "libcurl.h"
#include "csprogs.h"
#include "thread.h"

void SV_SaveEntFile_f(void);
void SV_StartDownload_f(void);
void SV_VM_Init(void);

int current_skill;
cvar_t sv_cheats = {CVAR_NOTIFY | CVAR_REPLICATED, "sv_cheats", "0", "enables cheat commands in any game, and cheat impulses in dpmod"};
cvar_t sv_worldmessage = {CVAR_READONLY, "sv_worldmessage", "", "title of current level"};
cvar_t sv_worldname = {CVAR_READONLY, "sv_worldname", "", "name of current worldmodel"};
cvar_t sv_worldnamenoextension = {CVAR_READONLY, "sv_worldnamenoextension", "", "name of current worldmodel without extension"};
cvar_t sv_worldbasename = {CVAR_READONLY, "sv_worldbasename", "", "name of current worldmodel without maps/ prefix or extension"};
cvar_t sv_adminnick = {CVAR_SAVE | CVAR_REPLICATED, "sv_adminnick", "Console", "nick name to use for admin messages instead of host name"};
cvar_t sv_status_privacy = {CVAR_SAVE | CVAR_REPLICATED, "sv_status_privacy", "1", "do not show IP addresses in 'status' replies to clients"};
cvar_t sv_status_show_qcstatus = {CVAR_SAVE | CVAR_REPLICATED, "sv_status_show_qcstatus", "0", "show the 'qcstatus' field in status replies, not the 'frags' field. Turn this on if your mod uses this field, and the 'frags' field on the other hand has no meaningful value."};
cvar_t sv_namechangetimer = {CVAR_SAVE | CVAR_REPLICATED, "sv_namechangetimer", "5", "how often to allow name changes, in seconds (prevents people from using animated names and other tricks"};


cvar_t sv_disablenotify = {0, "sv_disablenotify", "0", "suppress broadcast prints when certain cvars are changed (CVAR_NOTIFY flag in engine code)"};
cvar_t coop = {0, "coop","0", "coop mode, 0 = no coop, 1 = coop mode, multiple players playing through the singleplayer game (coop mode also shuts off deathmatch)"};
cvar_t deathmatch = {0, "deathmatch","0", "deathmatch mode, values depend on mod but typically 0 = no deathmatch, 1 = normal deathmatch with respawning weapons, 2 = weapons stay (players can only pick up new weapons)"};
cvar_t fraglimit = {CVAR_NOTIFY, "fraglimit","0", "ends level if this many frags is reached by any player"};
cvar_t gamecfg = {0, "gamecfg", "0", "unused cvar in quake, can be used by mods"};
cvar_t noexit = {CVAR_NOTIFY, "noexit","0", "kills anyone attempting to use an exit"};
cvar_t nomonsters = {0, "nomonsters", "0", "unused cvar in quake, can be used by mods"};
cvar_t pausable = {0, "pausable","1", "allow players to pause or not (otherwise, only the server admin can)"};
cvar_t pr_checkextension = {CVAR_READONLY, "pr_checkextension", "1", "indicates to QuakeC that the standard quakec extensions system is available (if 0, quakec should not attempt to use extensions)"};
cvar_t samelevel = {CVAR_NOTIFY, "samelevel","0", "repeats same level if level ends (due to timelimit or someone hitting an exit)"};
cvar_t skill = {0, "skill","1", "difficulty level of game, affects monster layouts in levels, 0 = easy, 1 = normal, 2 = hard, 3 = nightmare (same layout as hard but monsters fire twice)"};
cvar_t host_timescale = {CVAR_REPLICATED, "host_timescale", "1.0", "controls game speed, 0.5 is half speed, 2 is double speed"};

cvar_t sv_accelerate = {0, "sv_accelerate", "10", "rate at which a player accelerates to sv_maxspeed"};
cvar_t sv_aim = {CVAR_SAVE, "sv_aim", "2", "maximum cosine angle for quake's vertical autoaim, a value above 1 completely disables the autoaim, quake used 0.93"};
cvar_t sv_airaccel_qw = {0, "sv_airaccel_qw", "1", "ratio of QW-style air control as opposed to simple acceleration; when < 0, the speed is clamped against the maximum allowed forward speed after the move"};
cvar_t sv_airaccel_qw_stretchfactor = {0, "sv_airaccel_qw_stretchfactor", "0", "when set, the maximum acceleration increase the player may get compared to forward-acceleration when strafejumping"};
cvar_t sv_airaccel_sideways_friction = {0, "sv_airaccel_sideways_friction", "", "anti-sideways movement stabilization (reduces speed gain when zigzagging); when < 0, only so much friction is applied that braking (by accelerating backwards) cannot be stronger"};
cvar_t sv_airaccelerate = {0, "sv_airaccelerate", "-1", "rate at which a player accelerates to sv_maxairspeed while in the air, if less than 0 the sv_accelerate variable is used instead"};
cvar_t sv_airstopaccelerate = {0, "sv_airstopaccelerate", "0", "when set, replacement for sv_airaccelerate when moving backwards"};
cvar_t sv_airspeedlimit_nonqw = {0, "sv_airspeedlimit_nonqw", "0", "when set, this is a soft speed limit while in air when using airaccel_qw not equal to 1"};
cvar_t sv_airstrafeaccelerate = {0, "sv_airstrafeaccelerate", "0", "when set, replacement for sv_airaccelerate when just strafing"};
cvar_t sv_maxairstrafespeed = {0, "sv_maxairstrafespeed", "0", "when set, replacement for sv_maxairspeed when just strafing"};
cvar_t sv_airstrafeaccel_qw = {0, "sv_airstrafeaccel_qw", "0", "when set, replacement for sv_airaccel_qw when just strafing"};
cvar_t sv_aircontrol = {0, "sv_aircontrol", "0", "CPMA-style air control"};
cvar_t sv_aircontrol_power = {0, "sv_aircontrol_power", "2", "CPMA-style air control exponent"};
cvar_t sv_aircontrol_penalty = {0, "sv_aircontrol_penalty", "0", "deceleration while using CPMA-style air control"};
cvar_t sv_allowdownloads = {0, "sv_allowdownloads", "1", "whether to allow clients to download files from the server (does not affect http downloads)"};
cvar_t sv_allowdownloads_archive = {0, "sv_allowdownloads_archive", "0", "whether to allow downloads of archives (pak/pk3)"};
cvar_t sv_allowdownloads_config = {0, "sv_allowdownloads_config", "0", "whether to allow downloads of config files (cfg)"};
cvar_t sv_allowdownloads_dlcache = {0, "sv_allowdownloads_dlcache", "0", "whether to allow downloads of dlcache files (dlcache/)"};
cvar_t sv_allowdownloads_inarchive = {0, "sv_allowdownloads_inarchive", "0", "whether to allow downloads from archives (pak/pk3)"};
cvar_t sv_areagrid_mingridsize = {CVAR_NOTIFY, "sv_areagrid_mingridsize", "128", "minimum areagrid cell size, smaller values work better for lots of small objects, higher values for large objects"};
cvar_t sv_checkforpacketsduringsleep = {0, "sv_checkforpacketsduringsleep", "0", "uses select() function to wait between frames which can be interrupted by packets being received, instead of Sleep()/usleep()/SDL_Sleep() functions which do not check for packets"};
cvar_t sv_clmovement_enable = {0, "sv_clmovement_enable", "1", "whether to allow clients to use cl_movement prediction, which can cause choppy movement on the server which may annoy other players"};
cvar_t sv_clmovement_minping = {0, "sv_clmovement_minping", "0", "if client ping is below this time in milliseconds, then their ability to use cl_movement prediction is disabled for a while (as they don't need it)"};
cvar_t sv_clmovement_minping_disabletime = {0, "sv_clmovement_minping_disabletime", "1000", "when client falls below minping, disable their prediction for this many milliseconds (should be at least 1000 or else their prediction may turn on/off frequently)"};
cvar_t sv_clmovement_inputtimeout = {0, "sv_clmovement_inputtimeout", "0.2", "when a client does not send input for this many seconds, force them to move anyway (unlike QuakeWorld)"};
cvar_t sv_cullentities_nevercullbmodels = {0, "sv_cullentities_nevercullbmodels", "0", "if enabled the clients are always notified of moving doors and lifts and other submodels of world (warning: eats a lot of network bandwidth on some levels!)"};
cvar_t sv_cullentities_pvs = {0, "sv_cullentities_pvs", "1", "fast but loose culling of hidden entities"};
cvar_t sv_cullentities_stats = {0, "sv_cullentities_stats", "0", "displays stats on network entities culled by various methods for each client"};
cvar_t sv_cullentities_trace = {0, "sv_cullentities_trace", "0", "somewhat slow but very tight culling of hidden entities, minimizes network traffic and makes wallhack cheats useless"};
cvar_t sv_cullentities_trace_delay = {0, "sv_cullentities_trace_delay", "1", "number of seconds until the entity gets actually culled"};
cvar_t sv_cullentities_trace_delay_players = {0, "sv_cullentities_trace_delay_players", "0.2", "number of seconds until the entity gets actually culled if it is a player entity"};
cvar_t sv_cullentities_trace_enlarge = {0, "sv_cullentities_trace_enlarge", "0", "box enlargement for entity culling"};
cvar_t sv_cullentities_trace_expand = { 0, "sv_cullentities_trace_expand", "0", "box is expanded by this many units for entity culling" };
cvar_t sv_cullentities_trace_eyejitter = {0, "sv_cullentities_trace_eyejitter", "16", "jitter the eye by this much for each trace"};
cvar_t sv_cullentities_trace_prediction = {0, "sv_cullentities_trace_prediction", "1", "also trace from the predicted player position"};
cvar_t sv_cullentities_trace_prediction_time = {0, "sv_cullentities_trace_prediction_time", "0.2", "how many seconds of prediction to use"};
cvar_t sv_cullentities_trace_entityocclusion = {0, "sv_cullentities_trace_entityocclusion", "0", "also check if doors and other bsp models are in the way"};
cvar_t sv_cullentities_trace_samples = {0, "sv_cullentities_trace_samples", "2", "number of samples to test for entity culling"};
cvar_t sv_cullentities_trace_samples_extra = {0, "sv_cullentities_trace_samples_extra", "2", "number of samples to test for entity culling when the entity affects its surroundings by e.g. dlight"};
cvar_t sv_cullentities_trace_samples_players = {0, "sv_cullentities_trace_samples_players", "8", "number of samples to test for entity culling when the entity is a player entity"};
cvar_t sv_debugmove = {CVAR_NOTIFY, "sv_debugmove", "0", "disables collision detection optimizations for debugging purposes"};
cvar_t sv_echobprint = {CVAR_SAVE, "sv_echobprint", "1", "prints gamecode bprint() calls to server console"};
cvar_t sv_edgefriction = {0, "edgefriction", "1", "how much you slow down when nearing a ledge you might fall off, multiplier of sv_friction (Quake used 2, QuakeWorld used 1 due to a bug in physics code)"};
cvar_t sv_entpatch = {0, "sv_entpatch", "1", "enables loading of .ent files to override entities in the bsp (for example Threewave CTF server pack contains .ent patch files enabling play of CTF on id1 maps)"};
cvar_t host_limitlocal = {0, "host_limitlocal", "1", "allows you to use server-style timing system in singleplayer (don't run faster than sv_cmdrate)"};
cvar_t sv_freezenonclients = {CVAR_NOTIFY, "sv_freezenonclients", "0", "freezes time, except for players, allowing you to walk around and take screenshots of explosions"};
cvar_t sv_friction = {CVAR_NOTIFY, "sv_friction","4", "how fast you slow down"};
cvar_t sv_gameplayfix_blowupfallenzombies = {0, "sv_gameplayfix_blowupfallenzombies", "1", "causes findradius to detect SOLID_NOT entities such as zombies and corpses on the floor, allowing splash damage to apply to them"};
cvar_t sv_gameplayfix_consistentplayerprethink = {0, "sv_gameplayfix_consistentplayerprethink", "0", "improves fairness in multiplayer by running all PlayerPreThink functions (which fire weapons) before performing physics, then running all PlayerPostThink functions"};
cvar_t sv_gameplayfix_delayprojectiles = {0, "sv_gameplayfix_delayprojectiles", "1", "causes entities to not move on the same frame they are spawned, meaning that projectiles wait until the next frame to perform their first move, giving proper interpolation and rocket trails, but making weapons harder to use at low framerates"};
cvar_t sv_gameplayfix_droptofloorstartsolid = {0, "sv_gameplayfix_droptofloorstartsolid", "1", "prevents items and monsters that start in a solid area from falling out of the level (makes droptofloor treat trace_startsolid as an acceptable outcome)"};
cvar_t sv_gameplayfix_droptofloorstartsolid_nudgetocorrect = {0, "sv_gameplayfix_droptofloorstartsolid_nudgetocorrect", "1", "tries to nudge stuck items and monsters out of walls before droptofloor is performed"};
cvar_t sv_gameplayfix_easierwaterjump = {0, "sv_gameplayfix_easierwaterjump", "1", "changes water jumping to make it easier to get out of water (exactly like in QuakeWorld)"};
cvar_t sv_gameplayfix_findradiusdistancetobox = {0, "sv_gameplayfix_findradiusdistancetobox", "1", "causes findradius to check the distance to the corner of a box rather than the center of the box, makes findradius detect bmodels such as very large doors that would otherwise be unaffected by splash damage"};
cvar_t sv_gameplayfix_gravityunaffectedbyticrate = {0, "sv_gameplayfix_gravityunaffectedbyticrate", "0", "fix some ticrate issues in physics."};
cvar_t sv_gameplayfix_grenadebouncedownslopes = {0, "sv_gameplayfix_grenadebouncedownslopes", "1", "prevents MOVETYPE_BOUNCE (grenades) from getting stuck when fired down a downward sloping surface"};
cvar_t sv_gameplayfix_multiplethinksperframe = {0, "sv_gameplayfix_multiplethinksperframe", "1", "allows entities to think more often than the server framerate, primarily useful for very high fire rate weapons"};
cvar_t sv_gameplayfix_noairborncorpse = {0, "sv_gameplayfix_noairborncorpse", "1", "causes entities (corpses, items, etc) sitting ontop of moving entities (players) to fall when the moving entity (player) is no longer supporting them"};
cvar_t sv_gameplayfix_noairborncorpse_allowsuspendeditems = {0, "sv_gameplayfix_noairborncorpse_allowsuspendeditems", "1", "causes entities sitting ontop of objects that are instantaneously remove to float in midair (special hack to allow a common level design trick for floating items)"};
cvar_t sv_gameplayfix_nudgeoutofsolid = {0, "sv_gameplayfix_nudgeoutofsolid", "0", "attempts to fix physics errors (where an object ended up in solid for some reason)"};
cvar_t sv_gameplayfix_nudgeoutofsolid_separation = {0, "sv_gameplayfix_nudgeoutofsolid_separation", "0.03125", "keep objects this distance apart to prevent collision issues on seams"};
cvar_t sv_gameplayfix_q2airaccelerate = {0, "sv_gameplayfix_q2airaccelerate", "0", "Quake2-style air acceleration"};
cvar_t sv_gameplayfix_nogravityonground = {0, "sv_gameplayfix_nogravityonground", "0", "turn off gravity when on ground (to get rid of sliding)"};
cvar_t sv_gameplayfix_setmodelrealbox = {0, "sv_gameplayfix_setmodelrealbox", "1", "fixes a bug in Quake that made setmodel always set the entity box to ('-16 -16 -16', '16 16 16') rather than properly checking the model box, breaks some poorly coded mods"};
cvar_t sv_gameplayfix_slidemoveprojectiles = {0, "sv_gameplayfix_slidemoveprojectiles", "1", "allows MOVETYPE_FLY/FLYMISSILE/TOSS/BOUNCE/BOUNCEMISSILE entities to finish their move in a frame even if they hit something, fixes 'gravity accumulation' bug for grenades on steep slopes"};
cvar_t sv_gameplayfix_stepdown = {0, "sv_gameplayfix_stepdown", "0", "attempts to step down stairs, not just up them (prevents the familiar thud..thud..thud.. when running down stairs and slopes)"};
cvar_t sv_gameplayfix_stepmultipletimes = {0, "sv_gameplayfix_stepmultipletimes", "0", "applies step-up onto a ledge more than once in a single frame, when running quickly up stairs"};
cvar_t sv_gameplayfix_nostepmoveonsteepslopes = {0, "sv_gameplayfix_nostepmoveonsteepslopes", "0", "crude fix which prevents MOVETYPE_STEP (not swimming or flying) to move on slopes whose angle is bigger than 45 degree"};
cvar_t sv_gameplayfix_swiminbmodels = {0, "sv_gameplayfix_swiminbmodels", "1", "causes pointcontents (used to determine if you are in a liquid) to check bmodel entities as well as the world model, so you can swim around in (possibly moving) water bmodel entities"};
cvar_t sv_gameplayfix_upwardvelocityclearsongroundflag = {0, "sv_gameplayfix_upwardvelocityclearsongroundflag", "1", "prevents monsters, items, and most other objects from being stuck to the floor when pushed around by damage, and other situations in mods"};
cvar_t sv_gameplayfix_downtracesupportsongroundflag = {0, "sv_gameplayfix_downtracesupportsongroundflag", "1", "prevents very short moves from clearing onground (which may make the player stick to the floor at high netfps)"};
cvar_t sv_gameplayfix_q1bsptracelinereportstexture = {0, "sv_gameplayfix_q1bsptracelinereportstexture", "1", "enables mods to get accurate trace_texture results on q1bsp by using a surface-hitting traceline implementation rather than the standard solidbsp method, q3bsp always reports texture accurately"};
cvar_t sv_gameplayfix_unstickplayers = {0, "sv_gameplayfix_unstickplayers", "1", "big hack to try and fix the rare case of MOVETYPE_WALK entities getting stuck in the world clipping hull."};
cvar_t sv_gameplayfix_unstickentities = {0, "sv_gameplayfix_unstickentities", "1", "hack to check if entities are crossing world collision hull and try to move them to the right position"};
cvar_t sv_gameplayfix_fixedcheckwatertransition = {0, "sv_gameplayfix_fixedcheckwatertransition", "1", "fix two very stupid bugs in SV_CheckWaterTransition when watertype is CONTENTS_EMPTY (the bugs causes waterlevel to be 1 on first frame, -1 on second frame - the fix makes it 0 on both frames)"};
cvar_t sv_gravity = {CVAR_NOTIFY, "sv_gravity","800", "how fast you fall (512 = roughly earth gravity)"};
cvar_t sv_init_frame_count = {0, "sv_init_frame_count", "2", "number of frames to run to allow everything to settle before letting clients connect"};
cvar_t sv_idealpitchscale = {0, "sv_idealpitchscale","0.8", "how much to look up/down slopes and stairs when not using freelook"};
cvar_t sv_jumpstep = {CVAR_NOTIFY, "sv_jumpstep", "0", "whether you can step up while jumping"};
cvar_t sv_jumpvelocity = {0, "sv_jumpvelocity", "270", "cvar that can be used by QuakeC code for jump velocity"};
cvar_t sv_maxairspeed = {0, "sv_maxairspeed", "30", "maximum speed a player can accelerate to when airborn (note that it is possible to completely stop by moving the opposite direction)"};
cvar_t sv_maxrate = {CVAR_SAVE | CVAR_NOTIFY, "sv_maxrate", "1000000", "upper limit on client rate cvar, should reflect your network connection quality"};
cvar_t sv_maxspeed = {CVAR_NOTIFY, "sv_maxspeed", "320", "maximum speed a player can accelerate to when on ground (can be exceeded by tricks)"};
cvar_t sv_maxvelocity = {CVAR_NOTIFY, "sv_maxvelocity","2000", "universal speed limit on all entities"};
cvar_t sv_nostep = {CVAR_NOTIFY, "sv_nostep","0", "prevents MOVETYPE_STEP entities (monsters) from moving"};
cvar_t sv_playerphysicsqc = {CVAR_NOTIFY, "sv_playerphysicsqc", "1", "enables QuakeC function to override player physics"};
cvar_t sv_progs = {0, "sv_progs", "progs.dat", "selects which quakec progs.dat file to run" };
cvar_t sv_protocol = {0, "sv_protocol", "DP7", "selects network protocol to host for (values include QUAKE, QUAKEDP, NEHAHRAMOVIE, DP1 and up)"};
cvar_t sv_random_seed = {0, "sv_random_seed", "", "random seed; when set, on every map start this random seed is used to initialize the random number generator. Don't touch it unless for benchmarking or debugging"};
cvar_t sv_ratelimitlocalplayer = {0, "sv_ratelimitlocalplayer", "0", "whether to apply rate limiting to the local player in a listen server (only useful for testing)"};
cvar_t sv_sound_land = {0, "sv_sound_land", "demon/dland2.wav", "sound to play when MOVETYPE_STEP entity hits the ground at high speed (empty cvar disables the sound)"};
cvar_t sv_sound_watersplash = {0, "sv_sound_watersplash", "misc/h2ohit1.wav", "sound to play when MOVETYPE_FLY/TOSS/BOUNCE/STEP entity enters or leaves water (empty cvar disables the sound)"};
cvar_t sv_stepheight = {CVAR_NOTIFY, "sv_stepheight", "18", "how high you can step up (TW_SV_STEPCONTROL extension)"};
cvar_t sv_stopspeed = {CVAR_NOTIFY, "sv_stopspeed","100", "how fast you come to a complete stop"};
cvar_t sv_wallfriction = {CVAR_NOTIFY, "sv_wallfriction", "1", "how much you slow down when sliding along a wall"};
cvar_t sv_wateraccelerate = {0, "sv_wateraccelerate", "-1", "rate at which a player accelerates to sv_maxspeed while in the air, if less than 0 the sv_accelerate variable is used instead"};
cvar_t sv_waterfriction = {CVAR_NOTIFY, "sv_waterfriction","-1", "how fast you slow down, if less than 0 the sv_friction variable is used instead"};
cvar_t sv_warsowbunny_airforwardaccel = {0, "sv_warsowbunny_airforwardaccel", "1.00001", "how fast you accelerate until you reach sv_maxspeed"};
cvar_t sv_warsowbunny_accel = {0, "sv_warsowbunny_accel", "0.1585", "how fast you accelerate until after reaching sv_maxspeed (it gets harder as you near sv_warsowbunny_topspeed)"};
cvar_t sv_warsowbunny_topspeed = {0, "sv_warsowbunny_topspeed", "925", "soft speed limit (can get faster with rjs and on ramps)"};
cvar_t sv_warsowbunny_turnaccel = {0, "sv_warsowbunny_turnaccel", "0", "max sharpness of turns (also master switch for the sv_warsowbunny_* mode; set this to 9 to enable)"};
cvar_t sv_warsowbunny_backtosideratio = {0, "sv_warsowbunny_backtosideratio", "0.8", "lower values make it easier to change direction without losing speed; the drawback is \"understeering\" in sharp turns"};
cvar_t sv_onlycsqcnetworking = {0, "sv_onlycsqcnetworking", "0", "disables legacy entity networking code for higher performance (except on clients, which can still be legacy)"};
cvar_t sv_areadebug = {0, "sv_areadebug", "0", "disables physics culling for debugging purposes (only for development)"};
cvar_t sys_ticrate = {CVAR_SAVE, "sys_ticrate","0.0138889", "how long a server frame is in seconds, 0.05 is 20fps server rate, 0.1 is 10fps (can not be set higher than 0.1), 0 runs as many server frames as possible (makes games against bots a little smoother, overwhelms network players), 0.0138889 matches QuakeWorld physics"};
cvar_t teamplay = {CVAR_NOTIFY, "teamplay","0", "teamplay mode, values depend on mod but typically 0 = no teams, 1 = no team damage no self damage, 2 = team damage and self damage, some mods support 3 = no team damage but can damage self"};
cvar_t timelimit = {CVAR_NOTIFY, "timelimit","0", "ends level at this time (in minutes)"};
cvar_t sv_threaded = {0, "sv_threaded", "0", "enables a separate thread for server code, improving performance, especially when hosting a game while playing, EXPERIMENTAL, may be crashy"};

cvar_t saved1 = {CVAR_SAVE, "saved1", "0", "unused cvar in quake that is saved to config.cfg on exit, can be used by mods"};
cvar_t saved2 = {CVAR_SAVE, "saved2", "0", "unused cvar in quake that is saved to config.cfg on exit, can be used by mods"};
cvar_t saved3 = {CVAR_SAVE, "saved3", "0", "unused cvar in quake that is saved to config.cfg on exit, can be used by mods"};
cvar_t saved4 = {CVAR_SAVE, "saved4", "0", "unused cvar in quake that is saved to config.cfg on exit, can be used by mods"};
cvar_t savedgamecfg = {CVAR_SAVE, "savedgamecfg", "0", "unused cvar in quake that is saved to config.cfg on exit, can be used by mods"};
cvar_t scratch1 = {0, "scratch1", "0", "unused cvar in quake, can be used by mods"};
cvar_t scratch2 = {0,"scratch2", "0", "unused cvar in quake, can be used by mods"};
cvar_t scratch3 = {0, "scratch3", "0", "unused cvar in quake, can be used by mods"};
cvar_t scratch4 = {0, "scratch4", "0", "unused cvar in quake, can be used by mods"};
cvar_t temp1 = {0, "temp1","0", "general cvar for mods to use, in stock id1 this selects which death animation to use on players (0 = random death, other values select specific death scenes)"};

cvar_t nehx00 = {0, "nehx00", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx01 = {0, "nehx01", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx02 = {0, "nehx02", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx03 = {0, "nehx03", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx04 = {0, "nehx04", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx05 = {0, "nehx05", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx06 = {0, "nehx06", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx07 = {0, "nehx07", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx08 = {0, "nehx08", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx09 = {0, "nehx09", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx10 = {0, "nehx10", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx11 = {0, "nehx11", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx12 = {0, "nehx12", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx13 = {0, "nehx13", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx14 = {0, "nehx14", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx15 = {0, "nehx15", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx16 = {0, "nehx16", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx17 = {0, "nehx17", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx18 = {0, "nehx18", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t nehx19 = {0, "nehx19", "0", "nehahra data storage cvar (used in singleplayer)"};
cvar_t cutscene = {0, "cutscene", "1", "enables cutscenes in nehahra, can be used by other mods"};

cvar_t sv_autodemo_perclient = {CVAR_SAVE, "sv_autodemo_perclient", "0", "set to 1 to enable autorecorded per-client demos (they'll start to record at the beginning of a match); set it to 2 to also record client->server packets (for debugging)"};
cvar_t sv_autodemo_perclient_nameformat = {CVAR_SAVE, "sv_autodemo_perclient_nameformat", "sv_autodemos/%Y-%m-%d_%H-%M", "The format of the sv_autodemo_perclient filename, followed by the map name, the client number and the IP address + port number, separated by underscores (the date is encoded using strftime escapes)" };
cvar_t sv_autodemo_perclient_discardable = {CVAR_SAVE, "sv_autodemo_perclient_discardable", "0", "Allow game code to decide whether a demo should be kept or discarded."};

cvar_t halflifebsp = {0, "halflifebsp", "0", "indicates the current map is hlbsp format (useful to know because of different bounding box sizes)"};
cvar_t sv_mapformat_is_quake2 = {0, "sv_mapformat_is_quake2", "0", "indicates the current map is q2bsp format (useful to know because of different entity behaviors, .frame on submodels and other things)"};
cvar_t sv_mapformat_is_quake3 = {0, "sv_mapformat_is_quake3", "0", "indicates the current map is q2bsp format (useful to know because of different entity behaviors)"};

event_t on_host_drop;

server_t sv;
server_static_t svs;

mempool_t *sv_mempool = NULL;

extern cvar_t host_timescale;
//extern float		scr_centertime_off;

// MUST match effectnameindex_t in client.h
static const char *standardeffectnames[EFFECT_TOTAL] =
{
	"",
	"TE_GUNSHOT",
	"TE_GUNSHOTQUAD",
	"TE_SPIKE",
	"TE_SPIKEQUAD",
	"TE_SUPERSPIKE",
	"TE_SUPERSPIKEQUAD",
	"TE_WIZSPIKE",
	"TE_KNIGHTSPIKE",
	"TE_EXPLOSION",
	"TE_EXPLOSIONQUAD",
	"TE_TAREXPLOSION",
	"TE_TELEPORT",
	"TE_LAVASPLASH",
	"TE_SMALLFLASH",
	"TE_FLAMEJET",
	"EF_FLAME",
	"TE_BLOOD",
	"TE_SPARK",
	"TE_PLASMABURN",
	"TE_TEI_G3",
	"TE_TEI_SMOKE",
	"TE_TEI_BIGEXPLOSION",
	"TE_TEI_PLASMAHIT",
	"EF_STARDUST",
	"TR_ROCKET",
	"TR_GRENADE",
	"TR_BLOOD",
	"TR_WIZSPIKE",
	"TR_SLIGHTBLOOD",
	"TR_KNIGHTSPIKE",
	"TR_VORESPIKE",
	"TR_NEHAHRASMOKE",
	"TR_NEXUIZPLASMA",
	"TR_GLOWTRAIL",
	"SVC_PARTICLE"
};

#define SV_REQFUNCS 0
#define sv_reqfuncs NULL

//#define SV_REQFUNCS (sizeof(sv_reqfuncs) / sizeof(const char *))
//static const char *sv_reqfuncs[] = {
//};

#define SV_REQFIELDS (sizeof(sv_reqfields) / sizeof(prvm_required_field_t))

prvm_required_field_t sv_reqfields[] =
{
#define PRVM_DECLARE_serverglobalfloat(x)
#define PRVM_DECLARE_serverglobalvector(x)
#define PRVM_DECLARE_serverglobalstring(x)
#define PRVM_DECLARE_serverglobaledict(x)
#define PRVM_DECLARE_serverglobalfunction(x)
#define PRVM_DECLARE_clientglobalfloat(x)
#define PRVM_DECLARE_clientglobalvector(x)
#define PRVM_DECLARE_clientglobalstring(x)
#define PRVM_DECLARE_clientglobaledict(x)
#define PRVM_DECLARE_clientglobalfunction(x)
#define PRVM_DECLARE_menuglobalfloat(x)
#define PRVM_DECLARE_menuglobalvector(x)
#define PRVM_DECLARE_menuglobalstring(x)
#define PRVM_DECLARE_menuglobaledict(x)
#define PRVM_DECLARE_menuglobalfunction(x)
#define PRVM_DECLARE_serverfieldfloat(x) {ev_float, #x},
#define PRVM_DECLARE_serverfieldvector(x) {ev_vector, #x},
#define PRVM_DECLARE_serverfieldstring(x) {ev_string, #x},
#define PRVM_DECLARE_serverfieldedict(x) {ev_entity, #x},
#define PRVM_DECLARE_serverfieldfunction(x) {ev_function, #x},
#define PRVM_DECLARE_clientfieldfloat(x)
#define PRVM_DECLARE_clientfieldvector(x)
#define PRVM_DECLARE_clientfieldstring(x)
#define PRVM_DECLARE_clientfieldedict(x)
#define PRVM_DECLARE_clientfieldfunction(x)
#define PRVM_DECLARE_menufieldfloat(x)
#define PRVM_DECLARE_menufieldvector(x)
#define PRVM_DECLARE_menufieldstring(x)
#define PRVM_DECLARE_menufieldedict(x)
#define PRVM_DECLARE_menufieldfunction(x)
#define PRVM_DECLARE_serverfunction(x)
#define PRVM_DECLARE_clientfunction(x)
#define PRVM_DECLARE_menufunction(x)
#define PRVM_DECLARE_field(x)
#define PRVM_DECLARE_global(x)
#define PRVM_DECLARE_function(x)
#include "prvm_offsets.h"
#undef PRVM_DECLARE_serverglobalfloat
#undef PRVM_DECLARE_serverglobalvector
#undef PRVM_DECLARE_serverglobalstring
#undef PRVM_DECLARE_serverglobaledict
#undef PRVM_DECLARE_serverglobalfunction
#undef PRVM_DECLARE_clientglobalfloat
#undef PRVM_DECLARE_clientglobalvector
#undef PRVM_DECLARE_clientglobalstring
#undef PRVM_DECLARE_clientglobaledict
#undef PRVM_DECLARE_clientglobalfunction
#undef PRVM_DECLARE_menuglobalfloat
#undef PRVM_DECLARE_menuglobalvector
#undef PRVM_DECLARE_menuglobalstring
#undef PRVM_DECLARE_menuglobaledict
#undef PRVM_DECLARE_menuglobalfunction
#undef PRVM_DECLARE_serverfieldfloat
#undef PRVM_DECLARE_serverfieldvector
#undef PRVM_DECLARE_serverfieldstring
#undef PRVM_DECLARE_serverfieldedict
#undef PRVM_DECLARE_serverfieldfunction
#undef PRVM_DECLARE_clientfieldfloat
#undef PRVM_DECLARE_clientfieldvector
#undef PRVM_DECLARE_clientfieldstring
#undef PRVM_DECLARE_clientfieldedict
#undef PRVM_DECLARE_clientfieldfunction
#undef PRVM_DECLARE_menufieldfloat
#undef PRVM_DECLARE_menufieldvector
#undef PRVM_DECLARE_menufieldstring
#undef PRVM_DECLARE_menufieldedict
#undef PRVM_DECLARE_menufieldfunction
#undef PRVM_DECLARE_serverfunction
#undef PRVM_DECLARE_clientfunction
#undef PRVM_DECLARE_menufunction
#undef PRVM_DECLARE_field
#undef PRVM_DECLARE_global
#undef PRVM_DECLARE_function
};

#define SV_REQGLOBALS (sizeof(sv_reqglobals) / sizeof(prvm_required_field_t))

prvm_required_field_t sv_reqglobals[] =
{
#define PRVM_DECLARE_serverglobalfloat(x) {ev_float, #x},
#define PRVM_DECLARE_serverglobalvector(x) {ev_vector, #x},
#define PRVM_DECLARE_serverglobalstring(x) {ev_string, #x},
#define PRVM_DECLARE_serverglobaledict(x) {ev_entity, #x},
#define PRVM_DECLARE_serverglobalfunction(x) {ev_function, #x},
#define PRVM_DECLARE_clientglobalfloat(x)
#define PRVM_DECLARE_clientglobalvector(x)
#define PRVM_DECLARE_clientglobalstring(x)
#define PRVM_DECLARE_clientglobaledict(x)
#define PRVM_DECLARE_clientglobalfunction(x)
#define PRVM_DECLARE_menuglobalfloat(x)
#define PRVM_DECLARE_menuglobalvector(x)
#define PRVM_DECLARE_menuglobalstring(x)
#define PRVM_DECLARE_menuglobaledict(x)
#define PRVM_DECLARE_menuglobalfunction(x)
#define PRVM_DECLARE_serverfieldfloat(x)
#define PRVM_DECLARE_serverfieldvector(x)
#define PRVM_DECLARE_serverfieldstring(x)
#define PRVM_DECLARE_serverfieldedict(x)
#define PRVM_DECLARE_serverfieldfunction(x)
#define PRVM_DECLARE_clientfieldfloat(x)
#define PRVM_DECLARE_clientfieldvector(x)
#define PRVM_DECLARE_clientfieldstring(x)
#define PRVM_DECLARE_clientfieldedict(x)
#define PRVM_DECLARE_clientfieldfunction(x)
#define PRVM_DECLARE_menufieldfloat(x)
#define PRVM_DECLARE_menufieldvector(x)
#define PRVM_DECLARE_menufieldstring(x)
#define PRVM_DECLARE_menufieldedict(x)
#define PRVM_DECLARE_menufieldfunction(x)
#define PRVM_DECLARE_serverfunction(x)
#define PRVM_DECLARE_clientfunction(x)
#define PRVM_DECLARE_menufunction(x)
#define PRVM_DECLARE_field(x)
#define PRVM_DECLARE_global(x)
#define PRVM_DECLARE_function(x)
#include "prvm_offsets.h"
#undef PRVM_DECLARE_serverglobalfloat
#undef PRVM_DECLARE_serverglobalvector
#undef PRVM_DECLARE_serverglobalstring
#undef PRVM_DECLARE_serverglobaledict
#undef PRVM_DECLARE_serverglobalfunction
#undef PRVM_DECLARE_clientglobalfloat
#undef PRVM_DECLARE_clientglobalvector
#undef PRVM_DECLARE_clientglobalstring
#undef PRVM_DECLARE_clientglobaledict
#undef PRVM_DECLARE_clientglobalfunction
#undef PRVM_DECLARE_menuglobalfloat
#undef PRVM_DECLARE_menuglobalvector
#undef PRVM_DECLARE_menuglobalstring
#undef PRVM_DECLARE_menuglobaledict
#undef PRVM_DECLARE_menuglobalfunction
#undef PRVM_DECLARE_serverfieldfloat
#undef PRVM_DECLARE_serverfieldvector
#undef PRVM_DECLARE_serverfieldstring
#undef PRVM_DECLARE_serverfieldedict
#undef PRVM_DECLARE_serverfieldfunction
#undef PRVM_DECLARE_clientfieldfloat
#undef PRVM_DECLARE_clientfieldvector
#undef PRVM_DECLARE_clientfieldstring
#undef PRVM_DECLARE_clientfieldedict
#undef PRVM_DECLARE_clientfieldfunction
#undef PRVM_DECLARE_menufieldfloat
#undef PRVM_DECLARE_menufieldvector
#undef PRVM_DECLARE_menufieldstring
#undef PRVM_DECLARE_menufieldedict
#undef PRVM_DECLARE_menufieldfunction
#undef PRVM_DECLARE_serverfunction
#undef PRVM_DECLARE_clientfunction
#undef PRVM_DECLARE_menufunction
#undef PRVM_DECLARE_field
#undef PRVM_DECLARE_global
#undef PRVM_DECLARE_function
};



//============================================================================

static void SV_AreaStats_f(void)
{
	World_PrintAreaStats(&sv.world, "server");
}

/*
===============
SV_Init
===============
*/
void SV_Init (void)
{
	// init the csqc progs cvars, since they are updated/used by the server code
	// TODO: fix this since this is a quick hack to make some of [515]'s broken code run ;) [9/13/2006 Black]
	extern cvar_t csqc_progname;	//[515]: csqc crc check and right csprogs name according to progs.dat
	extern cvar_t csqc_progcrc;
	extern cvar_t csqc_progsize;
	extern cvar_t csqc_usedemoprogs;

	// Client only commands
	SV_Init_Commands();

	Cvar_RegisterVariable(&sv_cheats);
	Cvar_RegisterVariable(&sv_adminnick);
	Cvar_RegisterVariable(&sv_status_privacy);
	Cvar_RegisterVariable(&sv_status_show_qcstatus);
	Cvar_RegisterVariable(&sv_namechangetimer);

	Cvar_RegisterVariable(&sv_worldmessage);
	Cvar_RegisterVariable(&sv_worldname);
	Cvar_RegisterVariable(&sv_worldnamenoextension);
	Cvar_RegisterVariable(&sv_worldbasename);

	Cvar_RegisterVariable (&csqc_progname);
	Cvar_RegisterVariable (&csqc_progcrc);
	Cvar_RegisterVariable (&csqc_progsize);
	Cvar_RegisterVariable (&csqc_usedemoprogs);

	Cmd_AddCommand(CMD_SERVER, "sv_saveentfile", SV_SaveEntFile_f, "save map entities to .ent file (to allow external editing)");
	Cmd_AddCommand(CMD_SERVER, "sv_areastats", SV_AreaStats_f, "prints statistics on entity culling during collision traces");
	Cmd_AddCommand(CMD_SERVER, "sv_startdownload", SV_StartDownload_f, "begins sending a file to the client (network protocol use only)");

	Cvar_RegisterVariable (&sv_disablenotify);
	Cvar_RegisterVariable (&coop);
	Cvar_RegisterVariable (&deathmatch);
	Cvar_RegisterVariable (&fraglimit);
	Cvar_RegisterVariable (&gamecfg);
	Cvar_RegisterVariable (&noexit);
	Cvar_RegisterVariable (&nomonsters);
	Cvar_RegisterVariable (&pausable);
	Cvar_RegisterVariable (&pr_checkextension);
	Cvar_RegisterVariable (&samelevel);
	Cvar_RegisterVariable (&skill);
	Cvar_RegisterVariable (&host_timescale);
	Cvar_RegisterVariable (&sv_accelerate);
	Cvar_RegisterVariable (&sv_aim);
	Cvar_RegisterVariable (&sv_airaccel_qw);
	Cvar_RegisterVariable (&sv_airaccel_qw_stretchfactor);
	Cvar_RegisterVariable (&sv_airaccel_sideways_friction);
	Cvar_RegisterVariable (&sv_airaccelerate);
	Cvar_RegisterVariable (&sv_airstopaccelerate);
	Cvar_RegisterVariable (&sv_airstrafeaccelerate);
	Cvar_RegisterVariable (&sv_maxairstrafespeed);
	Cvar_RegisterVariable (&sv_airstrafeaccel_qw);
	Cvar_RegisterVariable (&sv_airspeedlimit_nonqw);
	Cvar_RegisterVariable (&sv_aircontrol);
	Cvar_RegisterVariable (&sv_aircontrol_power);
	Cvar_RegisterVariable (&sv_aircontrol_penalty);
	Cvar_RegisterVariable (&sv_allowdownloads);
	Cvar_RegisterVariable (&sv_allowdownloads_archive);
	Cvar_RegisterVariable (&sv_allowdownloads_config);
	Cvar_RegisterVariable (&sv_allowdownloads_dlcache);
	Cvar_RegisterVariable (&sv_allowdownloads_inarchive);
	Cvar_RegisterVariable (&sv_areagrid_mingridsize);
	Cvar_RegisterVariable (&sv_checkforpacketsduringsleep);
	Cvar_RegisterVariable (&sv_clmovement_enable);
	Cvar_RegisterVariable (&sv_clmovement_minping);
	Cvar_RegisterVariable (&sv_clmovement_minping_disabletime);
	Cvar_RegisterVariable (&sv_clmovement_inputtimeout);
	Cvar_RegisterVariable (&sv_cullentities_nevercullbmodels);
	Cvar_RegisterVariable (&sv_cullentities_pvs);
	Cvar_RegisterVariable (&sv_cullentities_stats);
	Cvar_RegisterVariable (&sv_cullentities_trace);
	Cvar_RegisterVariable (&sv_cullentities_trace_delay);
	Cvar_RegisterVariable (&sv_cullentities_trace_delay_players);
	Cvar_RegisterVariable (&sv_cullentities_trace_enlarge);
	Cvar_RegisterVariable (&sv_cullentities_trace_expand);
	Cvar_RegisterVariable (&sv_cullentities_trace_eyejitter);
	Cvar_RegisterVariable (&sv_cullentities_trace_entityocclusion);
	Cvar_RegisterVariable (&sv_cullentities_trace_prediction);
	Cvar_RegisterVariable (&sv_cullentities_trace_prediction_time);
	Cvar_RegisterVariable (&sv_cullentities_trace_samples);
	Cvar_RegisterVariable (&sv_cullentities_trace_samples_extra);
	Cvar_RegisterVariable (&sv_cullentities_trace_samples_players);
	Cvar_RegisterVariable (&sv_debugmove);
	Cvar_RegisterVariable (&sv_echobprint);
	Cvar_RegisterVariable (&sv_edgefriction);
	Cvar_RegisterVariable (&sv_entpatch);
	Cvar_RegisterVariable (&host_limitlocal);
	Cvar_RegisterVariable (&sv_freezenonclients);
	Cvar_RegisterVariable (&sv_friction);
	Cvar_RegisterVariable (&sv_gameplayfix_blowupfallenzombies);
	Cvar_RegisterVariable (&sv_gameplayfix_consistentplayerprethink);
	Cvar_RegisterVariable (&sv_gameplayfix_delayprojectiles);
	Cvar_RegisterVariable (&sv_gameplayfix_droptofloorstartsolid);
	Cvar_RegisterVariable (&sv_gameplayfix_droptofloorstartsolid_nudgetocorrect);
	Cvar_RegisterVariable (&sv_gameplayfix_easierwaterjump);
	Cvar_RegisterVariable (&sv_gameplayfix_findradiusdistancetobox);
	Cvar_RegisterVariable (&sv_gameplayfix_gravityunaffectedbyticrate);
	Cvar_RegisterVariable (&sv_gameplayfix_grenadebouncedownslopes);
	Cvar_RegisterVariable (&sv_gameplayfix_multiplethinksperframe);
	Cvar_RegisterVariable (&sv_gameplayfix_noairborncorpse);
	Cvar_RegisterVariable (&sv_gameplayfix_noairborncorpse_allowsuspendeditems);
	Cvar_RegisterVariable (&sv_gameplayfix_nudgeoutofsolid);
	Cvar_RegisterVariable (&sv_gameplayfix_nudgeoutofsolid_separation);
	Cvar_RegisterVariable (&sv_gameplayfix_q2airaccelerate);
	Cvar_RegisterVariable (&sv_gameplayfix_nogravityonground);
	Cvar_RegisterVariable (&sv_gameplayfix_setmodelrealbox);
	Cvar_RegisterVariable (&sv_gameplayfix_slidemoveprojectiles);
	Cvar_RegisterVariable (&sv_gameplayfix_stepdown);
	Cvar_RegisterVariable (&sv_gameplayfix_stepmultipletimes);
	Cvar_RegisterVariable (&sv_gameplayfix_nostepmoveonsteepslopes);
	Cvar_RegisterVariable (&sv_gameplayfix_swiminbmodels);
	Cvar_RegisterVariable (&sv_gameplayfix_upwardvelocityclearsongroundflag);
	Cvar_RegisterVariable (&sv_gameplayfix_downtracesupportsongroundflag);
	Cvar_RegisterVariable (&sv_gameplayfix_q1bsptracelinereportstexture);
	Cvar_RegisterVariable (&sv_gameplayfix_unstickplayers);
	Cvar_RegisterVariable (&sv_gameplayfix_unstickentities);
	Cvar_RegisterVariable (&sv_gameplayfix_fixedcheckwatertransition);
	Cvar_RegisterVariable (&sv_gravity);
	Cvar_RegisterVariable (&sv_init_frame_count);
	Cvar_RegisterVariable (&sv_idealpitchscale);
	Cvar_RegisterVariable (&sv_jumpstep);
	Cvar_RegisterVariable (&sv_jumpvelocity);
	Cvar_RegisterVariable (&sv_maxairspeed);
	Cvar_RegisterVariable (&sv_maxrate);
	Cvar_RegisterVariable (&sv_maxspeed);
	Cvar_RegisterVariable (&sv_maxvelocity);
	Cvar_RegisterVariable (&sv_nostep);
	Cvar_RegisterVariable (&sv_playerphysicsqc);
	Cvar_RegisterVariable (&sv_progs);
	Cvar_RegisterVariable (&sv_protocol);
	Cvar_RegisterVariable (&sv_random_seed);
	Cvar_RegisterVariable (&sv_ratelimitlocalplayer);
	Cvar_RegisterVariable (&sv_sound_land);
	Cvar_RegisterVariable (&sv_sound_watersplash);
	Cvar_RegisterVariable (&sv_stepheight);
	Cvar_RegisterVariable (&sv_stopspeed);
	Cvar_RegisterVariable (&sv_wallfriction);
	Cvar_RegisterVariable (&sv_wateraccelerate);
	Cvar_RegisterVariable (&sv_waterfriction);
	Cvar_RegisterVariable (&sv_warsowbunny_airforwardaccel);
	Cvar_RegisterVariable (&sv_warsowbunny_accel);
	Cvar_RegisterVariable (&sv_warsowbunny_topspeed);
	Cvar_RegisterVariable (&sv_warsowbunny_turnaccel);
	Cvar_RegisterVariable (&sv_warsowbunny_backtosideratio);
	Cvar_RegisterVariable (&sv_onlycsqcnetworking);
	Cvar_RegisterVariable (&sv_areadebug);
	Cvar_RegisterVariable (&sys_ticrate);
	Cvar_RegisterVariable (&teamplay);
	Cvar_RegisterVariable (&timelimit);
	Cvar_RegisterVariable (&sv_threaded);

	Cvar_RegisterVariable (&saved1);
	Cvar_RegisterVariable (&saved2);
	Cvar_RegisterVariable (&saved3);
	Cvar_RegisterVariable (&saved4);
	Cvar_RegisterVariable (&savedgamecfg);
	Cvar_RegisterVariable (&scratch1);
	Cvar_RegisterVariable (&scratch2);
	Cvar_RegisterVariable (&scratch3);
	Cvar_RegisterVariable (&scratch4);
	Cvar_RegisterVariable (&temp1);

	// LordHavoc: Nehahra uses these to pass data around cutscene demos
	Cvar_RegisterVariable (&nehx00);
	Cvar_RegisterVariable (&nehx01);
	Cvar_RegisterVariable (&nehx02);
	Cvar_RegisterVariable (&nehx03);
	Cvar_RegisterVariable (&nehx04);
	Cvar_RegisterVariable (&nehx05);
	Cvar_RegisterVariable (&nehx06);
	Cvar_RegisterVariable (&nehx07);
	Cvar_RegisterVariable (&nehx08);
	Cvar_RegisterVariable (&nehx09);
	Cvar_RegisterVariable (&nehx10);
	Cvar_RegisterVariable (&nehx11);
	Cvar_RegisterVariable (&nehx12);
	Cvar_RegisterVariable (&nehx13);
	Cvar_RegisterVariable (&nehx14);
	Cvar_RegisterVariable (&nehx15);
	Cvar_RegisterVariable (&nehx16);
	Cvar_RegisterVariable (&nehx17);
	Cvar_RegisterVariable (&nehx18);
	Cvar_RegisterVariable (&nehx19);
	Cvar_RegisterVariable (&cutscene); // for Nehahra but useful to other mods as well

	Cvar_RegisterVariable (&sv_autodemo_perclient);
	Cvar_RegisterVariable (&sv_autodemo_perclient_nameformat);
	Cvar_RegisterVariable (&sv_autodemo_perclient_discardable);

	Cvar_RegisterVariable (&halflifebsp);
	Cvar_RegisterVariable (&sv_mapformat_is_quake2);
	Cvar_RegisterVariable (&sv_mapformat_is_quake3);

	sv_mempool = Mem_AllocPool("server", 0, NULL);
	Event_Push(&on_host_drop, (void*)SV_Shutdown);
}

extern cvar_t cl_maxphysicsframesperserverframe; //FIXME
void SV_Frame(void)
{
	// limit the frametime steps to no more than 100ms each
	if ((host.state & H_SS_ACTIVE) && host.sv_timer > 0 && !svs.threaded)
	{
		// receive packets on each main loop iteration, as the main loop may
		// be undersleeping due to select() detecting a new packet
		NetConn_ServerFrame();

		// execute one or more server frames, with an upper limit on how much
		// execution time to spend on server frames to avoid freezing the game if
		// the server is overloaded, this execution time limit means the game will
		// slow down if the server is taking too long.
		int framecount, framelimit = 1;
		double advancetime, aborttime = 0;
		float offset;
		prvm_prog_t *prog = SVVM_prog;

		// run the world state
		// don't allow simulation to run too fast or too slow or logic glitches can occur

		// stop running server frames if the wall time reaches this value
		if (sys_ticrate.value <= 0)
		{
			advancetime = host.sv_timer;
		}
		else if ((host.state & H_SS_LOCALGAME) && !host_limitlocal.integer)
		{
			// synchronize to the client frametime, but no less than 10ms and no more than 100ms
			advancetime = bound(0.01, host.cl_timer, 0.1);
		}
		else
		{
			advancetime = sys_ticrate.value;
			// listen servers can run multiple server frames per client frame
			framelimit = cl_maxphysicsframesperserverframe.integer;
			aborttime = Sys_DirtyTime() + 0.1;
		}

		if(host_timescale.value > 0 && host_timescale.value < 1)
		{
			advancetime = min(advancetime, 0.1 / host_timescale.value);
		}
		else
		{
			advancetime = min(advancetime, 0.1);
		}

		if(advancetime > 0)
		{
			offset = Sys_DirtyTime() - host.dirtytime;if (offset < 0 || offset >= 1800) offset = 0;
			offset += host.sv_timer;
			++svs.perf_acc_offset_samples;
			svs.perf_acc_offset += offset;
			svs.perf_acc_offset_squared += offset * offset;

			if(svs.perf_acc_offset_max < offset)
			{
				svs.perf_acc_offset_max = offset;
			}
		}

		// only advance time if not paused
		// the game also pauses in singleplayer when menu or console is used
		sv.frametime = advancetime * host_timescale.value;

		if (host_framerate.value)
		{
			sv.frametime = host_framerate.value;
		}

		// TODO: The server doesn't need to know what the client is doing directly.
		// Perhaps a host.paused that gets set when any of these client-side conditions are met, and it checks
		// that instead?
		if (sv.paused || ((host.state & H_SS_LOCALGAME) && (key_dest != key_game || key_consoleactive || cl.paused)))
		{
			sv.frametime = 0;
		}

		for (framecount = 0;framecount < framelimit && host.sv_timer > 0;framecount++)
		{
			host.sv_timer -= advancetime;

			// move things around and think unless paused
			if (sv.frametime)
			{
				SV_Physics();
			}

			// if this server frame took too long, break out of the loop
			if (framelimit > 1 && Sys_DirtyTime() >= aborttime)
			{
				break;
			}
		}

#ifndef SERVER
		R_TimeReport("serverphysics");
#endif
		// send all messages to the clients
		SV_SendClientMessages();

		if (sv.paused == 1 && host.realtime > sv.pausedstart && sv.pausedstart > 0)
		{
			prog->globals.fp[OFS_PARM0] = host.realtime - sv.pausedstart;
			PRVM_serverglobalfloat(time) = sv.time;
			prog->ExecuteProgram(prog, PRVM_serverfunction(SV_PausedTic), "QC function SV_PausedTic is missing");
		}

		// send an heartbeat if enough time has passed since the last one
		NetConn_Heartbeat(0);
#ifndef SERVER
		R_TimeReport("servernetwork");
#endif
	}
	else if (!svs.threaded)
	{
		// don't let r_speeds display jump around
#ifndef SERVER
		R_TimeReport("serverphysics");
		R_TimeReport("servernetwork");
#endif
	}
}

/*
==================
SV_Shutdown

This only happens at the end of a game, not between levels
==================
*/
void SV_Shutdown(void)
{
	prvm_prog_t *prog = SVVM_prog;
	int i;

	Con_DPrintf("SV_Shutdown\n");

	if (!(host.state & H_SS_ACTIVE))
	{
		return;
	}

	NetConn_Heartbeat(2);
	NetConn_Heartbeat(2);

	// make sure all the clients know we're disconnecting
	World_End(&sv.world);
	if(prog->loaded)
	{
		if(PRVM_serverfunction(SV_Shutdown))
		{
			func_t s = PRVM_serverfunction(SV_Shutdown);
			PRVM_serverglobalfloat(time) = sv.time;
			PRVM_serverfunction(SV_Shutdown) = 0; // prevent it from getting called again
			prog->ExecuteProgram(prog, s,"SV_Shutdown() required");
		}
	}

	for (i = 0, host_client = svs.clients;i < svs.maxclients;i++, host_client++)
	{
		if (host_client->active)
		{
			SV_DropClient(false,"Server shutting down"); // server shutdown
		}
	}

	NetConn_CloseServerPorts();

	host.state &= ~H_SS_ACTIVE;

	// clear structures
	memset(&sv, 0, sizeof(sv));
	memset(svs.clients, 0, svs.maxclients*sizeof(client_t));

	host.state &= ~H_SS_LOCALGAME;
}

void SV_SaveEntFile_f(void)
{
	char vabuf[1024];
	if (!(host.state & H_SS_ACTIVE) || !sv.worldmodel)
	{
		Con_Print("Not running a server\n");
		return;
	}
	FS_WriteFile(va(vabuf, sizeof(vabuf), "%s.ent", sv.worldnamenoextension), sv.worldmodel->brush.entities, (fs_offset_t)strlen(sv.worldmodel->brush.entities));
}



/*
================
SV_ConnectClient

Initializes a client_t for a new net connection.  This will only be called
once for a player each game, not once for each level change.
================
*/
void SV_ConnectClient (int clientnum, netconn_t *netconnection)
{
	prvm_prog_t *prog = SVVM_prog;
	client_t		*client;
	int				i;

	client = svs.clients + clientnum;

// set up the client_t
	if (sv.loadgame)
	{
		float backupparms[NUM_SPAWN_PARMS];
		memcpy(backupparms, client->spawn_parms, sizeof(backupparms));
		memset(client, 0, sizeof(*client));
		memcpy(client->spawn_parms, backupparms, sizeof(backupparms));
	}
	else
		memset(client, 0, sizeof(*client));
	client->active = true;
	client->netconnection = netconnection;

	Con_DPrintf("Client connecting (%s)\n", client->netconnection ? client->netconnection->address : "botclient");

	if(client->netconnection && client->netconnection->crypto.authenticated)
	{
		Con_Printf("%s connection to %s has been established: client is %s@%s%.*s, I am %.*s@%s%.*s\n",
				client->netconnection->crypto.use_aes ? "Encrypted" : "Authenticated",
				client->netconnection->address,
				client->netconnection->crypto.client_idfp[0] ? client->netconnection->crypto.client_idfp : "-",
				(client->netconnection->crypto.client_issigned || !client->netconnection->crypto.client_keyfp[0]) ? "" : "~",
				crypto_keyfp_recommended_length, client->netconnection->crypto.client_keyfp[0] ? client->netconnection->crypto.client_keyfp : "-",
				crypto_keyfp_recommended_length, client->netconnection->crypto.server_idfp[0] ? client->netconnection->crypto.server_idfp : "-",
				(client->netconnection->crypto.server_issigned || !client->netconnection->crypto.server_keyfp[0]) ? "" : "~",
				crypto_keyfp_recommended_length, client->netconnection->crypto.server_keyfp[0] ? client->netconnection->crypto.server_keyfp : "-"
				);
	}

	strlcpy(client->name, "unconnected", sizeof(client->name));
	strlcpy(client->old_name, "unconnected", sizeof(client->old_name));
	client->prespawned = false;
	client->spawned = false;
	client->begun = false;
	client->edict = PRVM_EDICT_NUM(clientnum+1);
	if (client->netconnection)
		client->netconnection->message.allowoverflow = true;		// we can catch it
	// prepare the unreliable message buffer
	client->unreliablemsg.data = client->unreliablemsg_data;
	client->unreliablemsg.maxsize = sizeof(client->unreliablemsg_data);
	// updated by receiving "rate" command from client, this is also the default if not using a DP client
	client->rate = 1000000000;
	client->connecttime = host.realtime;

	if (!sv.loadgame)
	{
		// call the progs to get default spawn parms for the new client
		// set self to world to intentionally cause errors with broken SetNewParms code in some mods
		PRVM_serverglobalfloat(time) = sv.time;
		PRVM_serverglobaledict(self) = 0;
		prog->ExecuteProgram(prog, PRVM_serverfunction(SetNewParms), "QC function SetNewParms is missing");
		for (i=0 ; i<NUM_SPAWN_PARMS ; i++)
			client->spawn_parms[i] = (&PRVM_serverglobalfloat(parm1))[i];

		// set up the entity for this client (including .colormap, .team, etc)
		PRVM_ED_ClearEdict(prog, client->edict);
	}

	// don't call SendServerinfo for a fresh botclient because its fields have
	// not been set up by the qc yet
	if (client->netconnection)
		SV_SendServerinfo (client);
	else
		client->prespawned = client->spawned = client->begun = true;
}

/*
=====================
SV_DropClient

Called when the player is getting totally kicked off the host
if (crash = true), don't bother sending signofs
=====================
*/
void SV_DropClient(qboolean crash, const char *reason, ... )
{
	prvm_prog_t *prog = SVVM_prog;
	int i;

	SV_StopDemoRecording(host_client);

	// make sure edict is not corrupt (from a level change for example)
	host_client->edict = PRVM_EDICT_NUM(host_client - svs.clients + 1);

	// if the client is still connected, we're probably booting them for some reason
	if (host_client->netconnection)
	{
		// It's not a crash, so tell the client to go away
		if (!crash)
		{
			// Prepare buffer for unreliable message
			unsigned char bufdata[8+strlen(reason)];
			sizebuf_t buf;
			memset(&buf, 0, sizeof(buf));
			buf.data = bufdata;
			buf.maxsize = sizeof(bufdata);

			// Write the disconnect signal into the buffer
			MSG_WriteByte(&buf, svc_disconnect);

			if (sv.protocol == &protocol_horsepower1)
			{
				// Send the reason off with the disconnect signal.

				// CAUTION: May result in interpersonal conflict if
				// the reason is personal and the circumstances are right.
				// Cloudwalk claims no responsibility for lost friends.

				MSG_WriteString(&buf, reason);
			}
			else
			{
				SV_ClientPrintf("%s\n",reason);
			}

			// LordHavoc: no opportunity for resending, so use unreliable 3 times
			NetConn_SendUnreliableMessage(host_client->netconnection, &buf, sv.protocol, 10000, 0, false);
			NetConn_SendUnreliableMessage(host_client->netconnection, &buf, sv.protocol, 10000, 0, false);
			NetConn_SendUnreliableMessage(host_client->netconnection, &buf, sv.protocol, 10000, 0, false);

			// Break the net connection
			NetConn_Close(host_client->netconnection);
			host_client->netconnection = NULL;
		}
	}

	// call qc ClientDisconnect function
	// LordHavoc: don't call QC if server is dead (avoids recursive
	// Host_Error in some mods when they run out of edicts)
	if (host_client->clientconnectcalled && (host.state & H_SS_ACTIVE) && host_client->edict)
	{
		// call the prog function for removing a client
		// this will set the body to a dead frame, among other things
		int saveSelf = PRVM_serverglobaledict(self);
		host_client->clientconnectcalled = false;
		PRVM_serverglobalfloat(time) = sv.time;
		PRVM_serverglobaledict(self) = PRVM_EDICT_TO_PROG(host_client->edict);
		prog->ExecuteProgram(prog, PRVM_serverfunction(ClientDisconnect), "QC function ClientDisconnect is missing");
		PRVM_serverglobaledict(self) = saveSelf;
	}

	// if a download is active, close it
	if (host_client->download_file)
	{
		FS_Close(host_client->download_file);
		host_client->download_file = NULL;
		host_client->download_name[0] = 0;
		host_client->download_expectedposition = 0;
		host_client->download_started = false;
		Con_DPrintf("Download of %s aborted when %s dropped\n", host_client->download_name, host_client->name);
	}

	// Let the console know
	Con_Printf("Dropped %s from server (%s)\n", host_client->name,reason);

	// Let the players know
	SV_BroadcastPrintf("\001%s left the game (%s)\n",host_client->name,reason);

	// remove leaving player from scoreboard
	host_client->name[0] = 0;
	host_client->colors = 0;
	host_client->frags = 0;

	// send notification to all clients
	// get number of client manually just to make sure we get it right...
	i = host_client - svs.clients;
	MSG_WriteByte (&sv.reliable_datagram, svc_updatename);
	MSG_WriteByte (&sv.reliable_datagram, i);
	MSG_WriteString (&sv.reliable_datagram, host_client->name);
	MSG_WriteByte (&sv.reliable_datagram, svc_updatecolors);
	MSG_WriteByte (&sv.reliable_datagram, i);
	MSG_WriteByte (&sv.reliable_datagram, host_client->colors);
	MSG_WriteByte (&sv.reliable_datagram, svc_updatefrags);
	MSG_WriteByte (&sv.reliable_datagram, i);
	MSG_WriteShort (&sv.reliable_datagram, host_client->frags);

	// free the client now
	if (host_client->entitydatabase) {EntityFrame_FreeDatabase(host_client->entitydatabase);}
	if (host_client->entitydatabase4) {EntityFrame4_FreeDatabase(host_client->entitydatabase4);}
	if (host_client->entitydatabase5) {EntityFrame5_FreeDatabase(host_client->entitydatabase5);}

	if (host.state & H_SS_ACTIVE)
	{
		// clear a fields that matter to DP_SV_CLIENTNAME and DP_SV_CLIENTCOLORS, and also frags
		PRVM_ED_ClearEdict(prog, host_client->edict);
	}

	// clear the client struct (this sets active to false)
	memset(host_client, 0, sizeof(*host_client));

	// update server listing on the master because player count changed
	// (which the master uses for filtering empty/full servers)
	NetConn_Heartbeat(1);

	if (sv.loadgame)
	{
		for (i = 0; i < svs.maxclients; i++)
		{
			if (svs.clients[i].active && !svs.clients[i].spawned)
			{
				break;
			}
		}
		if (i == svs.maxclients)
		{
			Con_Printf("Loaded game, everyone rejoined - unpausing\n");
			sv.paused = sv.loadgame = false; // we're basically done with loading now
		}
	}
}

/*
==============================================================================

SERVER SPAWNING

==============================================================================
*/

/*
================
SV_ModelIndex

================
*/
int SV_ModelIndex(const char *s, int precachemode)
{
	int i, limit = sv.protocol->maxmodels;
	char filename[MAX_QPATH];
	if (!s || !*s)
		return 0;
	// testing
	//if (precachemode == 2)
	//	return 0;
	strlcpy(filename, s, sizeof(filename));
	for (i = 2;i < limit;i++)
	{
		if (!sv.model_precache[i][0])
		{
			if (precachemode)
			{
				if (!(host.state & H_SS_LOADING) && (sv.protocol == &protocol_netquake || sv.protocol == &protocol_quakedp || sv.protocol == &protocol_nehahramovie || sv.protocol == &protocol_nehahrabjp || sv.protocol == &protocol_nehahrabjp2 || sv.protocol == &protocol_nehahrabjp3 || sv.protocol == &protocol_darkplaces1 || sv.protocol == &protocol_darkplaces2 || sv.protocol == &protocol_darkplaces3 || sv.protocol == &protocol_darkplaces4 || sv.protocol == &protocol_darkplaces5))
				{
					Con_Printf("SV_ModelIndex(\"%s\"): precache_model can only be done in spawn functions\n", filename);
					return 0;
				}
				if (precachemode == 1)
					Con_Printf("SV_ModelIndex(\"%s\"): not precached (fix your code), precaching anyway\n", filename);
				strlcpy(sv.model_precache[i], filename, sizeof(sv.model_precache[i]));
				if (host.state & H_SS_LOADING)
				{
					// running from SV_SpawnServer which is launched from the client console command interpreter
					sv.models[i] = Mod_ForName (sv.model_precache[i], true, false, s[0] == '*' ? sv.worldname : NULL);
				}
				else
				{
					if (svs.threaded)
					{
						// this is running on the server thread, we can't load a model here (it would crash on renderer calls), so only look it up, the svc_precache will cause it to be loaded when it reaches the client
						sv.models[i] = Mod_FindName (sv.model_precache[i], s[0] == '*' ? sv.worldname : NULL);
					}
					else
					{
						// running single threaded, so we can load the model here
						sv.models[i] = Mod_ForName (sv.model_precache[i], true, false, s[0] == '*' ? sv.worldname : NULL);
					}
					MSG_WriteByte(&sv.reliable_datagram, svc_precache);
					MSG_WriteShort(&sv.reliable_datagram, i);
					MSG_WriteString(&sv.reliable_datagram, filename);
				}
				return i;
			}
			Con_Printf("SV_ModelIndex(\"%s\"): not precached\n", filename);
			return 0;
		}
		if (!strcmp(sv.model_precache[i], filename))
			return i;
	}
	Con_Printf("SV_ModelIndex(\"%s\"): i (%i) == MAX_MODELS (%i)\n", filename, i, MAX_MODELS);
	return 0;
}

/*
================
SV_SoundIndex

================
*/
int SV_SoundIndex(const char *s, int precachemode)
{
	int i, limit = ((sv.protocol == &protocol_netquake || sv.protocol == &protocol_quakedp || sv.protocol == &protocol_nehahramovie || sv.protocol == &protocol_nehahrabjp || sv.protocol == &protocol_nehahrabjp2 || sv.protocol == &protocol_nehahrabjp3) ? 256 : MAX_SOUNDS);
	char filename[MAX_QPATH];
	if (!s || !*s)
		return 0;
	// testing
	//if (precachemode == 2)
	//	return 0;
	strlcpy(filename, s, sizeof(filename));
	for (i = 1;i < limit;i++)
	{
		if (!sv.sound_precache[i][0])
		{
			if (precachemode)
			{
				if (!(host.state & H_SS_LOADING) && (sv.protocol == &protocol_netquake || sv.protocol == &protocol_quakedp || sv.protocol == &protocol_nehahramovie || sv.protocol == &protocol_nehahrabjp || sv.protocol == &protocol_nehahrabjp2 || sv.protocol == &protocol_nehahrabjp3 || sv.protocol == &protocol_darkplaces1 || sv.protocol == &protocol_darkplaces2 || sv.protocol == &protocol_darkplaces3 || sv.protocol == &protocol_darkplaces4 || sv.protocol == &protocol_darkplaces5))
				{
					Con_Printf("SV_SoundIndex(\"%s\"): precache_sound can only be done in spawn functions\n", filename);
					return 0;
				}
				if (precachemode == 1)
					Con_Printf("SV_SoundIndex(\"%s\"): not precached (fix your code), precaching anyway\n", filename);
				strlcpy(sv.sound_precache[i], filename, sizeof(sv.sound_precache[i]));
				if (!(host.state & H_SS_LOADING))
				{
					MSG_WriteByte(&sv.reliable_datagram, svc_precache);
					MSG_WriteShort(&sv.reliable_datagram, i + 32768);
					MSG_WriteString(&sv.reliable_datagram, filename);
				}
				return i;
			}
			Con_Printf("SV_SoundIndex(\"%s\"): not precached\n", filename);
			return 0;
		}
		if (!strcmp(sv.sound_precache[i], filename))
			return i;
	}
	Con_Printf("SV_SoundIndex(\"%s\"): i (%i) == MAX_SOUNDS (%i)\n", filename, i, MAX_SOUNDS);
	return 0;
}

/*
================
SV_ParticleEffectIndex

================
*/
int SV_ParticleEffectIndex(const char *name)
{
	int i, argc, linenumber, effectnameindex;
	int filepass;
	fs_offset_t filesize;
	unsigned char *filedata;
	const char *text;
	const char *textstart;
	//const char *textend;
	char argv[16][1024];
	char filename[MAX_QPATH];
	if (!sv.particleeffectnamesloaded)
	{
		sv.particleeffectnamesloaded = true;
		memset(sv.particleeffectname, 0, sizeof(sv.particleeffectname));
		for (i = 0;i < EFFECT_TOTAL;i++)
			strlcpy(sv.particleeffectname[i], standardeffectnames[i], sizeof(sv.particleeffectname[i]));
		for (filepass = 0;;filepass++)
		{
			if (filepass == 0)
				dpsnprintf(filename, sizeof(filename), "effectinfo.txt");
			else if (filepass == 1)
				dpsnprintf(filename, sizeof(filename), "%s_effectinfo.txt", sv.worldnamenoextension);
			else
				break;
			filedata = FS_LoadFile(filename, tempmempool, true, &filesize);
			if (!filedata)
				continue;
			textstart = (const char *)filedata;
			//textend = (const char *)filedata + filesize;
			text = textstart;
			for (linenumber = 1;;linenumber++)
			{
				argc = 0;
				for (;;)
				{
					if (!COM_ParseToken_Simple(&text, true, false, true) || !strcmp(com_token, "\n"))
						break;
					if (argc < 16)
					{
						strlcpy(argv[argc], com_token, sizeof(argv[argc]));
						argc++;
					}
				}
				if (com_token[0] == 0)
					break; // if the loop exited and it's not a \n, it's EOF
				if (argc < 1)
					continue;
				if (!strcmp(argv[0], "effect"))
				{
					if (argc == 2)
					{
						for (effectnameindex = 1;effectnameindex < MAX_PARTICLEEFFECTNAME;effectnameindex++)
						{
							if (sv.particleeffectname[effectnameindex][0])
							{
								if (!strcmp(sv.particleeffectname[effectnameindex], argv[1]))
									break;
							}
							else
							{
								strlcpy(sv.particleeffectname[effectnameindex], argv[1], sizeof(sv.particleeffectname[effectnameindex]));
								break;
							}
						}
						// if we run out of names, abort
						if (effectnameindex == MAX_PARTICLEEFFECTNAME)
						{
							Con_Printf("%s:%i: too many effects!\n", filename, linenumber);
							break;
						}
					}
				}
			}
			Mem_Free(filedata);
		}
	}
	// search for the name
	for (effectnameindex = 1;effectnameindex < MAX_PARTICLEEFFECTNAME && sv.particleeffectname[effectnameindex][0];effectnameindex++)
		if (!strcmp(sv.particleeffectname[effectnameindex], name))
			return effectnameindex;
	// return 0 if we couldn't find it
	return 0;
}

dp_model_t *SV_GetModelByIndex(int modelindex)
{
	return (modelindex > 0 && modelindex < MAX_MODELS) ? sv.models[modelindex] : NULL;
}

dp_model_t *SV_GetModelFromEdict(prvm_edict_t *ed)
{
	prvm_prog_t *prog = SVVM_prog;
	int modelindex;
	if (!ed || ed->priv.server->free)
		return NULL;
	modelindex = (int)PRVM_serveredictfloat(ed, modelindex);
	return (modelindex > 0 && modelindex < MAX_MODELS) ? sv.models[modelindex] : NULL;
}

/*
================
SV_CreateBaseline

================
*/
static void SV_CreateBaseline (void)
{
	prvm_prog_t *prog = SVVM_prog;
	int i, entnum, large;
	prvm_edict_t *svent;

	// LordHavoc: clear *all* baselines (not just active ones)
	for (entnum = 0;entnum < prog->max_edicts;entnum++)
	{
		// get the current server version
		svent = PRVM_EDICT_NUM(entnum);

		// LordHavoc: always clear state values, whether the entity is in use or not
		svent->priv.server->baseline = defaultstate;

		if (svent->priv.server->free)
			continue;
		if (entnum > svs.maxclients && !PRVM_serveredictfloat(svent, modelindex))
			continue;

		// create entity baseline
		VectorCopy (PRVM_serveredictvector(svent, origin), svent->priv.server->baseline.origin);
		VectorCopy (PRVM_serveredictvector(svent, angles), svent->priv.server->baseline.angles);
		svent->priv.server->baseline.frame = (int)PRVM_serveredictfloat(svent, frame);
		svent->priv.server->baseline.skin = (int)PRVM_serveredictfloat(svent, skin);
		if (entnum > 0 && entnum <= svs.maxclients)
		{
			svent->priv.server->baseline.colormap = entnum;
			svent->priv.server->baseline.modelindex = SV_ModelIndex("progs/player.mdl", 1);
		}
		else
		{
			svent->priv.server->baseline.colormap = 0;
			svent->priv.server->baseline.modelindex = (int)PRVM_serveredictfloat(svent, modelindex);
		}

		large = false;
		if (svent->priv.server->baseline.modelindex & 0xFF00 || svent->priv.server->baseline.frame & 0xFF00)
		{
			large = true;
			if (sv.protocol == &protocol_nehahrabjp || sv.protocol == &protocol_nehahrabjp2 || sv.protocol == &protocol_nehahrabjp3)
				large = false;
		}

		// add to the message
		if (large)
			MSG_WriteByte (&sv.signon, svc_spawnbaseline2);
		else
			MSG_WriteByte (&sv.signon, svc_spawnbaseline);
		MSG_WriteShort (&sv.signon, entnum);

		if (large)
		{
			MSG_WriteShort (&sv.signon, svent->priv.server->baseline.modelindex);
			MSG_WriteShort (&sv.signon, svent->priv.server->baseline.frame);
		}
		else if (sv.protocol == &protocol_nehahrabjp || sv.protocol == &protocol_nehahrabjp2 || sv.protocol == &protocol_nehahrabjp3)
		{
			MSG_WriteShort (&sv.signon, svent->priv.server->baseline.modelindex);
			MSG_WriteByte (&sv.signon, svent->priv.server->baseline.frame);
		}
		else
		{
			MSG_WriteByte (&sv.signon, svent->priv.server->baseline.modelindex);
			MSG_WriteByte (&sv.signon, svent->priv.server->baseline.frame);
		}
		MSG_WriteByte (&sv.signon, svent->priv.server->baseline.colormap);
		MSG_WriteByte (&sv.signon, svent->priv.server->baseline.skin);
		for (i=0 ; i<3 ; i++)
		{
			MSG_WriteCoord(&sv.signon, svent->priv.server->baseline.origin[i], sv.protocol);
			MSG_WriteAngle(&sv.signon, svent->priv.server->baseline.angles[i], sv.protocol);
		}
	}
}

/*
================
SV_Prepare_CSQC

Load csprogs.dat and comperss it so it doesn't need to be
reloaded on request.
================
*/
static void SV_Prepare_CSQC(void)
{
	fs_offset_t progsize;

	if(svs.csqc_progdata)
	{
		Con_DPrintf("Unloading old CSQC data.\n");
		Mem_Free(svs.csqc_progdata);
		if(svs.csqc_progdata_deflated)
			Mem_Free(svs.csqc_progdata_deflated);
	}

	svs.csqc_progdata = NULL;
	svs.csqc_progdata_deflated = NULL;
	
	sv.csqc_progname[0] = 0;
	svs.csqc_progdata = FS_LoadFile(csqc_progname.string, sv_mempool, false, &progsize);

	if(progsize > 0)
	{
		size_t deflated_size;

		sv.csqc_progsize = (int)progsize;
		sv.csqc_progcrc = CRC_Block(svs.csqc_progdata, progsize);
		strlcpy(sv.csqc_progname, csqc_progname.string, sizeof(sv.csqc_progname));
		Con_DPrintf("server detected csqc progs file \"%s\" with size %i and crc %i\n", sv.csqc_progname, sv.csqc_progsize, sv.csqc_progcrc);

		Con_DPrint("Compressing csprogs.dat\n");
		//unsigned char *FS_Deflate(const unsigned char *data, size_t size, size_t *deflated_size, int level, mempool_t *mempool);
		svs.csqc_progdata_deflated = FS_Deflate(svs.csqc_progdata, progsize, &deflated_size, -1, sv_mempool);
		svs.csqc_progsize_deflated = (int)deflated_size;
		if(svs.csqc_progdata_deflated)
		{
			Con_DPrintf("Deflated: %g%%\n", 100.0 - 100.0 * (deflated_size / (float)progsize));
			Con_DPrintf("Uncompressed: %u\nCompressed:   %u\n", (unsigned)sv.csqc_progsize, (unsigned)svs.csqc_progsize_deflated);
		}
		else
			Con_DPrintf("Cannot compress - need zlib for this. Using uncompressed progs only.\n");
	}
}

/*
================
SV_SaveSpawnparms

Grabs the current state of each client for saving across the
transition to another level
================
*/
void SV_SaveSpawnparms (void)
{
	prvm_prog_t *prog = SVVM_prog;
	int		i, j;

	svs.serverflags = (int)PRVM_serverglobalfloat(serverflags);

	for (i = 0, host_client = svs.clients;i < svs.maxclients;i++, host_client++)
	{
		if (!host_client->active)
			continue;

	// call the progs to get default spawn parms for the new client
		PRVM_serverglobalfloat(time) = sv.time;
		PRVM_serverglobaledict(self) = PRVM_EDICT_TO_PROG(host_client->edict);
		prog->ExecuteProgram(prog, PRVM_serverfunction(SetChangeParms), "QC function SetChangeParms is missing");
		for (j=0 ; j<NUM_SPAWN_PARMS ; j++)
			host_client->spawn_parms[j] = (&PRVM_serverglobalfloat(parm1))[j];
	}
}

/*
================
SV_SpawnServer

This is called at the start of each level
================
*/

void SV_SpawnServer (const char *server)
{
	prvm_prog_t *prog = SVVM_prog;
	prvm_edict_t *ent;
	int i;
	char *entities;
	dp_model_t *worldmodel;
	char modelname[sizeof(sv.worldname)];
	char vabuf[1024];

	Con_DPrintf("SpawnServer: %s\n", server);

	dpsnprintf (modelname, sizeof(modelname), "maps/%s.bsp", server);

	if (!FS_FileExists(modelname))
	{
		dpsnprintf (modelname, sizeof(modelname), "maps/%s", server);
		if (!FS_FileExists(modelname))
		{
			Con_Printf("No map file named maps/%s.bsp\n", server);
			return;
		}
	}

//	SV_LockThreadMutex();

	if(host.state & H_DEDICATED)
		Sys_MakeProcessNice();

	if(host.state & H_SS_ACTIVE)
	{
		World_End(&sv.world);
		if(PRVM_serverfunction(SV_Shutdown))
		{
			func_t s = PRVM_serverfunction(SV_Shutdown);
			PRVM_serverglobalfloat(time) = sv.time;
			PRVM_serverfunction(SV_Shutdown) = 0; // prevent it from getting called again
			prog->ExecuteProgram(prog, s,"SV_Shutdown() required");
		}
	}

	// free q3 shaders so that any newly downloaded shaders will be active
	Mod_FreeQ3Shaders();

	worldmodel = Mod_ForName(modelname, false, developer.integer > 0, NULL);
	if (!worldmodel || !worldmodel->TraceBox)
	{
		Con_Printf("Couldn't load map %s\n", modelname);

		if(host.state & H_DEDICATED)
			Sys_MakeProcessMean();

//		SV_UnlockThreadMutex();

		return;
	}

	Collision_Cache_Reset(true);

	// let's not have any servers with no name
	if (hostname.string[0] == 0)
		Cvar_Set ("hostname", "UNNAMED");
	//scr_centertime_off = 0;

	svs.changelevel_issued = false;		// now safe to issue another

	// make the map a required file for clients
	Curl_ClearRequirements();
	Curl_RequireFile(modelname);

//
// tell all connected clients that we are going to a new level
//
	if (host.state & H_SS_ACTIVE)
	{
		client_t *client;
		for (i = 0, client = svs.clients;i < svs.maxclients;i++, client++)
		{
			if (client->netconnection)
			{
				MSG_WriteByte(&client->netconnection->message, svc_stufftext);
				MSG_WriteString(&client->netconnection->message, "reconnect\n");
			}
		}
	}
	else
	{
		// open server port
		NetConn_OpenServerPorts(true);
	}

//
// make cvars consistant
//
	if (coop.integer)
		Cvar_SetValue ("deathmatch", 0);
	// LordHavoc: it can be useful to have skills outside the range 0-3...
	//current_skill = bound(0, (int)(skill.value + 0.5), 3);
	//Cvar_SetValue ("skill", (float)current_skill);
	current_skill = (int)(skill.value + 0.5);

//
// set up the new server
//
	memset (&sv, 0, sizeof(sv));

	Cvar_SetValue("halflifebsp", worldmodel->brush.ishlbsp);
	Cvar_SetValue("sv_mapformat_is_quake2", worldmodel->brush.isq2bsp);
	Cvar_SetValue("sv_mapformat_is_quake3", worldmodel->brush.isq3bsp);

	if(*sv_random_seed.string)
	{
		srand(sv_random_seed.integer);
		Con_Printf("NOTE: random seed is %d; use for debugging/benchmarking only!\nUnset sv_random_seed to get real random numbers again.\n", sv_random_seed.integer);
	}

	Protocol_GetByName(sv_protocol.string,(&sv.protocol));
	if (!sv.protocol)
	{
		char buffer[1024];
		Protocol_Names(buffer, sizeof(buffer));
		Con_Printf("Unknown sv_protocol \"%s\", valid values are:\n%s\n", sv_protocol.string, buffer);
		Protocol_GetByName("QUAKE",(&sv.protocol));
	}

	SV_VM_Init();

	host.state |= H_SS_ACTIVE;

	// set level base name variables for later use
	strlcpy (sv.name, server, sizeof (sv.name));
	strlcpy(sv.worldname, modelname, sizeof(sv.worldname));
	FS_StripExtension(sv.worldname, sv.worldnamenoextension, sizeof(sv.worldnamenoextension));
	strlcpy(sv.worldbasename, !strncmp(sv.worldnamenoextension, "maps/", 5) ? sv.worldnamenoextension + 5 : sv.worldnamenoextension, sizeof(sv.worldbasename));
	//Cvar_SetQuick(&sv_worldmessage, sv.worldmessage); // set later after QC is spawned
	Cvar_SetQuick(&sv_worldname, sv.worldname);
	Cvar_SetQuick(&sv_worldnamenoextension, sv.worldnamenoextension);
	Cvar_SetQuick(&sv_worldbasename, sv.worldbasename);

// load progs to get entity field count
	//PR_LoadProgs ( sv_progs.string );

	sv.datagram.maxsize = sizeof(sv.datagram_buf);
	sv.datagram.cursize = 0;
	sv.datagram.data = sv.datagram_buf;

	sv.reliable_datagram.maxsize = sizeof(sv.reliable_datagram_buf);
	sv.reliable_datagram.cursize = 0;
	sv.reliable_datagram.data = sv.reliable_datagram_buf;

	sv.signon.maxsize = sizeof(sv.signon_buf);
	sv.signon.cursize = 0;
	sv.signon.data = sv.signon_buf;

// leave slots at start for clients only
	//prog->num_edicts = svs.maxclients+1;

	host.state |= H_SS_LOADING;
	prog->allowworldwrites = true;
	sv.paused = false;

	sv.time = 1.0;

	Mod_ClearUsed();
	worldmodel->used = true;

	sv.worldmodel = worldmodel;
	sv.models[1] = sv.worldmodel;

//
// clear world interaction links
//
	World_SetSize(&sv.world, sv.worldname, sv.worldmodel->normalmins, sv.worldmodel->normalmaxs, prog);
	World_Start(&sv.world);

	strlcpy(sv.sound_precache[0], "", sizeof(sv.sound_precache[0]));

	strlcpy(sv.model_precache[0], "", sizeof(sv.model_precache[0]));
	strlcpy(sv.model_precache[1], sv.worldname, sizeof(sv.model_precache[1]));
	for (i = 1;i < sv.worldmodel->brush.numsubmodels && i+1 < MAX_MODELS;i++)
	{
		dpsnprintf(sv.model_precache[i+1], sizeof(sv.model_precache[i+1]), "*%i", i);
		sv.models[i+1] = Mod_ForName (sv.model_precache[i+1], false, false, sv.worldname);
	}
	if(i < sv.worldmodel->brush.numsubmodels)
		Con_Printf("Too many submodels (MAX_MODELS is %i)\n", MAX_MODELS);

//
// load the rest of the entities
//
	// AK possible hack since num_edicts is still 0
	ent = PRVM_EDICT_NUM(0);
	memset (ent->fields.fp, 0, prog->entityfields * sizeof(prvm_vec_t));
	ent->priv.server->free = false;
	PRVM_serveredictstring(ent, model) = PRVM_SetEngineString(prog, sv.worldname);
	PRVM_serveredictfloat(ent, modelindex) = 1;		// world model
	PRVM_serveredictfloat(ent, solid) = SOLID_BSP;
	PRVM_serveredictfloat(ent, movetype) = MOVETYPE_PUSH;
	VectorCopy(sv.world.mins, PRVM_serveredictvector(ent, mins));
	VectorCopy(sv.world.maxs, PRVM_serveredictvector(ent, maxs));
	VectorCopy(sv.world.mins, PRVM_serveredictvector(ent, absmin));
	VectorCopy(sv.world.maxs, PRVM_serveredictvector(ent, absmax));

	if (coop.value)
		PRVM_serverglobalfloat(coop) = coop.integer;
	else
		PRVM_serverglobalfloat(deathmatch) = deathmatch.integer;

	PRVM_serverglobalstring(mapname) = PRVM_SetEngineString(prog, sv.name);

// serverflags are for cross level information (sigils)
	PRVM_serverglobalfloat(serverflags) = svs.serverflags;

	// we need to reset the spawned flag on all connected clients here so that
	// their thinks don't run during startup (before PutClientInServer)
	// we also need to set up the client entities now
	// and we need to set the ->edict pointers to point into the progs edicts
	for (i = 0, host_client = svs.clients;i < svs.maxclients;i++, host_client++)
	{
		host_client->begun = false;
		host_client->edict = PRVM_EDICT_NUM(i + 1);
		PRVM_ED_ClearEdict(prog, host_client->edict);
	}

	// load replacement entity file if found
	if (sv_entpatch.integer && (entities = (char *)FS_LoadFile(va(vabuf, sizeof(vabuf), "%s.ent", sv.worldnamenoextension), tempmempool, true, NULL)))
	{
		Con_Printf("Loaded %s.ent\n", sv.worldnamenoextension);
		PRVM_ED_LoadFromFile(prog, entities);
		Mem_Free(entities);
	}
	else
		PRVM_ED_LoadFromFile(prog, sv.worldmodel->brush.entities);


	// LordHavoc: clear world angles (to fix e3m3.bsp)
	VectorClear(PRVM_serveredictvector(prog->edicts, angles));

// all setup is completed, any further precache statements are errors
//	sv.state = ss_active; // LordHavoc: workaround for svc_precache bug
	prog->allowworldwrites = false;

// run two frames to allow everything to settle
	sv.time = 1.0001;
	for (i = 0;i < sv_init_frame_count.integer;i++)
	{
		sv.frametime = 0.1;
		SV_Physics ();
	}

	// Once all init frames have been run, we consider svqc code fully initialized.
	prog->inittime = host.realtime;

	if (host.state & H_DEDICATED)
		Mod_PurgeUnused();

// create a baseline for more efficient communications
	if (sv.protocol == &protocol_netquake || sv.protocol == &protocol_quakedp || sv.protocol == &protocol_nehahramovie || sv.protocol == &protocol_nehahrabjp || sv.protocol == &protocol_nehahrabjp2 || sv.protocol == &protocol_nehahrabjp3)
		SV_CreateBaseline ();

	//host.state |= H_SS_ACTIVE; // LordHavoc: workaround for svc_precache bug

// send serverinfo to all connected clients, and set up botclients coming back from a level change
	for (i = 0, host_client = svs.clients;i < svs.maxclients;i++, host_client++)
	{
		host_client->clientconnectcalled = false; // do NOT call ClientDisconnect if he drops before ClientConnect!
		if (!host_client->active)
			continue;
		if (host_client->netconnection)
			SV_SendServerinfo(host_client);
		else
		{
			int j;
			// if client is a botclient coming from a level change, we need to
			// set up client info that normally requires networking

			// copy spawn parms out of the client_t
			for (j=0 ; j< NUM_SPAWN_PARMS ; j++)
				(&PRVM_serverglobalfloat(parm1))[j] = host_client->spawn_parms[j];

			// call the spawn function
			host_client->clientconnectcalled = true;
			PRVM_serverglobalfloat(time) = sv.time;
			PRVM_serverglobaledict(self) = PRVM_EDICT_TO_PROG(host_client->edict);
			prog->ExecuteProgram(prog, PRVM_serverfunction(ClientConnect), "QC function ClientConnect is missing");
			prog->ExecuteProgram(prog, PRVM_serverfunction(PutClientInServer), "QC function PutClientInServer is missing");
			host_client->begun = true;
		}
	}

	// update the map title cvar
	strlcpy(sv.worldmessage, PRVM_GetString(prog, PRVM_serveredictstring(prog->edicts, message)), sizeof(sv.worldmessage)); // map title (not related to filename)
	Cvar_SetQuick(&sv_worldmessage, sv.worldmessage);

	Con_Printf("Server spawned.\n");
	NetConn_Heartbeat (2);

	if(host.state & H_DEDICATED)
		Sys_MakeProcessMean();

//	SV_UnlockThreadMutex();
	host.state &= ~H_SS_LOADING;
}

/////////////////////////////////////////////////////
// SV VM stuff

static void SVVM_begin_increase_edicts(prvm_prog_t *prog)
{
	// links don't survive the transition, so unlink everything
	World_UnlinkAll(&sv.world);
}

static void SVVM_end_increase_edicts(prvm_prog_t *prog)
{
	int i;
	prvm_edict_t *ent;

	// link every entity except world
	for (i = 1, ent = prog->edicts;i < prog->num_edicts;i++, ent++)
		if (!ent->priv.server->free)
			SV_LinkEdict(ent);
}

static void SVVM_init_edict(prvm_prog_t *prog, prvm_edict_t *e)
{
	// LordHavoc: for consistency set these here
	int num = PRVM_NUM_FOR_EDICT(e) - 1;

	e->priv.server->move = false; // don't move on first frame

	if (num >= 0 && num < svs.maxclients)
	{
		// set colormap and team on newly created player entity
		PRVM_serveredictfloat(e, colormap) = num + 1;
		PRVM_serveredictfloat(e, team) = (svs.clients[num].colors & 15) + 1;
		// set netname/clientcolors back to client values so that
		// DP_SV_CLIENTNAME and DP_SV_CLIENTCOLORS will not immediately
		// reset them
		PRVM_serveredictstring(e, netname) = PRVM_SetEngineString(prog, svs.clients[num].name);
		PRVM_serveredictfloat(e, clientcolors) = svs.clients[num].colors;
		// NEXUIZ_PLAYERMODEL and NEXUIZ_PLAYERSKIN
		PRVM_serveredictstring(e, playermodel) = PRVM_SetEngineString(prog, svs.clients[num].playermodel);
		PRVM_serveredictstring(e, playerskin) = PRVM_SetEngineString(prog, svs.clients[num].playerskin);
		// Assign netaddress (IP Address, etc)
		if(svs.clients[num].netconnection != NULL)
		{
			// Acquire Readable Address
			LHNETADDRESS_ToString(&svs.clients[num].netconnection->peeraddress, svs.clients[num].netaddress, sizeof(svs.clients[num].netaddress), false);
			PRVM_serveredictstring(e, netaddress) = PRVM_SetEngineString(prog, svs.clients[num].netaddress);
		}
		else
			PRVM_serveredictstring(e, netaddress) = PRVM_SetEngineString(prog, "null/botclient");
		if(svs.clients[num].netconnection != NULL && svs.clients[num].netconnection->crypto.authenticated && svs.clients[num].netconnection->crypto.client_idfp[0])
			PRVM_serveredictstring(e, crypto_idfp) = PRVM_SetEngineString(prog, svs.clients[num].netconnection->crypto.client_idfp);
		else
			PRVM_serveredictstring(e, crypto_idfp) = 0;
		PRVM_serveredictfloat(e, crypto_idfp_signed) = (svs.clients[num].netconnection != NULL && svs.clients[num].netconnection->crypto.authenticated && svs.clients[num].netconnection->crypto.client_issigned);
		if(svs.clients[num].netconnection != NULL && svs.clients[num].netconnection->crypto.authenticated && svs.clients[num].netconnection->crypto.client_keyfp[0])
			PRVM_serveredictstring(e, crypto_keyfp) = PRVM_SetEngineString(prog, svs.clients[num].netconnection->crypto.client_keyfp);
		else
			PRVM_serveredictstring(e, crypto_keyfp) = 0;
		if(svs.clients[num].netconnection != NULL && svs.clients[num].netconnection->crypto.authenticated && svs.clients[num].netconnection->crypto.server_keyfp[0])
			PRVM_serveredictstring(e, crypto_mykeyfp) = PRVM_SetEngineString(prog, svs.clients[num].netconnection->crypto.server_keyfp);
		else
			PRVM_serveredictstring(e, crypto_mykeyfp) = 0;
		if(svs.clients[num].netconnection != NULL && svs.clients[num].netconnection->crypto.authenticated && svs.clients[num].netconnection->crypto.use_aes)
			PRVM_serveredictstring(e, crypto_encryptmethod) = PRVM_SetEngineString(prog, "AES128");
		else
			PRVM_serveredictstring(e, crypto_encryptmethod) = 0;
		if(svs.clients[num].netconnection != NULL && svs.clients[num].netconnection->crypto.authenticated)
			PRVM_serveredictstring(e, crypto_signmethod) = PRVM_SetEngineString(prog, "HMAC-SHA256");
		else
			PRVM_serveredictstring(e, crypto_signmethod) = 0;
	}
}

static void SVVM_free_edict(prvm_prog_t *prog, prvm_edict_t *ed)
{
	int i;
	int e;

	World_UnlinkEdict(ed);		// unlink from world bsp

	PRVM_serveredictstring(ed, model) = 0;
	PRVM_serveredictfloat(ed, takedamage) = 0;
	PRVM_serveredictfloat(ed, modelindex) = 0;
	PRVM_serveredictfloat(ed, colormap) = 0;
	PRVM_serveredictfloat(ed, skin) = 0;
	PRVM_serveredictfloat(ed, frame) = 0;
	VectorClear(PRVM_serveredictvector(ed, origin));
	VectorClear(PRVM_serveredictvector(ed, angles));
	PRVM_serveredictfloat(ed, nextthink) = -1;
	PRVM_serveredictfloat(ed, solid) = 0;

	VM_RemoveEdictSkeleton(prog, ed);
	World_Physics_RemoveFromEntity(&sv.world, ed);
	World_Physics_RemoveJointFromEntity(&sv.world, ed);

	// make sure csqc networking is aware of the removed entity
	e = PRVM_NUM_FOR_EDICT(ed);
	sv.csqcentityversion[e] = 0;
	for (i = 0;i < svs.maxclients;i++)
		svs.clients[i].csqcentitysendflags[e] = 0xFFFFFF;
}

static void SVVM_count_edicts(prvm_prog_t *prog)
{
	int		i;
	prvm_edict_t	*ent;
	int		active, models, solid, step;

	active = models = solid = step = 0;
	for (i=0 ; i<prog->num_edicts ; i++)
	{
		ent = PRVM_EDICT_NUM(i);
		if (ent->priv.server->free)
			continue;
		active++;
		if (PRVM_serveredictfloat(ent, solid))
			solid++;
		if (PRVM_serveredictstring(ent, model))
			models++;
		if (PRVM_serveredictfloat(ent, movetype) == MOVETYPE_STEP)
			step++;
	}

	Con_Printf("num_edicts:%3i\n", prog->num_edicts);
	Con_Printf("active    :%3i\n", active);
	Con_Printf("view      :%3i\n", models);
	Con_Printf("touch     :%3i\n", solid);
	Con_Printf("step      :%3i\n", step);
}

static qboolean SVVM_load_edict(prvm_prog_t *prog, prvm_edict_t *ent)
{
	// remove things from different skill levels or deathmatch
	if (gamemode != GAME_TRANSFUSION) //Transfusion does this in QC
	{
		if (deathmatch.integer)
		{
			if (((int)PRVM_serveredictfloat(ent, spawnflags) & SPAWNFLAG_NOT_DEATHMATCH))
			{
				return false;
			}
		}
		else if ((current_skill <= 0 && ((int)PRVM_serveredictfloat(ent, spawnflags) & SPAWNFLAG_NOT_EASY  ))
			|| (current_skill == 1 && ((int)PRVM_serveredictfloat(ent, spawnflags) & SPAWNFLAG_NOT_MEDIUM))
			|| (current_skill >= 2 && ((int)PRVM_serveredictfloat(ent, spawnflags) & SPAWNFLAG_NOT_HARD  )))
		{
			return false;
		}
	}
	return true;
}

void SV_VM_Init(void)
{
	prvm_prog_t *prog = SVVM_prog;
	PRVM_Prog_Init(prog);
	// allocate the mempools
	// TODO: move the magic numbers/constants into #defines [9/13/2006 Black]
	prog->progs_mempool = Mem_AllocPool("Server Progs", 0, NULL);
	prog->builtins = vm_sv_builtins;
	prog->numbuiltins = vm_sv_numbuiltins;
	prog->max_edicts = 512;
	prog->limit_edicts = sv.protocol->maxedicts;
	prog->reserved_edicts = svs.maxclients;
	prog->edictprivate_size = sizeof(edict_engineprivate_t);
	prog->name = "server";
	prog->extensionstring = vm_sv_extensions;
	prog->loadintoworld = true;

	// all callbacks must be defined (pointers are not checked before calling)
	prog->begin_increase_edicts = SVVM_begin_increase_edicts;
	prog->end_increase_edicts   = SVVM_end_increase_edicts;
	prog->init_edict            = SVVM_init_edict;
	prog->free_edict            = SVVM_free_edict;
	prog->count_edicts          = SVVM_count_edicts;
	prog->load_edict            = SVVM_load_edict;
	prog->init_cmd              = SVVM_init_cmd;
	prog->reset_cmd             = SVVM_reset_cmd;
	prog->error_cmd             = Host_Error;
	prog->ExecuteProgram        = SVVM_ExecuteProgram;

	PRVM_Prog_Load(prog, sv_progs.string, NULL, 0, SV_REQFUNCS, sv_reqfuncs, SV_REQFIELDS, sv_reqfields, SV_REQGLOBALS, sv_reqglobals);

	// some mods compiled with scrambling compilers lack certain critical
	// global names and field names such as "self" and "time" and "nextthink"
	// so we have to set these offsets manually, matching the entvars_t
	// but we only do this if the prog header crc matches, otherwise it's totally freeform
	if (prog->progs_crc == PROGHEADER_CRC || prog->progs_crc == PROGHEADER_CRC_TENEBRAE)
	{
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, modelindex);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, absmin);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, absmax);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, ltime);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, movetype);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, solid);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, origin);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, oldorigin);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, velocity);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, angles);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, avelocity);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, punchangle);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, classname);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, model);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, frame);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, skin);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, effects);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, mins);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, maxs);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, size);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, touch);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, use);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, think);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, blocked);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, nextthink);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, groundentity);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, health);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, frags);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, weapon);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, weaponmodel);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, weaponframe);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, currentammo);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, ammo_shells);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, ammo_nails);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, ammo_rockets);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, ammo_cells);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, items);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, takedamage);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, chain);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, deadflag);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, view_ofs);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, button0);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, button1);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, button2);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, impulse);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, fixangle);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, v_angle);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, idealpitch);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, netname);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, enemy);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, flags);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, colormap);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, team);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, max_health);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, teleport_time);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, armortype);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, armorvalue);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, waterlevel);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, watertype);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, ideal_yaw);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, yaw_speed);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, aiment);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, goalentity);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, spawnflags);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, target);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, targetname);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, dmg_take);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, dmg_save);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, dmg_inflictor);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, owner);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, movedir);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, message);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, sounds);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, noise);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, noise1);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, noise2);
		PRVM_ED_FindFieldOffset_FromStruct(entvars_t, noise3);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, self);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, other);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, world);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, time);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, frametime);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, force_retouch);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, mapname);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, deathmatch);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, coop);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, teamplay);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, serverflags);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, total_secrets);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, total_monsters);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, found_secrets);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, killed_monsters);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm1);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm2);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm3);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm4);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm5);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm6);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm7);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm8);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm9);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm10);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm11);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm12);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm13);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm14);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm15);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, parm16);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, v_forward);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, v_up);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, v_right);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, trace_allsolid);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, trace_startsolid);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, trace_fraction);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, trace_endpos);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, trace_plane_normal);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, trace_plane_dist);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, trace_ent);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, trace_inopen);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, trace_inwater);
		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, msg_entity);
//		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, main);
//		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, StartFrame);
//		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, PlayerPreThink);
//		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, PlayerPostThink);
//		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, ClientKill);
//		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, ClientConnect);
//		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, PutClientInServer);
//		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, ClientDisconnect);
//		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, SetNewParms);
//		PRVM_ED_FindGlobalOffset_FromStruct(globalvars_t, SetChangeParms);
	}
	else
		Con_DPrintf("%s: %s system vars have been modified (CRC %i != engine %i), will not load in other engines\n", prog->name, sv_progs.string, prog->progs_crc, PROGHEADER_CRC);

	// OP_STATE is always supported on server because we add fields/globals for it
	prog->flag |= PRVM_OP_STATE;

	VM_CustomStats_Clear();//[515]: csqc

	SV_Prepare_CSQC();
}

extern cvar_t host_maxwait;
extern cvar_t host_framerate;

static int SV_ThreadFunc(void *voiddata)
{
	prvm_prog_t *prog = SVVM_prog;
	qboolean playing = false;
	double sv_timer = 0;
	double sv_deltarealtime, sv_oldrealtime, sv_realtime;
	double wait;
	int i;
	char vabuf[1024];
	sv_realtime = Sys_DirtyTime();
	while (!svs.threadstop)
	{
		// FIXME: we need to handle Host_Error in the server thread somehow
//		if (setjmp(sv_abortframe))
//			continue;			// something bad happened in the server game

		sv_oldrealtime = sv_realtime;
		sv_realtime = Sys_DirtyTime();
		sv_deltarealtime = sv_realtime - sv_oldrealtime;
		if (sv_deltarealtime < 0 || sv_deltarealtime >= 1800) sv_deltarealtime = 0;

		sv_timer += sv_deltarealtime;

		svs.perf_acc_realtime += sv_deltarealtime;

		// at this point we start doing real server work, and must block on any client activity pertaining to the server (such as executing SV_SpawnServer)
		SV_LockThreadMutex();

		// Look for clients who have spawned
		playing = false;
		if (host.state & H_SS_ACTIVE)
			for (i = 0, host_client = svs.clients;i < svs.maxclients;i++, host_client++)
				if(host_client->begun)
					if(host_client->netconnection)
						playing = true;
		if(sv.time < 10)
		{
			// don't accumulate time for the first 10 seconds of a match
			// so things can settle
			svs.perf_acc_realtime = svs.perf_acc_sleeptime = svs.perf_acc_lost = svs.perf_acc_offset = svs.perf_acc_offset_squared = svs.perf_acc_offset_max = svs.perf_acc_offset_samples = 0;
		}
		else if(svs.perf_acc_realtime > 5)
		{
			svs.perf_cpuload = 1 - svs.perf_acc_sleeptime / svs.perf_acc_realtime;
			svs.perf_lost = svs.perf_acc_lost / svs.perf_acc_realtime;
			if(svs.perf_acc_offset_samples > 0)
			{
				svs.perf_offset_max = svs.perf_acc_offset_max;
				svs.perf_offset_avg = svs.perf_acc_offset / svs.perf_acc_offset_samples;
				svs.perf_offset_sdev = sqrt(svs.perf_acc_offset_squared / svs.perf_acc_offset_samples - svs.perf_offset_avg * svs.perf_offset_avg);
			}
			if(svs.perf_lost > 0 && developer_extra.integer)
				if(playing)
					Con_DPrintf("Server can't keep up: %s\n", Host_TimingReport(vabuf, sizeof(vabuf)));
			svs.perf_acc_realtime = svs.perf_acc_sleeptime = svs.perf_acc_lost = svs.perf_acc_offset = svs.perf_acc_offset_squared = svs.perf_acc_offset_max = svs.perf_acc_offset_samples = 0;
		}

		// get new packets
		if (host.state & H_SS_ACTIVE)
			NetConn_ServerFrame();

		// if the accumulators haven't become positive yet, wait a while
		wait = sv_timer * -1000000.0;
		if (wait >= 1)
		{
			double time0, delta;
			SV_UnlockThreadMutex(); // don't keep mutex locked while sleeping
			if (host_maxwait.value <= 0)
				wait = min(wait, 1000000.0);
			else
				wait = min(wait, host_maxwait.value * 1000.0);
			if(wait < 1)
				wait = 1; // because we cast to int
			time0 = Sys_DirtyTime();
			Sys_Sleep((int)wait);
			delta = Sys_DirtyTime() - time0;if (delta < 0 || delta >= 1800) delta = 0;
			svs.perf_acc_sleeptime += delta;
			continue;
		}

		if ((host.state & H_SS_ACTIVE) && sv_timer > 0)
		{
			// execute one server frame
			double advancetime;
			float offset;

			if (sys_ticrate.value <= 0)
				advancetime = min(sv_timer, 0.1); // don't step more than 100ms
			else
				advancetime = sys_ticrate.value;

			if(advancetime > 0)
			{
				offset = sv_timer + (Sys_DirtyTime() - sv_realtime); // LordHavoc: FIXME: I don't understand this line
				++svs.perf_acc_offset_samples;
				svs.perf_acc_offset += offset;
				svs.perf_acc_offset_squared += offset * offset;
				if(svs.perf_acc_offset_max < offset)
					svs.perf_acc_offset_max = offset;
			}

			// only advance time if not paused
			// the game also pauses in singleplayer when menu or console is used
			sv.frametime = advancetime * host_timescale.value;

			if (host_framerate.value)
			{
				sv.frametime = host_framerate.value;
			}

			// TODO: The server doesn't need to know what the client is doing directly.
			// Perhaps a host.paused that gets set when any of these client-side conditions are met, and it checks
			// that instead?
			if (sv.paused || ((host.state & H_SS_LOCALGAME) && (key_dest != key_game || key_consoleactive || cl.paused)))
			{
				sv.frametime = 0;
			}

			sv_timer -= advancetime;

			// move things around and think unless paused
			if (sv.frametime)
				SV_Physics();

			// send all messages to the clients
			SV_SendClientMessages();

			if (sv.paused == 1 && sv_realtime > sv.pausedstart && sv.pausedstart > 0)
			{
				PRVM_serverglobalfloat(time) = sv.time;
				prog->globals.fp[OFS_PARM0] = sv_realtime - sv.pausedstart;
				prog->ExecuteProgram(prog, PRVM_serverfunction(SV_PausedTic), "QC function SV_PausedTic is missing");
			}

			// send an heartbeat if enough time has passed since the last one
			NetConn_Heartbeat(0);

		}

		// we're back to safe code now
		SV_UnlockThreadMutex();

		// if there is some time remaining from this frame, reset the timers
		if (sv_timer >= 0)
		{
			svs.perf_acc_lost += sv_timer;
			sv_timer = 0;
		}
	}
	return 0;
}

void SV_StartThread(void)
{
	if (!sv_threaded.integer || !Thread_HasThreads())
		return;
	svs.threaded = true;
	svs.threadstop = false;
	svs.threadmutex = Thread_CreateMutex();
	svs.thread = Thread_CreateThread(SV_ThreadFunc, NULL);
}

void SV_StopThread(void)
{
	if (!svs.threaded)
		return;
	svs.threadstop = true;
	Thread_WaitThread(svs.thread, 0);
	Thread_DestroyMutex(svs.threadmutex);
	svs.threaded = false;
}

// Just gonna... shove this down here.
qboolean SV_CheatsAllowed(void)
{
	if(sv_cheats.integer)
	{
		return true;
	}
	return false;
}
