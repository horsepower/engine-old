#include "quakedef.h"

void netmsg_clc_nop(void);
void netmsg_clc_disconnect(void);
void netmsg_clc_move(void);
void netmsg_clc_stringcmd(void);

protocol_netmsg_t protocol_netquake_clc =
{
		netmsg_clc_nop,
		netmsg_clc_disconnect,
		netmsg_clc_move,
		netmsg_clc_stringcmd
};
