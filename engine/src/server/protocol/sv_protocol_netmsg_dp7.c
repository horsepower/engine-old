#include "quakedef.h"

void netmsg_clc_nop(void);
void netmsg_clc_disconnect(void);
void netmsg_clc_move(void);
void netmsg_clc_stringcmd(void);
void netmsg_clc_ackframe(void);
void netmsg_clc_ackdownloaddata(void);

protocol_netmsg_t protocol_darkplaces7_clc =
{
		NULL,
		netmsg_clc_nop,
		netmsg_clc_disconnect,
		netmsg_clc_move,
		netmsg_clc_stringcmd,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		netmsg_clc_ackframe,
		netmsg_clc_ackdownloaddata
};
