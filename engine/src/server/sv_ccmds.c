/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

// sv_ccmds.c - Server console and client commands

#include "quakedef.h"
#include "prvm_cmds.h"
#include "utf8lib.h"
extern cvar_t developer_entityparsing;
extern cvar_t sv_namechangetimer;
extern cvar_t sv_status_privacy;
extern cvar_t sv_status_show_qcstatus;
extern cvar_t sv_adminnick;

event_t on_sv_starting;
event_t on_sv_start;
event_t on_new_map;

qboolean noclip_anglehack;

/*
==================
SV_Status_f

Prints server information
to the console.
==================
*/
static void SV_Status_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	char qcstatus[256];
	client_t *client;
	int seconds = 0, minutes = 0, hours = 0, i, j, k, in, players, ping = 0, packetloss = 0;
	void (*print) (const char *fmt, ...);
	char ip[48]; // can contain a full length v6 address with [] and a port
	int frags;
	char vabuf[1024];

	if (cmd_source == src_command)
	{
		// if running a client, try to send over network so the client's status report parser will see the report
		if (host.state & H_CS_CONNECTED)
		{
			Cmd_ForwardToServer ();
			return;
		}
		print = Con_Printf;
	}
	else
		print = SV_ClientPrintf;

	if (!(host.state & H_SS_ACTIVE))
		return;

	in = 0;
	if (Cmd_Argc() == 2)
	{
		if (strcmp(Cmd_Argv(1), "1") == 0)
			in = 1;
		else if (strcmp(Cmd_Argv(1), "2") == 0)
			in = 2;
	}

	for (players = 0, i = 0;i < svs.maxclients;i++)
		if (svs.clients[i].active)
			players++;
	print ("host:     %s\n", Cvar_VariableString ("hostname"));
	print ("version:  %s build %s (gamename %s)\n", gamename, buildstring, gamenetworkfiltername);
	print ("protocol: %i (%s)\n", sv.protocol->num, sv.protocol->name);
	print ("map:      %s\n", sv.name);
	print ("timing:   %s\n", Host_TimingReport(vabuf, sizeof(vabuf)));
	print ("players:  %i active (%i max)\n\n", players, svs.maxclients);

	if (in == 1)
		print ("^2IP                                             %%pl ping  time   frags  no   name\n");
	else if (in == 2)
		print ("^5IP                                              no   name\n");

	for (i = 0, k = 0, client = svs.clients;i < svs.maxclients;i++, client++)
	{
		if (!client->active)
			continue;

		++k;

		if (in == 0 || in == 1)
		{
			seconds = (int)(host.realtime - client->connecttime);
			minutes = seconds / 60;
			if (minutes)
			{
				seconds -= (minutes * 60);
				hours = minutes / 60;
				if (hours)
					minutes -= (hours * 60);
			}
			else
				hours = 0;

			packetloss = 0;
			if (client->netconnection)
				for (j = 0;j < NETGRAPH_PACKETS;j++)
					if (client->netconnection->incoming_netgraph[j].unreliablebytes == NETGRAPH_LOSTPACKET)
						packetloss++;
			packetloss = (packetloss * 100 + NETGRAPH_PACKETS - 1) / NETGRAPH_PACKETS;
			ping = bound(0, (int)floor(client->ping*1000+0.5), 9999);
		}

		if(sv_status_privacy.integer && cmd_source != src_command)
			strlcpy(ip, client->netconnection ? "" : "botclient", 48);
		else
			strlcpy(ip, (client->netconnection && client->netconnection->address) ? client->netconnection->address : "botclient", 48);

		frags = client->frags;

		if(sv_status_show_qcstatus.integer)
		{
			prvm_edict_t *ed = PRVM_EDICT_NUM(i + 1);
			const char *str = PRVM_GetString(prog, PRVM_serveredictstring(ed, clientstatus));
			if(str && *str)
			{
				char *p;
				const char *q;
				p = qcstatus;
				for(q = str; *q && p != qcstatus + sizeof(qcstatus) - 1; ++q)
					if(*q != '\\' && *q != '"' && !ISWHITESPACE(*q))
						*p++ = *q;
				*p = 0;
				if(*qcstatus)
					frags = atoi(qcstatus);
			}
		}

		if (in == 0) // default layout
		{
			if (sv.protocol == &protocol_netquake && svs.maxclients <= 99)
			{
				// LordHavoc: this is very touchy because we must maintain ProQuake compatible status output
				print ("#%-2u %-16.16s  %3i  %2i:%02i:%02i\n", i+1, client->name, frags, hours, minutes, seconds);
				print ("   %s\n", ip);
			}
			else
			{
				// LordHavoc: no real restrictions here, not a ProQuake-compatible protocol anyway...
				print ("#%-3u %-16.16s %4i  %2i:%02i:%02i\n", i+1, client->name, frags, hours, minutes, seconds);
				print ("   %s\n", ip);
			}
		}
		else if (in == 1) // extended layout
		{
			print ("%s%-47s %2i %4i %2i:%02i:%02i %4i  #%-3u ^7%s\n", k%2 ? "^3" : "^7", ip, packetloss, ping, hours, minutes, seconds, frags, i+1, client->name);
		}
		else if (in == 2) // reduced layout
		{
			print ("%s%-47s #%-3u ^7%s\n", k%2 ? "^3" : "^7", ip, i+1, client->name);
		}
	}
}

/*
==================
SV_God_f

Makes client invulnerable
==================
*/
static void SV_God_f (void)
{
	prvm_prog_t *prog = SVVM_prog;

	PRVM_serveredictfloat(host_client->edict, flags) = (int)PRVM_serveredictfloat(host_client->edict, flags) ^ FL_GODMODE;
	if (!((int)PRVM_serveredictfloat(host_client->edict, flags) & FL_GODMODE) )
		SV_ClientPrint("godmode OFF\n");
	else
		SV_ClientPrint("godmode ON\n");
}

/*
==================
SV_Noclip_f

Sets client movetype to noclip
(flymode with incorporeality)
==================
*/
static void SV_Noclip_f (void)
{
	prvm_prog_t *prog = SVVM_prog;

	if (PRVM_serveredictfloat(host_client->edict, movetype) != MOVETYPE_NOCLIP)
	{
		noclip_anglehack = true;
		PRVM_serveredictfloat(host_client->edict, movetype) = MOVETYPE_NOCLIP;
		SV_ClientPrint("noclip ON\n");
	}
	else
	{
		noclip_anglehack = false;
		PRVM_serveredictfloat(host_client->edict, movetype) = MOVETYPE_WALK;
		SV_ClientPrint("noclip OFF\n");
	}
}

/*
==================
SV_Fly_f

Sets client to flymode
==================
*/
static void SV_Fly_f (void)
{
	prvm_prog_t *prog = SVVM_prog;

	if (PRVM_serveredictfloat(host_client->edict, movetype) != MOVETYPE_FLY)
	{
		PRVM_serveredictfloat(host_client->edict, movetype) = MOVETYPE_FLY;
		SV_ClientPrint("flymode ON\n");
	}
	else
	{
		PRVM_serveredictfloat(host_client->edict, movetype) = MOVETYPE_WALK;
		SV_ClientPrint("flymode OFF\n");
	}
}

static void SV_Notarget_f (void)
{
	prvm_prog_t *prog = SVVM_prog;

	PRVM_serveredictfloat(host_client->edict, flags) = (int)PRVM_serveredictfloat(host_client->edict, flags) ^ FL_NOTARGET;
	if (!((int)PRVM_serveredictfloat(host_client->edict, flags) & FL_NOTARGET) )
		SV_ClientPrint("notarget OFF\n");
	else
		SV_ClientPrint("notarget ON\n");
}

/*
======================
SV_Map_f

handle a
map <servername>
command from the console.  Active clients are kicked off.
======================
*/
static void SV_Map_f (void)
{
	char level[MAX_QPATH];

	if (Cmd_Argc() != 2)
	{
		Con_Print("map <levelname> : start a new game (kicks off all players)\n");
		return;
	}

	// GAME_DELUXEQUAKE - clear warpmark (used by QC)
	if (gamemode == GAME_DELUXEQUAKE)
		Cvar_Set("warpmark", "");

	if(host.state & H_SS_ACTIVE)
	{
		SV_Shutdown();
	}

	if(svs.maxclients != svs.maxclients_next)
	{
		svs.maxclients = svs.maxclients_next;
		if (svs.clients)
			Mem_Free(svs.clients);
		svs.clients = (client_t *)Mem_Alloc(sv_mempool, sizeof(client_t) * svs.maxclients);
	}

	svs.serverflags = 0;			// haven't completed an episode yet
	strlcpy(level, Cmd_Argv(1), sizeof(level));
	SV_SpawnServer(level);
	Event_Trigger(&on_sv_start,NULL);
}

/*
==================
SV_Changelevel_f

Goes to a new map, taking all clients along
==================
*/
static void SV_Changelevel_f (void)
{
	char level[MAX_QPATH];

	if (Cmd_Argc() != 2)
	{
		Con_Print("changelevel <levelname> : continue game on a new level\n");
		return;
	}

	if (!(host.state & H_SS_ACTIVE)) {
		Con_Printf("Can't changelevel. Server not running.");
		return;
	}

	SV_SaveSpawnparms ();
	strlcpy(level, Cmd_Argv(1), sizeof(level));
	SV_SpawnServer(level);

	Event_Trigger(&on_new_map,NULL);
}

/*
==================
SV_Restart_f

Restarts the current server for a dead player
==================
*/
static void SV_Restart_f (void)
{
	char mapname[MAX_QPATH];

	if (Cmd_Argc() != 1)
	{
		Con_Print("restart : restart current level\n");
		return;
	}
	if (!(host.state & H_SS_ACTIVE))
	{
		Con_Print("Only the server may restart\n");
		return;
	}

	strlcpy(mapname, sv.name, sizeof(mapname));
	SV_SpawnServer(mapname);

	Event_Trigger(&on_new_map,NULL);
}

/*
====================
SV_Pings_f

Send back ping and packet loss update for all current players to this player
====================
*/
static void SV_Pings_f (void)
{
	int		i, j, ping, packetloss, movementloss;
	char temp[128];

	if (!host_client->netconnection)
		return;

	if (sv.protocol != &protocol_quakeworld)
	{
		MSG_WriteByte(&host_client->netconnection->message, svc_stufftext);
		MSG_WriteUnterminatedString(&host_client->netconnection->message, "pingplreport");
	}
	for (i = 0;i < svs.maxclients;i++)
	{
		packetloss = 0;
		movementloss = 0;
		if (svs.clients[i].netconnection)
		{
			for (j = 0;j < NETGRAPH_PACKETS;j++)
				if (svs.clients[i].netconnection->incoming_netgraph[j].unreliablebytes == NETGRAPH_LOSTPACKET)
					packetloss++;
			for (j = 0;j < NETGRAPH_PACKETS;j++)
				if (svs.clients[i].movement_count[j] < 0)
					movementloss++;
		}
		packetloss = (packetloss * 100 + NETGRAPH_PACKETS - 1) / NETGRAPH_PACKETS;
		movementloss = (movementloss * 100 + NETGRAPH_PACKETS - 1) / NETGRAPH_PACKETS;
		ping = (int)floor(svs.clients[i].ping*1000+0.5);
		ping = bound(0, ping, 9999);
		if (sv.protocol == &protocol_quakeworld)
		{
			// send qw_svc_updateping and qw_svc_updatepl messages
			MSG_WriteByte(&host_client->netconnection->message, svc_hidelmp);
			MSG_WriteShort(&host_client->netconnection->message, ping);
			MSG_WriteByte(&host_client->netconnection->message, svc_effect2);
			MSG_WriteByte(&host_client->netconnection->message, packetloss);
		}
		else
		{
			// write the string into the packet as multiple unterminated strings to avoid needing a local buffer
			if(movementloss)
				dpsnprintf(temp, sizeof(temp), " %d %d,%d", ping, packetloss, movementloss);
			else
				dpsnprintf(temp, sizeof(temp), " %d %d", ping, packetloss);
			MSG_WriteUnterminatedString(&host_client->netconnection->message, temp);
		}
	}
	if (sv.protocol != &protocol_quakeworld)
		MSG_WriteString(&host_client->netconnection->message, "\n");
}

/*
==================
SV_Ping_f

==================
*/
static void SV_Ping_f (void)
{
	int i;
	client_t *client;
	void (*print) (const char *fmt, ...);

	if (cmd_source == src_command)
	{
		print = Con_Printf;
	}
	else
		print = SV_ClientPrintf;

	if (!(host.state & H_SS_ACTIVE))
		return;

	print("Client ping times:\n");
	for (i = 0, client = svs.clients;i < svs.maxclients;i++, client++)
	{
		if (!client->active)
			continue;
		print("%4i %s\n", bound(0, (int)floor(client->ping*1000+0.5), 9999), client->name);
	}

	// now call the Pings command also, which will send a report that contains packet loss for the scoreboard (as well as a simpler ping report)
	// actually, don't, it confuses old clients (resulting in "unknown command pingplreport" flooding the console)
	//SV_Pings_f();
}

/*
==================
SV_Give_f
==================
*/
static void SV_Give_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	const char *t;
	int v;

	t = Cmd_Argv(1);
	v = atoi (Cmd_Argv(2));

	switch (t[0])
	{
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		// MED 01/04/97 added hipnotic give stuff
		if (gamemode == GAME_HIPNOTIC || gamemode == GAME_QUOTH)
		{
			if (t[0] == '6')
			{
				if (t[1] == 'a')
					PRVM_serveredictfloat(host_client->edict, items) = (int)PRVM_serveredictfloat(host_client->edict, items) | HIT_PROXIMITY_GUN;
				else
					PRVM_serveredictfloat(host_client->edict, items) = (int)PRVM_serveredictfloat(host_client->edict, items) | IT_GRENADE_LAUNCHER;
			}
			else if (t[0] == '9')
				PRVM_serveredictfloat(host_client->edict, items) = (int)PRVM_serveredictfloat(host_client->edict, items) | HIT_LASER_CANNON;
			else if (t[0] == '0')
				PRVM_serveredictfloat(host_client->edict, items) = (int)PRVM_serveredictfloat(host_client->edict, items) | HIT_MJOLNIR;
			else if (t[0] >= '2')
				PRVM_serveredictfloat(host_client->edict, items) = (int)PRVM_serveredictfloat(host_client->edict, items) | (IT_SHOTGUN << (t[0] - '2'));
		}
		else
		{
			if (t[0] >= '2')
				PRVM_serveredictfloat(host_client->edict, items) = (int)PRVM_serveredictfloat(host_client->edict, items) | (IT_SHOTGUN << (t[0] - '2'));
		}
		break;

	case 's':
		if (gamemode == GAME_ROGUE)
			PRVM_serveredictfloat(host_client->edict, ammo_shells1) = v;

		PRVM_serveredictfloat(host_client->edict, ammo_shells) = v;
		break;
	case 'n':
		if (gamemode == GAME_ROGUE)
		{
			PRVM_serveredictfloat(host_client->edict, ammo_nails1) = v;
			if (PRVM_serveredictfloat(host_client->edict, weapon) <= IT_LIGHTNING)
				PRVM_serveredictfloat(host_client->edict, ammo_nails) = v;
		}
		else
		{
			PRVM_serveredictfloat(host_client->edict, ammo_nails) = v;
		}
		break;
	case 'l':
		if (gamemode == GAME_ROGUE)
		{
			PRVM_serveredictfloat(host_client->edict, ammo_lava_nails) = v;
			if (PRVM_serveredictfloat(host_client->edict, weapon) > IT_LIGHTNING)
				PRVM_serveredictfloat(host_client->edict, ammo_nails) = v;
		}
		break;
	case 'r':
		if (gamemode == GAME_ROGUE)
		{
			PRVM_serveredictfloat(host_client->edict, ammo_rockets1) = v;
			if (PRVM_serveredictfloat(host_client->edict, weapon) <= IT_LIGHTNING)
				PRVM_serveredictfloat(host_client->edict, ammo_rockets) = v;
		}
		else
		{
			PRVM_serveredictfloat(host_client->edict, ammo_rockets) = v;
		}
		break;
	case 'm':
		if (gamemode == GAME_ROGUE)
		{
			PRVM_serveredictfloat(host_client->edict, ammo_multi_rockets) = v;
			if (PRVM_serveredictfloat(host_client->edict, weapon) > IT_LIGHTNING)
				PRVM_serveredictfloat(host_client->edict, ammo_rockets) = v;
		}
		break;
	case 'h':
		PRVM_serveredictfloat(host_client->edict, health) = v;
		break;
	case 'c':
		if (gamemode == GAME_ROGUE)
		{
			PRVM_serveredictfloat(host_client->edict, ammo_cells1) = v;
			if (PRVM_serveredictfloat(host_client->edict, weapon) <= IT_LIGHTNING)
				PRVM_serveredictfloat(host_client->edict, ammo_cells) = v;
		}
		else
		{
			PRVM_serveredictfloat(host_client->edict, ammo_cells) = v;
		}
		break;
	case 'p':
		if (gamemode == GAME_ROGUE)
		{
			PRVM_serveredictfloat(host_client->edict, ammo_plasma) = v;
			if (PRVM_serveredictfloat(host_client->edict, weapon) > IT_LIGHTNING)
				PRVM_serveredictfloat(host_client->edict, ammo_cells) = v;
		}
		break;
	}
}

/*
==================
SV_Kick_f

Kicks a user off of the server
==================
*/
static void SV_Kick_f (void)
{
	const char *who;
	const char *message = NULL;
	client_t *save;
	int i;
	qboolean byNumber = false;

	if (!(host.state & H_SS_ACTIVE))
		return;

	save = host_client;

	if (Cmd_Argc() > 2 && strcmp(Cmd_Argv(1), "#") == 0)
	{
		i = (int)(atof(Cmd_Argv(2)) - 1);
		if (i < 0 || i >= svs.maxclients || !(host_client = svs.clients + i)->active)
			return;
		byNumber = true;
	}
	else
	{
		for (i = 0, host_client = svs.clients;i < svs.maxclients;i++, host_client++)
		{
			if (!host_client->active)
				continue;
			if (strcasecmp(host_client->name, Cmd_Argv(1)) == 0)
				break;
		}
	}

	if (i < svs.maxclients)
	{
		if (cmd_source == src_command)
		{
			if (host.state & H_DEDICATED)
				who = "Console";
			else
				who = cl_name.string;
		}
		else
			who = save->name;

		// can't kick yourself!
		if (host_client == save)
			return;

		if (Cmd_Argc() > 2)
		{
			message = Cmd_Args();
			COM_ParseToken_Simple(&message, false, false, true);
			if (byNumber)
			{
				message++;							// skip the #
				while (*message == ' ')				// skip white space
					message++;
				message += strlen(Cmd_Argv(2));	// skip the number
			}
			while (*message && *message == ' ')
				message++;
		}

		char reason[512];

		if (message)
		{
			dpsnprintf(reason,512,"Kicked by %s: %s",who,message);
		}
		else
		{
			dpsnprintf(reason,512,"Kicked by %s",who);
		}

		SV_DropClient(false,reason); // kicked
	}

	host_client = save;
}

static void SV_Download_f(void)
{
	const char *whichpack, *whichpack2, *extension;
	qboolean is_csqc; // so we need to check only once

	if (Cmd_Argc() < 2)
	{
		SV_ClientPrintf("usage: download <filename> {<extensions>}*\n");
		SV_ClientPrintf("       supported extensions: deflate\n");
		return;
	}

	if (FS_CheckNastyPath(Cmd_Argv(1), false))
	{
		SV_ClientPrintf("Download rejected: nasty filename \"%s\"\n", Cmd_Argv(1));
		return;
	}

	if (host_client->download_file)
	{
		// at this point we'll assume the previous download should be aborted
		Con_DPrintf("Download of %s aborted by %s starting a new download\n", host_client->download_name, host_client->name);
		SV_ClientCommands("\nstopdownload\n");

		// close the file and reset variables
		FS_Close(host_client->download_file);
		host_client->download_file = NULL;
		host_client->download_name[0] = 0;
		host_client->download_expectedposition = 0;
		host_client->download_started = false;
	}

	is_csqc = (sv.csqc_progname[0] && strcmp(Cmd_Argv(1), sv.csqc_progname) == 0);

	if (!sv_allowdownloads.integer && !is_csqc)
	{
		SV_ClientPrintf("Downloads are disabled on this server\n");
		SV_ClientCommands("\nstopdownload\n");
		return;
	}

	SV_Download_CheckExtensions();

	strlcpy(host_client->download_name, Cmd_Argv(1), sizeof(host_client->download_name));
	extension = FS_FileExtension(host_client->download_name);

	// host_client is asking to download a specified file
	if (developer_extra.integer)
		Con_DPrintf("Download request for %s by %s\n", host_client->download_name, host_client->name);

	if(is_csqc)
	{
		char extensions[MAX_QPATH]; // make sure this can hold all extensions
		extensions[0] = '\0';

		if(host_client->download_deflate)
			strlcat(extensions, " deflate", sizeof(extensions));

		Con_DPrintf("Downloading %s to %s\n", host_client->download_name, host_client->name);

		if(host_client->download_deflate && svs.csqc_progdata_deflated)
			host_client->download_file = FS_FileFromData(svs.csqc_progdata_deflated, svs.csqc_progsize_deflated, true);
		else
			host_client->download_file = FS_FileFromData(svs.csqc_progdata, sv.csqc_progsize, true);

		// no, no space is needed between %s and %s :P
		SV_ClientCommands("\ncl_downloadbegin %i %s%s\n", (int)FS_FileSize(host_client->download_file), host_client->download_name, extensions);

		host_client->download_expectedposition = 0;
		host_client->download_started = false;
		host_client->sendsignon = true; // make sure this message is sent
		return;
	}

	if (!FS_FileExists(host_client->download_name))
	{
		SV_ClientPrintf("Download rejected: server does not have the file \"%s\"\nYou may need to separately download or purchase the data archives for this game/mod to get this file\n", host_client->download_name);
		SV_ClientCommands("\nstopdownload\n");
		return;
	}

	// check if the user is trying to download part of registered Quake(r)
	whichpack = FS_WhichPack(host_client->download_name);
	whichpack2 = FS_WhichPack("gfx/pop.lmp");
	if ((whichpack && whichpack2 && !strcasecmp(whichpack, whichpack2)) || FS_IsRegisteredQuakePack(host_client->download_name))
	{
		SV_ClientPrintf("Download rejected: file \"%s\" is part of registered Quake(r)\nYou must purchase Quake(r) from id Software or a retailer to get this file\nPlease go to http://www.idsoftware.com/games/quake/quake/index.php?game_section=buy\n", host_client->download_name);
		SV_ClientCommands("\nstopdownload\n");
		return;
	}

	// check if the server has forbidden archive downloads entirely
	if (!sv_allowdownloads_inarchive.integer)
	{
		whichpack = FS_WhichPack(host_client->download_name);
		if (whichpack)
		{
			SV_ClientPrintf("Download rejected: file \"%s\" is in an archive (\"%s\")\nYou must separately download or purchase the data archives for this game/mod to get this file\n", host_client->download_name, whichpack);
			SV_ClientCommands("\nstopdownload\n");
			return;
		}
	}

	if (!sv_allowdownloads_config.integer)
	{
		if (!strcasecmp(extension, "cfg"))
		{
			SV_ClientPrintf("Download rejected: file \"%s\" is a .cfg file which is forbidden for security reasons\nYou must separately download or purchase the data archives for this game/mod to get this file\n", host_client->download_name);
			SV_ClientCommands("\nstopdownload\n");
			return;
		}
	}

	if (!sv_allowdownloads_dlcache.integer)
	{
		if (!strncasecmp(host_client->download_name, "dlcache/", 8))
		{
			SV_ClientPrintf("Download rejected: file \"%s\" is in the dlcache/ directory which is forbidden for security reasons\nYou must separately download or purchase the data archives for this game/mod to get this file\n", host_client->download_name);
			SV_ClientCommands("\nstopdownload\n");
			return;
		}
	}

	if (!sv_allowdownloads_archive.integer)
	{
		if (!strcasecmp(extension, "pak") || !strcasecmp(extension, "pk3"))
		{
			SV_ClientPrintf("Download rejected: file \"%s\" is an archive\nYou must separately download or purchase the data archives for this game/mod to get this file\n", host_client->download_name);
			SV_ClientCommands("\nstopdownload\n");
			return;
		}
	}

	host_client->download_file = FS_OpenVirtualFile(host_client->download_name, true);
	if (!host_client->download_file)
	{
		SV_ClientPrintf("Download rejected: server could not open the file \"%s\"\n", host_client->download_name);
		SV_ClientCommands("\nstopdownload\n");
		return;
	}

	if (FS_FileSize(host_client->download_file) > 1<<30)
	{
		SV_ClientPrintf("Download rejected: file \"%s\" is very large\n", host_client->download_name);
		SV_ClientCommands("\nstopdownload\n");
		FS_Close(host_client->download_file);
		host_client->download_file = NULL;
		return;
	}

	if (FS_FileSize(host_client->download_file) < 0)
	{
		SV_ClientPrintf("Download rejected: file \"%s\" is not a regular file\n", host_client->download_name);
		SV_ClientCommands("\nstopdownload\n");
		FS_Close(host_client->download_file);
		host_client->download_file = NULL;
		return;
	}

	Con_DPrintf("Downloading %s to %s\n", host_client->download_name, host_client->name);

	/*
	 * we can only do this if we would actually deflate on the fly
	 * which we do not (yet)!
	{
		char extensions[MAX_QPATH]; // make sure this can hold all extensions
		extensions[0] = '\0';

		if(host_client->download_deflate)
			strlcat(extensions, " deflate", sizeof(extensions));

		// no, no space is needed between %s and %s :P
		Host_ClientCommands("\ncl_downloadbegin %i %s%s\n", (int)FS_FileSize(host_client->download_file), host_client->download_name, extensions);
	}
	*/
	SV_ClientCommands("\ncl_downloadbegin %i %s\n", (int)FS_FileSize(host_client->download_file), host_client->download_name);

	host_client->download_expectedposition = 0;
	host_client->download_started = false;
	host_client->sendsignon = true; // make sure this message is sent

	// the rest of the download process is handled in SV_SendClientDatagram
	// and other code dealing with svc_downloaddata and clc_ackdownloaddata
	//
	// no svc_downloaddata messages will be sent until sv_startdownload is
	// sent by the client
}

static void SV_Say(qboolean teamonly)
{
	prvm_prog_t *prog = SVVM_prog;
	client_t *save;
	int j, quoted;
	const char *p1;
	char *p2;
	// LordHavoc: long say messages
	char text[1024];
	qboolean fromServer = false;

	if (cmd_source == src_command)
	{
		if (host.state & H_DEDICATED)
		{
			fromServer = true;
			teamonly = false;
		}
		else
		{
			Cmd_ForwardToServer ();
			return;
		}
	}

	if (Cmd_Argc () < 2)
		return;

	if (!teamplay.integer)
		teamonly = false;

	p1 = Cmd_Args();
	quoted = false;
	if (*p1 == '\"')
	{
		quoted = true;
		p1++;
	}
	// note this uses the chat prefix \001
	if (!fromServer && !teamonly)
		dpsnprintf (text, sizeof(text), "\001%s: %s", host_client->name, p1);
	else if (!fromServer && teamonly)
		dpsnprintf (text, sizeof(text), "\001(%s): %s", host_client->name, p1);
	else if(*(sv_adminnick.string))
		dpsnprintf (text, sizeof(text), "\001<%s> %s", sv_adminnick.string, p1);
	else
		dpsnprintf (text, sizeof(text), "\001<%s> %s", hostname.string, p1);
	p2 = text + strlen(text);
	while ((const char *)p2 > (const char *)text && (p2[-1] == '\r' || p2[-1] == '\n' || (p2[-1] == '\"' && quoted)))
	{
		if (p2[-1] == '\"' && quoted)
			quoted = false;
		p2[-1] = 0;
		p2--;
	}
	strlcat(text, "\n", sizeof(text));

	// note: save is not a valid edict if fromServer is true
	save = host_client;
	for (j = 0, host_client = svs.clients;j < svs.maxclients;j++, host_client++)
		if (host_client->active && (!teamonly || PRVM_serveredictfloat(host_client->edict, team) == PRVM_serveredictfloat(save->edict, team)))
			SV_ClientPrint(text);
	host_client = save;

	if (host.state & H_DEDICATED)
		Con_Print(&text[1]);
}


static void SV_Say_f(void)
{
	SV_Say(false);
}


static void SV_Say_Team_f(void)
{
	SV_Say(true);
}


static void SV_Tell_f(void)
{
	const char *playername_start = NULL;
	size_t playername_length = 0;
	int playernumber = 0;
	client_t *save;
	int j;
	const char *p1, *p2;
	char text[MAX_INPUTLINE]; // LordHavoc: FIXME: temporary buffer overflow fix (was 64)
	qboolean fromServer = false;

	if (cmd_source == src_command)
	{
		if (host.state & H_DEDICATED)
			fromServer = true;
		else
		{
			Cmd_ForwardToServer ();
			return;
		}
	}

	if (Cmd_Argc () < 2)
		return;

	// note this uses the chat prefix \001
	if (!fromServer)
		dpsnprintf (text, sizeof(text), "\001%s tells you: ", host_client->name);
	else if(*(sv_adminnick.string))
		dpsnprintf (text, sizeof(text), "\001<%s tells you> ", sv_adminnick.string);
	else
		dpsnprintf (text, sizeof(text), "\001<%s tells you> ", hostname.string);

	p1 = Cmd_Args();
	p2 = p1 + strlen(p1);
	// remove the target name
	while (p1 < p2 && *p1 == ' ')
		p1++;
	if(*p1 == '#')
	{
		++p1;
		while (p1 < p2 && *p1 == ' ')
			p1++;
		while (p1 < p2 && isdigit(*p1))
		{
			playernumber = playernumber * 10 + (*p1 - '0');
			p1++;
		}
		--playernumber;
	}
	else if(*p1 == '"')
	{
		++p1;
		playername_start = p1;
		while (p1 < p2 && *p1 != '"')
			p1++;
		playername_length = p1 - playername_start;
		if(p1 < p2)
			p1++;
	}
	else
	{
		playername_start = p1;
		while (p1 < p2 && *p1 != ' ')
			p1++;
		playername_length = p1 - playername_start;
	}
	while (p1 < p2 && *p1 == ' ')
		p1++;
	if(playername_start)
	{
		// set playernumber to the right client
		char namebuf[128];
		if(playername_length >= sizeof(namebuf))
		{
			if (fromServer)
				Con_Print("Host_Tell: too long player name/ID\n");
			else
				SV_ClientPrint("Host_Tell: too long player name/ID\n");
			return;
		}
		memcpy(namebuf, playername_start, playername_length);
		namebuf[playername_length] = 0;
		for (playernumber = 0; playernumber < svs.maxclients; playernumber++)
		{
			if (!svs.clients[playernumber].active)
				continue;
			if (strcasecmp(svs.clients[playernumber].name, namebuf) == 0)
				break;
		}
	}
	if(playernumber < 0 || playernumber >= svs.maxclients || !(svs.clients[playernumber].active))
	{
		if (fromServer)
			Con_Print("Host_Tell: invalid player name/ID\n");
		else
			SV_ClientPrint("Host_Tell: invalid player name/ID\n");
		return;
	}
	// remove trailing newlines
	while (p2 > p1 && (p2[-1] == '\n' || p2[-1] == '\r'))
		p2--;
	// remove quotes if present
	if (*p1 == '"')
	{
		p1++;
		if (p2[-1] == '"')
			p2--;
		else if (fromServer)
			Con_Print("Host_Tell: missing end quote\n");
		else
			SV_ClientPrint("Host_Tell: missing end quote\n");
	}
	while (p2 > p1 && (p2[-1] == '\n' || p2[-1] == '\r'))
		p2--;
	if(p1 == p2)
		return; // empty say
	for (j = (int)strlen(text);j < (int)(sizeof(text) - 2) && p1 < p2;)
		text[j++] = *p1++;
	text[j++] = '\n';
	text[j++] = 0;

	save = host_client;
	host_client = svs.clients + playernumber;
	SV_ClientPrint(text);
	host_client = save;
}

/*
==================
SV_Pause_f
==================
*/
static void SV_Pause_f (void)
{
	void (*print) (const char *fmt, ...);
	if (cmd_source == src_command)
	{
		// if running a client, try to send over network so the pause is handled by the server
		if (host.state & H_CS_CONNECTED)
		{
			Cmd_ForwardToServer ();
			return;
		}
		print = Con_Printf;
	}
	else
		print = SV_ClientPrintf;

	if (!pausable.integer)
	{
		if (cmd_source == src_client)
		{
			if((host.state & H_DEDICATED) || host_client != &svs.clients[0]) // non-admin
			{
				print("Pause not allowed.\n");
				return;
			}
		}
	}

	sv.paused ^= 1;
	if (cmd_source != src_command)
		SV_BroadcastPrintf("%s %spaused the game\n", host_client->name, sv.paused ? "" : "un");
	else if(*(sv_adminnick.string))
		SV_BroadcastPrintf("%s %spaused the game\n", sv_adminnick.string, sv.paused ? "" : "un");
	else
		SV_BroadcastPrintf("%s %spaused the game\n", hostname.string, sv.paused ? "" : "un");
	// send notification to all clients
	MSG_WriteByte(&sv.reliable_datagram, svc_setpause);
	MSG_WriteByte(&sv.reliable_datagram, sv.paused);
}

/*
==================
SV_Kill_f
==================
*/
static void SV_Kill_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	if (PRVM_serveredictfloat(host_client->edict, health) <= 0)
	{
		SV_ClientPrint("Can't suicide -- already dead!\n");
		return;
	}

	PRVM_serverglobalfloat(time) = sv.time;
	PRVM_serverglobaledict(self) = PRVM_EDICT_TO_PROG(host_client->edict);
	prog->ExecuteProgram(prog, PRVM_serverfunction(ClientKill), "QC function ClientKill is missing");
}

static void SV_SendCvar_f (void)
{
	int		i;
	cvar_t	*c;
	const char *cvarname;
	client_t *old;
	char vabuf[1024];

	if(Cmd_Argc() != 2)
		return;
	cvarname = Cmd_Argv(1);
	if (host.state & H_CS_CONNECTED)
	{
		c = Cvar_FindVar(cvarname,true);
		// LordHavoc: if there is no such cvar or if it is private, send a
		// reply indicating that it has no value
		if(!c || (c->flags & CVAR_PRIVATE))
			Cmd_ForwardStringToServer(va(vabuf, sizeof(vabuf), "sentcvar %s", cvarname));
		else
			Cmd_ForwardStringToServer(va(vabuf, sizeof(vabuf), "sentcvar %s \"%s\"", c->name, c->string));
		return;
	}
	if(!(host.state & H_SS_ACTIVE))// || !PRVM_serverfunction(SV_ParseClientCommand))
		return;

	old = host_client;
	if (!(host.state & H_DEDICATED))
		i = 1;
	else
		i = 0;
	for(;i<svs.maxclients;i++)
		if(svs.clients[i].active && svs.clients[i].netconnection)
		{
			host_client = &svs.clients[i];
			SV_ClientCommands("sendcvar %s\n", cvarname);
		}
	host_client = old;
}

static void SV_MaxPlayers_f(void)
{
	int n;

	if (Cmd_Argc() != 2)
	{
		Con_Printf("\"maxplayers\" is \"%u\"\n", svs.maxclients_next);
		return;
	}

	if (host.state & H_SS_ACTIVE)
	{
		Con_Print("maxplayers can not be changed while a server is running.\n");
		Con_Print("It will be changed on next server startup (\"map\" command).\n");
	}

	n = atoi(Cmd_Argv(1));
	n = bound(1, n, MAX_SCOREBOARD);
	Con_Printf("\"maxplayers\" set to \"%u\"\n", n);

	svs.maxclients_next = n;
	if (n == 1)
		Cvar_Set ("deathmatch", "0");
	else
		Cvar_Set ("deathmatch", "1");
}

/*
==================
Host_PreSpawn_f
==================
*/
static void SV_PreSpawn_f (void)
{
	if (host_client->prespawned)
	{
		Con_Print("prespawn not valid -- already prespawned\n");
		return;
	}
	host_client->prespawned = true;

	if (host_client->netconnection)
	{
		SZ_Write (&host_client->netconnection->message, sv.signon.data, sv.signon.cursize);
		MSG_WriteByte (&host_client->netconnection->message, svc_signonnum);
		MSG_WriteByte (&host_client->netconnection->message, 2);
		host_client->sendsignon = 0;		// enable unlimited sends again
	}

	// reset the name change timer because the client will send name soon
	host_client->nametime = 0;
}

/*
==================
Host_Spawn_f
==================
*/
static void SV_Spawn_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	int i;
	client_t *client;
	int stats[MAX_CL_STATS];

	if (!host_client->prespawned)
	{
		Con_Print("Spawn not valid -- not yet prespawned\n");
		return;
	}
	if (host_client->spawned)
	{
		Con_Print("Spawn not valid -- already spawned\n");
		return;
	}
	host_client->spawned = true;

	// reset name change timer again because they might want to change name
	// again in the first 5 seconds after connecting
	host_client->nametime = 0;

	// LordHavoc: moved this above the QC calls at FrikaC's request
	// LordHavoc: commented this out
	//if (host_client->netconnection)
	//	SZ_Clear (&host_client->netconnection->message);

	// run the entrance script
	if (sv.loadgame)
	{
		// loaded games are fully initialized already
		if (PRVM_serverfunction(RestoreGame))
		{
			Con_DPrint("Calling RestoreGame\n");
			PRVM_serverglobalfloat(time) = sv.time;
			PRVM_serverglobaledict(self) = PRVM_EDICT_TO_PROG(host_client->edict);
			prog->ExecuteProgram(prog, PRVM_serverfunction(RestoreGame), "QC function RestoreGame is missing");
		}
	}
	else
	{
		//Con_Printf("Host_Spawn_f: host_client->edict->netname = %s, host_client->edict->netname = %s, host_client->name = %s\n", PRVM_GetString(PRVM_serveredictstring(host_client->edict, netname)), PRVM_GetString(PRVM_serveredictstring(host_client->edict, netname)), host_client->name);

		// copy spawn parms out of the client_t
		for (i=0 ; i< NUM_SPAWN_PARMS ; i++)
			(&PRVM_serverglobalfloat(parm1))[i] = host_client->spawn_parms[i];

		// call the spawn function
		host_client->clientconnectcalled = true;
		PRVM_serverglobalfloat(time) = sv.time;
		PRVM_serverglobaledict(self) = PRVM_EDICT_TO_PROG(host_client->edict);
		prog->ExecuteProgram(prog, PRVM_serverfunction(ClientConnect), "QC function ClientConnect is missing");

		PRVM_serverglobalfloat(time) = sv.time;
		prog->ExecuteProgram(prog, PRVM_serverfunction(PutClientInServer), "QC function PutClientInServer is missing");
	}

	if (!host_client->netconnection)
		return;

	// send time of update
	MSG_WriteByte (&host_client->netconnection->message, svc_time);
	MSG_WriteFloat (&host_client->netconnection->message, sv.time);

	// send all current names, colors, and frag counts
	for (i = 0, client = svs.clients;i < svs.maxclients;i++, client++)
	{
		if (!client->active)
			continue;
		MSG_WriteByte (&host_client->netconnection->message, svc_updatename);
		MSG_WriteByte (&host_client->netconnection->message, i);
		MSG_WriteString (&host_client->netconnection->message, client->name);
		MSG_WriteByte (&host_client->netconnection->message, svc_updatefrags);
		MSG_WriteByte (&host_client->netconnection->message, i);
		MSG_WriteShort (&host_client->netconnection->message, client->frags);
		MSG_WriteByte (&host_client->netconnection->message, svc_updatecolors);
		MSG_WriteByte (&host_client->netconnection->message, i);
		MSG_WriteByte (&host_client->netconnection->message, client->colors);
	}

	// send all current light styles
	for (i=0 ; i<MAX_LIGHTSTYLES ; i++)
	{
		if (sv.lightstyles[i][0])
		{
			MSG_WriteByte (&host_client->netconnection->message, svc_lightstyle);
			MSG_WriteByte (&host_client->netconnection->message, (char)i);
			MSG_WriteString (&host_client->netconnection->message, sv.lightstyles[i]);
		}
	}

	// send some stats
	MSG_WriteByte (&host_client->netconnection->message, svc_updatestat);
	MSG_WriteByte (&host_client->netconnection->message, STAT_TOTALSECRETS);
	MSG_WriteLong (&host_client->netconnection->message, (int)PRVM_serverglobalfloat(total_secrets));

	MSG_WriteByte (&host_client->netconnection->message, svc_updatestat);
	MSG_WriteByte (&host_client->netconnection->message, STAT_TOTALMONSTERS);
	MSG_WriteLong (&host_client->netconnection->message, (int)PRVM_serverglobalfloat(total_monsters));

	MSG_WriteByte (&host_client->netconnection->message, svc_updatestat);
	MSG_WriteByte (&host_client->netconnection->message, STAT_SECRETS);
	MSG_WriteLong (&host_client->netconnection->message, (int)PRVM_serverglobalfloat(found_secrets));

	MSG_WriteByte (&host_client->netconnection->message, svc_updatestat);
	MSG_WriteByte (&host_client->netconnection->message, STAT_MONSTERS);
	MSG_WriteLong (&host_client->netconnection->message, (int)PRVM_serverglobalfloat(killed_monsters));

	// send a fixangle
	// Never send a roll angle, because savegames can catch the server
	// in a state where it is expecting the client to correct the angle
	// and it won't happen if the game was just loaded, so you wind up
	// with a permanent head tilt
	if (sv.loadgame)
	{
		MSG_WriteByte (&host_client->netconnection->message, svc_setangle);
		MSG_WriteAngle (&host_client->netconnection->message, PRVM_serveredictvector(host_client->edict, v_angle)[0], sv.protocol);
		MSG_WriteAngle (&host_client->netconnection->message, PRVM_serveredictvector(host_client->edict, v_angle)[1], sv.protocol);
		MSG_WriteAngle (&host_client->netconnection->message, 0, sv.protocol);
	}
	else
	{
		MSG_WriteByte (&host_client->netconnection->message, svc_setangle);
		MSG_WriteAngle (&host_client->netconnection->message, PRVM_serveredictvector(host_client->edict, angles)[0], sv.protocol);
		MSG_WriteAngle (&host_client->netconnection->message, PRVM_serveredictvector(host_client->edict, angles)[1], sv.protocol);
		MSG_WriteAngle (&host_client->netconnection->message, 0, sv.protocol);
	}

	SV_WriteClientdataToMessage (host_client, host_client->edict, &host_client->netconnection->message, stats);

	MSG_WriteByte (&host_client->netconnection->message, svc_signonnum);
	MSG_WriteByte (&host_client->netconnection->message, 3);
}

/*
==================
Host_Begin_f
==================
*/
static void SV_Begin_f (void)
{
	if (!host_client->spawned)
	{
		Con_Print("Begin not valid -- not yet spawned\n");
		return;
	}
	if (host_client->begun)
	{
		Con_Print("Begin not valid -- already begun\n");
		return;
	}
	host_client->begun = true;

	// LordHavoc: note: this code also exists in SV_DropClient
	if (sv.loadgame)
	{
		int i;
		for (i = 0;i < svs.maxclients;i++)
			if (svs.clients[i].active && !svs.clients[i].spawned)
				break;
		if (i == svs.maxclients)
		{
			Con_Printf("Loaded game, everyone rejoined - unpausing\n");
			sv.paused = sv.loadgame = false; // we're basically done with loading now
		}
	}
}

/*
======================
Host_Name_f
======================
*/
cvar_t cl_name = {CVAR_SAVE | CVAR_NQUSERINFOHACK, "_cl_name", "player", "internal storage cvar for current player name (changed by name command)"};
static void SV_Name_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	int i, j;
	qboolean valid_colors;
	const char *newNameSource;
	char newName[sizeof(host_client->name)];

	if (Cmd_Argc () == 1)
	{
		if (cmd_source == src_command)
		{
			Con_Printf("name: %s\n", cl_name.string);
		}
		return;
	}

	if (Cmd_Argc () == 2)
		newNameSource = Cmd_Argv(1);
	else
		newNameSource = Cmd_Args();

	strlcpy(newName, newNameSource, sizeof(newName));

	if (cmd_source == src_command)
	{
		Cvar_Set ("_cl_name", newName);
		if (strlen(newNameSource) >= sizeof(newName)) // overflowed
		{
			Con_Printf("Your name is longer than %i chars! It has been truncated.\n", (int) (sizeof(newName) - 1));
			Con_Printf("name: %s\n", cl_name.string);
		}
		return;
	}

	if (host.realtime < host_client->nametime)
	{
		SV_ClientPrintf("You can't change name more than once every %.1f seconds!\n", max(0.0f, sv_namechangetimer.value));
		return;
	}

	host_client->nametime = host.realtime + max(0.0f, sv_namechangetimer.value);

	// point the string back at updateclient->name to keep it safe
	strlcpy (host_client->name, newName, sizeof (host_client->name));

	for (i = 0, j = 0;host_client->name[i];i++)
		if (host_client->name[i] != '\r' && host_client->name[i] != '\n')
			host_client->name[j++] = host_client->name[i];
	host_client->name[j] = 0;

	if(host_client->name[0] == 1 || host_client->name[0] == 2)
	// may interfere with chat area, and will needlessly beep; so let's add a ^7
	{
		memmove(host_client->name + 2, host_client->name, sizeof(host_client->name) - 2);
		host_client->name[sizeof(host_client->name) - 1] = 0;
		host_client->name[0] = STRING_COLOR_TAG;
		host_client->name[1] = '0' + STRING_COLOR_DEFAULT;
	}

	u8_COM_StringLengthNoColors(host_client->name, 0, &valid_colors);
	if(!valid_colors) // NOTE: this also proves the string is not empty, as "" is a valid colored string
	{
		size_t l;
		l = strlen(host_client->name);
		if(l < sizeof(host_client->name) - 1)
		{
			// duplicate the color tag to escape it
			host_client->name[i] = STRING_COLOR_TAG;
			host_client->name[i+1] = 0;
			//Con_DPrintf("abuse detected, adding another trailing color tag\n");
		}
		else
		{
			// remove the last character to fix the color code
			host_client->name[l-1] = 0;
			//Con_DPrintf("abuse detected, removing a trailing color tag\n");
		}
	}

	// find the last color tag offset and decide if we need to add a reset tag
	for (i = 0, j = -1;host_client->name[i];i++)
	{
		if (host_client->name[i] == STRING_COLOR_TAG)
		{
			if (host_client->name[i+1] >= '0' && host_client->name[i+1] <= '9')
			{
				j = i;
				// if this happens to be a reset  tag then we don't need one
				if (host_client->name[i+1] == '0' + STRING_COLOR_DEFAULT)
					j = -1;
				i++;
				continue;
			}
			if (host_client->name[i+1] == STRING_COLOR_RGB_TAG_CHAR && isxdigit(host_client->name[i+2]) && isxdigit(host_client->name[i+3]) && isxdigit(host_client->name[i+4]))
			{
				j = i;
				i += 4;
				continue;
			}
			if (host_client->name[i+1] == STRING_COLOR_TAG)
			{
				i++;
				continue;
			}
		}
	}
	// does not end in the default color string, so add it
	if (j >= 0 && strlen(host_client->name) < sizeof(host_client->name) - 2)
		memcpy(host_client->name + strlen(host_client->name), STRING_COLOR_DEFAULT_STR, strlen(STRING_COLOR_DEFAULT_STR) + 1);

	PRVM_serveredictstring(host_client->edict, netname) = PRVM_SetEngineString(prog, host_client->name);

	if (strcmp(host_client->old_name, host_client->name))
	{
		if (host_client->begun)
		{
			SV_BroadcastPrintf("\001%s ^7changed name to %s\n", host_client->old_name, host_client->name);
		}
		else
		{
			Con_Printf("Client '%s' connecting (%s)\n",host_client->name,host_client->netaddress);
			SV_BroadcastPrintf("\001Player %s has joined the game\n",host_client->name);
		}

		strlcpy(host_client->old_name, host_client->name, sizeof(host_client->old_name));
		// send notification to all clients
		MSG_WriteByte (&sv.reliable_datagram, svc_updatename);
		MSG_WriteByte (&sv.reliable_datagram, host_client - svs.clients);
		MSG_WriteString (&sv.reliable_datagram, host_client->name);
		SV_WriteNetnameIntoDemo(host_client);
	}
}

/*
======================
Host_Playermodel_f
======================
*/
cvar_t cl_playermodel = {CVAR_SAVE | CVAR_NQUSERINFOHACK, "_cl_playermodel", "", "internal storage cvar for current player model in Nexuiz/Xonotic (changed by playermodel command)"};
// the old cl_playermodel in cl_main has been renamed to __cl_playermodel
static void SV_Playermodel_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	int i, j;
	char newPath[sizeof(host_client->playermodel)];

	if (Cmd_Argc () == 1)
	{
		if (cmd_source == src_command)
		{
			Con_Printf("\"playermodel\" is \"%s\"\n", cl_playermodel.string);
		}
		return;
	}

	if (Cmd_Argc () == 2)
		strlcpy (newPath, Cmd_Argv(1), sizeof (newPath));
	else
		strlcpy (newPath, Cmd_Args(), sizeof (newPath));

	for (i = 0, j = 0;newPath[i];i++)
		if (newPath[i] != '\r' && newPath[i] != '\n')
			newPath[j++] = newPath[i];
	newPath[j] = 0;

	if (cmd_source == src_command)
	{
		Cvar_Set ("_cl_playermodel", newPath);
		return;
	}

	/*
	if (realtime < host_client->nametime)
	{
		SV_ClientPrintf("You can't change playermodel more than once every 5 seconds!\n");
		return;
	}

	host_client->nametime = realtime + 5;
	*/

	// point the string back at updateclient->name to keep it safe
	strlcpy (host_client->playermodel, newPath, sizeof (host_client->playermodel));
	PRVM_serveredictstring(host_client->edict, playermodel) = PRVM_SetEngineString(prog, host_client->playermodel);
	if (strcmp(host_client->old_model, host_client->playermodel))
	{
		strlcpy(host_client->old_model, host_client->playermodel, sizeof(host_client->old_model));
		/*// send notification to all clients
		MSG_WriteByte (&sv.reliable_datagram, svc_updatepmodel);
		MSG_WriteByte (&sv.reliable_datagram, host_client - svs.clients);
		MSG_WriteString (&sv.reliable_datagram, host_client->playermodel);*/
	}
}

/*
======================
Host_Playerskin_f
======================
*/
cvar_t cl_playerskin = {CVAR_SAVE | CVAR_NQUSERINFOHACK, "_cl_playerskin", "", "internal storage cvar for current player skin in Nexuiz/Xonotic (changed by playerskin command)"};
static void SV_Playerskin_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	int i, j;
	char newPath[sizeof(host_client->playerskin)];

	if (Cmd_Argc () == 1)
	{
		if (cmd_source == src_command)
		{
			Con_Printf("\"playerskin\" is \"%s\"\n", cl_playerskin.string);
		}
		return;
	}

	if (Cmd_Argc () == 2)
		strlcpy (newPath, Cmd_Argv(1), sizeof (newPath));
	else
		strlcpy (newPath, Cmd_Args(), sizeof (newPath));

	for (i = 0, j = 0;newPath[i];i++)
		if (newPath[i] != '\r' && newPath[i] != '\n')
			newPath[j++] = newPath[i];
	newPath[j] = 0;

	if (cmd_source == src_command)
	{
		Cvar_Set ("_cl_playerskin", newPath);
		return;
	}

	/*
	if (realtime < host_client->nametime)
	{
		SV_ClientPrintf("You can't change playermodel more than once every 5 seconds!\n");
		return;
	}

	host_client->nametime = realtime + 5;
	*/

	// point the string back at updateclient->name to keep it safe
	strlcpy (host_client->playerskin, newPath, sizeof (host_client->playerskin));
	PRVM_serveredictstring(host_client->edict, playerskin) = PRVM_SetEngineString(prog, host_client->playerskin);
	if (strcmp(host_client->old_skin, host_client->playerskin))
	{
		//if (host_client->begun)
		//	SV_BroadcastPrintf("%s changed skin to %s\n", host_client->name, host_client->playerskin);
		strlcpy(host_client->old_skin, host_client->playerskin, sizeof(host_client->old_skin));
		/*// send notification to all clients
		MSG_WriteByte (&sv.reliable_datagram, svc_updatepskin);
		MSG_WriteByte (&sv.reliable_datagram, host_client - svs.clients);
		MSG_WriteString (&sv.reliable_datagram, host_client->playerskin);*/
	}
}

/*
==================
Host_Color_f
==================
*/
cvar_t cl_color = {CVAR_SAVE | CVAR_NQUSERINFOHACK, "_cl_color", "0", "internal storage cvar for current player colors (changed by color command)"};
static void SV_Color(int changetop, int changebottom)
{
	prvm_prog_t *prog = SVVM_prog;
	int top, bottom, playercolor;

	// get top and bottom either from the provided values or the current values
	// (allows changing only top or bottom, or both at once)
	top = changetop >= 0 ? changetop : (cl_color.integer >> 4);
	bottom = changebottom >= 0 ? changebottom : cl_color.integer;

	top &= 15;
	bottom &= 15;
	// LordHavoc: allowing skin colormaps 14 and 15 by commenting this out
	//if (top > 13)
	//	top = 13;
	//if (bottom > 13)
	//	bottom = 13;

	playercolor = top*16 + bottom;

	if (cmd_source == src_command)
	{
		Cvar_SetValueQuick(&cl_color, playercolor);
		return;
	}

	if (cls.protocol == &protocol_quakeworld)
		return;

	if (host_client->edict && PRVM_serverfunction(SV_ChangeTeam))
	{
		Con_DPrint("Calling SV_ChangeTeam\n");
		prog->globals.fp[OFS_PARM0] = playercolor;
		PRVM_serverglobalfloat(time) = sv.time;
		PRVM_serverglobaledict(self) = PRVM_EDICT_TO_PROG(host_client->edict);
		prog->ExecuteProgram(prog, PRVM_serverfunction(SV_ChangeTeam), "QC function SV_ChangeTeam is missing");
	}
	else
	{
		if (host_client->edict)
		{
			PRVM_serveredictfloat(host_client->edict, clientcolors) = playercolor;
			PRVM_serveredictfloat(host_client->edict, team) = bottom + 1;
		}
		host_client->colors = playercolor;
		if (host_client->old_colors != host_client->colors)
		{
			host_client->old_colors = host_client->colors;
			// send notification to all clients
			MSG_WriteByte (&sv.reliable_datagram, svc_updatecolors);
			MSG_WriteByte (&sv.reliable_datagram, host_client - svs.clients);
			MSG_WriteByte (&sv.reliable_datagram, host_client->colors);
		}
	}
}

static void SV_Color_f(void)
{
	int		top, bottom;

	if (Cmd_Argc() == 1)
	{
		if (cmd_source == src_command)
		{
			Con_Printf("\"color\" is \"%i %i\"\n", cl_color.integer >> 4, cl_color.integer & 15);
			Con_Print("color <0-15> [0-15]\n");
		}
		return;
	}

	if (Cmd_Argc() == 2)
		top = bottom = atoi(Cmd_Argv(1));
	else
	{
		top = atoi(Cmd_Argv(1));
		bottom = atoi(Cmd_Argv(2));
	}
	SV_Color(top, bottom);
}

static void SV_TopColor_f(void)
{
	if (Cmd_Argc() == 1)
	{
		if (cmd_source == src_command)
		{
			Con_Printf("\"topcolor\" is \"%i\"\n", (cl_color.integer >> 4) & 15);
			Con_Print("topcolor <0-15>\n");
		}
		return;
	}

	SV_Color(atoi(Cmd_Argv(1)), -1);
}

static void SV_BottomColor_f(void)
{
	if (Cmd_Argc() == 1)
	{
		if (cmd_source == src_command)
		{
			Con_Printf("\"bottomcolor\" is \"%i\"\n", cl_color.integer & 15);
			Con_Print("bottomcolor <0-15>\n");
		}
		return;
	}

	SV_Color(-1, atoi(Cmd_Argv(1)));
}

cvar_t cl_rate = {CVAR_SAVE | CVAR_NQUSERINFOHACK, "_cl_rate", "20000", "internal storage cvar for current rate (changed by rate command)"};
cvar_t cl_rate_burstsize = {CVAR_SAVE | CVAR_NQUSERINFOHACK, "_cl_rate_burstsize", "1024", "internal storage cvar for current rate control burst size (changed by rate_burstsize command)"};
static void SV_Rate_f(void)
{
	int rate;

	if (Cmd_Argc() != 2)
	{
		if (cmd_source == src_command)
		{
			Con_Printf("\"rate\" is \"%i\"\n", cl_rate.integer);
			Con_Print("rate <bytespersecond>\n");
		}
		return;
	}

	rate = atoi(Cmd_Argv(1));

	if (cmd_source == src_command)
	{
		Cvar_SetValue ("_cl_rate", max(NET_MINRATE, rate));
		return;
	}

	host_client->rate = rate;
}

static void SV_Rate_BurstSize_f(void)
{
	int rate_burstsize;

	if (Cmd_Argc() != 2)
	{
		Con_Printf("\"rate_burstsize\" is \"%i\"\n", cl_rate_burstsize.integer);
		Con_Print("rate_burstsize <bytes>\n");
		return;
	}

	rate_burstsize = atoi(Cmd_Argv(1));

	if (cmd_source == src_command)
	{
		Cvar_SetValue ("_cl_rate_burstsize", rate_burstsize);
		return;
	}

	host_client->rate_burstsize = rate_burstsize;
}

/*
======================
Host_PModel_f
LordHavoc: only supported for Nehahra, I personally think this is dumb, but Mindcrime won't listen.
LordHavoc: correction, Mindcrime will be removing pmodel in the future, but it's still stuck here for compatibility.
======================
*/
cvar_t cl_pmodel = {CVAR_SAVE | CVAR_NQUSERINFOHACK, "_cl_pmodel", "0", "internal storage cvar for current player model number in nehahra (changed by pmodel command)"};
static void SV_PModel_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	int i;

	if (Cmd_Argc () == 1)
	{
		if (cmd_source == src_command)
		{
			Con_Printf("\"pmodel\" is \"%s\"\n", cl_pmodel.string);
		}
		return;
	}
	i = atoi(Cmd_Argv(1));

	if (cmd_source == src_command)
	{
		if (cl_pmodel.integer == i)
			return;
		Cvar_SetValue ("_cl_pmodel", i);
		if (host.state & H_CS_CONNECTED)
			Cmd_ForwardToServer ();
		return;
	}

	PRVM_serveredictfloat(host_client->edict, pmodel) = i;
}

/*
==================
Host_FullServerinfo_f

Sent by server when serverinfo changes
==================
*/
// TODO: shouldn't this be a cvar instead?
static void SV_FullServerinfo_f (void) // credit: taken from QuakeWorld
{
	char temp[512];
	if (Cmd_Argc() != 2)
	{
		Con_Printf ("usage: fullserverinfo <complete info string>\n");
		return;
	}

	strlcpy (cl.qw_serverinfo, Cmd_Argv(1), sizeof(cl.qw_serverinfo));
	InfoString_GetValue(cl.qw_serverinfo, "teamplay", temp, sizeof(temp));
	cl.qw_teamplay = atoi(temp);
}

#define	SAVEGAME_VERSION	5

void SV_Savegame_to(prvm_prog_t *prog, const char *name)
{
	qfile_t	*f;
	int		i, k, l, numbuffers, lightstyles = 64;
	char	comment[SAVEGAME_COMMENT_LENGTH+1];
	char	line[MAX_INPUTLINE];
	qboolean isserver;
	char	*s;

	// first we have to figure out if this can be saved in 64 lightstyles
	// (for Quake compatibility)
	for (i=64 ; i<MAX_LIGHTSTYLES ; i++)
		if (sv.lightstyles[i][0])
			lightstyles = i+1;

	isserver = prog == SVVM_prog;

	Con_Printf("Saving game to %s...\n", name);
	f = FS_OpenRealFile(name, "wb", false);
	if (!f)
	{
		Con_Print("ERROR: couldn't open.\n");
		return;
	}

	FS_Printf(f, "%i\n", SAVEGAME_VERSION);

	memset(comment, 0, sizeof(comment));
	if(isserver)
		dpsnprintf(comment, sizeof(comment), "%-21.21s kills:%3i/%3i", PRVM_GetString(prog, PRVM_serveredictstring(prog->edicts, message)), (int)PRVM_serverglobalfloat(killed_monsters), (int)PRVM_serverglobalfloat(total_monsters));
	else
		dpsnprintf(comment, sizeof(comment), "(crash dump of %s progs)", prog->name);
	// convert space to _ to make stdio happy
	// LordHavoc: convert control characters to _ as well
	for (i=0 ; i<SAVEGAME_COMMENT_LENGTH ; i++)
		if (ISWHITESPACEORCONTROL(comment[i]))
			comment[i] = '_';
	comment[SAVEGAME_COMMENT_LENGTH] = '\0';

	FS_Printf(f, "%s\n", comment);
	if(isserver)
	{
		for (i=0 ; i<NUM_SPAWN_PARMS ; i++)
			FS_Printf(f, "%f\n", svs.clients[0].spawn_parms[i]);
		FS_Printf(f, "%d\n", current_skill);
		FS_Printf(f, "%s\n", sv.name);
		FS_Printf(f, "%f\n",sv.time);
	}
	else
	{
		for (i=0 ; i<NUM_SPAWN_PARMS ; i++)
			FS_Printf(f, "(dummy)\n");
		FS_Printf(f, "%d\n", 0);
		FS_Printf(f, "%s\n", "(dummy)");
		FS_Printf(f, "%f\n", host.realtime);
	}

	// write the light styles
	for (i=0 ; i<lightstyles ; i++)
	{
		if (isserver && sv.lightstyles[i][0])
			FS_Printf(f, "%s\n", sv.lightstyles[i]);
		else
			FS_Print(f,"m\n");
	}

	PRVM_ED_WriteGlobals (prog, f);
	for (i=0 ; i<prog->num_edicts ; i++)
	{
		FS_Printf(f,"// edict %d\n", i);
		//Con_Printf("edict %d...\n", i);
		PRVM_ED_Write (prog, f, PRVM_EDICT_NUM(i));
	}

#if 1
	FS_Printf(f,"/*\n");
	FS_Printf(f,"// DarkPlaces extended savegame\n");
	// darkplaces extension - extra lightstyles, support for color lightstyles
	for (i=0 ; i<MAX_LIGHTSTYLES ; i++)
		if (isserver && sv.lightstyles[i][0])
			FS_Printf(f, "sv.lightstyles %i %s\n", i, sv.lightstyles[i]);

	// darkplaces extension - model precaches
	for (i=1 ; i<MAX_MODELS ; i++)
		if (sv.model_precache[i][0])
			FS_Printf(f,"sv.model_precache %i %s\n", i, sv.model_precache[i]);

	// darkplaces extension - sound precaches
	for (i=1 ; i<MAX_SOUNDS ; i++)
		if (sv.sound_precache[i][0])
			FS_Printf(f,"sv.sound_precache %i %s\n", i, sv.sound_precache[i]);

	// darkplaces extension - save buffers
	numbuffers = (int)Mem_ExpandableArray_IndexRange(&prog->stringbuffersarray);
	for (i = 0; i < numbuffers; i++)
	{
		prvm_stringbuffer_t *stringbuffer = (prvm_stringbuffer_t*) Mem_ExpandableArray_RecordAtIndex(&prog->stringbuffersarray, i);
		if(stringbuffer && (stringbuffer->flags & STRINGBUFFER_SAVED))
		{
			FS_Printf(f,"sv.buffer %i %i \"string\"\n", i, stringbuffer->flags & STRINGBUFFER_QCFLAGS);
			for(k = 0; k < stringbuffer->num_strings; k++)
			{
				if (!stringbuffer->strings[k])
					continue;
				// Parse the string a bit to turn special characters
				// (like newline, specifically) into escape codes
				s = stringbuffer->strings[k];
				for (l = 0;l < (int)sizeof(line) - 2 && *s;)
				{
					if (*s == '\n')
					{
						line[l++] = '\\';
						line[l++] = 'n';
					}
					else if (*s == '\r')
					{
						line[l++] = '\\';
						line[l++] = 'r';
					}
					else if (*s == '\\')
					{
						line[l++] = '\\';
						line[l++] = '\\';
					}
					else if (*s == '"')
					{
						line[l++] = '\\';
						line[l++] = '"';
					}
					else
						line[l++] = *s;
					s++;
				}
				line[l] = '\0';
				FS_Printf(f,"sv.bufstr %i %i \"%s\"\n", i, k, line);
			}
		}
	}
	FS_Printf(f,"*/\n");
#endif

	FS_Close (f);
	Con_Print("done.\n");
}

/*
===============
Host_Savegame_f
===============
*/
static void SV_Savegame_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	char	name[MAX_QPATH];
	qboolean deadflag = false;

	if (!(host.state & H_SS_ACTIVE))
	{
		Con_Print("Can't save - no server running.\n");
		return;
	}

	deadflag = (host.state & H_SS_LOCALGAME) && svs.clients[0].active && PRVM_serveredictfloat(svs.clients[0].edict, deadflag);

	if (host.state & H_SS_LOCALGAME)
	{
		// singleplayer checks
		if (cl.intermission)
		{
			Con_Print("Can't save in intermission.\n");
			return;
		}

		if (deadflag)
		{
			Con_Print("Can't save with a dead player.\n");
			return;
		}
	}
	else
	{
		Con_Print("Can't save multiplayer games.\n");
		return;
	}

	if (Cmd_Argc() != 2)
	{
		Con_Print("save <savename> : save a game\n");
		return;
	}

	if (strstr(Cmd_Argv(1), ".."))
	{
		Con_Print("Relative pathnames are not allowed.\n");
		return;
	}

	strlcpy (name, Cmd_Argv(1), sizeof (name));
	FS_DefaultExtension (name, ".sav", sizeof (name));

	SV_Savegame_to(prog, name);
}


/*
===============
Host_Loadgame_f
===============
*/
event_t on_load_game;

static void SV_Loadgame_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	char filename[MAX_QPATH];
	char mapname[MAX_QPATH];
	float time;
	const char *start;
	const char *end;
	const char *t;
	char *text;
	prvm_edict_t *ent;
	int i, k, numbuffers;
	int entnum;
	int version;
	float spawn_parms[NUM_SPAWN_PARMS];
	prvm_stringbuffer_t *stringbuffer;

	if (Cmd_Argc() != 2)
	{
		Con_Print("load <savename> : load a game\n");
		return;
	}

	strlcpy (filename, Cmd_Argv(1), sizeof(filename));
	FS_DefaultExtension (filename, ".sav", sizeof (filename));

	Con_Printf("Loading game from %s...\n", filename);


	// TODO: Make this an event_t trigger
	// stop playing demos
	if (host.state & H_CS_DEMO)
		Event_Trigger(&on_load_game,NULL);

#ifdef CONFIG_MENU
	// remove menu
	if (key_dest == key_menu || key_dest == key_menu_grabbed)
		MR_ToggleMenu(0);
#endif
	key_dest = key_game;

	cls.demonum = -1;		// stop demo loop in case this fails

	t = text = (char *)FS_LoadFile (filename, tempmempool, false, NULL);
	if (!text)
	{
		Con_Print("ERROR: couldn't open.\n");
		return;
	}

	if(developer_entityparsing.integer)
		Con_Printf("Host_Loadgame_f: loading version\n");

	// version
	COM_ParseToken_Simple(&t, false, false, true);
	version = atoi(com_token);
	if (version != SAVEGAME_VERSION)
	{
		Mem_Free(text);
		Con_Printf("Savegame is version %i, not %i\n", version, SAVEGAME_VERSION);
		return;
	}

	if(developer_entityparsing.integer)
		Con_Printf("Host_Loadgame_f: loading description\n");

	// description
	COM_ParseToken_Simple(&t, false, false, true);

	for (i = 0;i < NUM_SPAWN_PARMS;i++)
	{
		COM_ParseToken_Simple(&t, false, false, true);
		spawn_parms[i] = atof(com_token);
	}
	// skill
	COM_ParseToken_Simple(&t, false, false, true);
// this silliness is so we can load 1.06 save files, which have float skill values
	current_skill = (int)(atof(com_token) + 0.5);
	Cvar_SetValue ("skill", (float)current_skill);

	if(developer_entityparsing.integer)
		Con_Printf("Host_Loadgame_f: loading mapname\n");

	// mapname
	COM_ParseToken_Simple(&t, false, false, true);
	strlcpy (mapname, com_token, sizeof(mapname));

	if(developer_entityparsing.integer)
		Con_Printf("Host_Loadgame_f: loading time\n");

	// time
	COM_ParseToken_Simple(&t, false, false, true);
	time = atof(com_token);

	if(developer_entityparsing.integer)
		Con_Printf("Host_Loadgame_f: spawning server\n");

	SV_SpawnServer (mapname);
	if (!(host.state & H_SS_ACTIVE))
	{
		Mem_Free(text);
		Con_Print("Couldn't load map\n");
		return;
	}
	sv.paused = true;		// pause until all clients connect
	sv.loadgame = true;

	if(developer_entityparsing.integer)
		Con_Printf("Host_Loadgame_f: loading light styles\n");

// load the light styles

	// -1 is the globals
	entnum = -1;

	for (i = 0;i < MAX_LIGHTSTYLES;i++)
	{
		// light style
		start = t;
		COM_ParseToken_Simple(&t, false, false, true);
		// if this is a 64 lightstyle savegame produced by Quake, stop now
		// we have to check this because darkplaces may save more than 64
		if (com_token[0] == '{')
		{
			t = start;
			break;
		}
		strlcpy(sv.lightstyles[i], com_token, sizeof(sv.lightstyles[i]));
	}

	if(developer_entityparsing.integer)
		Con_Printf("Host_Loadgame_f: skipping until globals\n");

	// now skip everything before the first opening brace
	// (this is for forward compatibility, so that older versions (at
	// least ones with this fix) can load savegames with extra data before the
	// first brace, as might be produced by a later engine version)
	for (;;)
	{
		start = t;
		if (!COM_ParseToken_Simple(&t, false, false, true))
			break;
		if (com_token[0] == '{')
		{
			t = start;
			break;
		}
	}

	// unlink all entities
	World_UnlinkAll(&sv.world);

// load the edicts out of the savegame file
	end = t;
	for (;;)
	{
		start = t;
		while (COM_ParseToken_Simple(&t, false, false, true))
			if (!strcmp(com_token, "}"))
				break;
		if (!COM_ParseToken_Simple(&start, false, false, true))
		{
			// end of file
			break;
		}
		if (strcmp(com_token,"{"))
		{
			Mem_Free(text);
			Host_Error ("First token isn't a brace");
		}

		if (entnum == -1)
		{
			if(developer_entityparsing.integer)
				Con_Printf("Host_Loadgame_f: loading globals\n");

			// parse the global vars
			PRVM_ED_ParseGlobals (prog, start);

			// restore the autocvar globals
			Cvar_UpdateAllAutoCvars();
		}
		else
		{
			// parse an edict
			if (entnum >= MAX_EDICTS)
			{
				Mem_Free(text);
				Host_Error("Host_PerformLoadGame: too many edicts in save file (reached MAX_EDICTS %i)", MAX_EDICTS);
			}
			while (entnum >= prog->max_edicts)
				PRVM_MEM_IncreaseEdicts(prog);
			ent = PRVM_EDICT_NUM(entnum);
			memset(ent->fields.fp, 0, prog->entityfields * sizeof(prvm_vec_t));
			ent->priv.server->free = false;

			if(developer_entityparsing.integer)
				Con_Printf("Host_Loadgame_f: loading edict %d\n", entnum);

			PRVM_ED_ParseEdict (prog, start, ent);

			// link it into the bsp tree
			if (!ent->priv.server->free)
				SV_LinkEdict(ent);
		}

		end = t;
		entnum++;
	}

	prog->num_edicts = entnum;
	sv.time = time;

	for (i = 0;i < NUM_SPAWN_PARMS;i++)
		svs.clients[0].spawn_parms[i] = spawn_parms[i];

	if(developer_entityparsing.integer)
		Con_Printf("Host_Loadgame_f: skipping until extended data\n");

	// read extended data if present
	// the extended data is stored inside a /* */ comment block, which the
	// parser intentionally skips, so we have to check for it manually here
	if(end)
	{
		while (*end == '\r' || *end == '\n')
			end++;
		if (end[0] == '/' && end[1] == '*' && (end[2] == '\r' || end[2] == '\n'))
		{
			if(developer_entityparsing.integer)
				Con_Printf("Host_Loadgame_f: loading extended data\n");

			Con_Printf("Loading extended DarkPlaces savegame\n");
			t = end + 2;
			memset(sv.lightstyles[0], 0, sizeof(sv.lightstyles));
			memset(sv.model_precache[0], 0, sizeof(sv.model_precache));
			memset(sv.sound_precache[0], 0, sizeof(sv.sound_precache));
			BufStr_Flush(prog);

			while (COM_ParseToken_Simple(&t, false, false, true))
			{
				if (!strcmp(com_token, "sv.lightstyles"))
				{
					COM_ParseToken_Simple(&t, false, false, true);
					i = atoi(com_token);
					COM_ParseToken_Simple(&t, false, false, true);
					if (i >= 0 && i < MAX_LIGHTSTYLES)
						strlcpy(sv.lightstyles[i], com_token, sizeof(sv.lightstyles[i]));
					else
						Con_Printf("unsupported lightstyle %i \"%s\"\n", i, com_token);
				}
				else if (!strcmp(com_token, "sv.model_precache"))
				{
					COM_ParseToken_Simple(&t, false, false, true);
					i = atoi(com_token);
					COM_ParseToken_Simple(&t, false, false, true);
					if (i >= 0 && i < MAX_MODELS)
					{
						strlcpy(sv.model_precache[i], com_token, sizeof(sv.model_precache[i]));
						sv.models[i] = Mod_ForName (sv.model_precache[i], true, false, sv.model_precache[i][0] == '*' ? sv.worldname : NULL);
					}
					else
						Con_Printf("unsupported model %i \"%s\"\n", i, com_token);
				}
				else if (!strcmp(com_token, "sv.sound_precache"))
				{
					COM_ParseToken_Simple(&t, false, false, true);
					i = atoi(com_token);
					COM_ParseToken_Simple(&t, false, false, true);
					if (i >= 0 && i < MAX_SOUNDS)
						strlcpy(sv.sound_precache[i], com_token, sizeof(sv.sound_precache[i]));
					else
						Con_Printf("unsupported sound %i \"%s\"\n", i, com_token);
				}
				else if (!strcmp(com_token, "sv.buffer"))
				{
					if (COM_ParseToken_Simple(&t, false, false, true))
					{
						i = atoi(com_token);
						if (i >= 0)
						{
							k = STRINGBUFFER_SAVED;
							if (COM_ParseToken_Simple(&t, false, false, true))
								k |= atoi(com_token);
							if (!BufStr_FindCreateReplace(prog, i, k, "string"))
								Con_Printf("failed to create stringbuffer %i\n", i);
						}
						else
							Con_Printf("unsupported stringbuffer index %i \"%s\"\n", i, com_token);
					}
					else
						Con_Printf("unexpected end of line when parsing sv.buffer (expected buffer index)\n");
				}
				else if (!strcmp(com_token, "sv.bufstr"))
				{
					if (!COM_ParseToken_Simple(&t, false, false, true))
						Con_Printf("unexpected end of line when parsing sv.bufstr\n");
					else
					{
						i = atoi(com_token);
						stringbuffer = BufStr_FindCreateReplace(prog, i, STRINGBUFFER_SAVED, "string");
						if (stringbuffer)
						{
							if (COM_ParseToken_Simple(&t, false, false, true))
							{
								k = atoi(com_token);
								if (COM_ParseToken_Simple(&t, false, false, true))
									BufStr_Set(prog, stringbuffer, k, com_token);
								else
									Con_Printf("unexpected end of line when parsing sv.bufstr (expected string)\n");
							}
							else
								Con_Printf("unexpected end of line when parsing sv.bufstr (expected strindex)\n");
						}
						else
							Con_Printf("failed to create stringbuffer %i \"%s\"\n", i, com_token);
					}
				}
				// skip any trailing text or unrecognized commands
				while (COM_ParseToken_Simple(&t, true, false, true) && strcmp(com_token, "\n"))
					;
			}
		}
	}
	Mem_Free(text);

	// remove all temporary flagged string buffers (ones created with BufStr_FindCreateReplace)
	numbuffers = (int)Mem_ExpandableArray_IndexRange(&prog->stringbuffersarray);
	for (i = 0; i < numbuffers; i++)
	{
		if ( (stringbuffer = (prvm_stringbuffer_t *)Mem_ExpandableArray_RecordAtIndex(&prog->stringbuffersarray, i)) )
			if (stringbuffer->flags & STRINGBUFFER_TEMP)
				BufStr_Del(prog, stringbuffer);
	}

	if(developer_entityparsing.integer)
		Con_Printf("Host_Loadgame_f: finished\n");
}

void SV_Init_Commands(void)
{
	// Server only commands
	Cmd_AddCommand (0, "map", SV_Map_f, "kick everyone off the server and start a new level");
	Cmd_AddCommand (0, "restart", SV_Restart_f, "restart current level");
	Cmd_AddCommand (0, "changelevel", SV_Changelevel_f, "change to another level, bringing along all connected clients");
	Cmd_AddCommand (0, "kick", SV_Kick_f, "kick a player off the server by number or name");
	Cmd_AddCommand (CMD_SERVER, "pause", SV_Pause_f, "pause the game (if the server allows pausing)");

	Cmd_AddCommand (0, "load", SV_Loadgame_f, "load a saved game file");
	Cmd_AddCommand (0, "save", SV_Savegame_f, "save the game to a file");

	// Shared commands (client and server)
	Cmd_AddCommand (0, "status", SV_Status_f, "print server status information");
	Cmd_AddCommand (0, "ping", SV_Ping_f, "print ping times of all players on the server");
	Cmd_AddCommand (0, "say", SV_Say_f, "send a chat message to everyone on the server");
	Cmd_AddCommand (0, "say_team", SV_Say_Team_f, "send a chat message to your team on the server");
	Cmd_AddCommand (0, "tell", SV_Tell_f, "send a chat message to only one person on the server");

	// Client commands
	Cmd_AddCommand (CMD_CLIENT, "kill", SV_Kill_f, "die instantly");
	Cmd_AddCommand (CMD_CLIENT, "download", SV_Download_f, "downloads a specified file from the server");
	Cmd_AddCommand (CMD_CLIENT, "pings", SV_Pings_f, "command sent by clients to request updated ping and packetloss of players on scoreboard (originally from QW, but also used on NQ servers)");

	Cmd_AddCommand (CMD_CLIENT, "sendcvar", SV_SendCvar_f, "sends the value of a cvar to the server as a sentcvar command, for use by QuakeC");


	// Cheats
	Cmd_AddCommand (CMD_CLIENT | CMD_CHEAT, "god", SV_God_f, "god mode (invulnerability)");
	Cmd_AddCommand (CMD_CLIENT | CMD_CHEAT, "notarget", SV_Notarget_f, "notarget mode (monsters do not see you)");
	Cmd_AddCommand (CMD_CLIENT | CMD_CHEAT, "fly", SV_Fly_f, "fly mode (flight)");
	Cmd_AddCommand (CMD_CLIENT | CMD_CHEAT, "noclip", SV_Noclip_f, "noclip mode (flight without collisions, move through walls)");
	Cmd_AddCommand (CMD_CLIENT | CMD_CHEAT, "give", SV_Give_f, "alter inventory");

	Cmd_AddCommand (0, "fullserverinfo", SV_FullServerinfo_f, "internal use only, sent by server to client to update client's local copy of serverinfo string");
	Cmd_AddCommand (CMD_CLIENT, "prespawn", SV_PreSpawn_f, "signon 1 (client acknowledges that server information has been received)");
	Cmd_AddCommand (CMD_CLIENT, "spawn", SV_Spawn_f, "signon 2 (client has sent player information, and is asking server to send scoreboard rankings)");
	Cmd_AddCommand (CMD_CLIENT, "begin", SV_Begin_f, "signon 3 (client asks server to start sending entities, and will go to signon 4 (playing) when the first entity update is received)");
	Cmd_AddCommand (0, "maxplayers", SV_MaxPlayers_f, "sets limit on how many players (or bots) may be connected to the server at once");
	Cmd_AddCommand (CMD_CLIENT, "playerskin", SV_Playerskin_f, "change your player skin number");
	Cmd_AddCommand (CMD_CLIENT, "playermodel", SV_Playermodel_f, "change your player model");
	Cmd_AddCommand (CMD_CLIENT, "topcolor", SV_TopColor_f, "QW command to set top color without changing bottom color");
	Cmd_AddCommand (CMD_CLIENT, "bottomcolor", SV_BottomColor_f, "QW command to set bottom color without changing top color");

	Cmd_AddCommand (CMD_CLIENT, "name", SV_Name_f, "change your player name");
	Cmd_AddCommand (CMD_CLIENT, "color", SV_Color_f, "change your player shirt and pants colors");
	Cmd_AddCommand (CMD_CLIENT, "rate", SV_Rate_f, "change your network connection speed");
	Cmd_AddCommand (CMD_CLIENT, "rate_burstsize", SV_Rate_BurstSize_f, "change your network connection speed");
	Cmd_AddCommand (CMD_CLIENT, "pmodel", SV_PModel_f, "(Nehahra-only) change your player model choice");
}
