/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// con_screen.c - In-game dropdown console

#include "quakedef.h"
#include "ft2.h"
#include "thread.h"

cvar_t con_notifytime = {CVAR_SAVE, "con_notifytime","3", "how long notify lines last, in seconds"};
cvar_t con_notify = {CVAR_SAVE, "con_notify","4", "how many notify lines to show"};
cvar_t con_notifyalign = {CVAR_SAVE, "con_notifyalign", "", "how to align notify lines: 0 = left, 0.5 = center, 1 = right, empty string = game default)"};

cvar_t con_chattime = {CVAR_SAVE, "con_chattime","30", "how long chat lines last, in seconds"};
cvar_t con_chat = {CVAR_SAVE, "con_chat","0", "how many chat lines to show in a dedicated chat area"};
cvar_t con_chatpos = {CVAR_SAVE, "con_chatpos","0", "where to put chat (negative: lines from bottom of screen, positive: lines below notify, 0: at top)"};
cvar_t con_chatrect = {CVAR_SAVE, "con_chatrect","0", "use con_chatrect_x and _y to position con_notify and con_chat freely instead of con_chatpos"};
cvar_t con_chatrect_x = {CVAR_SAVE, "con_chatrect_x","", "where to put chat, relative x coordinate of left edge on screen (use con_chatwidth for width)"};
cvar_t con_chatrect_y = {CVAR_SAVE, "con_chatrect_y","", "where to put chat, relative y coordinate of top edge on screen (use con_chat for line count)"};
cvar_t con_chatwidth = {CVAR_SAVE, "con_chatwidth","1.0", "relative chat window width"};
cvar_t con_textsize = {CVAR_SAVE, "con_textsize","8", "console text size in virtual 2D pixels"};
cvar_t con_notifysize = {CVAR_SAVE, "con_notifysize","8", "notify text size in virtual 2D pixels"};
cvar_t con_chatsize = {CVAR_SAVE, "con_chatsize","8", "chat text size in virtual 2D pixels (if con_chat is enabled)"};
cvar_t con_chatsound = {CVAR_SAVE, "con_chatsound","1", "enables chat sound to play on message"};

float con_cursorspeed = 4;
int con_linewidth;
int con_vislines;

/*
================
Con_CheckResize
================
*/
void Con_CheckResize (void)
{
	int i, width;
	float f;

	f = bound(1, con_textsize.value, 128);
	if(f != con_textsize.value)
		Cvar_SetValueQuick(&con_textsize, f);
	width = (int)floor(vid_conwidth.value / con_textsize.value);
	width = bound(1, width, con.textsize/4);
		// FIXME uses con in a non abstracted way

	if (width == con_linewidth)
		return;

	con_linewidth = width;

	for(i = 0; i < CON_LINES_COUNT; ++i)
		CON_LINES(i).height = -1; // recalculate when next needed

	Con_ClearNotify();
	con_backscroll = 0;
}

/*
================
Con_DrawInput

The input line scrolls horizontally if typing goes beyond the right edge

Modified by EvilTypeGuy eviltypeguy@qeradiant.com
================
*/
static void Con_DrawInput (void)
{
	int		y;
	int		i;
	char text[sizeof(key_line)+5+1]; // space for ^^xRGB too
	float x, xo;
	size_t len_out;
	int col_out;

	if (!key_consoleactive)
		return;		// don't draw anything

	strlcpy(text, key_line, sizeof(text));

	// Advanced Console Editing by Radix radix@planetquake.com
	// Added/Modified by EvilTypeGuy eviltypeguy@qeradiant.com

	y = (int)strlen(text);

	// make the color code visible when the cursor is inside it
	if(text[key_linepos] != 0)
	{
		for(i=1; i < 5 && key_linepos - i > 0; ++i)
			if(text[key_linepos-i] == STRING_COLOR_TAG)
			{
				int caret_pos, ofs = 0;
				caret_pos = key_linepos - i;
				if(i == 1 && text[caret_pos+1] == STRING_COLOR_TAG)
					ofs = 1;
				else if(i == 1 && isdigit(text[caret_pos+1]))
					ofs = 2;
				else if(text[caret_pos+1] == STRING_COLOR_RGB_TAG_CHAR && isxdigit(text[caret_pos+2]) && isxdigit(text[caret_pos+3]) && isxdigit(text[caret_pos+4]))
					ofs = 5;
				if(ofs && (size_t)(y + ofs + 1) < sizeof(text))
				{
					int carets = 1;
					while(caret_pos - carets >= 1 && text[caret_pos - carets] == STRING_COLOR_TAG)
						++carets;
					if(carets & 1)
					{
						// str^2ing (displayed as string) --> str^2^^2ing (displayed as str^2ing)
						// str^^ing (displayed as str^ing) --> str^^^^ing (displayed as str^^ing)
						memmove(&text[caret_pos + ofs + 1], &text[caret_pos], y - caret_pos);
						text[caret_pos + ofs] = STRING_COLOR_TAG;
						y += ofs + 1;
						text[y] = 0;
					}
				}
				break;
			}
	}

	len_out = key_linepos;
	col_out = -1;
	xo = DrawQ_TextWidth_UntilWidth_TrackColors(text, &len_out, con_textsize.value, con_textsize.value, &col_out, false, FONT_CONSOLE, 1000000000);
	x = vid_conwidth.value * 0.95 - xo; // scroll
	if(x >= 0)
		x = 0;

	// draw it
	DrawQ_String(x, con_vislines - con_textsize.value*2, text, y + 3, con_textsize.value, con_textsize.value, 1.0, 1.0, 1.0, 1.0, 0, NULL, false, FONT_CONSOLE );

	// draw a cursor on top of this
	if ((int)(host.realtime*con_cursorspeed) & 1)		// cursor is visible
	{
		if (!utf8_enable.integer)
		{
			text[0] = 11 + 130 * key_insert;	// either solid or triangle facing right
			text[1] = 0;
		}
		else
		{
			size_t len;
			const char *curbuf;
			char charbuf16[16];
			curbuf = u8_encodech(0xE000 + 11 + 130 * key_insert, &len, charbuf16);
			memcpy(text, curbuf, len);
			text[len] = 0;
		}
		DrawQ_String(x + xo, con_vislines - con_textsize.value*2, text, 0, con_textsize.value, con_textsize.value, 1.0, 1.0, 1.0, 1.0, 0, &col_out, false, FONT_CONSOLE);
	}
}

typedef struct
{
	dp_font_t *font;
	float alignment; // 0 = left, 0.5 = center, 1 = right
	float fontsize;
	float x;
	float y;
	float width;
	float ymin, ymax;
	const char *continuationString;

	// PRIVATE:
	int colorindex; // init to -1
}
con_text_info_t;

static float Con_WordWidthFunc(void *passthrough, const char *w, size_t *length, float maxWidth)
{
	con_text_info_t *ti = (con_text_info_t *) passthrough;
	if(w == NULL)
	{
		ti->colorindex = -1;
		return ti->fontsize * ti->font->maxwidth;
	}
	if(maxWidth >= 0)
		return DrawQ_TextWidth_UntilWidth(w, length, ti->fontsize, ti->fontsize, false, ti->font, -maxWidth); // -maxWidth: we want at least one char
	else if(maxWidth == -1)
		return DrawQ_TextWidth(w, *length, ti->fontsize, ti->fontsize, false, ti->font);
	else
	{
		Sys_PrintfToTerminal("Con_WordWidthFunc: can't get here (maxWidth should never be %f)\n", maxWidth);
		// Note: this is NOT a Con_Printf, as it could print recursively
		return 0;
	}
}

static int Con_CountLineFunc(void *passthrough, const char *line, size_t length, float width, qboolean isContinuation)
{
	(void) passthrough;
	(void) line;
	(void) length;
	(void) width;
	(void) isContinuation;
	return 1;
}

static int Con_DisplayLineFunc(void *passthrough, const char *line, size_t length, float width, qboolean isContinuation)
{
	con_text_info_t *ti = (con_text_info_t *) passthrough;

	if(ti->y < ti->ymin - 0.001)
		(void) 0;
	else if(ti->y > ti->ymax - ti->fontsize + 0.001)
		(void) 0;
	else
	{
		int x = (int) (ti->x + (ti->width - width) * ti->alignment);
		if(isContinuation && *ti->continuationString)
			x = (int) DrawQ_String(x, ti->y, ti->continuationString, strlen(ti->continuationString), ti->fontsize, ti->fontsize, 1.0, 1.0, 1.0, 1.0, 0, NULL, false, ti->font);
		if(length > 0)
			DrawQ_String(x, ti->y, line, length, ti->fontsize, ti->fontsize, 1.0, 1.0, 1.0, 1.0, 0, &(ti->colorindex), false, ti->font);
	}

	ti->y += ti->fontsize;
	return 1;
}

static int Con_DrawNotifyRect(int mask_must, int mask_mustnot, float maxage, float x, float y, float width, float height, float fontsize, float alignment_x, float alignment_y, const char *continuationString)
{
	int i;
	int lines = 0;
	int maxlines = (int) floor(height / fontsize + 0.01f);
	int startidx;
	int nskip = 0;
	int continuationWidth = 0;
	size_t len;
	double t = cl.time; // saved so it won't change
	con_text_info_t ti;

	ti.font = (mask_must & CON_MASK_CHAT) ? FONT_CHAT : FONT_NOTIFY;
	ti.fontsize = fontsize;
	ti.alignment = alignment_x;
	ti.width = width;
	ti.ymin = y;
	ti.ymax = y + height;
	ti.continuationString = continuationString;

	len = 0;
	Con_WordWidthFunc(&ti, NULL, &len, -1);
	len = strlen(continuationString);
	continuationWidth = (int) Con_WordWidthFunc(&ti, continuationString, &len, -1);

	// first find the first line to draw by backwards iterating and word wrapping to find their length...
	startidx = CON_LINES_COUNT;
	for(i = CON_LINES_COUNT - 1; i >= 0; --i)
	{
		con_lineinfo_t *l = &CON_LINES(i);
		int mylines;

		if((l->mask & mask_must) != mask_must)
			continue;
		if(l->mask & mask_mustnot)
			continue;
		if(maxage && (l->addtime < t - maxage))
			continue;

		// WE FOUND ONE!
		// Calculate its actual height...
		mylines = COM_Wordwrap(l->start, l->len, continuationWidth, width, Con_WordWidthFunc, &ti, Con_CountLineFunc, &ti);
		if(lines + mylines >= maxlines)
		{
			nskip = lines + mylines - maxlines;
			lines = maxlines;
			startidx = i;
			break;
		}
		lines += mylines;
		startidx = i;
	}

	// then center according to the calculated amount of lines...
	ti.x = x;
	ti.y = y + alignment_y * (height - lines * fontsize) - nskip * fontsize;

	// then actually draw
	for(i = startidx; i < CON_LINES_COUNT; ++i)
	{
		con_lineinfo_t *l = &CON_LINES(i);

		if((l->mask & mask_must) != mask_must)
			continue;
		if(l->mask & mask_mustnot)
			continue;
		if(maxage && (l->addtime < t - maxage))
			continue;

		COM_Wordwrap(l->start, l->len, continuationWidth, width, Con_WordWidthFunc, &ti, Con_DisplayLineFunc, &ti);
	}

	return lines;
}

/*
================
Con_DrawNotify

Draws the last few lines of output transparently over the game top
================
*/
void Con_DrawNotify (void)
{
	float	x, v, xr;
	float chatstart, notifystart, inputsize, height;
	float align;
	char	temptext[MAX_INPUTLINE];
	int numChatlines;
	int chatpos;

	if (con_mutex) Thread_LockMutex(con_mutex);
	ConBuffer_FixTimes(&con);

	numChatlines = con_chat.integer;

	chatpos = con_chatpos.integer;

	if (con_notify.integer < 0)
		Cvar_SetValueQuick(&con_notify, 0);
	if (gamemode == GAME_TRANSFUSION)
		v = 8; // vertical offset
	else
		v = 0;

	// GAME_NEXUIZ: center, otherwise left justify
	align = con_notifyalign.value;
	if(!*con_notifyalign.string) // empty string, evaluated to 0 above
	{
		if(IS_OLDNEXUIZ_DERIVED(gamemode))
			align = 0.5;
	}

	if(numChatlines || !con_chatrect.integer)
	{
		if(chatpos == 0)
		{
			// first chat, input line, then notify
			chatstart = v;
			notifystart = v + (numChatlines + 1) * con_chatsize.value;
		}
		else if(chatpos > 0)
		{
			// first notify, then (chatpos-1) empty lines, then chat, then input
			notifystart = v;
			chatstart = v + (con_notify.value + (chatpos - 1)) * con_notifysize.value;
		}
		else // if(chatpos < 0)
		{
			// first notify, then much space, then chat, then input, then -chatpos-1 empty lines
			notifystart = v;
			chatstart = vid_conheight.value - (-chatpos-1 + numChatlines + 1) * con_chatsize.value;
		}
	}
	else
	{
		// just notify and input
		notifystart = v;
		chatstart = 0; // shut off gcc warning
	}

	v = notifystart + con_notifysize.value * Con_DrawNotifyRect(0, CON_MASK_INPUT | CON_MASK_HIDENOTIFY | (numChatlines ? CON_MASK_CHAT : 0) | CON_MASK_DEVELOPER, con_notifytime.value, 0, notifystart, vid_conwidth.value, con_notify.value * con_notifysize.value, con_notifysize.value, align, 0.0, "");

	if(con_chatrect.integer)
	{
		x = con_chatrect_x.value * vid_conwidth.value;
		v = con_chatrect_y.value * vid_conheight.value;
	}
	else
	{
		x = 0;
		if(numChatlines) // only do this if chat area is enabled, or this would move the input line wrong
			v = chatstart;
	}
	height = numChatlines * con_chatsize.value;

	if(numChatlines)
	{
		Con_DrawNotifyRect(CON_MASK_CHAT, CON_MASK_INPUT, con_chattime.value, x, v, vid_conwidth.value * con_chatwidth.value, height, con_chatsize.value, 0.0, 1.0, "^3 ... ");
		v += height;
	}
	if (key_dest == key_message)
	{
		//static char *cursor[2] = { "\xee\x80\x8a", "\xee\x80\x8b" }; // { off, on }
		int colorindex = -1;
		const char *cursor;
		char charbuf16[16];
		cursor = u8_encodech(0xE00A + ((int)(host.realtime * con_cursorspeed)&1), NULL, charbuf16);

		// LordHavoc: speedup, and other improvements
		if (chat_mode < 0)
			dpsnprintf(temptext, sizeof(temptext), "]%s%s", chat_buffer, cursor);
		else if(chat_mode)
			dpsnprintf(temptext, sizeof(temptext), "say_team:%s%s", chat_buffer, cursor);
		else
			dpsnprintf(temptext, sizeof(temptext), "say:%s%s", chat_buffer, cursor);

		// FIXME word wrap
		inputsize = (numChatlines ? con_chatsize : con_notifysize).value;
		xr = vid_conwidth.value - DrawQ_TextWidth(temptext, 0, inputsize, inputsize, false, FONT_CHAT);
		x = min(xr, x);
		DrawQ_String(x, v, temptext, 0, inputsize, inputsize, 1.0, 1.0, 1.0, 1.0, 0, &colorindex, false, FONT_CHAT);
	}
	if (con_mutex) Thread_UnlockMutex(con_mutex);
}

/*
================
Con_LineHeight

Returns the height of a given console line; calculates it if necessary.
================
*/
static int Con_LineHeight(int lineno)
{
	con_lineinfo_t *li = &CON_LINES(lineno);
	if(li->height == -1)
	{
		float width = vid_conwidth.value;
		con_text_info_t ti;
		ti.fontsize = con_textsize.value;
		ti.font = FONT_CONSOLE;
		li->height = COM_Wordwrap(li->start, li->len, 0, width, Con_WordWidthFunc, &ti, Con_CountLineFunc, NULL);
	}
	return li->height;
}

/*
================
Con_DrawConsoleLine

Draws a line of the console; returns its height in lines.
If alpha is 0, the line is not drawn, but still wrapped and its height
returned.
================
*/
static int Con_DrawConsoleLine(int mask_must, int mask_mustnot, float y, int lineno, float ymin, float ymax)
{
	float width = vid_conwidth.value;
	con_text_info_t ti;
	con_lineinfo_t *li = &CON_LINES(lineno);

	if((li->mask & mask_must) != mask_must)
		return 0;
	if((li->mask & mask_mustnot) != 0)
		return 0;

	ti.continuationString = "";
	ti.alignment = 0;
	ti.fontsize = con_textsize.value;
	ti.font = FONT_CONSOLE;
	ti.x = 0;
	ti.y = y - (Con_LineHeight(lineno) - 1) * ti.fontsize;
	ti.ymin = ymin;
	ti.ymax = ymax;
	ti.width = width;

	return COM_Wordwrap(li->start, li->len, 0, width, Con_WordWidthFunc, &ti, Con_DisplayLineFunc, &ti);
}

/*
================
Con_LastVisibleLine

Calculates the last visible line index and how much to show of it based on
con_backscroll.
================
*/
static void Con_LastVisibleLine(int mask_must, int mask_mustnot, int *last, int *limitlast)
{
	int lines_seen = 0;
	int i;

	if(con_backscroll < 0)
		con_backscroll = 0;

	*last = 0;

	// now count until we saw con_backscroll actual lines
	for(i = CON_LINES_COUNT - 1; i >= 0; --i)
	if((CON_LINES(i).mask & mask_must) == mask_must)
	if((CON_LINES(i).mask & mask_mustnot) == 0)
	{
		int h = Con_LineHeight(i);

		// line is the last visible line?
		*last = i;
		if(lines_seen + h > con_backscroll && lines_seen <= con_backscroll)
		{
			*limitlast = lines_seen + h - con_backscroll;
			return;
		}

		lines_seen += h;
	}

	// if we get here, no line was on screen - scroll so that one line is
	// visible then.
	con_backscroll = lines_seen - 1;
	*limitlast = 1;
}

/*
================
Con_DrawConsole

Draws the console with the solid background
The typing input line at the bottom should only be drawn if typing is allowed
================
*/
void Con_DrawConsole (int lines)
{
	float alpha, alpha0;
	double sx, sy;
	int mask_must = 0;
	int mask_mustnot = (developer.integer>0) ? 0 : CON_MASK_DEVELOPER;
	cachepic_t *conbackpic;

	if (lines <= 0)
		return;

	if (con_mutex) Thread_LockMutex(con_mutex);

	if (con_backscroll < 0)
		con_backscroll = 0;

	con_vislines = lines;

	r_draw2d_force = true;

// draw the background
	alpha0 = cls.signon == SIGNONS ? scr_conalpha.value : 1.0f; // always full alpha when not in game
	if((alpha = alpha0 * scr_conalphafactor.value) > 0)
	{
		sx = scr_conscroll_x.value;
		sy = scr_conscroll_y.value;
		conbackpic = scr_conbrightness.value >= 0.01f ? Draw_CachePic_Flags("gfx/conback", (sx != 0 || sy != 0) ? CACHEPICFLAG_NOCLAMP : 0) : NULL;
		sx *= host.realtime; sy *= host.realtime;
		sx -= floor(sx); sy -= floor(sy);
		if (Draw_IsPicLoaded(conbackpic))
			DrawQ_SuperPic(0, lines - vid_conheight.integer, conbackpic, vid_conwidth.integer, vid_conheight.integer,
					0 + sx, 0 + sy, scr_conbrightness.value, scr_conbrightness.value, scr_conbrightness.value, alpha,
					1 + sx, 0 + sy, scr_conbrightness.value, scr_conbrightness.value, scr_conbrightness.value, alpha,
					0 + sx, 1 + sy, scr_conbrightness.value, scr_conbrightness.value, scr_conbrightness.value, alpha,
					1 + sx, 1 + sy, scr_conbrightness.value, scr_conbrightness.value, scr_conbrightness.value, alpha,
					0);
		else
			DrawQ_Fill(0, lines - vid_conheight.integer, vid_conwidth.integer, vid_conheight.integer, 0.0f, 0.0f, 0.0f, alpha, 0);
	}
	if((alpha = alpha0 * scr_conalpha2factor.value) > 0)
	{
		sx = scr_conscroll2_x.value;
		sy = scr_conscroll2_y.value;
		conbackpic = Draw_CachePic_Flags("gfx/conback2", (sx != 0 || sy != 0) ? CACHEPICFLAG_NOCLAMP : 0);
		sx *= host.realtime; sy *= host.realtime;
		sx -= floor(sx); sy -= floor(sy);
		if(Draw_IsPicLoaded(conbackpic))
			DrawQ_SuperPic(0, lines - vid_conheight.integer, conbackpic, vid_conwidth.integer, vid_conheight.integer,
					0 + sx, 0 + sy, scr_conbrightness.value, scr_conbrightness.value, scr_conbrightness.value, alpha,
					1 + sx, 0 + sy, scr_conbrightness.value, scr_conbrightness.value, scr_conbrightness.value, alpha,
					0 + sx, 1 + sy, scr_conbrightness.value, scr_conbrightness.value, scr_conbrightness.value, alpha,
					1 + sx, 1 + sy, scr_conbrightness.value, scr_conbrightness.value, scr_conbrightness.value, alpha,
					0);
	}
	if((alpha = alpha0 * scr_conalpha3factor.value) > 0)
	{
		sx = scr_conscroll3_x.value;
		sy = scr_conscroll3_y.value;
		conbackpic = Draw_CachePic_Flags("gfx/conback3", (sx != 0 || sy != 0) ? CACHEPICFLAG_NOCLAMP : 0);
		sx *= host.realtime; sy *= host.realtime;
		sx -= floor(sx); sy -= floor(sy);
		if(Draw_IsPicLoaded(conbackpic))
			DrawQ_SuperPic(0, lines - vid_conheight.integer, conbackpic, vid_conwidth.integer, vid_conheight.integer,
					0 + sx, 0 + sy, scr_conbrightness.value, scr_conbrightness.value, scr_conbrightness.value, alpha,
					1 + sx, 0 + sy, scr_conbrightness.value, scr_conbrightness.value, scr_conbrightness.value, alpha,
					0 + sx, 1 + sy, scr_conbrightness.value, scr_conbrightness.value, scr_conbrightness.value, alpha,
					1 + sx, 1 + sy, scr_conbrightness.value, scr_conbrightness.value, scr_conbrightness.value, alpha,
					0);
	}
	DrawQ_String(vid_conwidth.integer - DrawQ_TextWidth(engineversion, 0, con_textsize.value, con_textsize.value, false, FONT_CONSOLE), lines - con_textsize.value, engineversion, 0, con_textsize.value, con_textsize.value, 1, 0, 0, 1, 0, NULL, true, FONT_CONSOLE);

// draw the text
#if 0
	{
		int i;
		int count = CON_LINES_COUNT;
		float ymax = con_vislines - 2 * con_textsize.value;
		float y = ymax + con_textsize.value * con_backscroll;
		for (i = 0;i < count && y >= 0;i++)
			y -= Con_DrawConsoleLine(mask_must, mask_mustnot, y - con_textsize.value, CON_LINES_COUNT - 1 - i, 0, ymax) * con_textsize.value;
		// fix any excessive scrollback for the next frame
		if (i >= count && y >= 0)
		{
			con_backscroll -= (int)(y / con_textsize.value);
			if (con_backscroll < 0)
				con_backscroll = 0;
		}
	}
#else
	if(CON_LINES_COUNT > 0)
	{
		int i, last, limitlast;
		float y;
		float ymax = con_vislines - 2 * con_textsize.value;
		Con_LastVisibleLine(mask_must, mask_mustnot, &last, &limitlast);
		//Con_LastVisibleLine(mask_must, mask_mustnot, &last, &limitlast);
		y = ymax - con_textsize.value;

		if(limitlast)
			y += (CON_LINES(last).height - limitlast) * con_textsize.value;
		i = last;

		for(;;)
		{
			y -= Con_DrawConsoleLine(mask_must, mask_mustnot, y, i, 0, ymax) * con_textsize.value;
			if(i == 0)
				break; // top of console buffer
			if(y < 0)
				break; // top of console window
			limitlast = 0;
			--i;
		}
	}
#endif

// draw the input prompt, user text, and cursor if desired
	Con_DrawInput ();

	r_draw2d_force = false;
	if (con_mutex) Thread_UnlockMutex(con_mutex);
}

void Con_SCR_Init(void)
{
	// register our cvars
	Cvar_RegisterVariable (&con_chat);
	Cvar_RegisterVariable (&con_chatpos);
	Cvar_RegisterVariable (&con_chatrect_x);
	Cvar_RegisterVariable (&con_chatrect_y);
	Cvar_RegisterVariable (&con_chatrect);
	Cvar_RegisterVariable (&con_chatsize);
	Cvar_RegisterVariable (&con_chattime);
	Cvar_RegisterVariable (&con_chatwidth);
	Cvar_RegisterVariable (&con_notify);
	Cvar_RegisterVariable (&con_notifyalign);
	Cvar_RegisterVariable (&con_notifysize);
	Cvar_RegisterVariable (&con_notifytime);
	Cvar_RegisterVariable (&con_textsize);
	Cvar_RegisterVariable (&con_chatsound);

	Cmd_AddCommand (0, "toggleconsole", Con_ToggleConsole_f, "opens or closes the console");
}
