/*
Copyright (C) 1996-1997 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

// cl_cmds.c - Client console commands

#include "quakedef.h"
#include "client.h"
#include "cl_cmds.h"
#include "mdfour.h"
#include "hmac.h"
#include <time.h>

extern cvar_t rcon_secure_challengetimeout;
/*
=====================
CL_Connect_f

User command to connect to server
=====================
*/
extern cvar_t rcon_secure; //FIXME

void CL_Connect_f (void)
{
	if (Cmd_Argc() < 2)
	{
		Con_Print("connect <serveraddress> [<key> <value> ...]: connect to a multiplayer game\n");
		return;
	}
	// clear the rcon password, to prevent vulnerability by stuffcmd-ing a connect command
	if(rcon_secure.integer <= 0)
		Cvar_SetQuick(&rcon_password, "");
	CL_EstablishConnection(Cmd_Argv(1), 2);
}

void CL_Disconnect_f (void)
{
	CL_Disconnect(NULL);
}

/*
==================
CL_Reconnect_f

This command causes the client to wait for the signon messages again.
This is sent just before a server changes levels
==================
*/
void CL_Reconnect_f (void)
{
	char temp[128];
	// if not connected, reconnect to the most recent server
	if (!cls.netcon)
	{
		// if we have connected to a server recently, the userinfo
		// will still contain its IP address, so get the address...
		InfoString_GetValue(cls.userinfo, "*ip", temp, sizeof(temp));
		if (temp[0])
			CL_EstablishConnection(temp, -1);
		else
			Con_Printf("Reconnect to what server?  (you have not connected to a server yet)\n");
		return;
	}
	// if connected, do something based on protocol
	if (cls.protocol == &protocol_quakeworld)
	{
		// quakeworld can just re-login
		if (cls.qw_downloadmemory)  // don't change when downloading
			return;

		S_StopAllSounds();

		if ((host.state & H_CS_CONNECTED) && cls.signon < SIGNONS)
		{
			Con_Printf("reconnecting...\n");
			MSG_WriteChar(&cls.netcon->message, qw_clc_stringcmd);
			MSG_WriteString(&cls.netcon->message, "new");
		}
	}
	else
	{
		// netquake uses reconnect on level changes (silly)
		if (Cmd_Argc() != 1)
		{
			Con_Print("reconnect : wait for signon messages again\n");
			return;
		}
		if (!cls.signon)
		{
			Con_Print("reconnect: no signon, ignoring reconnect\n");
			return;
		}
		cls.signon = 0;		// need new connection messages
	}
}

/*
===============
CL_ModelIndexList_f

List information on all models in the client modelindex
===============
*/
void CL_ModelIndexList_f (void)
{
	int i;
	dp_model_t *model;

	// Print Header
	Con_Printf("%3s: %-30s %-8s %-8s\n", "ID", "Name", "Type", "Triangles");

	for (i = -MAX_MODELS;i < MAX_MODELS;i++)
	{
		model = CL_GetModelByIndex(i);

		if (!model)
		{
			continue;
		}

		if(model->loaded || i == 1)
		{
			Con_Printf("%3i: %-30s %-8s %-10i\n", i, model->name, model->modeldatatypestring, model->surfmesh.num_triangles);
		}
		else
		{
			Con_Printf("%3i: %-30s %-30s\n", i, model->name, "--no local model found--");
		}

		i++;
	}
}

/*
===============
CL_SoundIndexList_f

List all sounds in the client soundindex
===============
*/
void CL_SoundIndexList_f (void)
{
	int i = 1;

	while(cl.sound_precache[i] && i != MAX_SOUNDS)
	{ // Valid Sound
		Con_Printf("%i : %s\n", i, cl.sound_precache[i]->name);
		i++;
	}
}

// LordHavoc: pausedemo command
void CL_PauseDemo_f (void)
{
	cls.demopaused = !cls.demopaused;

	Con_Printf("Demo %spaused", cls.demopaused ? "" : "un");
	/*
	if (cls.demopaused)
	{
		Con_Print("Demo paused\n");
	}
	else
	{
		Con_Print("Demo unpaused\n");
	}
	*/
}

/*
======================
CL_Fog_f
======================
*/
void CL_Fog_f (void)
{
	if (Cmd_Argc () == 1)
	{
		Con_Printf("\"fog\" is \"%f %f %f %f %f %f %f %f %f\"\n", r_refdef.fog_density, r_refdef.fog_red, r_refdef.fog_green, r_refdef.fog_blue, r_refdef.fog_alpha, r_refdef.fog_start, r_refdef.fog_end, r_refdef.fog_height, r_refdef.fog_fadedepth);
		return;
	}

	FOG_clear(); // so missing values get good defaults

	if(Cmd_Argc() > 1)
	{
		r_refdef.fog_density = atof(Cmd_Argv(1));
	}
	if(Cmd_Argc() > 2)
	{
		r_refdef.fog_red = atof(Cmd_Argv(2));
	}
	if(Cmd_Argc() > 3)
	{
		r_refdef.fog_green = atof(Cmd_Argv(3));
	}
	if(Cmd_Argc() > 4)
	{
		r_refdef.fog_blue = atof(Cmd_Argv(4));
	}
	if(Cmd_Argc() > 5)
	{
		r_refdef.fog_alpha = atof(Cmd_Argv(5));
	}
	if(Cmd_Argc() > 6)
	{
		r_refdef.fog_start = atof(Cmd_Argv(6));
	}
	if(Cmd_Argc() > 7)
	{
		r_refdef.fog_end = atof(Cmd_Argv(7));
	}
	if(Cmd_Argc() > 8)
	{
		r_refdef.fog_height = atof(Cmd_Argv(8));
	}
	if(Cmd_Argc() > 9)
	{
		r_refdef.fog_fadedepth = atof(Cmd_Argv(9));
	}
}

/*
======================
CL_FogHeightTexture_f
======================
*/
void CL_Fog_HeightTexture_f (void)
{
	if (Cmd_Argc () < 11)
	{
		Con_Printf("\"fog_heighttexture\" is \"%f %f %f %f %f %f %f %f %f %s\"\n", r_refdef.fog_density, r_refdef.fog_red, r_refdef.fog_green, r_refdef.fog_blue, r_refdef.fog_alpha, r_refdef.fog_start, r_refdef.fog_end, r_refdef.fog_height, r_refdef.fog_fadedepth, r_refdef.fog_height_texturename);
		return;
	}

	FOG_clear(); // so missing values get good defaults

	r_refdef.fog_density = atof(Cmd_Argv(1));
	r_refdef.fog_red = atof(Cmd_Argv(2));
	r_refdef.fog_green = atof(Cmd_Argv(3));
	r_refdef.fog_blue = atof(Cmd_Argv(4));
	r_refdef.fog_alpha = atof(Cmd_Argv(5));
	r_refdef.fog_start = atof(Cmd_Argv(6));
	r_refdef.fog_end = atof(Cmd_Argv(7));
	r_refdef.fog_height = atof(Cmd_Argv(8));
	r_refdef.fog_fadedepth = atof(Cmd_Argv(9));

	strlcpy(r_refdef.fog_height_texturename, Cmd_Argv(10), sizeof(r_refdef.fog_height_texturename));
}


/*
====================
CL_TimeRefresh_f

For program optimization
====================
*/
void CL_TimeRefresh_f (void)
{
	int i;
	double timestart, timedelta;

	r_refdef.scene.extraupdate = false;

	timestart = Sys_DirtyTime();

	for (i = 0;i < 128;i++)
	{
		Matrix4x4_CreateFromQuakeEntity(&r_refdef.view.matrix, r_refdef.view.origin[0], r_refdef.view.origin[1], r_refdef.view.origin[2], 0, i / 128.0 * 360.0, 0, 1);
		r_refdef.view.quality = 1;
		CL_UpdateScreen();
	}

	timedelta = Sys_DirtyTime() - timestart;

	Con_Printf("%f seconds (%f fps)\n", timedelta, 128/timedelta);
}

void CL_AreaStats_f (void)
{
	World_PrintAreaStats(&cl.world, "client");
}

/*
====================
CL_Packet_f

packet <destination> <contents>

Contents allows \n escape character
====================
*/
void CL_Packet_f (void)
{
	char send[2048];
	int i, l;
	const char *in;
	char *out;
	lhnetaddress_t address;
	lhnetsocket_t *mysocket;

	if (Cmd_Argc() != 3)
	{
		Con_Printf ("packet <destination> <contents>\n");
		return;
	}

	if (!LHNETADDRESS_FromString (&address, Cmd_Argv(1), sv_netport.integer))
	{
		Con_Printf ("Bad address\n");
		return;
	}

	in = Cmd_Argv(2);
	out = send+4;
	send[0] = send[1] = send[2] = send[3] = -1;

	l = (int)strlen (in);
	for (i=0 ; i<l ; i++)
	{
		if (out >= send + sizeof(send) - 1)
			break;
		if (in[i] == '\\' && in[i+1] == 'n')
		{
			*out++ = '\n';
			i++;
		}
		else if (in[i] == '\\' && in[i+1] == '0')
		{
			*out++ = '\0';
			i++;
		}
		else if (in[i] == '\\' && in[i+1] == 't')
		{
			*out++ = '\t';
			i++;
		}
		else if (in[i] == '\\' && in[i+1] == 'r')
		{
			*out++ = '\r';
			i++;
		}
		else if (in[i] == '\\' && in[i+1] == '"')
		{
			*out++ = '\"';
			i++;
		}
		else
			*out++ = in[i];
	}

	mysocket = NetConn_ChooseClientSocketForAddress(&address);
	if (!mysocket)
		mysocket = NetConn_ChooseServerSocketForAddress(&address);
	if (mysocket)
		NetConn_Write(mysocket, send, out - send, &address);
}

/*
===============================================================================

DEBUGGING TOOLS

===============================================================================
*/

prvm_edict_t *CL_FindViewthing_f (prvm_prog_t *prog)
{
	int		i;
	prvm_edict_t	*e;

	for (i=0 ; i<prog->num_edicts ; i++)
	{
		e = PRVM_EDICT_NUM(i);
		if (!strcmp (PRVM_GetString(prog, PRVM_serveredictstring(e, classname)), "viewthing"))
			return e;
	}
	Con_Print("No viewthing on map\n");
	return NULL;
}

/*
==================
Host_Viewmodel_f
==================
*/
void CL_Viewmodel_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	prvm_edict_t	*e;
	dp_model_t	*m;

	if (!(host.state & H_SS_ACTIVE))
		return;

	e = CL_FindViewthing_f (prog);
	if (e)
	{
		m = Mod_ForName (Cmd_Argv(1), false, true, NULL);
		if (m && m->loaded && m->Draw)
		{
			PRVM_serveredictfloat(e, frame) = 0;
			cl.model_precache[(int)PRVM_serveredictfloat(e, modelindex)] = m;
		}
		else
			Con_Printf("viewmodel: can't load %s\n", Cmd_Argv(1));
	}
}

/*
==================
Host_Viewframe_f
==================
*/
void CL_Viewframe_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	prvm_edict_t	*e;
	int		f;
	dp_model_t	*m;

	if (!(host.state & H_SS_ACTIVE))
		return;

	e = CL_FindViewthing_f (prog);
	if (e)
	{
		m = cl.model_precache[(int)PRVM_serveredictfloat(e, modelindex)];

		f = atoi(Cmd_Argv(1));
		if (f >= m->numframes)
			f = m->numframes-1;

		PRVM_serveredictfloat(e, frame) = f;
	}
}


void CL_PrintFrameName_f (dp_model_t *m, int frame)
{
	if (m->animscenes)
		Con_Printf("frame %i: %s\n", frame, m->animscenes[frame].name);
	else
		Con_Printf("frame %i\n", frame);
}

/*
==================
Host_Viewnext_f
==================
*/
void CL_Viewnext_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	prvm_edict_t	*e;
	dp_model_t	*m;

	if (!(host.state & H_SS_ACTIVE))
		return;

	e = CL_FindViewthing_f (prog);
	if (e)
	{
		m = cl.model_precache[(int)PRVM_serveredictfloat(e, modelindex)];

		PRVM_serveredictfloat(e, frame) = PRVM_serveredictfloat(e, frame) + 1;
		if (PRVM_serveredictfloat(e, frame) >= m->numframes)
			PRVM_serveredictfloat(e, frame) = m->numframes - 1;

		CL_PrintFrameName_f (m, (int)PRVM_serveredictfloat(e, frame));
	}
}

/*
==================
CL_Viewprev_f
==================
*/
void CL_Viewprev_f (void)
{
	prvm_prog_t *prog = SVVM_prog;
	prvm_edict_t	*e;
	dp_model_t	*m;

	if (!(host.state & H_SS_ACTIVE))
		return;

	e = CL_FindViewthing_f (prog);
	if (e)
	{
		m = cl.model_precache[(int)PRVM_serveredictfloat(e, modelindex)];

		PRVM_serveredictfloat(e, frame) = PRVM_serveredictfloat(e, frame) - 1;
		if (PRVM_serveredictfloat(e, frame) < 0)
			PRVM_serveredictfloat(e, frame) = 0;

		CL_PrintFrameName_f (m, (int)PRVM_serveredictfloat(e, frame));
	}
}

/*
=====================
CL_Rcon_f

  Send the rest of the command line over as
  an unconnected command.
=====================
*/
void CL_Rcon_f (void) // credit: taken from QuakeWorld
{
	int i, n;
	const char *e;
	lhnetsocket_t *mysocket;

	if (Cmd_Argc() == 1)
	{
		Con_Printf("%s: Usage: %s command\n", Cmd_Argv(0), Cmd_Argv(0));
		return;
	}

	if (!rcon_password.string || !rcon_password.string[0])
	{
		Con_Printf ("You must set rcon_password before issuing an rcon command.\n");
		return;
	}

	e = strchr(rcon_password.string, ' ');
	n = e ? e-rcon_password.string : (int)strlen(rcon_password.string);

	if (cls.netcon)
		cls.rcon_address = cls.netcon->peeraddress;
	else
	{
		if (!rcon_address.string[0])
		{
			Con_Printf ("You must either be connected, or set the rcon_address cvar to issue rcon commands\n");
			return;
		}
		LHNETADDRESS_FromString(&cls.rcon_address, rcon_address.string, sv_netport.integer);
	}
	mysocket = NetConn_ChooseClientSocketForAddress(&cls.rcon_address);
	if (mysocket && Cmd_Args()[0])
	{
		// simply put together the rcon packet and send it
		if(Cmd_Argv(0)[0] == 's' || rcon_secure.integer > 1)
		{
			if(cls.rcon_commands[cls.rcon_ringpos][0])
			{
				char s[128];
				LHNETADDRESS_ToString(&cls.rcon_addresses[cls.rcon_ringpos], s, sizeof(s), true);
				Con_Printf("rcon to %s (for command %s) failed: too many buffered commands (possibly increase MAX_RCONS)\n", s, cls.rcon_commands[cls.rcon_ringpos]);
				cls.rcon_commands[cls.rcon_ringpos][0] = 0;
				--cls.rcon_trying;
			}
			for (i = 0;i < MAX_RCONS;i++)
				if(cls.rcon_commands[i][0])
					if (!LHNETADDRESS_Compare(&cls.rcon_address, &cls.rcon_addresses[i]))
						break;
			++cls.rcon_trying;
			if(i >= MAX_RCONS)
				NetConn_WriteString(mysocket, "\377\377\377\377getchallenge", &cls.rcon_address); // otherwise we'll request the challenge later
			strlcpy(cls.rcon_commands[cls.rcon_ringpos], Cmd_Args(), sizeof(cls.rcon_commands[cls.rcon_ringpos]));
			cls.rcon_addresses[cls.rcon_ringpos] = cls.rcon_address;
			cls.rcon_timeout[cls.rcon_ringpos] = host.realtime + rcon_secure_challengetimeout.value;
			cls.rcon_ringpos = (cls.rcon_ringpos + 1) % MAX_RCONS;
		}
		else if(rcon_secure.integer > 0)
		{
			char buf[1500];
			char argbuf[1500];
			dpsnprintf(argbuf, sizeof(argbuf), "%ld.%06d %s", (long) time(NULL), (int) (rand() % 1000000), Cmd_Args());
			memcpy(buf, "\377\377\377\377srcon HMAC-MD4 TIME ", 24);
			if(HMAC_MDFOUR_16BYTES((unsigned char *) (buf + 24), (unsigned char *) argbuf, (int)strlen(argbuf), (unsigned char *) rcon_password.string, n))
			{
				buf[40] = ' ';
				strlcpy(buf + 41, argbuf, sizeof(buf) - 41);
				NetConn_Write(mysocket, buf, 41 + (int)strlen(buf + 41), &cls.rcon_address);
			}
		}
		else
		{
			char buf[1500];
			memcpy(buf, "\377\377\377\377", 4);
			dpsnprintf(buf+4, sizeof(buf)-4, "rcon %.*s %s",  n, rcon_password.string, Cmd_Args());
			NetConn_WriteString(mysocket, buf, &cls.rcon_address);
		}
	}
}

/*
====================
CL_User_f

user <name or userid>

Dump userdata / masterdata for a user
====================
*/
void CL_User_f (void) // credit: taken from QuakeWorld
{
	int		uid;
	int		i;

	if (Cmd_Argc() != 2)
	{
		Con_Printf ("Usage: user <username / userid>\n");
		return;
	}

	uid = atoi(Cmd_Argv(1));

	for (i = 0;i < cl.maxclients;i++)
	{
		if (!cl.scores[i].name[0])
			continue;
		if (cl.scores[i].qw_userid == uid || !strcasecmp(cl.scores[i].name, Cmd_Argv(1)))
		{
			InfoString_Print(cl.scores[i].qw_userinfo);
			return;
		}
	}
	Con_Printf ("User not in server.\n");
}

/*
====================
CL_Users_f

Dump userids for all current players
====================
*/
void CL_Users_f (void) // credit: taken from QuakeWorld
{
	int		i;
	int		c;

	c = 0;
	Con_Printf ("userid frags name\n");
	Con_Printf ("------ ----- ----\n");
	for (i = 0;i < cl.maxclients;i++)
	{
		if (cl.scores[i].name[0])
		{
			Con_Printf ("%6i %4i %s\n", cl.scores[i].qw_userid, cl.scores[i].frags, cl.scores[i].name);
			c++;
		}
	}

	Con_Printf ("%i total users\n", c);
}

/*
=====================
CL_PQRcon_f

ProQuake rcon support
=====================
*/
void CL_PQRcon_f (void)
{
	int n;
	const char *e;
	lhnetsocket_t *mysocket;

	if (Cmd_Argc() == 1)
	{
		Con_Printf("%s: Usage: %s command\n", Cmd_Argv(0), Cmd_Argv(0));
		return;
	}

	if (!rcon_password.string || !rcon_password.string[0] || rcon_secure.integer > 0)
	{
		Con_Printf ("You must set rcon_password before issuing an pqrcon command, and rcon_secure must be 0.\n");
		return;
	}

	e = strchr(rcon_password.string, ' ');
	n = e ? e-rcon_password.string : (int)strlen(rcon_password.string);

	if (cls.netcon)
		cls.rcon_address = cls.netcon->peeraddress;
	else
	{
		if (!rcon_address.string[0])
		{
			Con_Printf ("You must either be connected, or set the rcon_address cvar to issue rcon commands\n");
			return;
		}
		LHNETADDRESS_FromString(&cls.rcon_address, rcon_address.string, sv_netport.integer);
	}
	mysocket = NetConn_ChooseClientSocketForAddress(&cls.rcon_address);
	if (mysocket)
	{
		sizebuf_t buf;
		unsigned char bufdata[64];
		buf.data = bufdata;
		SZ_Clear(&buf);
		MSG_WriteLong(&buf, 0);
		MSG_WriteByte(&buf, CCREQ_RCON);
		SZ_Write(&buf, (const unsigned char*)rcon_password.string, n);
		MSG_WriteByte(&buf, 0); // terminate the (possibly partial) string
		MSG_WriteString(&buf, Cmd_Args());
		StoreBigLong(buf.data, NETFLAG_CTL | (buf.cursize & NETFLAG_LENGTH_MASK));
		NetConn_Write(mysocket, buf.data, buf.cursize, &cls.rcon_address);
		SZ_Clear(&buf);
	}
}

/*
==================
CL_SetInfo_f

Allow clients to change userinfo
==================
*/
void CL_SetInfo_f (void) // credit: taken from QuakeWorld
{
	if (Cmd_Argc() == 1)
	{
		InfoString_Print(cls.userinfo);
		return;
	}
	if (Cmd_Argc() != 3)
	{
		Con_Printf ("usage: setinfo [ <key> <value> ]\n");
		return;
	}
	CL_SetInfo(Cmd_Argv(1), Cmd_Argv(2), true, false, false, false);
}

/*
==================
CL_FullInfo_f

Allow clients to change userinfo
==================
Casey was here :)
*/
void CL_FullInfo_f (void) // credit: taken from QuakeWorld
{
	char key[512];
	char value[512];
	const char *s;

	if (Cmd_Argc() != 2)
	{
		Con_Printf ("fullinfo <complete info string>\n");
		return;
	}

	s = Cmd_Argv(1);
	if (*s == '\\')
		s++;
	while (*s)
	{
		size_t len = strcspn(s, "\\");
		if (len >= sizeof(key)) {
			len = sizeof(key) - 1;
		}
		strlcpy(key, s, len + 1);
		s += len;
		if (!*s)
		{
			Con_Printf ("MISSING VALUE\n");
			return;
		}
		++s; // Skip over backslash.

		len = strcspn(s, "\\");
		if (len >= sizeof(value)) {
			len = sizeof(value) - 1;
		}
		strlcpy(value, s, len + 1);

		CL_SetInfo(key, value, false, false, false, false);

		s += len;
		if (!*s)
		{
			break;
		}
		++s; // Skip over backslash.
	}
}

/*
==============
CL_PrintEntities_f
==============
*/
void CL_PrintEntities_f(void)
{
	entity_t *ent;
	int i;

	for (i = 0, ent = cl.entities;i < cl.num_entities;i++, ent++)
	{
		const char* modelname;

		if (!ent->state_current.active)
		{
			continue;
		}

		if (ent->render.model)
		{
			modelname = ent->render.model->name;
		}
		else
		{
			modelname = "--no model--";
		}

		Con_Printf("%3i: %-25s:%4i (%5i %5i %5i) [%3i %3i %3i] %4.2f %5.3f\n", i, modelname, ent->render.framegroupblend[0].frame, (int) ent->state_current.origin[0], (int) ent->state_current.origin[1], (int) ent->state_current.origin[2], (int) ent->state_current.angles[0] % 360, (int) ent->state_current.angles[1] % 360, (int) ent->state_current.angles[2] % 360, ent->render.scale, ent->render.alpha);
	}
}

/*
====================
CL_TimeDemo_f

timedemo [demoname]
====================
*/
void CL_TimeDemo_f (void)
{
	if (Cmd_Argc() != 2)
	{
		Con_Print("timedemo <demoname> : gets demo speeds\n");
		return;
	}

	srand(0); // predictable random sequence for benchmarking

	CL_PlayDemo_f ();

// cls.td_starttime will be grabbed at the second frame of the demo, so
// all the loading time doesn't get counted

	// instantly hide console and deactivate it
	key_dest = key_game;
	key_consoleactive = 0;
	scr_con_current = 0;

	cls.timedemo = true;
	cls.td_frames = -2;		// skip the first frame
	cls.demonum = -1;		// stop demo loop
}

/*
==================
CL_Startdemos_f
==================
*/
void CL_Startdemos_f (void)
{
	int		i, c;

	if ((host.state & H_DEDICATED) || Sys_CheckParm("-listen") || Sys_CheckParm("-benchmark") || Sys_CheckParm("-demo") || Sys_CheckParm("-capturedemo"))
		return;

	c = Cmd_Argc() - 1;
	if (c > MAX_DEMOS)
	{
		Con_Printf("Max %i demos in demoloop\n", MAX_DEMOS);
		c = MAX_DEMOS;
	}
	Con_DPrintf("%i demo(s) in loop\n", c);

	for (i=1 ; i<c+1 ; i++)
		strlcpy (cls.demos[i-1], Cmd_Argv(i), sizeof (cls.demos[i-1]));

	// LordHavoc: clear the remaining slots
	for (;i <= MAX_DEMOS;i++)
		cls.demos[i-1][0] = 0;

	if (!(host.state & H_SS_ACTIVE) && cls.demonum != -1 && !(host.state & H_CS_DEMO))
	{
		cls.demonum = 0;
		CL_NextDemo ();
	}
	else
		cls.demonum = -1;
}


/*
==================
CL_Demos_f

Return to looping demos
==================
*/
void CL_Demos_f (void)
{
	if (host.state & H_DEDICATED)
		return;
	if (cls.demonum == -1)
		cls.demonum = 1;
	CL_Disconnect_f ();
	CL_NextDemo ();
}

/*
==================
CL_Stopdemo_f

Return to looping demos
==================
*/
void CL_Stopdemo_f (void)
{
	if (!(host.state & H_CS_DEMO))
		return;
	CL_Disconnect (NULL);
	SV_Shutdown ();
}

/*
================
Con_MessageMode_f
================
*/
void Con_MessageMode_f (void)
{
	key_dest = key_message;
	chat_mode = 0; // "say"
	if(Cmd_Argc() > 1)
	{
		dpsnprintf(chat_buffer, sizeof(chat_buffer), "%s ", Cmd_Args());
		chat_bufferlen = (unsigned int)strlen(chat_buffer);
	}
}


/*
================
Con_MessageMode2_f
================
*/
void Con_MessageMode2_f (void)
{
	key_dest = key_message;
	chat_mode = 1; // "say_team"
	if(Cmd_Argc() > 1)
	{
		dpsnprintf(chat_buffer, sizeof(chat_buffer), "%s ", Cmd_Args());
		chat_bufferlen = (unsigned int)strlen(chat_buffer);
	}
}

/*
================
Con_CommandMode_f
================
*/
void Con_CommandMode_f (void)
{
	key_dest = key_message;
	if(Cmd_Argc() > 1)
	{
		dpsnprintf(chat_buffer, sizeof(chat_buffer), "%s ", Cmd_Args());
		chat_bufferlen = (unsigned int)strlen(chat_buffer);
	}
	chat_mode = -1; // command
}
