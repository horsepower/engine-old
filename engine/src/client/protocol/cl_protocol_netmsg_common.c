#include "quakedef.h"
#include "cl_parse.h"
#include "cdaudio.h"

void netmsg_svc_bad(void)
{
	int i;
	char description[32*64], logtemp[64];
	int count;
	strlcpy(description, "packet dump: ", sizeof(description));
	i = cl.cmdcount - 32;
	if (i < 0)
		i = 0;
	count = cl.cmdcount - i;
	i &= 31;
	while(count > 0)
	{
		dpsnprintf(logtemp, sizeof(logtemp), "%3i:%s ", (*cl.cmdlog[i]), cl.cmdlogname[i]);
		strlcat(description, logtemp, sizeof(description));
		count--;
		i++;
		i &= 31;
	}
	description[strlen(description)-1] = '\n'; // replace the last space with a newline
	Con_Print(description);
	Host_Error("CL_ParseServerMessage: Illegible server message");
}

void netmsg_svc_disconnect(void)
{
	if (cls.demonum != -1)
	{
		CL_NextDemo ();
	}
	else
	{
		if (cls.protocol == &protocol_horsepower1)
		{
			CL_Disconnect(MSG_ReadString(&cl_message, cl_readstring, sizeof(cl_readstring)));
		}
		else
		{
			CL_Disconnect(NULL);
		}
	}
}

void netmsg_svc_updatestat(void)
{
	int i;
	i = MSG_ReadByte(&cl_message);
	if (i < 0 || i >= MAX_CL_STATS)
		Host_Error ("svc_updatestat: %i is invalid", i);
	if (cls.protocol != &protocol_quakeworld)
	{
		cl.stats[i] = MSG_ReadLong(&cl_message); // Non-Quakeworld is long
	}
	else
	{
		cl.stats[i] = MSG_ReadByte(&cl_message); // Quakeworld is byte
	}
}

void netmsg_svc_version (void)
{
	// parse protocol version number
	int i = MSG_ReadLong(&cl_message);
	Protocol_GetByNumber(i,(&cls.protocol));
	if (!cls.protocol)
	{
		Host_Error("CL_ParseServerInfo: Server is unrecognized protocol number (%i)", i);
		return;
	}
	// hack for unmarked Nehahra movie demos which had a custom protocol
	if (cls.protocol == &protocol_quakedp && (host.state & H_CS_DEMO) && gamemode == GAME_NEHAHRA)
		cls.protocol = &protocol_nehahramovie;
}
void netmsg_svc_setview(void)
{
	cl.viewentity = (unsigned short)MSG_ReadShort(&cl_message);
	if (cl.viewentity >= MAX_EDICTS)
		Host_Error("svc_setview >= MAX_EDICTS");
	if (cl.viewentity >= cl.max_entities)
		CL_ExpandEntities(cl.viewentity);
	// LordHavoc: assume first setview recieved is the real player entity
	if (!cl.realplayerentity)
		cl.realplayerentity = cl.viewentity;
	// update cl.playerentity to this one if it is a valid player
	if (cl.viewentity >= 1 && cl.viewentity <= cl.maxclients)
		cl.playerentity = cl.viewentity;
}
void netmsg_svc_sound (void)
{
	CL_ParseStartSoundPacket(false);
}
void netmsg_svc_time(void)
{
	CL_NetworkTimeReceived(MSG_ReadFloat(&cl_message));
}
void netmsg_svc_print (void)
{
	char *temp;
	int i;
	char vabuf[1024];

	if(cls.protocol == &protocol_quakeworld)
	{
		i = MSG_ReadByte(&cl_message);
	}

	temp = MSG_ReadString(&cl_message, cl_readstring, sizeof(cl_readstring));

	if (CL_ExaminePrintString(temp)) // look for anything interesting like player IP addresses or ping reports
	{
		if (cls.protocol == &protocol_quakeworld && i == 3) // chat
		{
			CSQC_AddPrintText(va(vabuf, sizeof(vabuf), "\1%s", temp));	//[515]: csqc
		}
		else
		{
			CSQC_AddPrintText(temp);
		}
	}
}
void netmsg_svc_stufftext(void)
{
	qboolean strip_pqc;
	char *temp = MSG_ReadString(&cl_message, cl_readstring, sizeof(cl_readstring));
	/* if(utf8_enable.integer)
		{
			strip_pqc = true;
			// we can safely strip and even
			// interpret these in utf8 mode
		}
		else */
		if(cls.protocol == &protocol_netquake ||
				cls.protocol == &protocol_quakedp)
			// maybe add other protocols if
			// so desired, but not DP7
			strip_pqc = true;
		else
		{
			// ProQuake does not support
			// these protocols
			strip_pqc = false;
		}

		if(strip_pqc)
		{
			// skip over ProQuake messages,
			// TODO actually interpret them
			// (they are sbar team score
			// updates), see proquake cl_parse.c
			if(*temp == 0x01)
			{
				++temp;
				while(*temp >= 0x01 && *temp <= 0x1F)
					++temp;
			}
		}
		CL_VM_Parse_StuffCmd(temp);	//[515]: csqc
}
void netmsg_svc_setangle(void)
{
	for (int i=0 ; i<3 ; i++)
		cl.viewangles[i] = MSG_ReadAngle(&cl_message, cls.protocol);
	if (!(host.state & H_CS_DEMO))
	{
		cl.fixangle[0] = true;
		VectorCopy(cl.viewangles, cl.mviewangles[0]);
		// disable interpolation if this is new
		if (!cl.fixangle[1])
			VectorCopy(cl.viewangles, cl.mviewangles[1]);
	}
}
void netmsg_svc_serverinfo(void)
{
	CL_ParseServerInfo ();
}
void netmsg_svc_lightstyle(void)
{
	int i = MSG_ReadByte(&cl_message);
	if (i >= cl.max_lightstyle)
	{
		Con_Printf ("svc_lightstyle >= MAX_LIGHTSTYLES");
		return;
	}
	strlcpy (cl.lightstyle[i].map,  MSG_ReadString(&cl_message, cl_readstring, sizeof(cl_readstring)), sizeof (cl.lightstyle[i].map));
	cl.lightstyle[i].map[MAX_STYLESTRING - 1] = 0;
	cl.lightstyle[i].length = (int)strlen(cl.lightstyle[i].map);
}

void netmsg_svc_updatename(void)		//13	// [byte] [string]
{
	int i = MSG_ReadByte(&cl_message);
	if (i >= cl.maxclients)
		Host_Error ("CL_ParseServerMessage: svc_updatename >= cl.maxclients");
	strlcpy (cl.scores[i].name, MSG_ReadString(&cl_message, cl_readstring, sizeof(cl_readstring)), sizeof (cl.scores[i].name));
}
void netmsg_svc_updatefrags(void)		//14	// [byte] [short]
{
	int i = MSG_ReadByte(&cl_message);
	if (i >= cl.maxclients)
		Host_Error ("CL_ParseServerMessage: svc_updatefrags >= cl.maxclients");
	cl.scores[i].frags = (signed short) MSG_ReadShort(&cl_message);
}
void netmsg_svc_clientdata(void)		//15	// <shortbits + data>
{
	CL_ParseClientdata();
}
void netmsg_svc_stopsound(void)		//16	// <see code>
{
	int i = (unsigned short) MSG_ReadShort(&cl_message);
	S_StopSound(i>>3, i&7);
}
void netmsg_svc_updatecolors(void)	//17	// [byte] [byte]
{
	int i = MSG_ReadByte(&cl_message);
	if (i >= cl.maxclients)
		Host_Error ("CL_ParseServerMessage: svc_updatecolors >= cl.maxclients");
	cl.scores[i].colors = MSG_ReadByte(&cl_message);
}
void netmsg_svc_particle(void)		//18	// [vec3] <variable>
{
	CL_ParseParticleEffect ();
}
void netmsg_svc_damage(void)			//19
{
	V_ParseDamage ();
}
void netmsg_svc_spawnstatic(void)		//20
{
	CL_ParseStatic (false);
}
void netmsg_svc_spawnbinary(void)		//21
{
	Con_Printf("svc_spawnbinary UNIMPLEMENTED!\n");
}
void netmsg_svc_spawnbaseline(void)	//22
{
	unsigned int i = (unsigned short) MSG_ReadShort(&cl_message);
	if (i < 0 || i >= MAX_EDICTS)
		Host_Error ("CL_ParseServerMessage: svc_spawnbaseline: invalid entity number %i", i);
	if (i >= cl.max_entities)
		CL_ExpandEntities(i);
	CL_ParseBaseline (cl.entities + i, false);
}
void netmsg_svc_temp_entity(void)		//23
{
	if(!CL_VM_Parse_TempEntity())
		CL_ParseTempEntity ();
}
void netmsg_svc_setpause(void)		//24	// [byte] on / off
{
	cl.paused = MSG_ReadByte(&cl_message) != 0;
	if (cl.paused)
		CDAudio_Pause ();
	else
		CDAudio_Resume ();
	S_PauseGameSounds (cl.paused);
}
void netmsg_svc_signonnum(void)		//25	// [byte]  used for the signon sequence
{
	int i = MSG_ReadByte(&cl_message);
	// LordHavoc: it's rude to kick off the client if they missed the
	// reconnect somehow, so allow signon 1 even if at signon 1
	if (i <= cls.signon && i != 1)
		Host_Error ("Received signon %i when at %i", i, cls.signon);
	cls.signon = i;
	CL_SignonReply ();
}
void netmsg_svc_centerprint(void)		//26	// [string] to put in center of the screen
{
	CL_VM_Parse_CenterPrint(MSG_ReadString(&cl_message, cl_readstring, sizeof(cl_readstring)));	//[515]: csqc
}
void netmsg_svc_killedmonster(void)	//27
{
	cl.stats[STAT_MONSTERS]++;
}
void netmsg_svc_foundsecret(void)		//28
{
	cl.stats[STAT_SECRETS]++;
}
void netmsg_svc_spawnstaticsound(void)	//29	// [coord3] [byte] samp [byte] vol [byte] aten
{
	CL_ParseStaticSound (false);
}
void netmsg_svc_intermission(void)	//30		// [string] music
{
	int i;
	if(!cl.intermission)
		cl.completed_time = cl.time;
	cl.intermission = 1;
	if(cls.protocol == &protocol_quakeworld)
	{
		MSG_ReadVector(&cl_message, cl.qw_intermission_origin, cls.protocol);
		for (i = 0;i < 3;i++)
		{
			cl.qw_intermission_angles[i] = MSG_ReadAngle(&cl_message, cls.protocol);
		}
	}
	else
	{
		CL_VM_UpdateIntermissionState(cl.intermission);
	}
}
void netmsg_svc_finale(void)			//31		// [string] music [string] text
{
	if(!cl.intermission)
		cl.completed_time = cl.time;
	cl.intermission = 2;
	if(cls.protocol != &protocol_quakeworld)
	{
		CL_VM_UpdateIntermissionState(cl.intermission);
	}
	SCR_CenterPrint(MSG_ReadString(&cl_message, cl_readstring, sizeof(cl_readstring)));

}
void netmsg_svc_cdtrack(void)			//32		// [byte] track [byte] looptrack
{
	if(cls.protocol == &protocol_quakeworld)
	{
		cl.cdtrack = cl.looptrack = MSG_ReadByte(&cl_message);
	}
	else
	{
		cl.cdtrack = MSG_ReadByte(&cl_message);
		cl.looptrack = MSG_ReadByte(&cl_message);
	}
	if ( ((host.state & H_CS_DEMO) || cls.demorecording) && (cls.forcetrack != -1) )
		CDAudio_Play ((unsigned char)cls.forcetrack, true);
	else
		CDAudio_Play ((unsigned char)cl.cdtrack, true);
}
void netmsg_svc_sellscreen(void)		//33
{
	Cmd_ExecuteString ("help", src_command, true);
}
void netmsg_svc_cutscene(void)		//34
{
	if(cls.protocol == &protocol_quakeworld)
	{
		cl.qw_weaponkick = -2;
	}
	else
	{
		if(!cl.intermission)
			cl.completed_time = cl.time;
		cl.intermission = 3;
		CL_VM_UpdateIntermissionState(cl.intermission);
		SCR_CenterPrint(MSG_ReadString(&cl_message, cl_readstring, sizeof(cl_readstring)));
	}
}
void netmsg_svc_showlmp(void)			//35		// also svc_bigkick... [string] slotname [string] lmpfilename [short] x [short] y
{
	if (cls.protocol == &protocol_quakeworld)
	{
		cl.qw_weaponkick = -4;
	}
	else
	{
		if (gamemode == GAME_TENEBRAE)
		{
			// particle effect
			MSG_ReadCoord(&cl_message, cls.protocol);
			MSG_ReadCoord(&cl_message, cls.protocol);
			MSG_ReadCoord(&cl_message, cls.protocol);
			(void) MSG_ReadByte(&cl_message);
			MSG_ReadString(&cl_message, cl_readstring, sizeof(cl_readstring));
		}
		else
			SHOWLMP_decodeshow();
	}
}
void netmsg_svc_hidelmp(void)			//36		// also svc_updateping... [string] slotname
{
	int i;
	if (cls.protocol == &protocol_quakeworld)
	{
		i = MSG_ReadByte(&cl_message);
		if (i >= cl.maxclients)
			Host_Error("CL_ParseServerMessage: svc_updateping >= cl.maxclients");
		cl.scores[i].qw_ping = MSG_ReadShort(&cl_message);
	}
	else
	{
		if (gamemode == GAME_TENEBRAE)
		{
			// repeating particle effect
			MSG_ReadCoord(&cl_message, cls.protocol);
			MSG_ReadCoord(&cl_message, cls.protocol);
			MSG_ReadCoord(&cl_message, cls.protocol);
			MSG_ReadCoord(&cl_message, cls.protocol);
			MSG_ReadCoord(&cl_message, cls.protocol);
			MSG_ReadCoord(&cl_message, cls.protocol);
			(void) MSG_ReadByte(&cl_message);
			MSG_ReadLong(&cl_message);
			MSG_ReadLong(&cl_message);
			MSG_ReadString(&cl_message, cl_readstring, sizeof(cl_readstring));
		}
		else
			SHOWLMP_decodehide();
	}
}
void netmsg_svc_skybox(void)			//37		// [string] skyname // also svc_updateentertime
{
	int i;
	if (cls.protocol == &protocol_quakeworld)
	{
		i = MSG_ReadByte(&cl_message);
		if (i >= cl.maxclients)
			Host_Error("CL_ParseServerMessage: svc_updateentertime >= cl.maxclients");
		// seconds ago
		cl.scores[i].qw_entertime = cl.time - MSG_ReadFloat(&cl_message);
	}
	else
	{
		R_SetSkyBox(MSG_ReadString(&cl_message, cl_readstring, sizeof(cl_readstring)));
	}
}
void netmsg_svc_updatestatlong(void) 	//38
{
	int i = MSG_ReadByte(&cl_message);
	if (i < 0 || i >= MAX_CL_STATS)
		Host_Error ("svc_updatestatlong: %i is invalid", i);
	cl.stats[i] = MSG_ReadLong(&cl_message);
}
void netmsg_svc_muzzleflash(void)		//39
{
	int i = (unsigned short) MSG_ReadShort(&cl_message);
	// NOTE: in QW this only worked on clients
	if (i < 0 || i >= MAX_EDICTS)
		Host_Error("CL_ParseServerMessage: svc_spawnbaseline: invalid entity number %i", i);
	if (i >= cl.max_entities)
		CL_ExpandEntities(i);
	cl.entities[i].persistent.muzzleflash = 1.0f;
}
void netmsg_svc_updateuserinfo(void)	//40
{
	QW_CL_UpdateUserInfo();
}
void netmsg_svc_download(void)		//41
{
	QW_CL_ParseDownload();
}
void netmsg_svc_playerinfo(void)		//42
{
	int i;
	// slightly kill qw player entities now that we know there is
	// an update of player entities this frame...
	if (!cl.qwplayerupdatereceived)
	{
		cl.qwplayerupdatereceived = true;
		for (i = 1;i < cl.maxclients;i++)
			cl.entities_active[i] = false;
	}
	EntityStateQW_ReadPlayerUpdate();
}
void netmsg_svc_nails(void)			//43
{
	QW_CL_ParseNails();
}
void netmsg_svc_chokecount(void)		//44
{
	(void) MSG_ReadByte(&cl_message);
	// FIXME: apply to netgraph
	//for (j = 0;j < i;j++)
	//	cl.frames[(cls.netcon->qw.incoming_acknowledged-1-j)&QW_UPDATE_MASK].receivedtime = -2;
}
void netmsg_svc_modellist(void)		//45
{
	QW_CL_ParseModelList();
}
void netmsg_svc_soundlist(void)		//46
{
	QW_CL_ParseSoundList();
}
void netmsg_svc_packetentities(void)	//47
{
	EntityFrameQW_CL_ReadFrame(false);
	// first update is the final signon stage
	if (cls.signon == SIGNONS - 1)
	{
		cls.signon = SIGNONS;
		CL_SignonReply ();
	}
}
void netmsg_svc_deltapacketentities(void)	//48
{
	EntityFrameQW_CL_ReadFrame(true);
	// first update is the final signon stage
	if (cls.signon == SIGNONS - 1)
	{
		cls.signon = SIGNONS;
		CL_SignonReply ();
	}
}
void netmsg_svc_maxspeed(void)		//49
{
	cl.movevars_maxspeed = MSG_ReadFloat(&cl_message);
}
// LordHavoc: my svc_ range, 50-69
void netmsg_svc_downloaddata(void)	//50		// [int] start [short] size
{
	CL_ParseDownload();
}
void netmsg_svc_updatestatubyte(void)	//51		// [byte] stat [byte] value
{
	int i;
	if (cls.protocol == &protocol_quakeworld)
	{
		QW_CL_SetInfo();
	}
	else
	{
		i = MSG_ReadByte(&cl_message);
		if (i < 0 || i >= MAX_CL_STATS)
			Host_Error ("svc_updatestat: %i is invalid", i);
		cl.stats[i] = MSG_ReadByte(&cl_message);
	}
}
void netmsg_svc_effect(void)			//52		// [vector] org [byte] modelindex [byte] startframe [byte] framecount [byte] framerate
{
	if (cls.protocol == &protocol_quakeworld)
	{
		QW_CL_ServerInfo();
	}
	else
	{
		CL_ParseEffect ();
	}
}
void netmsg_svc_effect2(void)			//53		// [vector] org [short] modelindex [short] startframe [byte] framecount [byte] framerate
{
	int i;
	if (cls.protocol == &protocol_quakeworld)
	{
		i = MSG_ReadByte(&cl_message);
		if (i >= cl.maxclients)
			Host_Error("CL_ParseServerMessage: svc_updatepl >= cl.maxclients");
		cl.scores[i].qw_packetloss = MSG_ReadByte(&cl_message);

	}
	else
	{
		CL_ParseEffect2 ();
	}
}
/*
void netmsg_svc_sound2(void)			//54		// (obsolete in DP6 and later) short soundindex instead of byte
{

}
*/
void netmsg_svc_precache(void)		//54		// [short] precacheindex [string] filename, precacheindex is + 0 for modelindex and +32768 for soundindex
{
	int i;
	if (cls.protocol == &protocol_darkplaces1 || cls.protocol == &protocol_darkplaces2 || cls.protocol == &protocol_darkplaces3)
	{
		// was svc_sound2 in protocols 1, 2, 3, removed in 4, 5, changed to svc_precache in 6
		CL_ParseStartSoundPacket(true);
	}
	else
	{
		char *s;
		i = (unsigned short)MSG_ReadShort(&cl_message);
		s = MSG_ReadString(&cl_message, cl_readstring, sizeof(cl_readstring));
		if (i < 32768)
		{
			if (i >= 1 && i < MAX_MODELS)
			{
				dp_model_t *model = Mod_ForName(s, false, false, s[0] == '*' ? cl.model_name[1] : NULL);
				if (!model)
					Con_DPrintf("svc_precache: Mod_ForName(\"%s\") failed\n", s);
				cl.model_precache[i] = model;
			}
			else
				Con_Printf("svc_precache: index %i outside range %i...%i\n", i, 1, MAX_MODELS);
		}
		else
		{
			i -= 32768;
			if (i >= 1 && i < MAX_SOUNDS)
			{
				sfx_t *sfx = S_PrecacheSound (s, true, true);
				if (!sfx && snd_initialized.integer)
					Con_DPrintf("svc_precache: S_PrecacheSound(\"%s\") failed\n", s);
				cl.sound_precache[i] = sfx;
			}
				else
				Con_Printf("svc_precache: index %i outside range %i...%i\n", i, 1, MAX_SOUNDS);
		}
	}
}
void netmsg_svc_spawnbaseline2(void)	//55		// short modelindex instead of byte
{
	unsigned int i = (unsigned short) MSG_ReadShort(&cl_message);
	if (i < 0 || i >= MAX_EDICTS)
		Host_Error ("CL_ParseServerMessage: svc_spawnbaseline2: invalid entity number %i", i);
	if (i >= cl.max_entities)
		CL_ExpandEntities(i);
	CL_ParseBaseline (cl.entities + i, true);
}
void netmsg_svc_spawnstatic2(void)	//56		// short modelindex instead of byte
{
	CL_ParseStatic (true);
}
void netmsg_svc_entities(void)		//57		// [int] deltaframe [int] thisframe [float vector] eye [variable length] entitydata
{
	if (cls.signon == SIGNONS - 1)
	{
		// first update is the final signon stage
		cls.signon = SIGNONS;
		CL_SignonReply ();
	}
	if (cls.protocol == &protocol_darkplaces1 || cls.protocol == &protocol_darkplaces2 || cls.protocol == &protocol_darkplaces3)
		EntityFrame_ReadFrame();
	else if (cls.protocol == &protocol_darkplaces4)
		EntityFrame4_ReadFrame();
	else
		EntityFrame5_ReadFrame();
}
void netmsg_svc_csqcentities(void)	//58		// [short] entnum [variable length] entitydata ... [short] 0x0000
{
	CSQC_ReadEntities();
}
void netmsg_svc_spawnstaticsound2(void)	//59	// [coord3] [short] samp [byte] vol [byte] aten
{
	CL_ParseStaticSound (true);
}
void netmsg_svc_trailparticles(void)	//60		// [short] entnum [short] effectnum [vector] start [vector] end
{
	CL_ParseTrailParticles();
}
void netmsg_svc_pointparticles(void)	//61		// [short] effectnum [vector] start [vector] velocity [short] count
{
	CL_ParsePointParticles();
}
void netmsg_svc_pointparticles1(void)	//62		// [short] effectnum [vector] start, same as svc_pointparticles except velocity is zero and count is 1
{
	CL_ParsePointParticles1();
}

