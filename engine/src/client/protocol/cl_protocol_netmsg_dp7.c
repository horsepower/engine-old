#include "quakedef.h"

protocol_netmsg_t protocol_darkplaces7_svc =
{
			netmsg_svc_bad,					// 0
			netmsg_svc_nop, 				// 1
			netmsg_svc_disconnect,			// 2
			netmsg_svc_updatestat,			// 3
			netmsg_svc_version,				// 4
			netmsg_svc_setview,				// 5
			netmsg_svc_sound,				// 6
			netmsg_svc_time,				// 7
			netmsg_svc_print,				// 8
			netmsg_svc_stufftext,			// 9
			netmsg_svc_setangle,			// 10
			netmsg_svc_serverinfo,			// 11
			netmsg_svc_lightstyle,			// 12
			netmsg_svc_updatename,			// 13
			netmsg_svc_updatefrags,			// 14
			netmsg_svc_clientdata,			// 15
			netmsg_svc_stopsound,			// 16
			netmsg_svc_updatecolors,		// 17
			netmsg_svc_particle,			// 18
			netmsg_svc_damage,				// 19
			netmsg_svc_spawnstatic,			// 20
			netmsg_svc_spawnbinary,			// 21
			netmsg_svc_spawnbaseline,		// 22
			netmsg_svc_temp_entity,			// 23
			netmsg_svc_setpause,			// 24
			netmsg_svc_signonnum,			// 25
			netmsg_svc_centerprint,			// 26
			netmsg_svc_killedmonster,		// 27
			netmsg_svc_foundsecret,			// 28
			netmsg_svc_spawnstaticsound,	// 29
			netmsg_svc_intermission,		// 30
			netmsg_svc_finale,				// 31
			netmsg_svc_cdtrack,				// 32
			netmsg_svc_sellscreen,			// 33
			netmsg_svc_cutscene,			// 34
			netmsg_svc_showlmp,				// 35
			netmsg_svc_hidelmp,				// 36
			netmsg_svc_skybox,				// 37
			netmsg_svc_updatestatlong,		// 38
			netmsg_svc_muzzleflash,			// 39
			netmsg_svc_updateuserinfo,		// 40
			netmsg_svc_download,			// 41
			netmsg_svc_playerinfo,			// 42
			netmsg_svc_nails,				// 43
			netmsg_svc_chokecount,			// 44
			netmsg_svc_modellist,			// 45
			netmsg_svc_soundlist,			// 46
			netmsg_svc_packetentities,		// 47
			netmsg_svc_deltapacketentities,	// 48
			netmsg_svc_maxspeed,			// 49
			netmsg_svc_downloaddata,		// 50
			netmsg_svc_updatestatubyte,		// 51
			netmsg_svc_effect,				// 52
			netmsg_svc_effect2,				// 53
			netmsg_svc_precache,			// 54
			netmsg_svc_spawnbaseline2,		// 55
			netmsg_svc_spawnstatic2,		// 56
			netmsg_svc_entities,			// 57
			netmsg_svc_csqcentities,		// 58
			netmsg_svc_spawnstaticsound2,	// 59
			netmsg_svc_trailparticles,		// 60
			netmsg_svc_pointparticles,		// 61
			netmsg_svc_pointparticles1		// 62
};
