#   ----------------------------------------------------------------------------
#	COPYRIGHT (C) 2018-2019 David Knapp
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#   ----------------------------------------------------------------------------


### VARIABLES ###
set(ENGINE_PLATFLAGS "-static-libgcc -Wall -Wno-pointer-sign -Wno-unknown-pragmas -Wno-format-zero-length -Wno-strict-aliasing -Dstrnicmp=strncasecmp -Dstricmp=strcasecmp")

set(ENGINE_CL_FLAGS "-DCLIENT -DLINK_TO_LIBJPEG")
set(ENGINE_SV_FLAGS -DSERVER)

if(ENGINE_CONFIG_LEGACY)
	set(ENGINE_CL_FLAGS "${ENGINE_CL_FLAGS} -DQUAKELEGACY")
endif()

if(ENGINE_CONFIG_MENU)
	set(ENGINE_CL_FLAGS "${ENGINE_CL_FLAGS} -DCONFIG_MENU")
endif()

if(ENGINE_VERSION)
	set(ENGINE_CL_FLAGS "${ENGINE_CL_FLAGS} -DVSTRING='${ENGINE_VERSION}'")
endif()

if(CMAKE_BUILD_TYPE)
	set(ENGINE_CL_FLAGS "${ENGINE_CL_FLAGS} -DBUILDTYPE='${CMAKE_BUILD_TYPE}'")
endif()

if(ENGINE_NO_BUILD_TIMESTAMPS)
	set(ENGINE_CL_FLAGS "${ENGINE_CL_FLAGS} -DNO_BUILD_TIMESTAMPS")
endif()

if(MINGW)
	set(ENGINE_PLATLIBS "-lm -lwinmm -limm32 -lversion -lwsock32 -lws2_32")
elseif(LINUX)
	set(ENGINE_PLATLIBS "-lm -ldl -lz -ljpeg -lpng -lcurl -lrt")
endif()

### INCLUDE ###

# Yuck! I have to find a better way.

set(OBJ_SND_COMMON
	${ENGINE_SRC_DIR}/system/snd_main.c
	${ENGINE_SRC_DIR}/system/snd_mem.c
	${ENGINE_SRC_DIR}/system/snd_mix.c
	${ENGINE_SRC_DIR}/system/snd_ogg.c
	${ENGINE_SRC_DIR}/system/snd_sdl.c
#	${ENGINE_SRC_DIR}/system/snd_voip.c
	${ENGINE_SRC_DIR}/system/snd_wav.c
)

set(OBJ_VIDEO_CAPTURE 
    ${ENGINE_SRC_DIR}/client/cap_avi.c 
    ${ENGINE_SRC_DIR}/client/cap_ogg.c
)

set(OBJ_COMMON
	${ENGINE_SRC_DIR}/common/bih.c
	${ENGINE_SRC_DIR}/system/builddate.c
	${ENGINE_SRC_DIR}/system/cd_shared.c
	${ENGINE_SRC_DIR}/qcvm/clvm_cmds.c
	${ENGINE_SRC_DIR}/common/crypto.c
	${ENGINE_SRC_DIR}/qcvm/csprogs.c
	${ENGINE_SRC_DIR}/client/cl_cmds.c
	${ENGINE_SRC_DIR}/client/cl_collision.c
	${ENGINE_SRC_DIR}/client/cl_demo.c
	${ENGINE_SRC_DIR}/client/cl_input.c
	${ENGINE_SRC_DIR}/client/cl_keys.c
	${ENGINE_SRC_DIR}/client/cl_main.c
	${ENGINE_SRC_DIR}/client/cl_parse.c
	${ENGINE_SRC_DIR}/client/cl_particles.c
	${ENGINE_SRC_DIR}/client/cl_screen.c
	${ENGINE_SRC_DIR}/client/cl_video.c
	${ENGINE_SRC_DIR}/common/cmd.c
	${ENGINE_SRC_DIR}/common/cmd_legacy.c
	${ENGINE_SRC_DIR}/common/collision.c
	${ENGINE_SRC_DIR}/common/common.c
	${ENGINE_SRC_DIR}/common/com_mod.c
	${ENGINE_SRC_DIR}/common/com_msg.c
	${ENGINE_SRC_DIR}/common/console.c
	${ENGINE_SRC_DIR}/common/con_log.c
	${ENGINE_SRC_DIR}/client/con_screen.c
	${ENGINE_SRC_DIR}/common/curves.c
	${ENGINE_SRC_DIR}/common/cvar.c
	${ENGINE_SRC_DIR}/client/dpvsimpledecode.c
	${ENGINE_SRC_DIR}/common/event.c
	${ENGINE_SRC_DIR}/common/filematch.c
	${ENGINE_SRC_DIR}/common/fractalnoise.c
	${ENGINE_SRC_DIR}/common/fs.c
	${ENGINE_SRC_DIR}/common/ft2.c
	${ENGINE_SRC_DIR}/system/utf8lib.c
	${ENGINE_SRC_DIR}/client/render/gl/gl_backend.c
	${ENGINE_SRC_DIR}/client/render/gl/gl_draw.c
	${ENGINE_SRC_DIR}/client/render/gl/gl_rmain.c
	${ENGINE_SRC_DIR}/client/render/gl/gl_rsurf.c
	${ENGINE_SRC_DIR}/client/render/gl/gl_textures.c
	${ENGINE_SRC_DIR}/common/hmac.c
	${ENGINE_SRC_DIR}/common/host.c
	${ENGINE_SRC_DIR}/client/image.c
	${ENGINE_SRC_DIR}/client/image_png.c
	${ENGINE_SRC_DIR}/client/image_jpeg.c
	${ENGINE_SRC_DIR}/common/lhnet.c
	${ENGINE_SRC_DIR}/system/libcurl.c
	${ENGINE_SRC_DIR}/system/mathlib.c
	${ENGINE_SRC_DIR}/system/matrixlib.c
	${ENGINE_SRC_DIR}/common/mdfour.c
	${ENGINE_SRC_DIR}/common/meshqueue.c
	${ENGINE_SRC_DIR}/common/mod_skeletal_animatevertices_sse.c
	${ENGINE_SRC_DIR}/common/mod_skeletal_animatevertices_generic.c
	${ENGINE_SRC_DIR}/common/model_alias.c
	${ENGINE_SRC_DIR}/common/model_brush.c
	${ENGINE_SRC_DIR}/common/model_shared.c
	${ENGINE_SRC_DIR}/common/model_sprite.c
	${ENGINE_SRC_DIR}/common/netconn.c
	${ENGINE_SRC_DIR}/common/palette.c
	${ENGINE_SRC_DIR}/common/polygon.c
	${ENGINE_SRC_DIR}/common/portals.c
	${ENGINE_SRC_DIR}/common/protocol.c
	${ENGINE_SRC_DIR}/common/protocol_version.c
	${ENGINE_SRC_DIR}/qcvm/prvm_cmds.c
	${ENGINE_SRC_DIR}/qcvm/prvm_edict.c
	${ENGINE_SRC_DIR}/qcvm/prvm_exec.c
	${ENGINE_SRC_DIR}/client/render/r_explosion.c
	${ENGINE_SRC_DIR}/client/render/r_lerpanim.c
	${ENGINE_SRC_DIR}/client/render/r_lightning.c
	${ENGINE_SRC_DIR}/client/render/r_modules.c
	${ENGINE_SRC_DIR}/client/render/r_shadow.c
	${ENGINE_SRC_DIR}/client/render/r_sky.c
	${ENGINE_SRC_DIR}/client/render/r_sprites.c
	${ENGINE_SRC_DIR}/client/sbar.c
	${ENGINE_SRC_DIR}/server/sv_ccmds.c
	${ENGINE_SRC_DIR}/server/sv_demo.c
	${ENGINE_SRC_DIR}/server/sv_main.c
	${ENGINE_SRC_DIR}/server/sv_move.c
	${ENGINE_SRC_DIR}/server/sv_phys.c
	${ENGINE_SRC_DIR}/server/sv_send.c
	${ENGINE_SRC_DIR}/server/sv_user.c
	${ENGINE_SRC_DIR}/common/bsp.c
	${ENGINE_SRC_DIR}/qcvm/svvm_cmds.c
	${ENGINE_SRC_DIR}/system/sys_main.c
	${ENGINE_SRC_DIR}/system/vid_shared.c
	${ENGINE_SRC_DIR}/common/view.c
	${ENGINE_SRC_DIR}/common/wad.c
	${ENGINE_SRC_DIR}/common/world.c
	${ENGINE_SRC_DIR}/common/zone.c
	${ENGINE_SRC_DIR}/server/protocol/sv_protocol_netmsg_common.c
	${ENGINE_SRC_DIR}/server/protocol/sv_protocol_netmsg_dp7.c
	${ENGINE_SRC_DIR}/server/protocol/sv_protocol_netmsg_nq.c
	#${ENGINE_SRC_DIR}/server/protocol/sv_netmsg_qw.c
	#${ENGINE_SRC_DIR}/utils/dpmodel/dpmodel.c
	${ENGINE_SRC_DIR}/server/protocol/sv_protocol_entframe5.c
	${ENGINE_SRC_DIR}/server/protocol/sv_protocol_entframe4.c
	${ENGINE_SRC_DIR}/server/protocol/sv_protocol_entframe_csqc.c
	${ENGINE_SRC_DIR}/common/protocol_entframe.c
	${ENGINE_SRC_DIR}/server/protocol/sv_protocol_entframe_nq.c
	${ENGINE_SRC_DIR}/server/protocol/sv_protocol_entframe.c
	${ENGINE_SRC_DIR}/client/protocol/cl_protocol_netmsg_common.c
	${ENGINE_SRC_DIR}/client/protocol/cl_protocol_netmsg_dp7.c
	${ENGINE_SRC_DIR}/client/protocol/cl_protocol_netmsg_nq.c
	${ENGINE_SRC_DIR}/client/protocol/cl_protocol_entframe_nq.c
	${ENGINE_SRC_DIR}/client/protocol/cl_protocol_entframe4.c
	${ENGINE_SRC_DIR}/client/protocol/cl_protocol_entframe5.c
	${ENGINE_SRC_DIR}/client/protocol/cl_protocol_entframe.c	
	${ENGINE_SRC_DIR}/client/protocol/cl_protocol_entframe_qw.c
	${ENGINE_SRC_DIR}/common/protocol/entframe_qw.c
	${ENGINE_SRC_DIR}/common/protocol/entframe4.c
	${ENGINE_SRC_DIR}/common/protocol/entframe5.c	
)

set(OBJ_MENU
	${ENGINE_SRC_DIR}/client/menu.c
	${ENGINE_SRC_DIR}/qcvm/mvm_cmds.c
)

set(OBJ_SV
	${ENGINE_SRC_DIR}/system/vid_null.c
	${ENGINE_SRC_DIR}/system/thread_null.c
	${ENGINE_SRC_DIR}/system/snd_null.c
	${ENGINE_SRC_DIR}/system/sys_console.c
)

set(OBJ_CL
	${ENGINE_SRC_DIR}/system/vid_sdl.c
	${ENGINE_SRC_DIR}/system/thread_sdl.c
	${ENGINE_SRC_DIR}/system/sys_sdl.c
)
