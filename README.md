# Horsepower Engine


Horsepower is a 3D engine. It's based on Darkplaces engine (which itself is based on Quake engine) with some modifications including:

* All of the features of Darkplaces
* Voice chat (hopefully?)
* Legacy stuff moved to cvars.
* A CMake build system to massively simplify deployment

## Building on Windows - MinGW:

1. Install CMake and MSYS2.
2. Once you've installed MSYS2 and have fully updated it, open an MSYS2 terminal and input the following command:

```
pacman -S --needed git curl zip unzip p7zip make automake autoconf libtool gcc gmp-devel mingw-w64-{i686,x86_64}-{toolchain,cmake,gmp,libjpeg-turbo,libpng,libogg,libvorbis,sdl2}
```

3. Add the following your system PATH in this order: `C:\msys64\mingw32\bin;C:\msys2\mingw64\bin`. Replace "C:\msys64" with where ever you installed MSYS2.
4. Run CMake, set source code path to the root folder of the repository. Set binary path to the "build" directory in the repository root. You can set it to anything you want, however. Mainly personal preference. 
5. Click configure and set the generator to anything that ends in "MinGW Makefiles", and "Use default native compilers".
6. Afterwards, click Generate. 
7. Open the resulting project file in your IDE of choice and compile. Or, optionally, use the generated makefile to compile directly.

## Building on Linux - Debian/Ubuntu:

1. Install the following packages using your choice of APT frontend or by running the following command:

```
sudo apt install build-essential git cmake{,-qt-gui} lib{sdl2,jpeg,png,vorbis,ogg}-dev

```
2. Run CMake in the main directory and generate using "Unix Makefiles" or your preference of IDE.
3. Run make in the output directory to compile or open your IDE of choice.
## Building on MacOS:

meh

## Building on Android:

Go ahead.
